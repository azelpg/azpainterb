/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**********************************
 * TileImage カラータイプ別の関数
 **********************************/

#ifndef TILEIMAGE_COLTYPE_FUNCS_H
#define TILEIMAGE_COLTYPE_FUNCS_H

/* RGBA */

uint8_t *__TileImage_RGBA_getPixelBuf_fromTile(TileImage *p,uint8_t *tile,int x,int y);
void __TileImage_RGBA_getPixelCol_fromTile(TileImage *p,uint8_t *tile,int x,int y,PixelRGBA *pix);
void __TileImage_RGBA_setPixel(TileImage *p,uint8_t *buf,int x,int y,PixelRGBA *pix);

mBool __TileImage_RGBA_isTransparentTile(uint8_t *tile);
void __TileImage_RGBA_blendTile(TileImageBlendInfo *info);

mBool __TileImage_RGBA_setRow_fromRGBA(TileImage *p,int y,uint8_t *buf,int width,int height,mBool bottomup);

void __TileImage_RGBA_tileReverseHorz(uint8_t *tile);
void __TileImage_RGBA_tileReverseVert(uint8_t *tile);

/* 1bit */

uint8_t *__TileImage_1bit_getPixelBuf_fromTile(TileImage *p,uint8_t *tile,int x,int y);
void __TileImage_1bit_getPixelCol_fromTile(TileImage *p,uint8_t *tile,int x,int y,PixelRGBA *pix);
void __TileImage_1bit_setPixel(TileImage *p,uint8_t *buf,int x,int y,PixelRGBA *pix);

void __TileImage_1bit_blendXorToPixbuf(TileImage *p,mPixbuf *pixbuf,TileImageBlendInfo *info);

/* 8bit */

uint8_t *__TileImage_8bit_getPixelBuf_fromTile(TileImage *p,uint8_t *tile,int x,int y);
void __TileImage_8bit_getPixelCol_fromTile(TileImage *p,uint8_t *tile,int x,int y,PixelRGBA *pix);
void __TileImage_8bit_setPixel(TileImage *p,uint8_t *buf,int x,int y,PixelRGBA *pix);

#endif

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * TileImage
 *
 * 画像ファイルへの読み書き
 *****************************************/

#include <string.h>

#include "mDef.h"
#include "mLoadImage.h"
#include "mSaveImage.h"
#include "mPopupProgress.h"

#include "defTileImage.h"
#include "TileImage.h"
#include "TileImage_pv.h"
#include "TileImage_coltype.h"
#include "TileImageDrawInfo.h"

#include "defFileFormat.h"


//===============================
// 画像読み込み
//===============================


typedef struct
{
	mLoadImage base;
	TileImageLoadFileInfo *dstinfo;
	mPopupProgress *prog;
	TileImage *img;
	int y,
		ignore_alpha;
}_loadimageinfo;


/** 情報取得 */

static int _loadfile_getinfo(mLoadImage *param,mLoadImageInfo *info)
{
	_loadimageinfo *p = (_loadimageinfo *)param;
	int h,v;

	//TileImage 作成

	p->img = TileImage_new(TILEIMAGE_COLTYPE_RGBA, info->width, info->height);
	if(!p->img)
		return MLOADIMAGE_ERR_ALLOC;

	p->y = (info->bottomup)? info->height - 1: 0;

	//情報セット

	if(p->dstinfo)
	{
		p->dstinfo->width = info->width;
		p->dstinfo->height = info->height;
		p->dstinfo->transparent = info->transparent_col;

		if(mLoadImage_getResolution_dpi(param, &h, &v))
			p->dstinfo->dpi = h;
		else
			p->dstinfo->dpi = 0;
	}

	//プログレス

	if(p->prog)
		mPopupProgressThreadBeginSubStep_onestep(p->prog, 20, info->height);

	return MLOADIMAGE_ERR_OK;
}

/** Y1行取得 */

static int _loadfile_getrow(mLoadImage *param,uint8_t *buf,int pitch)
{
	_loadimageinfo *p = (_loadimageinfo *)param;
	int i;
	uint8_t *ps;

	//アルファ値を無視

	if(p->ignore_alpha)
	{
		ps = buf + 3;
		
		for(i = param->info.width; i > 0; i--, ps += 4)
			*ps = 255;
	}

	//タイルにセット

	__TileImage_RGBA_setRow_fromRGBA(p->img, p->y, buf,
		param->info.width, param->info.height, param->info.bottomup);

	//次へ

	if(param->info.bottomup)
		p->y--;
	else
		p->y++;

	if(p->prog)
		mPopupProgressThreadIncSubStep(p->prog);

	return MLOADIMAGE_ERR_OK;
}

/** ファイルから画像読み込み
 *
 * @param format   -1 で自動判別
 * @param fileinfo NULL でなし
 * @param prog     NULL でなし
 * @param errmes   エラーメッセージ文字列のポインタが入る (NULLでなし。解放必要) */

TileImage *TileImage_loadFile(const char *filename,
	int format,mBool ignore_alpha,mBool leave_trans,int maxsize,
	TileImageLoadFileInfo *fileinfo,mPopupProgress *prog,char **errmes)
{
	_loadimageinfo *p;
	mLoadImageFunc func;
	mBool ret;
	TileImage *img;

	p = (_loadimageinfo *)mLoadImage_create(sizeof(_loadimageinfo));
	if(!p) return NULL;

	p->dstinfo = fileinfo;
	p->prog = prog;
	p->ignore_alpha = ignore_alpha;

	p->base.format = MLOADIMAGE_FORMAT_RGBA;
	p->base.flags = (leave_trans)? MLOADIMAGE_FLAGS_LEAVE_TRANSPARENT: 0;
	p->base.max_width = p->base.max_height = maxsize;
	p->base.src.type = MLOADIMAGE_SOURCE_TYPE_PATH;
	p->base.src.filename = filename;
	p->base.getinfo = _loadfile_getinfo;
	p->base.getrow = _loadfile_getrow;

	//読込関数

	if(format < 0)
	{
		//自動判別
		
		if(!mLoadImage_checkFormat(&p->base.src, &func, MLOADIMAGE_CHECKFORMAT_F_ALL))
			return NULL;
	}
	else
	{
		switch(format)
		{
			case FILEFORMAT_PNG: func = mLoadImagePNG; break;
			case FILEFORMAT_JPEG: func = mLoadImageJPEG; break;
			case FILEFORMAT_GIF: func = mLoadImageGIF; break;
			case FILEFORMAT_BMP: func = mLoadImageBMP; break;
		}
	}

	//実行

	ret = (func)(M_LOADIMAGE(p));

	img = p->img;

	if(!ret)
	{
		TileImage_free(img);
		img = NULL;

		if(errmes)
			*errmes = mStrdup(p->base.message);
	}

	mLoadImage_free(M_LOADIMAGE(p));

	return img;
}


//=========================================
// ADW/APD/PSD 読み込み用
//=========================================


/** タイルイメージを A8 => RGBA8 変換 */

void TileImage_convertTile_fromA8(uint8_t *buf,uint32_t col)
{
	int i;
	uint8_t *ps,*pd,r,g,b;

	r = M_GET_R(col);
	g = M_GET_G(col);
	b = M_GET_B(col);

	ps = buf + 64 * 64 - 1;
	pd = buf + (64 * 64 - 1) * 4;

	for(i = 64 * 64; i; i--, ps--, pd -= 4)
	{
		pd[3] = *ps;
		pd[0] = r;
		pd[1] = g;
		pd[2] = b;
	}
}

/** APD (ver2,ver3) のタイルイメージを RGBA8 に変換 */

void TileImage_convertTile_fromAPD(uint8_t *dst,uint8_t *src,int type,uint32_t col)
{
	int i,j;
	uint8_t *ps,*pd,r,g,b,f;

	ps = src;
	pd = dst;

	r = M_GET_R(col);
	g = M_GET_G(col);
	b = M_GET_B(col);

	switch(type)
	{
		//RGBA 16bit (R(64x64)-G-B-A : BE)
		case 0:
			for(i = 0; i < 4; i++)
			{
				ps = src + (i << (6 + 6 + 1));
				pd = dst + i;
				
				for(j = 64 * 64; j; j--, ps += 2, pd += 4)
					*pd = (((ps[0] << 8) | ps[1]) * 255 + 0x4000) >> 15;
			}
			break;

		//GRAY+A 16bit (COL-A)
		case 1:
			//col
			
			for(i = 64 * 64; i; i--, ps += 2, pd += 4)
				pd[0] = pd[1] = pd[2] = (((ps[0] << 8) | ps[1]) * 255 + 0x4000) >> 15;

			//A

			ps = src + 64 * 64 * 2;
			pd = dst + 3;
			
			for(i = 64 * 64; i; i--, ps += 2, pd += 4)
				*pd = (((ps[0] << 8) | ps[1]) * 255 + 0x4000) >> 15;
			break;

		//A 16bit
		case 2:
			for(i = 64 * 64; i; i--, ps += 2, pd += 4)
			{
				pd[0] = r;
				pd[1] = g;
				pd[2] = b;
				pd[3] = (((ps[0] << 8) | ps[1]) * 255 + 0x4000) >> 15;
			}
			break;

		//A 1bit
		case 3:
			for(i = 64 * 64, f = 0x80; i; i--, pd += 4)
			{
				pd[0] = r;
				pd[1] = g;
				pd[2] = b;
				pd[3] = (*ps & f)? 255: 0;

				f >>= 1;
				if(f == 0) f = 0x80, ps++;
			}
			break;
	}
}

/** A8 イメージのY1行をセット */

void TileImage_setRowImage_forA8(TileImage *p,uint8_t *buf,int y,int w,int h,uint32_t col)
{
	int i;
	uint8_t r,g,b,*ps,*pd;

	r = M_GET_R(col);
	g = M_GET_G(col);
	b = M_GET_B(col);

	//A8 => RGBA 変換

	ps = buf + w - 1;
	pd = buf + ((w - 1) << 2);

	for(i = w; i > 0; i--, ps--, pd -= 4)
	{
		pd[3] = *ps;
		pd[0] = r;
		pd[1] = g;
		pd[2] = b;
	}

	//セット

	__TileImage_RGBA_setRow_fromRGBA(p, y, buf, w, h, FALSE);
}

/** BGRA イメージからY1行セット (bottom-up) */

void TileImage_setRowImage_forBGRA_bottomup(TileImage *p,uint8_t *buf,int y,int w,int h)
{
	int i;
	uint8_t *pd,tmp;

	//BGRA => RGBA

	for(i = w, pd = buf; i > 0; i--, pd += 4)
	{
		tmp = pd[0];
		pd[0] = pd[2];
		pd[2] = tmp;
	}

	//セット

	__TileImage_RGBA_setRow_fromRGBA(p, y, buf, w, h, TRUE);
}

/** 8bit グレイスケールイメージからセット
 *
 * PSD 一枚絵 (1bit, グレイスケール) 読み込み用 */

void TileImage_setImage_from8bitGray(TileImage *p,uint8_t *buf,int w,int h,mPopupProgress *prog)
{
	int ix,iy;
	PixelRGBA pix;

	pix.a = 255;

	for(iy = 0; iy < h; iy++)
	{
		for(ix = 0; ix < w; ix++)
		{
			pix.r = pix.g = pix.b = *(buf++);

			TileImage_setPixel_new(p, ix, iy, &pix);
		}

		mPopupProgressThreadIncSubStep(prog);
	}
}

/** PSD のチャンネルイメージをセット
 *
 * 色より先にアルファ値をセットすること。
 *
 * @param chno 0-3
 * @param grayscale G,B にも同じ値をセット */

mBool TileImage_setChannelImage(TileImage *p,uint8_t *chbuf,int chno,int srcw,int srch,mBool grayscale)
{
	uint8_t **pptile = p->ppbuf,*buf,*ps,*psX,*psY,*pd;
	uint32_t *p32;
	int tx,ty,i,pitch,w,h,remw,remh;

	buf = (uint8_t *)mMalloc(64 * 64, FALSE);
	if(!buf) return FALSE;

	//

	psY = chbuf;
	pitch = srcw * 64;

	for(ty = p->tileh, remh = srch; ty; ty--, psY += pitch, remh -= 64)
	{
		psX = psY;
		h = (remh > 64)? 64: remh;
		
		for(tx = p->tilew, remw = srcw; tx; tx--, pptile++, psX += 64, remw -= 64)
		{
			/* 色のチャンネルで、タイルが作成されていない場合、
			 * すべて透明なので、何もしない */

			if(chno != 3 && !(*pptile)) continue;
		
			//buf に 64x64 で取得

			ps = psX;
			pd = buf;
			w = (remw > 64)? 64: remw;

			if(w != 64 || h != 64)
				memset(buf, 0, 64 * 64);

			for(i = h; i; i--)
			{
				memcpy(pd, ps, w);

				ps += srcw;
				pd += 64;
			}

			//アルファ値の場合、タイル作成

			if(chno == 3)
			{
				//すべて 0 なら何もしない

				p32 = (uint32_t *)buf;

				for(i = 64 * 64 / 4; i && !(*p32); i--, p32++);

				if(i == 0) continue;
			
				//タイル作成

				*pptile = TileImage_allocTile(p, TRUE);
				if(!(*pptile))
				{
					mFree(buf);
					return FALSE;
				}
			}

			//タイルにセット

			ps = buf;
			pd = *pptile + chno;

			if(grayscale)
			{
				for(i = 64 * 64; i; i--, pd += 4, ps++)
					pd[0] = pd[1] = pd[2] = *ps;
			}
			else
			{
				for(i = 64 * 64; i; i--, pd += 4)
					*pd = *(ps++);
			}
		}
	}

	mFree(buf);

	return TRUE;
}


//=================================
// PNG 保存
//=================================


typedef struct
{
	mSaveImage base;

	TileImage *img;
	mPopupProgress *prog;
	mBox box;
	int cury;
}_saveimg_png_box;


/** Y1行を送る */

static int _savepng_box_send_row(mSaveImage *info,uint8_t *buf,int pitch)
{
	_saveimg_png_box *p = (_saveimg_png_box *)info;
	int i,w,x,y;
	PixelRGBA pix;

	w = p->box.w;
	x = p->box.x;
	y = p->cury;

	for(i = 0; i < w; i++, buf += 4)
	{
		TileImage_getPixel(p->img, x + i, y, &pix);

		buf[0] = pix.r;
		buf[1] = pix.g;
		buf[2] = pix.b;
		buf[3] = pix.a;
	}

	p->cury++;

	mPopupProgressThreadIncSubStep(p->prog);

	return MSAVEIMAGE_ERR_OK;
}

/** PNG (アルファ付き) に保存 */

mBool TileImage_savePNG_rgba(TileImage *p,const char *filename,int dpi,mBox *box,
	mPopupProgress *prog)
{
	_saveimg_png_box *info;
	mBool ret;

	info = (_saveimg_png_box *)mSaveImage_create(sizeof(_saveimg_png_box));
	if(!info) return FALSE;

	info->img = p;
	info->prog = prog;
	info->box = *box;
	info->cury = box->y;

	info->base.output.type = MSAVEIMAGE_OUTPUT_TYPE_PATH;
	info->base.output.filename = filename;
	info->base.width = box->w;
	info->base.height = box->h;
	info->base.sample_bits = 8;
	info->base.coltype = MSAVEIMAGE_COLTYPE_RGBA;
	info->base.resolution_unit = MSAVEIMAGE_RESOLITION_UNIT_DPI;
	info->base.resolution_horz = dpi;
	info->base.resolution_vert = dpi;
	info->base.send_row = _savepng_box_send_row;

	//保存

	mPopupProgressThreadBeginSubStep_onestep(prog, 20, box->h);

	ret = mSaveImagePNG(M_SAVEIMAGE(info));

	mSaveImage_free(M_SAVEIMAGE(info));

	return ret;
}

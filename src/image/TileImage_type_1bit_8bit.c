/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * TileImage
 *
 * 1bit/8bit タイプ
 *****************************************/
/*
 * [1bit] x は 上位ビットから順に 0,1,2...
 */

#include "mDef.h"
#include "mPixbuf.h"

#include "defTileImage.h"
#include "defPixel.h"
#include "TileImage_pv.h"



//==========================
// 1bit
//==========================


/** タイルと px 位置からバッファ位置取得 */

uint8_t *__TileImage_1bit_getPixelBuf_fromTile(TileImage *p,uint8_t *tile,int x,int y)
{
	x = (x - p->offx) & 63;
	y = (y - p->offy) & 63;

	return tile + (y << 3) + (x >> 3);
}

/** タイルと px 位置から色取得 */

void __TileImage_1bit_getPixelCol_fromTile(TileImage *p,uint8_t *tile,int x,int y,PixelRGBA *pix)
{
	x = (x - p->offx) & 63;
	y = (y - p->offy) & 63;

	tile += (y << 3) + (x >> 3);

	pix->c = 0;
	pix->a = (( *tile & (1 << (7 - (x & 7))) ) != 0);
}

/** 色をセット */

void __TileImage_1bit_setPixel(TileImage *p,uint8_t *buf,int x,int y,PixelRGBA *pix)
{
	x = (x - p->offx) & 63;
	x = 1 << (7 - (x & 7));

	if(pix->a)
		*buf |= x;
	else
		*buf &= ~x;
}

/** mPixbuf に XOR 合成 */

void __TileImage_1bit_blendXorToPixbuf(TileImage *p,mPixbuf *pixbuf,TileImageBlendInfo *info)
{
	int ix,iy,pitchd,bpp;
	uint8_t *ps,*pd,*psY,f,fleft;

	pd = mPixbufGetBufPtFast(pixbuf, info->dx, info->dy);
	psY = info->tile + (info->sy << 3) + (info->sx >> 3);

	bpp = pixbuf->bpp;
	pitchd = pixbuf->pitch_dir - info->w * bpp;

	fleft = 1 << (7 - (info->sx & 7));

	for(iy = info->h; iy; iy--)
	{
		ps = psY;
		f  = fleft;
	
		for(ix = info->w; ix; ix--, pd += bpp)
		{
			if(*ps & f)
				(pixbuf->setbuf)(pd, MPIXBUF_COL_XOR);
			
			f >>= 1;
			if(!f) f = 0x80, ps++;
		}

		psY += 8;
		pd += pitchd;
	}
}


//==========================
// 8bit
//==========================


/** タイルと px 位置からバッファ位置取得 */

uint8_t *__TileImage_8bit_getPixelBuf_fromTile(TileImage *p,uint8_t *tile,int x,int y)
{
	x = (x - p->offx) & 63;
	y = (y - p->offy) & 63;

	return tile + (y << 6) + x;
}

/** タイルと px 位置から色取得 */

void __TileImage_8bit_getPixelCol_fromTile(TileImage *p,uint8_t *tile,int x,int y,PixelRGBA *pix)
{
	x = (x - p->offx) & 63;
	y = (y - p->offy) & 63;

	pix->a = *(tile + (y << 6) + x);
}

/** 色をセット */

void __TileImage_8bit_setPixel(TileImage *p,uint8_t *buf,int x,int y,PixelRGBA *pix)
{
	*buf = pix->a;
}

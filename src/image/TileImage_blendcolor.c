/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * 色合成テーブル作成 & 色合成
 ************************************/
/*
 * http://www.pegtop.net/delphi/articles/blendmodes/
 * http://dunnbypaul.net/blends/
 * http://web.archive.org/web/20110830103044/http://www.bea.hi-ho.ne.jp/gaku-iwa/color/conjn.html
 */

#include "mDef.h"

#include "defPixel.h"
#include "TileImage_pv.h"


//===============================
// 色合成テーブルセット
//===============================


/** 乗算 */

static void _settable_mul(uint8_t *buf)
{
	int is,id;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
			*(buf++) = is * id / 255;
	}
}

/** 加算 */

static void _settable_add(uint8_t *buf)
{
	int is,id,n;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			n = is + id;
			if(n > 255) n = 255;

			*(buf++) = n;
		}
	}
}

/** 減算 */

static void _settable_sub(uint8_t *buf)
{
	int is,id,n;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			n = id - is;
			if(n < 0) n = 0;

			*(buf++) = n;
		}
	}
}

/** スクリーン */

static void _settable_screen(uint8_t *buf)
{
	int is,id;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
			*(buf++) = 255 - (255 - is) * (255 - id) / 255;
	}
}

/** オーバーレイ */

static void _settable_overlay(uint8_t *buf)
{
	int is,id;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			if(id < 128)
				*(buf++) = is * id >> 7;
			else
				*(buf++) = 255 - ((255 - id) * (255 - is) >> 7);
		}
	}
}

/** ハードライト */

static void _settable_hardlight(uint8_t *buf)
{
	int is,id;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			if(is < 128)
				*(buf++) = is * id >> 7;
			else
				*(buf++) = 255 - ((255 - id) * (255 - is) >> 7);
		}
	}
}

/** ソフトライト */

static void _settable_softlight(uint8_t *buf)
{
	int is,id,n;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			n = is * id / 255;

			*(buf++) = n + id * (255 - n - (255 - is) * (255 - id) / 255) / 255;
		}
	}
}

/** 覆い焼き */

static void _settable_dodge(uint8_t *buf)
{
	int is,id,n;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			if(is == 255)
				n = 255;
			else
			{
				n = id * 255 / (255 - is);
				if(n > 255) n = 255;
			}

			*(buf++) = n;
		}
	}
}

/** 焼き込み */

static void _settable_burn(uint8_t *buf)
{
	int is,id,n;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			if(is == 0)
				n = 0;
			else
			{
				n = 255 - (255 - id) * 255 / is;
				if(n < 0) n = 0;
			}

			*(buf++) = n;
		}
	}
}

/** 焼き込みリニア */

static void _settable_linearburn(uint8_t *buf)
{
	int is,id,n;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			n = id + is - 255;
			if(n < 0) n = 0;

			*(buf++) = n;
		}
	}
}

/** ビビットライト */

static void _settable_vividlight(uint8_t *buf)
{
	int is,id,n;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			if(is < 128)
			{
				n = 255 - (is << 1);

				if(id <= n || is == 0)
					n = 0;
				else
					n = (id - n) * 255 / (is << 1);
			}
			else
			{
				n = 255 * 2 - (is << 1);

				if(id >= n || n == 0)
					n = 255;
				else
					n = id * 255 / n;
			}

			*(buf++) = n;
		}
	}
}

/** リニアライト */

static void _settable_linearlight(uint8_t *buf)
{
	int is,id,n;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			n = (is << 1) + id - 255;

			if(n < 0) n = 0;
			else if(n > 255) n = 255;

			*(buf++) = n;
		}
	}
}

/** ピンライト */

static void _settable_pinlight(uint8_t *buf)
{
	int is,id,n;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
		{
			if(is >= 128)
			{
				n = (is << 1) - 255;
				if(n < id) n = id;
			}
			else
			{
				n = is << 1;
				if(n > id) n = id;
			}

			*(buf++) = n;
		}
	}
}

/** 比較(暗) */

static void _settable_darken(uint8_t *buf)
{
	int is,id;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
			*(buf++) = (id < is)? id: is;
	}
}

/** 比較(明) */

static void _settable_lighten(uint8_t *buf)
{
	int is,id;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
			*(buf++) = (id > is)? id: is;
	}
}

/** 差の絶対値 */

static void _settable_diff(uint8_t *buf)
{
	int is,id;

	for(is = 0; is < 256; is++)
	{
		for(id = 0; id < 256; id++)
			*(buf++) = (id > is)? id - is: is - id;
	}
}


//===============================
// 色合成を行う
//===============================


/** 乗算 */

static void _blendcol_mul(PixelRGBA *src,PixelRGBA *dst)
{
	src->r = dst->r * src->r / 255;
	src->g = dst->g * src->g / 255;
	src->b = dst->b * src->b / 255;
}

/** 加算 */

static void _blendcol_add(PixelRGBA *src,PixelRGBA *dst)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = dst->ar[i] + src->ar[i];
		if(n > 255) n = 255;

		src->ar[i] = n;
	}
}

/** 減算 */

static void _blendcol_sub(PixelRGBA *src,PixelRGBA *dst)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = dst->ar[i] - src->ar[i];
		if(n < 0) n = 0;

		src->ar[i] = n;
	}
}

/** スクリーン */

static void _blendcol_screen(PixelRGBA *src,PixelRGBA *dst)
{
	int i;

	for(i = 0; i < 3; i++)
		src->ar[i] = 255 - (255 - dst->ar[i]) * (255 - src->ar[i]) / 255;
}

/** オーバーレイ */

static void _blendcol_overlay(PixelRGBA *src,PixelRGBA *dst)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		if(dst->ar[i] < 128)
			src->ar[i] = src->ar[i] * dst->ar[i] >> 7;
		else
			src->ar[i] = 255 - ((255 - dst->ar[i]) * (255 - src->ar[i]) >> 7);
	}
}

/** ハードライト */

static void _blendcol_hardlight(PixelRGBA *src,PixelRGBA *dst)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		if(src->ar[i] < 128)
			src->ar[i] = src->ar[i] * dst->ar[i] >> 7;
		else
			src->ar[i] = 255 - ((255 - dst->ar[i]) * (255 - src->ar[i]) >> 7);
	}
}

/** ソフトライト */

static void _blendcol_softlight(PixelRGBA *src,PixelRGBA *dst)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = src->ar[i] * dst->ar[i] / 255;

		src->ar[i] = n + dst->ar[i] * (255 - n - (255 - src->ar[i]) * (255 - dst->ar[i]) / 255) / 255;
	}
}

/** 覆い焼き */

static void _blendcol_dodge(PixelRGBA *src,PixelRGBA *dst)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		if(src->ar[i] == 255)
			n = 255;
		else
		{
			n = dst->ar[i] * 255 / (255 - src->ar[i]);
			if(n > 255) n = 255;
		}

		src->ar[i] = n;
	}
}

/** 焼き込み */

static void _blendcol_burn(PixelRGBA *src,PixelRGBA *dst)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		if(src->ar[i] == 0)
			n = 0;
		else
		{
			n = 255 - (255 - dst->ar[i]) * 255 / src->ar[i];
			if(n < 0) n = 0;
		}

		src->ar[i] = n;
	}
}

/** 焼き込みリニア */

static void _blendcol_linearburn(PixelRGBA *src,PixelRGBA *dst)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = dst->ar[i] + src->ar[i] - 255;
		if(n < 0) n = 0;

		src->ar[i] = n;
	}
}

/** ビビットライト */

static void _blendcol_vividlight(PixelRGBA *src,PixelRGBA *dst)
{
	int i,n,ss,dd;

	for(i = 0; i < 3; i++)
	{
		ss = src->ar[i];
		dd = dst->ar[i];
	
		if(ss < 128)
		{
			n = 255 - (ss << 1);

			if(dd <= n || ss == 0)
				n = 0;
			else
				n = (dd - n) * 255 / (ss << 1);
		}
		else
		{
			n = 255 * 2 - (ss << 1);

			if(dd >= n || n == 0)
				n = 255;
			else
				n = dd * 255 / n;
		}

		src->ar[i] = n;
	}
}

/** リニアライト */

static void _blendcol_linearlight(PixelRGBA *src,PixelRGBA *dst)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = (src->ar[i] << 1) + dst->ar[i] - 255;

		if(n < 0) n = 0;
		else if(n > 255) n = 255;

		src->ar[i] = n;
	}
}

/** ピンライト */

static void _blendcol_pinlight(PixelRGBA *src,PixelRGBA *dst)
{
	int i,n,ss,dd;

	for(i = 0; i < 3; i++)
	{
		ss = src->ar[i];
		dd = dst->ar[i];
	
		if(ss >= 128)
		{
			n = (ss << 1) - 255;
			if(n < dd) n = dd;
		}
		else
		{
			n = ss << 1;
			if(n > dd) n = dd;
		}

		src->ar[i] = n;
	}
}

/** 比較(暗) */

static void _blendcol_darken(PixelRGBA *src,PixelRGBA *dst)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		if(dst->ar[i] < src->ar[i])
			src->ar[i] = dst->ar[i];
	}
}

/** 比較(明) */

static void _blendcol_lighten(PixelRGBA *src,PixelRGBA *dst)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		if(src->ar[i] < dst->ar[i])
			src->ar[i] = dst->ar[i];
	}
}

/** 差の絶対値 */

static void _blendcol_diff(PixelRGBA *src,PixelRGBA *dst)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = dst->ar[i] - src->ar[i];
		if(n < 0) n = -n;
		
		src->ar[i] = n;
	}
}


//=============================
// main
//=============================


static void (*g_funcs_settable[])(uint8_t *) = {
	_settable_mul, _settable_add, _settable_sub, _settable_screen,
	_settable_overlay, _settable_hardlight, _settable_softlight,
	_settable_dodge, _settable_burn, _settable_linearburn,
	_settable_vividlight, _settable_linearlight, _settable_pinlight,
	_settable_darken, _settable_lighten, _settable_diff
};

static void (*g_funcs_blendcol[])(PixelRGBA *,PixelRGBA *) = {
	_blendcol_mul, _blendcol_add, _blendcol_sub, _blendcol_screen,
	_blendcol_overlay, _blendcol_hardlight, _blendcol_softlight,
	_blendcol_dodge, _blendcol_burn, _blendcol_linearburn,
	_blendcol_vividlight, _blendcol_linearlight, _blendcol_pinlight,
	_blendcol_darken, _blendcol_lighten, _blendcol_diff
};


/** 色合成テーブルをセット
 *
 * @return テーブルを使う合成モードか (mode != 0 で TRUE) */

mBool __TileImage_setBlendColorTable(int mode)
{
	if(mode == 0)
		return FALSE;
	else
	{
		if(mode != g_tileimage_work.blend_curmode)
		{
			(g_funcs_settable[mode - 1])(g_tileimage_work.blendbuf);

			g_tileimage_work.blend_curmode = mode;
		}

		return TRUE;
	}
}

/** 色合成を行う */

void __TileImage_getBlendColor(int mode,PixelRGBA *src,PixelRGBA *dst)
{
	if(mode != 0)
		(g_funcs_blendcol[mode - 1])(src, dst);
}

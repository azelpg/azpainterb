/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * TileImage
 *
 * 回転
 *****************************************/

#include <math.h>

#include "mDef.h"
#include "mRectBox.h"
#include "mPopupProgress.h"

#include "TileImage.h"


/** ソースの点を取得 (ソースの範囲内にクリッピング) */

static void _getpix_src_clip(TileImage *src,int x,int y,mBox *box,PixelRGBA *pix)
{
	if(x < 0) x = 0;
	else if(x >= box->w) x = box->w - 1;

	if(y < 0) y = 0;
	else if(y >= box->h) y = box->h - 1;

	TileImage_getPixel(src, x, y, pix);
}

/** Lagrange の重み取得 */

static double _get_weight_lagrange(double d)
{
	d = fabs(d);

	if(d < 1.0)
		return 0.5 * (d - 2) * (d + 1) * (d - 1);
	else if(d < 2.0)
		return -1.0 / 6.0 * (d - 3) * (d - 2) * (d - 1);
	else
		return 0;
}

/** バイキュービックの重み取得 */

static double _get_weight_bicubic(double d)
{
	d = fabs(d);

	if(d < 1.0)
		return 1.0 - 2 * d * d + d * d * d;
	else if(d < 2.0)
		return 4.0 - 8 * d + 5 * d * d - d * d * d;
	else
		return 0;
}

/** 4x4 近傍 */

static void _getpix_4x4(TileImage *src,double sx,double sy,mBox *box,PixelRGBA *pixdst,
	double (*func_weight)(double))
{
	int ix,iy,nx,ny;
	double wx[4],wy[4],dd,a,c[3];
	PixelRGBA pix;

	nx = floor(sx) - 1;
	ny = floor(sy) - 1;

	//重みテーブル

	for(ix = 0; ix < 4; ix++)
	{
		wx[ix] = (func_weight)(sx - (nx + ix + 0.5));
		wy[ix] = (func_weight)(sy - (ny + ix + 0.5));
	}

	//4x4 近傍

	c[0] = c[1] = c[2] = a = 0;

	for(iy = 0; iy < 4; iy++)
	{
		for(ix = 0; ix < 4; ix++)
		{
			_getpix_src_clip(src, nx + ix, ny + iy, box, &pix);

			if(pix.a)
			{
				dd = pix.a / 255.0 * wx[ix] * wy[iy];

				c[0] += pix.r * dd;
				c[1] += pix.g * dd;
				c[2] += pix.b * dd;
				a += dd;
			}
		}
	}

	//A

	ix = (int)(a * 255 + 0.5);
	if(ix < 0) ix = 0;
	else if(ix > 255) ix = 255;

	if(ix == 0)
		pixdst->c = 0;
	else
	{
		//RGB
		
		pixdst->a = ix;

		a = 1.0 / a;

		for(ix = 0; ix < 3; ix++)
		{
			nx = (int)(c[ix] * a + 0.5);
			if(nx < 0) nx = 0;
			else if(nx > 255) nx = 255;

			pixdst->ar[ix] = nx;
		}
	}
}


//======================


/** 回転後の描画範囲を取得 */

static mBool _get_draw_area(TileImage *dst,mRect *rcdst,mBox *box,double rd)
{
	double cx,cy,dcos,dsin,xx,yy;
	mPoint pt[4];
	mRect rc;
	int i,ix,iy;

	cx = box->x + box->w * 0.5;
	cy = box->y + box->h * 0.5;

	dcos = cos(rd);
	dsin = sin(rd);

	//

	mRectEmpty(&rc);

	pt[0].x = pt[3].x = box->x;
	pt[0].y = pt[1].y = box->y;
	pt[1].x = pt[2].x = box->x + box->w;
	pt[2].y = pt[3].y = box->y + box->h;

	//4隅の点を回転

	for(i = 0; i < 4; i++)
	{
		xx = pt[i].x - cx;
		yy = pt[i].y - cy;

		ix = floor(xx * dcos - yy * dsin + cx);
		iy = floor(xx * dsin + yy * dcos + cy);

		mRectIncPoint(&rc, ix, iy);
	}

	//セット (描画可能範囲にクリッピング)

	*rcdst = rc;

	return TileImage_clipCanDrawRect(dst, rcdst);
}

/** 矩形範囲の回転 */

void TileImage_rotate(TileImage *dst,TileImage *src,mBox *box,
	int type,int angle,mPopupProgress *prog)
{
	mRect rc;
	double cx,cy,dcos,dsin,rd,xx,xy,xx_y,xy_y;
	int ix,iy,nx,ny,sw,sh;
	PixelRGBA pix;
	double (*func_weight[2])(double) = {
		_get_weight_lagrange, _get_weight_bicubic
	};

	rd = -angle / 180.0 * M_MATH_PI;

	//描画範囲

	if(!_get_draw_area(dst, &rc, box, rd))
		return;

	//

	sw = box->w;
	sh = box->h;

	cx = box->w * 0.5;
	cy = box->h * 0.5;

	dcos = cos(-rd);
	dsin = sin(-rd);

	//(rc.x1, rc.y1) のソース位置

	xx = rc.x1 - (box->x + cx);
	xy = rc.y1 - (box->y + cy);

	xx_y = xx * dcos - xy * dsin + cx;
	xy_y = xx * dsin + xy * dcos + cy;

	//

	mPopupProgressThreadBeginSubStep_onestep(prog, 20, rc.y2 - rc.y1 + 1);

	for(iy = rc.y1; iy <= rc.y2; iy++)
	{
		xx = xx_y;
		xy = xy_y;

		for(ix = rc.x1; ix <= rc.x2; ix++)
		{
			nx = floor(xx);
			ny = floor(xy);

			if(nx < 0 || ny < 0 || nx >= sw || ny >= sh)
				//ソース範囲外
				pix.c = 0;
			else if(type == 0)
				//ニアレストネイバー
				TileImage_getPixel(src, nx, ny, &pix);
			else
				//4x4 近傍
				_getpix_4x4(src, xx, xy, box, &pix, func_weight[type - 1]);
		
			TileImage_setPixel_draw_direct(dst, ix, iy, &pix);

			xx += dcos;
			xy += dsin;
		}

		xx_y -= dsin;
		xy_y += dcos;

		mPopupProgressThreadIncSubStep(prog);
	}
}

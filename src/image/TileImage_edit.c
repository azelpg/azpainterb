/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * TileImage
 *
 * 編集関連
 *****************************************/

#include "mDef.h"
#include "mRectBox.h"
#include "mPopupProgress.h"

#include "defTileImage.h"
#include "TileImage.h"
#include "TileImage_pv.h"
#include "TileImage_coltype.h"
#include "TileImageDrawInfo.h"

#include "ImageBuf24.h"


/** タイル単位でピクセル処理を行う
 *
 * [!] 関数内でタイル配列を変更しない場合のみ使うこと。 */

void TileImage_procTilePixels(TileImage *p,
	void (*pixfunc)(TileImage *,PixelRGBA *,int,int,void *),void *param)
{
	uint8_t **pptile = p->ppbuf;
	PixelRGBA *buf;
	int itx,ity,ix,iy,x,y;

	for(ity = p->tileh, y = p->offy; ity; ity--, y += 64)
	{
		for(itx = p->tilew, x = p->offx; itx; itx--, x += 64, pptile++)
		{
			if(!(*pptile)) continue;

			buf = (PixelRGBA *)(*pptile);

			for(iy = 0; iy < 64; iy++)
			{
				for(ix = 0; ix < 64; ix++, buf++)
				{
					(pixfunc)(p, buf, x + ix, y + iy, param);
				}
			}
		}
	}
}

/** 透明色を指定色に置換
 *
 * [!] setpixel 時にタイル配列がリサイズされる場合があるので、
 *     タイルごとに処理することは出来ない。*/

void TileImage_replaceColor_fromTP(TileImage *p,PixelRGBA *pixdst)
{
	mRect rc;
	int ix,iy;

	TileImage_getCanDrawRect_pixel(p, &rc);

	for(iy = rc.y1; iy <= rc.y2; iy++)
	{
		for(ix = rc.x1; ix <= rc.x2; ix++)
		{
			if(TileImage_isPixelTransparent(p, ix, iy))
				TileImage_setPixel_draw_direct(p, ix, iy, pixdst);
		}
	}
}


//===============================
// 色置き換え (透明以外の色から)
//===============================

typedef struct
{
	PixelRGBA src,dst;
}_replace_color;


static void _replacecolor_pixfunc(TileImage *p,PixelRGBA *buf,int x,int y,void *param)
{
	_replace_color *dat = (_replace_color *)param;

	//置き換え元と同じ色なら置換える

	if(buf->a && dat->src.r == buf->r && dat->src.g == buf->g && dat->src.b == buf->b)
	{
		//置換え先が透明でないなら、アルファ値は元のまま
		
		if(dat->dst.a)
			dat->dst.a = buf->a;
		
		TileImage_setPixel_draw_direct(p, x, y, &dat->dst);
	}
}

/** 色置き換え (不透明 -> 指定値 or 透明)
 *
 * 色のある部分を置き換えるためタイル配列が変化することはないので、タイルごとに処理。 */

void TileImage_replaceColor(TileImage *p,PixelRGBA *pixsrc,PixelRGBA *pixdst)
{
	_replace_color dat;

	dat.src = *pixsrc;
	dat.dst = *pixdst;

	TileImage_procTilePixels(p, _replacecolor_pixfunc, &dat);
}


//====================


/** イメージ結合 */

void TileImage_combine(TileImage *dst,TileImage *src,mRect *rc,
	int opasrc,int opadst,int blendmode,mPopupProgress *prog)
{
	int ix,iy,dsta;
	PixelRGBA pixsrc,pixdst,pixd;
	uint8_t *blendbuf;

	//配列リサイズ

	if(!TileImage_resizeTileBuf_combine(dst, src)) return;

	//色合成テーブルセット

	__TileImage_setBlendColorTable(blendmode);

	blendbuf = g_tileimage_work.blendbuf;

	//結合

	mPopupProgressThreadBeginSubStep_onestep(prog, 20, rc->y2 - rc->y1 + 1);

	for(iy = rc->y1; iy <= rc->y2; iy++)
	{
		for(ix = rc->x1; ix <= rc->x2; ix++)
		{
			//src
			
			TileImage_getPixel(src, ix, iy, &pixsrc);

			pixsrc.a = pixsrc.a * opasrc >> 7;

			//dst

			TileImage_getPixel(dst, ix, iy, &pixdst);
			dsta = pixdst.a;

			if(pixsrc.a == 0 && dsta == 0) continue;

			pixdst.a = dsta * opadst >> 7;

			//色合成

			if(blendmode)
			{
				if(pixdst.a == 0)
					pixd.r = pixd.g = pixd.b = 255;
				else
					pixd = pixdst;

				pixsrc.r = blendbuf[(pixsrc.r << 8) + pixd.r];
				pixsrc.g = blendbuf[(pixsrc.g << 8) + pixd.g];
				pixsrc.b = blendbuf[(pixsrc.b << 8) + pixd.b];
			}

			//アルファ合成

			TileImage_colfunc_normal(dst, &pixdst, &pixsrc, NULL);

			//セット
			/* 結合前と結合後がどちらも透明ならそのまま。
			 * opadst の値によっては結合後に透明になる場合があるので、
			 * 結合前=不透明、結合後=透明 なら色をセットしなければならない。 */

			if(pixdst.a || dsta)
				TileImage_setPixel_new(dst, ix, iy, &pixdst);
		}

		mPopupProgressThreadIncSubStep(prog);
	}

	//透明タイルを解放

	TileImage_freeEmptyTiles(dst);
}

/** ImageBuf24 からイメージをセット
 *
 * [画像の統合] [PSD (一枚絵、8bit RGB) 読み込み]
 *
 * p は ImageBuf24 と同じサイズで作成済み。 */

void TileImage_setImage_fromImageBuf24(TileImage *p,
	ImageBuf24 *src,mPopupProgress *prog,mBool psdload)
{
	uint8_t *ps,*psY,*psX,**pptile,*pd;
	int ix,iy,jx,jy,pitchs,pitchs_y,pitchd,w,h,px,py;

	pptile = p->ppbuf;
	psY = src->buf;
	pitchs_y = src->pitch << 6;

	if(psdload)
		mPopupProgressThreadBeginSubStep(prog, 20, p->tileh);
	else
		mPopupProgressThreadBeginSubStep_onestep(prog, 20, p->tileh);

	for(iy = p->tileh, py = 0; iy; iy--, py += 64)
	{
		psX = psY;

		h = src->h - py;
		if(h > 64) h = 64;
		
		for(ix = p->tilew, px = 0; ix; ix--, px += 64, pptile++, psX += 64 * 3)
		{
			//タイル確保

			*pptile = TileImage_allocTile(p, TRUE);
			if(!(*pptile)) return;
		
			//タイルにセット

			w = src->w - px;
			if(w > 64) w = 64;

			pd = *pptile;
			ps = psX;
			pitchd = (64 - w) << 2;
			pitchs = src->pitch - w * 3;

			for(jy = h; jy; jy--, pd += pitchd, ps += pitchs)
			{
				for(jx = w; jx; jx--, pd += 4, ps += 3)
				{
					pd[0] = ps[0];
					pd[1] = ps[1];
					pd[2] = ps[2];
					pd[3] = 255;
				}
			}
		}

		psY += pitchs_y;

		mPopupProgressThreadIncSubStep(prog);
	}
}

/** src の指定範囲内のイメージをコピー
 *
 * p は空状態である。 */

void TileImage_copyImage_inRect(TileImage *p,TileImage *src,mRect *rc,int offx,int offy)
{
	int ix,iy;
	PixelRGBA pix;

	for(iy = rc->y1; iy <= rc->y2; iy++)
	{
		for(ix = rc->x1; ix <= rc->x2; ix++)
		{
			TileImage_getPixel(src, ix - offx, iy - offy, &pix);

			if(pix.a)
				TileImage_setPixel_new(p, ix, iy, &pix);
		}
	}
}


//===============================
// 選択範囲系用
//===============================


/** 矩形範囲内のイメージをコピー & 作成
 *
 * コピーイメージは (0,0) へ。
 * 拡大縮小や回転などの元イメージ用。  */

TileImage *TileImage_createCopyImage_box(TileImage *p,mBox *box)
{
	TileImage *dst;
	int ix,iy,x,y;
	PixelRGBA pix;

	//作成

	dst = TileImage_new(TILEIMAGE_COLTYPE_RGBA, box->w, box->h);
	if(!dst) return NULL;

	//コピー

	for(iy = 0, y = box->y; iy < box->h; iy++, y++)
	{
		for(ix = 0, x = box->x; ix < box->w; ix++, x++)
		{
			TileImage_getPixel(p, x, y, &pix);

			if(pix.a)
				TileImage_setPixel_new(dst, ix, iy, &pix);
		}
	}

	return dst;
}

/** 編集後イメージの貼り付け & 元範囲の消去
 *
 * @param boxsrc  元範囲
 * @param boxdst  貼り付け範囲 */

void TileImage_pasteEditImage(TileImage *dst,TileImage *src,mBox *boxsrc,mBox *boxdst)
{
	int ix,iy,x,y;
	PixelRGBA pix;
	mBox box;

	//範囲を合成

	box = *boxsrc;
	mBoxUnion(&box, boxdst);

	//

	for(iy = box.h, y = box.y; iy > 0; iy--, y++)
	{
		for(ix = box.w, x = box.x; ix > 0; ix--, x++)
		{
			//src の色
			/* 貼り付け範囲が元範囲より小さい場合、元範囲の範囲外部分は透明になる */

			TileImage_getPixel(src, x - boxsrc->x, y - boxsrc->y, &pix);

			//セット

			TileImage_setPixel_draw_direct(dst, x, y, &pix);
		}
	}
}

/** 矩形選択範囲内のイメージをコピー(カット) & 作成
 *
 * 選択範囲コピー/切り取り用  */

TileImage *TileImage_createCopyImage_forBoxSel(TileImage *p,mBox *box,mBool cut)
{
	TileImage *dst;
	int ix,iy,x,y;
	PixelRGBA pix,pixe;

	//作成

	dst = TileImage_new(TILEIMAGE_COLTYPE_RGBA, box->w, box->h);
	if(!dst) return NULL;

	//

	pixe.c = 0;

	for(iy = 0, y = box->y; iy < box->h; iy++, y++)
	{
		for(ix = 0, x = box->x; ix < box->w; ix++, x++)
		{
			//コピー
			//矩形内すべて範囲内なので、pix.c == 0 にしない
			
			TileImage_getPixel(p, x, y, &pix);

			if(pix.a == 0) pix.r = 255;

			TileImage_setPixel_new(dst, ix, iy, &pix);

			//消去

			if(cut && pix.a)
				TileImage_setPixel_draw_direct(p, x, y, &pixe);
		}
	}

	return dst;
}

/** スタンプ用に、sel で点のある部分の src のイメージをコピー & 作成
 *
 * [src] rc => [dst] (0,0)
 * 
 * 範囲外は pix.c == 0。範囲内で透明の場合は a=0,r=255。
 * 
 * [!] 貼付け時に上書きで範囲内に透明部分が含まれる場合は、それも上書きしなければならないため、
 *     範囲外の透明と範囲内の透明は区別できるようにする必要がある。  */

TileImage *TileImage_createCopyImage_forStamp(TileImage *src,TileImage *sel,mRect *rc)
{
	TileImage *dst;
	int ix,iy;
	PixelRGBA pix;

	//作成

	dst = TileImage_new(TILEIMAGE_COLTYPE_RGBA, rc->x2 - rc->x1 + 1, rc->y2 - rc->y1 + 1);
	if(!dst) return NULL;

	//コピー

	for(iy = rc->y1; iy <= rc->y2; iy++)
	{
		for(ix = rc->x1; ix <= rc->x2; ix++)
		{
			if(TileImage_isPixelOpaque(sel, ix, iy))
			{
				TileImage_getPixel(src, ix, iy, &pix);

				if(pix.a == 0) pix.r = 255;

				TileImage_setPixel_new(dst, ix - rc->x1, iy - rc->y1, &pix);
			}
		}
	}

	return dst;
}

/** イメージの貼り付け (スタンプ用) */

void TileImage_pasteImage_forStamp(TileImage *dst,int x,int y,
	TileImage *src,int srcw,int srch)
{
	int ix,iy;
	PixelRGBA pix;

	for(iy = 0; iy < srch; iy++)
	{
		for(ix = 0; ix < srcw; ix++)
		{
			TileImage_getPixel(src, ix, iy, &pix);

			/* pix.c == 0 なら選択範囲外 */

			if(pix.c)
			{
				//a=0,r=255 は範囲内で透明。すべて0にする
				if(pix.a == 0) pix.c = 0;
				
				TileImage_setPixel_draw_direct(dst, x + ix, y + iy, &pix);
			}
		}
	}
}

/** (範囲イメージの移動/コピー用) src の sel 範囲内イメージをコピー&作成
 *
 * @param cut  src の範囲内を透明に */

TileImage *TileImage_createCopyImage_forSelImage(TileImage *src,TileImage *sel,mRect *rc,mBool cut)
{
	TileImage *dst;
	int ix,iy;
	PixelRGBA pix,pixe;

	//作成

	dst = TileImage_newFromRect(TILEIMAGE_COLTYPE_RGBA, rc);
	if(!dst) return NULL;

	//

	pixe.c = 0;

	for(iy = rc->y1; iy <= rc->y2; iy++)
	{
		for(ix = rc->x1; ix <= rc->x2; ix++)
		{
			if(TileImage_isPixelOpaque(sel, ix, iy))
			{
				//コピー
				
				TileImage_getPixel(src, ix, iy, &pix);

				if(pix.a == 0) pix.r = 255;
				
				TileImage_setPixel_new(dst, ix, iy, &pix);

				//消去

				if(cut && pix.a)
					TileImage_setPixel_draw_direct(src, ix, iy, &pixe);
			}
		}
	}

	return dst;
}

/** イメージの貼り付け (範囲イメージの移動/コピー、選択範囲貼付け用) */

void TileImage_pasteImage_forSelImage(TileImage *dst,TileImage *src,mBox *box)
{
	int ix,iy,x,y;
	PixelRGBA pix;

	for(iy = box->h, y = box->y; iy > 0; iy--, y++)
	{
		for(ix = box->w, x = box->x; ix > 0; ix--, x++)
		{
			TileImage_getPixel(src, x, y, &pix);

			//pix.c == 0 は選択範囲外

			if(pix.c)
			{
				if(pix.a == 0) pix.c = 0;
			
				TileImage_setPixel_draw_direct(dst, x, y, &pix);
			}
		}
	}
}

/** 矩形範囲のイメージの入れ替え
 *
 * 範囲は重なっていないことが条件。 */

void TileImage_replaceImage_box(TileImage *p,mBox *box,mPoint *ptdst)
{
	int ix,iy,dx,dy,sx,sy;
	PixelRGBA pix1,pix2;

	sy = box->y;
	dy = ptdst->y;

	for(iy = 0; iy < box->h; iy++, sy++, dy++)
	{
		sx = box->x;
		dx = ptdst->x;
	
		for(ix = 0; ix < box->w; ix++, sx++, dx++)
		{
			TileImage_getPixel(p, sx, sy, &pix1);
			TileImage_getPixel(p, dx, dy, &pix2);

			TileImage_setPixel_draw_direct(p, sx, sy, &pix2);
			TileImage_setPixel_draw_direct(p, dx, dy, &pix1);
		}
	}
}


//===============================
// イメージ全体の編集
//===============================


/** 全体を左右反転 */

void TileImage_fullReverse_horz(TileImage *p)
{
	int ix,iy,cnt;
	uint8_t **pp,**pp2,*ptmp;

	//offset

	p->offx = (g_tileimage_dinfo.imgw - 1) - p->offx - (p->tilew * 64 - 1);

	//各タイルのイメージを左右反転

	pp = p->ppbuf;

	for(ix = p->tilew * p->tileh; ix; ix--, pp++)
	{
		if(*pp)
			__TileImage_RGBA_tileReverseHorz(*pp);
	}

	//タイル配列を左右反転

	cnt = p->tilew >> 1;
	pp  = p->ppbuf;
	pp2 = pp + p->tilew - 1;

	for(iy = p->tileh; iy; iy--)
	{
		for(ix = cnt; ix; ix--, pp++, pp2--)
		{
			ptmp = *pp;
			*pp  = *pp2;
			*pp2 = ptmp;
		}

		pp  += p->tilew - cnt;
		pp2 += p->tilew + cnt;
	}
}

/** 全体を上下反転 */

void TileImage_fullReverse_vert(TileImage *p)
{
	int ix,iy,ww;
	uint8_t **pp,**pp2,*ptmp;

	//offset

	p->offy = (g_tileimage_dinfo.imgh - 1) - p->offy - (p->tileh * 64 - 1);

	//各タイルを上下反転

	pp = p->ppbuf;

	for(ix = p->tilew * p->tileh; ix; ix--, pp++)
	{
		if(*pp)
			__TileImage_RGBA_tileReverseVert(*pp);
	}

	//タイル配列を上下反転

	ww = p->tilew << 1;
	pp  = p->ppbuf;
	pp2 = pp + (p->tileh - 1) * p->tilew;

	for(iy = p->tileh >> 1; iy; iy--)
	{
		for(ix = p->tilew; ix; ix--, pp++, pp2++)
		{
			ptmp = *pp;
			*pp  = *pp2;
			*pp2 = ptmp;
		}

		pp2 -= ww;
	}
}


//===============================
// 範囲イメージの編集
//===============================


/** 範囲内のイメージを copy にコピーした後、p の範囲内を消去する
 *
 * 90度回転で使用する。
 * copy は新規状態であること。 */

void TileImage_copyImage_and_clear(TileImage *p,TileImage *copy,mBox *box)
{
	int ix,iy,x,y;
	PixelRGBA pix,pixe;

	pixe.c = 0;

	for(iy = box->h, y = box->y; iy; iy--, y++)
	{
		for(ix = box->w, x = box->x; ix; ix--, x++)
		{
			TileImage_getPixel(p, x, y, &pix);

			if(pix.a)
			{
				TileImage_setPixel_new(copy, x, y, &pix);
				TileImage_setPixel_draw_direct(p, x, y, &pixe);
			}
		}
	}
}

/** 範囲 左右反転 */

void TileImage_rectReverse_horz(TileImage *p,mBox *box)
{
	int x1,x2,ix,iy,y,cnt;
	PixelRGBA pix1,pix2;

	x1 = box->x;
	x2 = box->x + box->w - 1;
	cnt = box->w >> 1;

	for(iy = box->h, y = box->y; iy > 0; iy--, y++)
	{
		for(ix = 0; ix < cnt; ix++)
		{
			//両端の色を入れ替えてセット

			TileImage_getPixel(p, x1 + ix, y, &pix1);
			TileImage_getPixel(p, x2 - ix, y, &pix2);

			TileImage_setPixel_draw_direct(p, x1 + ix, y, &pix2);
			TileImage_setPixel_draw_direct(p, x2 - ix, y, &pix1);
		}
	}
}

/** 範囲 上下反転 */

void TileImage_rectReverse_vert(TileImage *p,mBox *box)
{
	int y1,y2,ix,iy,x;
	PixelRGBA pix1,pix2;

	y1 = box->y;
	y2 = box->y + box->h - 1;

	for(iy = box->h >> 1; iy > 0; iy--, y1++, y2--)
	{
		for(ix = box->w, x = box->x; ix > 0; ix--, x++)
		{
			TileImage_getPixel(p, x, y1, &pix1);
			TileImage_getPixel(p, x, y2, &pix2);

			TileImage_setPixel_draw_direct(p, x, y1, &pix2);
			TileImage_setPixel_draw_direct(p, x, y2, &pix1);
		}
	}
}

/** 範囲を左に90度回転 */

void TileImage_rectRotate_left(TileImage *p,TileImage *src,mBox *box)
{
	int w,h,sx,sy,dx,dy,ix,iy;
	PixelRGBA pix;

	if(!src) return;

	w = box->w, h = box->h;

	sx = box->x + w - 1;
	dx = box->x + (w >> 1) - (h >> 1);
	dy = box->y + (h >> 1) - (w >> 1);

	for(iy = 0; iy < w; iy++, sx--)
	{
		for(ix = 0, sy = box->y; ix < h; ix++, sy++)
		{
			TileImage_getPixel(src, sx, sy, &pix);
			TileImage_setPixel_draw_direct(p, dx + ix, dy + iy, &pix);
		}
	}
}

/** 範囲を右に90度回転 */

void TileImage_rectRotate_right(TileImage *p,TileImage *src,mBox *box)
{
	int w,h,sx,sy,dx,dy,ix,iy;
	PixelRGBA pix;

	if(!src) return;

	w = box->w, h = box->h;

	sx = box->x;
	dx = box->x + (w >> 1) - (h >> 1);
	dy = box->y + (h >> 1) - (w >> 1);

	for(iy = 0; iy < w; iy++, sx++)
	{
		sy = box->y + h - 1;
		
		for(ix = 0; ix < h; ix++, sy--)
		{
			TileImage_getPixel(src, sx, sy, &pix);
			TileImage_setPixel_draw_direct(p, dx + ix, dy + iy, &pix);
		}
	}
}

/** [タイル状に並べる] 矩形範囲コピー */

static void _arrangetile_copy(TileImage *p,PixelRGBA *srcbuf,int dx,int dy,int w,int h)
{
	int ix,iy;

	for(iy = 0; iy < h; iy++)
	{
		for(ix = 0; ix < w; ix++, srcbuf++)
			TileImage_setPixel_draw_direct(p, dx + ix, dy + iy, srcbuf);
	}
}

/** 範囲イメージをタイル状に並べる */

void TileImage_arrangeTile(TileImage *p,int type,mBox *box)
{
	int ix,iy,topx,topy,imgw,imgh,w,h;
	PixelRGBA *srcbuf,*ps;

	w = box->w, h = box->h;

	//範囲内イメージをバッファに取得

	srcbuf = (PixelRGBA *)mMalloc(w * h * 4, FALSE);
	if(!srcbuf) return;

	ps = srcbuf;

	for(iy = 0; iy < h; iy++)
	{
		for(ix = 0; ix < w; ix++, ps++)
			TileImage_getPixel(p, box->x + ix, box->y + iy, ps);
	}

	//コピー先の先頭

	topx = box->x % w;
	if(topx) topx = topx - w;

	topy = box->y % h;
	if(topy) topy = topy - h;

	//

	imgw = g_tileimage_dinfo.imgw;
	imgh = g_tileimage_dinfo.imgh;

	switch(type)
	{
		//全体
		case 0:
			for(iy = topy; iy < imgh; iy += h)
			{
				for(ix = topx; ix < imgw; ix += w)
					_arrangetile_copy(p, srcbuf, ix, iy, w, h);
			}
			break;
		//横一列
		case 1:
			for(ix = topx; ix < imgw; ix += w)
				_arrangetile_copy(p, srcbuf, ix, box->y, w, h);
			break;
		//縦一列
		case 2:
			for(iy = topy; iy < imgh; iy += h)
				_arrangetile_copy(p, srcbuf, box->x, iy, w, h);
			break;
	}

	mFree(srcbuf);
}

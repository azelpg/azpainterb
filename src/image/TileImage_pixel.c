/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * TileImage
 *
 * ピクセル関連
 *****************************************/

#include <math.h>
#include <string.h>

#include "mDef.h"
#include "mRectBox.h"

#include "defTileImage.h"

#include "TileImage.h"
#include "TileImage_pv.h"
#include "TileImage_coltype.h"
#include "TileImageDrawInfo.h"

#include "ImageBuf8.h"


//-------------------

typedef struct
{
	uint8_t **pptile;	//描画先のタイルバッファポインタ (NULL でタイル範囲外 [!]描画対象ではある)
	int tx,ty,			//タイル位置
		max_alpha;		//レイヤマスクによるアルファ値制限
	PixelRGBA pixdst;	//描画先の色
}_setpixelinfo;


typedef struct
{
	int x,y;
	uint8_t *ptile;
}_colparam;

//-------------------

#define _EQUAL_PIXRGBA(p1,p2) ((p1).c == (p2).c || ((p1).a == 0 && (p2).a == 0))

//-------------------



//================================
// sub
//================================


/** 色マスク判定
 *
 * @return TRUE で保護 */

static mBool _setpixeldraw_colormask(int type,PixelRGBA *pix)
{
	int *pmc,col,f;

	//描画先が透明の場合
	/* マスク => 判定しない。逆マスク => 保護 */

	if(pix->a == 0)
		return (type == 2);

	//マスク色の中に描画先の色があるか

	col = M_RGB(pix->r, pix->g, pix->b);

	for(pmc = g_tileimage_dinfo.colmask_col, f = 0; *pmc != -1; pmc++)
	{
		if(col == *pmc)
		{
			f = 1;
			break;
		}
	}

	if(type == 2) f = !f;

	return f;
}

/** 描画前の元イメージの色を取得
 *
 * _setpixeldraw_dstpixel() 後に使用可能。 */

static void _setpixeldraw_get_save_color(TileImage *p,int x,int y,
	_setpixelinfo *info,PixelRGBA *pix)
{
	uint8_t **pptile,*buf;

	pix->c = 0;

	pptile = info->pptile;

	/* 描画先と保存イメージのタイル配列構成は同じなので、
	 * 描画先がタイル範囲外なら保存イメージも範囲外。 */

	if(pptile)
	{
		//保存イメージのタイル
		
		buf = TILEIMAGE_GETTILE_PT(g_tileimage_dinfo.img_save, info->tx, info->ty);

		if(!buf)
		{
			//保存イメージのタイルが未確保なら、まだ描画されていない部分なので、描画先の色を取得

			if(pptile && *pptile)
				__TileImage_RGBA_getPixelCol_fromTile(p, *pptile, x, y, pix);
		}
		else if(buf != TILEIMAGE_TILE_EMPTY)
			//空でなければ、保存イメージから色取得
			__TileImage_RGBA_getPixelCol_fromTile(g_tileimage_dinfo.img_save, buf, x, y, pix);
	}
}

/** 描画先の情報取得 + マスク処理
 *
 * @param pixdraw   描画色。テクスチャが適用されて返る。
 * @return TRUE で処理しない */

static mBool _setpixeldraw_dstpixel(TileImage *p,int x,int y,PixelRGBA *pixdraw,
	_setpixelinfo *info)
{
	uint8_t **pptile;
	int tx,ty,maska,n;
	PixelRGBA pix;

	//エラー時は処理しない

	if(g_tileimage_dinfo.err) return TRUE;
	
	//テクスチャ適用
	/* 色を上書きする場合は透明時も色をセットしなければいけないので、
	 * テクスチャの値が 0 でも処理を続ける。 */

	if(g_tileimage_dinfo.texture)
	{
		n = ImageBuf8_getPixel_forTexture(g_tileimage_dinfo.texture, x, y);
		
		pixdraw->a = (pixdraw->a * n + 127) / 255;
	}

	//レイヤマスク値取得

	if(!g_tileimage_dinfo.img_mask)
		maska = 255;
	else
	{
		TileImage_getPixel(g_tileimage_dinfo.img_mask, x, y, &pix);
		maska = pix.a;

		if(maska == 0) return TRUE;
	}

	//描画先の色とタイル位置取得
	//pix = 描画先の色

	pix.c = 0;

	if(TileImage_pixel_to_tile(p, x, y, &tx, &ty))
	{
		//タイル配列範囲内
		
		pptile = TILEIMAGE_GETTILE_BUFPT(p, tx, ty);

		if(*pptile)
			__TileImage_RGBA_getPixelCol_fromTile(p, *pptile, x, y, &pix);

		info->pptile = pptile;
		info->tx = tx;
		info->ty = ty;
	}
	else
	{
		//タイル配列範囲外

		info->pptile = NULL;

		//キャンバス範囲外なら処理しない

		if(x < 0 || y < 0 || x >= g_tileimage_dinfo.imgw || y >= g_tileimage_dinfo.imgh)
			return TRUE;
	}

	info->pixdst = pix;

	//描画前状態の色によるマスク

	if(g_tileimage_dinfo.alphamask_type >= 2 || g_tileimage_dinfo.colmask_type)
	{
		//描画前の色取得

		_setpixeldraw_get_save_color(p, x, y, info, &pix);
	
		//アルファマスク

		if(g_tileimage_dinfo.alphamask_type == 2 && pix.a == 0)
			//透明色保護
			return TRUE;
		else if(g_tileimage_dinfo.alphamask_type == 3 && pix.a)
			//不透明色保護
			return TRUE;

		//色マスク判定

		if(g_tileimage_dinfo.colmask_type
			&& _setpixeldraw_colormask(g_tileimage_dinfo.colmask_type, &pix))
			return TRUE;
	}

	//

	info->max_alpha = maska;

	return FALSE;
}

/** 描画前のタイル関連処理
 *
 * これが終了した時点で、エラー時を除き info->pptile には必ず NULL 以外が入る。
 * 
 * @return FALSE で確保エラー */

static mBool _setpixeldraw_tile(TileImage *p,int x,int y,_setpixelinfo *info)
{
	TileImage *img_save;
	uint8_t **pptile,**pptmp;
	int tx,ty;
	mBool bEmptyTile = FALSE;

	pptile = info->pptile;
	tx = info->tx;
	ty = info->ty;

	img_save = g_tileimage_dinfo.img_save;

	//タイル配列が範囲外の場合

	if(!pptile)
	{
		//描画先のタイル配列リサイズ

		if(!TileImage_resizeTileBuf_includeImage(p)) return FALSE;

		//タイル位置を再取得

		TileImage_pixel_to_tile_nojudge(p, x, y, &tx, &ty);

		pptile = TILEIMAGE_GETTILE_BUFPT(p, tx, ty);

		//作業用イメージのリサイズ

		if(!__TileImage_resizeTileBuf_clone(img_save, p))
			return FALSE;

		if(g_tileimage_dinfo.img_brush_stroke
			&& !__TileImage_resizeTileBuf_clone(g_tileimage_dinfo.img_brush_stroke, p))
			return FALSE;
	}

	//描画先にタイルがない場合、確保

	if(!(*pptile))
	{
		*pptile = TileImage_allocTile(p, TRUE);
		if(!(*pptile)) return FALSE;

		bEmptyTile = TRUE;  //描画前は空のタイル
	}

	//描画前の元イメージをコピー

	pptmp = TILEIMAGE_GETTILE_BUFPT(img_save, tx, ty);

	if(bEmptyTile)
		//元が空の場合 1 をセット
		*pptmp = TILEIMAGE_TILE_EMPTY;
	else if(!(*pptmp))
	{
		/* タイルが確保されていなければ、確保＆コピー
		 * (すでにタイルがある場合はコピー済みである) */

		*pptmp = TileImage_allocTile(img_save, FALSE);
		if(!(*pptmp)) return FALSE;
	
		memcpy(*pptmp, *pptile, p->tilesize);
	}

	//描画先タイル情報

	info->pptile = pptile;
	info->tx = tx;
	info->ty = ty;

	return TRUE;
}

/** 結果の色をセット
 *
 * @return 0:OK 1:色が変化しない -1:エラー */

static int _setpixeldraw_setcolor(TileImage *p,int x,int y,
	PixelRGBA *pixres,_setpixelinfo *info)
{
	uint8_t *buf;

	//レイヤマスク適用

	if(pixres->a > info->max_alpha)
		pixres->a = info->max_alpha;

	//アルファマスク (アルファ値維持)

	if(g_tileimage_dinfo.alphamask_type == 1)
		pixres->a = info->pixdst.a;

	//描画前と描画後が同じなら色をセットしない

	if(pixres->c == info->pixdst.c
		|| (pixres->a == 0 && info->pixdst.a == 0))
		return 1;

	//タイル処理

	if(!_setpixeldraw_tile(p, x, y, info))
	{
		g_tileimage_dinfo.err = 1;
		return -1;
	}

	//描画先に色セット

	buf = __TileImage_RGBA_getPixelBuf_fromTile(p, *(info->pptile), x, y);

	*((PixelRGBA *)buf) = *pixres;

	//描画範囲

	mRectIncPoint(&g_tileimage_dinfo.rcdraw, x, y);

	return 0;
}


//================================
// 描画用、色のセット
//================================
/*
 * タイルがない場合は作成される。
 * タイル配列外の場合は、キャンバス範囲内なら配列がリサイズされる。
 *
 * img_save : 描画前の元イメージ保存用。
 *            元のタイルが空の場合は TILEIMAGE_TILE_EMPTY がポインタにセットされる。
 */


/** 直接描画で色セット */

void TileImage_setPixel_draw_direct(TileImage *p,int x,int y,PixelRGBA *pixdraw)
{
	PixelRGBA pixres,pixsrc;
	_setpixelinfo info;
	_colparam param;

	//描画先の情報取得 + マスク処理

	pixsrc = *pixdraw;

	if(_setpixeldraw_dstpixel(p, x, y, &pixsrc, &info))
		return;

	//色処理

	param.ptile = (info.pptile)? *info.pptile: NULL;
	param.x = x;
	param.y = y;

	pixres = info.pixdst;

	(g_tileimage_dinfo.funcColor)(p, &pixres, &pixsrc, &param);

	//色セット

	_setpixeldraw_setcolor(p, x, y, &pixres, &info);
}

/** ストローク重ね塗り描画
 *
 * [!] ドットペン用。
 * ドットペンは描画濃度が一定なので、作業用イメージは必要ない。 */

void TileImage_setPixel_draw_stroke(TileImage *p,int x,int y,PixelRGBA *pixdraw)
{
	PixelRGBA pixres,pixsrc;
	_setpixelinfo info;

	//描画先の情報取得 + マスク処理

	pixsrc = *pixdraw;

	if(_setpixeldraw_dstpixel(p, x, y, &pixsrc, &info))
		return;

	//描画前の色を取得

	_setpixeldraw_get_save_color(p, x, y, &info, &pixres);

	//色処理

	(g_tileimage_dinfo.funcColor)(p, &pixres, &pixsrc, NULL);

	//色セット

	_setpixeldraw_setcolor(p, x, y, &pixres, &info);
}

/** ブラシでのストローク重ね塗り描画
 *
 * ストローク中の、描画された最大濃度を記録しておく必要があるので、
 * 作業用イメージが必要。 */

void TileImage_setPixel_draw_brush_stroke(TileImage *p,int x,int y,PixelRGBA *pixdraw)
{
	PixelRGBA pixres,pixsrc;
	_setpixelinfo info;
	TileImage *img_stroke = g_tileimage_dinfo.img_brush_stroke;
	uint8_t **pptile,*buf,stroke_a;
	mBool write_stroke_img;

	//描画先の情報取得 + マスク処理

	pixsrc = *pixdraw;

	if(_setpixeldraw_dstpixel(p, x, y, &pixsrc, &info))
		return;

	//濃度イメージから、ストローク中の現在濃度を取得

	pptile = NULL;
	buf = NULL;
	stroke_a = 0;

	if(info.pptile)
	{
		pptile = TILEIMAGE_GETTILE_BUFPT(img_stroke, info.tx, info.ty);

		if(*pptile)
		{
			buf = __TileImage_8bit_getPixelBuf_fromTile(img_stroke, *pptile, x, y);
			stroke_a = *buf;
		}
	}

	//濃度の大きい方で描画

	if(pixsrc.a > stroke_a)
		write_stroke_img = TRUE;
	else
	{
		pixsrc.a = stroke_a;

		write_stroke_img = FALSE;
	}

	//描画前の色を取得

	_setpixeldraw_get_save_color(p, x, y, &info, &pixres);

	//色処理

	(g_tileimage_dinfo.funcColor)(p, &pixres, &pixsrc, NULL);

	//色セット
	/* ここでタイル配列がリサイズされる場合がある */

	if(_setpixeldraw_setcolor(p, x, y, &pixres, &info) == 0)
	{
		//ストローク濃度イメージに描画

		/* > _setpixeldraw_setcolor() を実行する前はタイル配列外の場合があり、
		 *   配列リサイズが必要なので、描画はここで行う。
		 * > 描画後に色が変化しない場合はタイル配列リサイズが行われないので、
		 *   この処理はしない。 */

		if(write_stroke_img && !g_tileimage_dinfo.err)
		{
			//配列がリサイズされたら再取得
			
			if(!pptile)
				pptile = TILEIMAGE_GETTILE_BUFPT(img_stroke, info.tx, info.ty);

			//タイル未確保なら作成

			if(!buf)
			{
				*pptile = TileImage_allocTile(img_stroke, TRUE);
				if(!(*pptile))
				{
					g_tileimage_dinfo.err = 1;
					return;
				}

				buf = __TileImage_8bit_getPixelBuf_fromTile(img_stroke, *pptile, x, y);
			}

			//セット

			*buf = pixsrc.a;
		}
	}
}


//================================
// ドットペン形状で描画
//================================


/** ドットペン形状を使って描画 (共通処理)
 *
 * @param overwrite 形状の透明部分も含める */

static void _setpixeldraw_dotpen(TileImage *p,int x,int y,PixelRGBA *pixdraw,
	void (*setpix)(TileImage *,int,int,PixelRGBA *),mBool overwrite)
{
	uint8_t *ps,f,val;
	int size,ix,iy,xx,yy;
	PixelRGBA pix;

	ps = g_tileimage_dinfo.dotpen_buf;
	size = g_tileimage_dinfo.dotpen_size;

	pix = *pixdraw;

	yy = y - (size >> 1);
	g_tileimage_work.dotpen_suby = 0;

	for(iy = size, f = 0x80; iy > 0; iy--, yy++)
	{
		xx = x - (size >> 1);
		g_tileimage_work.dotpen_subx = 0;
	
		for(ix = size; ix > 0; ix--, xx++)
		{
			if(f == 0x80) val = *(ps++);

			if(overwrite)
			{
				//矩形上書き
				
				pix.a = (val & f)? pixdraw->a: 0;
				
				(setpix)(p, xx, yy, &pix);
			}
			else if(val & f)
				(setpix)(p, xx, yy, &pix);

			f >>= 1;
			if(f == 0) f = 0x80;

			g_tileimage_work.dotpen_subx++;
		}

		g_tileimage_work.dotpen_suby++;
	}
}

/** ドットペン形状を使って直接描画 */

void TileImage_setPixel_draw_dotpen_direct(TileImage *p,int x,int y,PixelRGBA *pix)
{
	_setpixeldraw_dotpen(p, x, y, pix, TileImage_setPixel_draw_direct, FALSE);
}

/** ドットペン形状を使ってストローク重ね塗り描画 */

void TileImage_setPixel_draw_dotpen_stroke(TileImage *p,int x,int y,PixelRGBA *pix)
{
	_setpixeldraw_dotpen(p, x, y, pix, TileImage_setPixel_draw_stroke, FALSE);
}

/** ドットペン形状を使って矩形上書き描画 */

void TileImage_setPixel_draw_dotpen_overwrite_square(TileImage *p,int x,int y,PixelRGBA *pix)
{
	_setpixeldraw_dotpen(p, x, y, pix, TileImage_setPixel_draw_direct, TRUE);
}

/** 元の色に指定値を足して描画 (フィルタ用) */

void TileImage_setPixel_draw_addsub(TileImage *p,int x,int y,int v)
{
	PixelRGBA pix;
	int i,n;

	TileImage_getPixel(p, x, y, &pix);

	if(pix.a)
	{
		for(i = 0; i < 3; i++)
		{
			n = pix.ar[i] + v;
			if(n < 0) n = 0;
			else if(n > 255) n = 255;

			pix.ar[i] = n;
		}

		TileImage_setPixel_draw_direct(p, x, y, &pix);
	}
}


//================================
// 色のセット
//================================


/** 色をセット (タイル作成。透明の場合もセット) */

void TileImage_setPixel_new(TileImage *p,int x,int y,PixelRGBA *pix)
{
	uint8_t *buf;

	if((buf = __TileImage_getPixelBuf_new(p, x, y)))
		(p->setPixel)(p, buf, x, y, pix);
}

/** 色をセット (タイル作成。透明の場合はセットしない) */

void TileImage_setPixel_new_notp(TileImage *p,int x,int y,PixelRGBA *pix)
{
	if(pix->a)
	{
		uint8_t *buf;

		if((buf = __TileImage_getPixelBuf_new(p, x, y)))
			(p->setPixel)(p, buf, x, y, pix);
	}
}

/** 色をセット
 *
 * タイルは作成するが、配列拡張は行わない。描画範囲の計算あり。
 * 透明の場合もセット。 */

void TileImage_setPixel_new_drawrect(TileImage *p,int x,int y,PixelRGBA *pix)
{
	uint8_t *buf;

	if((buf = __TileImage_getPixelBuf_new(p, x, y)))
	{
		(p->setPixel)(p, buf, x, y, pix);

		mRectIncPoint(&g_tileimage_dinfo.rcdraw, x, y);
	}
}

/** 色をセット
 *
 * 色計算あり。配列拡張なし。 */

void TileImage_setPixel_new_colfunc(TileImage *p,int x,int y,PixelRGBA *pixdraw)
{
	PixelRGBA pix,pixdst;
	uint8_t *buf;

	//描画後の色

	TileImage_getPixel(p, x, y, &pixdst);

	pix = pixdst;

	(g_tileimage_dinfo.funcColor)(p, &pix, pixdraw, NULL);

	//色が変わらない

	if(_EQUAL_PIXRGBA(pix, pixdst)) return;

	//セット
	
	if((buf = __TileImage_getPixelBuf_new(p, x, y)))
		(p->setPixel)(p, buf, x, y, &pix);
}

/** 色をセット (作業用イメージなどへの描画時)
 *
 * 配列拡張、色計算、範囲セット あり */

void TileImage_setPixel_subdraw(TileImage *p,int x,int y,PixelRGBA *pixdraw)
{
	PixelRGBA pixdst,pixres;
	int tx,ty;
	uint8_t **pptile,*buf;

	//描画後の色

	TileImage_getPixel(p, x, y, &pixdst);

	pixres = pixdst;

	(g_tileimage_dinfo.funcColor)(p, &pixres, pixdraw, NULL);

	//色が変わらない

	if(_EQUAL_PIXRGBA(pixres, pixdst)) return;

	//タイル位置

	if(!TileImage_pixel_to_tile(p, x, y, &tx, &ty))
	{
		//キャンバス範囲外なら何もしない

		if(x < 0 || y < 0 || x >= g_tileimage_dinfo.imgw || y >= g_tileimage_dinfo.imgh)
			return;

		//配列リサイズ & タイル位置再計算

		if(!TileImage_resizeTileBuf_includeImage(p))
			return;

		TileImage_pixel_to_tile(p, x, y, &tx, &ty);
	}

	//タイル確保

	pptile = TILEIMAGE_GETTILE_BUFPT(p, tx, ty);

	if(!(*pptile))
	{
		*pptile = TileImage_allocTile(p, TRUE);
		if(!(*pptile)) return;
	}

	//セット

	buf = (p->getPixelBuf_fromTile)(p, *pptile, x, y);

	(p->setPixel)(p, buf, x, y, &pixres);

	//範囲

	mRectIncPoint(&g_tileimage_dinfo.rcdraw, x, y);
}


//================================
// 取得
//================================


/** 指定 px のバッファ位置取得
 *
 * その位置にタイルがない場合は作成する。ただし、タイルバッファの拡張は行わない。
 *
 * @return 範囲外、または確保失敗で NULL */

uint8_t *__TileImage_getPixelBuf_new(TileImage *p,int x,int y)
{
	int tx,ty;
	uint8_t **pp;

	//タイル位置

	if(!TileImage_pixel_to_tile(p, x, y, &tx, &ty))
		return NULL;

	//タイルがない場合は確保

	pp = TILEIMAGE_GETTILE_BUFPT(p, tx, ty);

	if(!(*pp))
	{
		*pp = TileImage_allocTile(p, TRUE);
		if(!(*pp)) return NULL;
	}

	//バッファ位置

	return (p->getPixelBuf_fromTile)(p, *pp, x, y);
}

/** 指定位置の色が不透明か (A=0 でないか) */

mBool TileImage_isPixelOpaque(TileImage *p,int x,int y)
{
	PixelRGBA pix;

	TileImage_getPixel(p, x, y, &pix);

	return (pix.a != 0);
}

/** 指定位置の色が透明か */

mBool TileImage_isPixelTransparent(TileImage *p,int x,int y)
{
	PixelRGBA pix;

	TileImage_getPixel(p, x, y, &pix);

	return (pix.a == 0);
}

/** 指定位置の色を取得 (範囲外は透明) */

void TileImage_getPixel(TileImage *p,int x,int y,PixelRGBA *pix)
{
	int tx,ty;
	uint8_t *ptile;

	if(!TileImage_pixel_to_tile(p, x, y, &tx, &ty))
		pix->c = 0;
	else
	{
		ptile = TILEIMAGE_GETTILE_PT(p, tx, ty);

		if(ptile)
			(p->getPixelCol_fromTile)(p, ptile, x, y, pix);
		else
			pix->c = 0;
	}
}

/** 指定位置の色を取得 (イメージ範囲外はクリッピング) */

void TileImage_getPixel_clip(TileImage *p,int x,int y,PixelRGBA *pix)
{
	if(x < 0) x = 0;
	else if(x >= g_tileimage_dinfo.imgw) x = g_tileimage_dinfo.imgw - 1;

	if(y < 0) y = 0;
	else if(y >= g_tileimage_dinfo.imgh) y = g_tileimage_dinfo.imgh - 1;

	TileImage_getPixel(p, x, y, pix);
}

/** 色を取得 (32bit RGBA値を返す) */

uint32_t TileImage_getPixel_pac(TileImage *p,int x,int y)
{
	PixelRGBA pix;

	TileImage_getPixel(p, x, y, &pix);

	return ((uint32_t)pix.a << 24) | (pix.r << 16) | (pix.g << 8) | pix.b;
}

/** 色を取得 (UNDO 時の小範囲用)
 *
 * @return 0 で点あり、1 で空タイル、2 で元が空タイル */

int TileImage_getPixel_forUndo(TileImage *p,int x,int y,PixelRGBA *pix)
{
	int tx,ty;
	uint8_t *ptile;

	pix->c = 0;

	if(!TileImage_pixel_to_tile(p, x, y, &tx, &ty))
		return 1;
	else
	{
		ptile = TILEIMAGE_GETTILE_PT(p, tx, ty);

		if(!ptile)
			return 1;
		else if(ptile == TILEIMAGE_TILE_EMPTY)
			return 2;
		else
		{
			(p->getPixelCol_fromTile)(p, ptile, x, y, pix);
			return 0;
		}
	}
}

/** pix に対して色合成を行った色を取得 */

void TileImage_getBlendPixel(TileImage *p,int x,int y,PixelRGBA *pix,int opacity,int blendmode)
{
	PixelRGBA pixsrc;
	int a;

	TileImage_getPixel(p, x, y, &pixsrc);

	a = pixsrc.a * opacity >> 7;
	if(a == 0) return;

	//色合成

	__TileImage_getBlendColor(blendmode, &pixsrc, pix);

	//アルファ合成

	pix->r = (pixsrc.r - pix->r) * a / 255 + pix->r;
	pix->g = (pixsrc.g - pix->g) * a / 255 + pix->g;
	pix->b = (pixsrc.b - pix->b) * a / 255 + pix->b;
}


//============================
// 特殊処理
//============================


/** 指定位置周辺の色を指先バッファにセット (指先描画開始時) */

mBool TileImage_setFingerBuf(TileImage *p,int x,int y)
{
	TileImageWorkData *work = &g_tileimage_work;
	uint8_t *ps,f,val;
	PixelRGBA *pixbuf;
	int size,ix,iy,xx,yy;

	size = g_tileimage_dinfo.dotpen_size;

	//指先バッファ確保

	if(!work->finger_buf || work->finger_cursize < size)
	{
		mFree(work->finger_buf);
		
		work->finger_buf = (PixelRGBA *)mMalloc(size * size * 4, FALSE);
		if(!work->finger_buf) return FALSE;

		work->finger_cursize = size;
	}

	//セット

	ps = g_tileimage_dinfo.dotpen_buf;
	pixbuf = work->finger_buf;

	yy = y - (size >> 1);

	for(iy = size, f = 0x80; iy > 0; iy--, yy++)
	{
		xx = x - (size >> 1);
	
		for(ix = size; ix > 0; ix--, xx++, pixbuf++)
		{
			if(f == 0x80) val = *(ps++);

			if(val & f)
				TileImage_getPixel_clip(p, xx, yy, pixbuf);

			f >>= 1;
			if(f == 0) f = 0x80;
		}
	}

	return TRUE;
}


//============================
// 色処理関数
//============================


/** 通常合成 */

void TileImage_colfunc_normal(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param)
{
	double sa,da,na;
	int a;

	if(src->a == 255)
	{
		*dst = *src;
		return;
	}

	sa = src->a / 255.0;
	da = dst->a / 255.0;

	//A

	na = sa + da - sa * da;
	a = (int)(na * 255 + 0.5);

	if(a == 0)
		dst->c = 0;
	else
	{
		//RGB
		
		da = da * (1.0 - sa);
		na = 1.0 / na;

		dst->r = (int)((src->r * sa + dst->r * da) * na + 0.5);
		dst->g = (int)((src->g * sa + dst->g * da) * na + 0.5);
		dst->b = (int)((src->b * sa + dst->b * da) * na + 0.5);
		dst->a = a;
	}
}

/** アルファ値を比較して大きければ上書き */

void TileImage_colfunc_compareA(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param)
{
	if(src->a > dst->a)
		*dst = *src;
}

/** 完全上書き */

void TileImage_colfunc_overwrite(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param)
{
	*dst = *src;
}

/** 消しゴム */

void TileImage_colfunc_erase(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param)
{
	int a;

	a = dst->a - src->a;
	if(a < 0) a = 0;

	dst->a = a;
}

/** 覆い焼き */

void TileImage_colfunc_dodge(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param)
{
	int i,c;
	double d;

	if(src->a == 255)
		//最大の場合は白
		dst->r = dst->g = dst->b = 255;
	else
	{
		d = 255.0 / (255 - src->a);

		for(i = 0; i < 3; i++)
		{
			c = (int)(dst->ar[i] * d + 0.5);
			if(c > 255) c = 255;

			dst->ar[i] = c;
		}
	}
}

/** 焼き込み */

void TileImage_colfunc_burn(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param)
{
	int i,c;
	double d;

	if(src->a == 255)
		//最大の場合は黒
		dst->r = dst->g = dst->b = 0;
	else
	{
		d = 255.0 / (255 - src->a);

		for(i = 0; i < 3; i++)
		{
			c = round(255 - (255 - dst->ar[i]) * d);
			if(c < 0) c = 0;

			dst->ar[i] = c;
		}
	}
}

/** 指先 */

void TileImage_colfunc_finger(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param)
{
	PixelRGBA *pixsave;
	double sa,da,na,strength;
	int a,x,y;

	//形状上における前回の色

	pixsave = g_tileimage_work.finger_buf
		+ g_tileimage_work.dotpen_suby * g_tileimage_dinfo.dotpen_size
		+ g_tileimage_work.dotpen_subx;

	//A

	strength = src->a / 255.0;
	sa = pixsave->a / 255.0;
	da = dst->a / 255.0;

	na = da + (sa - da) * strength;
	a = (int)(na * 255 + 0.5);

	if(a == 0)
		dst->c = 0;
	else
	{
		//RGB

		sa *= strength;
		da *= 1.0 - strength;
		na = 1.0 / na;

		dst->r = (int)((pixsave->r * sa + dst->r * da) * na + 0.5);
		dst->g = (int)((pixsave->g * sa + dst->g * da) * na + 0.5);
		dst->b = (int)((pixsave->b * sa + dst->b * da) * na + 0.5);
		dst->a = a;
	}

	//現在の色をバッファにセット
	/* 描画位置がイメージ範囲内ならそのままセット。
	 * 範囲外ならクリッピングした位置の色。 */

	x = ((_colparam *)param)->x;
	y = ((_colparam *)param)->y;

	if(x >= 0 && x < g_tileimage_dinfo.imgw
		&& y >= 0 && y < g_tileimage_dinfo.imgh)
		*pixsave = *dst;
	else
		TileImage_getPixel_clip(p, x, y, pixsave);
}

/** ぼかし
 *
 * 3x3 の周囲の色の平均値 */

void TileImage_colfunc_blur(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param)
{
	int x,y,tx,ty,ix,iy;
	uint8_t *ptile;
	double rr,gg,bb,aa,dd;
	PixelRGBA pix;

	x = ((_colparam *)param)->x;
	y = ((_colparam *)param)->y;
	ptile = ((_colparam *)param)->ptile;

	tx = (x - p->offx) & 63;
	ty = (y - p->offy) & 63;

	//総和

	rr = gg = bb = aa = 0;

	if(ptile && tx >= 1 && tx <= 62 && ty >= 1 && ty <= 62
		&& x >= 1 && x <= g_tileimage_dinfo.imgw - 2
		&& y >= 1 && y <= g_tileimage_dinfo.imgh - 2)
	{
		/* タイルが存在し、参照する 3x3 がすべてタイル内に収まっていて、
		 * かつ参照範囲がイメージ範囲内の場合 */

		for(iy = -1; iy <= 1; iy++)
		{
			for(ix = -1; ix <= 1; ix++)
			{
				__TileImage_RGBA_getPixelCol_fromTile(p, ptile, x + ix, y + iy, &pix);

				dd = pix.a / 255.0;

				rr += pix.r * dd;
				gg += pix.g * dd;
				bb += pix.b * dd;
				aa += dd;
			}
		}
	}
	else
	{
		//タイルが存在しない、または参照範囲が他タイルに及ぶ場合

		for(iy = -1; iy <= 1; iy++)
		{
			for(ix = -1; ix <= 1; ix++)
			{
				TileImage_getPixel_clip(p, x + ix, y + iy, &pix);

				dd = pix.a / 255.0;

				rr += pix.r * dd;
				gg += pix.g * dd;
				bb += pix.b * dd;
				aa += dd;
			}
		}
	}

	//平均値

	if(aa != 0)
	{
		dd = 1.0 / aa;

		dst->r = (uint8_t)(rr * dd + 0.5);
		dst->g = (uint8_t)(gg * dd + 0.5);
		dst->b = (uint8_t)(bb * dd + 0.5);
		dst->a = (uint8_t)(aa / 9.0 * 255 + 0.5);
	}
}

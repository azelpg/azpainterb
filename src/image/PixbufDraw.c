/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * mPixbuf への描画関数
 *****************************************/

#include <math.h>

#include "mDef.h"
#include "mPixbuf.h"
#include "mSysCol.h"
#include "mRectBox.h"

#include "defCanvasInfo.h"




//===========================
// sub
//===========================


/** クリッピング付き直線描画 (blend A=0-128) */

/* クリッピング範囲が異なっても、常に同じ位置に点が打たれるようにする。
 * でないと、キャンバス全体を更新した時と描画時に範囲更新した時とで
 * グリッドの線がずれる場合がある。 */

static void _drawline_blend_clip(mPixbuf *p,int x1,int y1,int x2,int y2,uint32_t col,
	mRect *rc)
{
	int dx,dy,tmp;
	int64_t f,inc;
	mRect rcb;

	//線からなる矩形が範囲外か

	if(x1 < x2)
		rcb.x1 = x1, rcb.x2 = x2;
	else
		rcb.x1 = x2, rcb.x2 = x1;

	if(y1 < y2)
		rcb.y1 = y1, rcb.y2 = y2;
	else
		rcb.y1 = y2, rcb.y2 = y1;

	if(rcb.x2 < rc->x1 || rcb.x1 > rc->x2 || rcb.y2 < rc->y1 || rcb.y1 > rc->y2)
		return;

	//

	if(x1 == x2 && y1 == y2)
	{
		//1px

		mPixbufSetPixel_blend128(p, x1, y1, col);
	}
	else if(y1 == y2)
	{
		//横線

		x1 = rcb.x1, x2 = rcb.x2;

		if(x1 < rc->x1) x1 = rc->x1;
		if(x2 > rc->x2) x2 = rc->x2;

		mPixbufLineH_blend(p, x1, y1, x2 - x1 + 1, col, (col >> 24) << 1); //A=0-256
	}
	else if(x1 == x2)
	{
		//縦線

		y1 = rcb.y1, y2 = rcb.y2;

		if(y1 < rc->y1) y1 = rc->y1;
		if(y2 > rc->y2) y2 = rc->y2;

		mPixbufLineV_blend(p, x1, y1, y2 - y1 + 1, col, (col >> 24) << 1);
	}
	else
	{
		//直線

		dx = rcb.x2 - rcb.x1;
		dy = rcb.y2 - rcb.y1;

		if(dx > dy)
		{
			//----- 横長

			//x1 -> x2 の方向にする

			if(x1 > x2)
			{
				x1 = rcb.x1, x2 = rcb.x2;
				tmp = y1, y1 = y2, y2 = tmp;
			}

			inc = ((int64_t)(y2 - y1) << 16) / dx;
			f = (int64_t)y1 << 16;

			//X クリッピング

			if(x1 < rc->x1)
			{
				f += inc * (rc->x1 - x1);
				x1 = rc->x1;
			}

			if(x2 > rc->x2) x2 = rc->x2;

			//

			tmp = (f + (1<<15)) >> 16;

			if(y1 < y2)
			{
				//----- Y が下方向

				//開始位置が範囲外

				if(tmp > rc->y2) return;

				//範囲内まで進める

				while(tmp < rc->y1)
				{
					x1++;
					f += inc;

					tmp = (f + (1<<15)) >> 16;
				}

				//描画

				for(; x1 <= x2; x1++, f += inc)
				{
					tmp = (f + (1<<15)) >> 16;
					if(tmp > rc->y2) break;

					mPixbufSetPixel_blend128(p, x1, tmp, col);
				}
			}
			else
			{
				//----- Y が上方向

				//開始位置が範囲外

				if(tmp < rc->y1) return;

				//範囲内まで進める

				while(tmp > rc->y2)
				{
					x1++;
					f += inc;

					tmp = (f + (1<<15)) >> 16;
				}

				//描画

				for(; x1 <= x2; x1++, f += inc)
				{
					tmp = (f + (1<<15)) >> 16;
					if(tmp < rc->y1) break;

					mPixbufSetPixel_blend128(p, x1, tmp, col);
				}
			}
		}
		else
		{
			//------ 縦長

			//y1 -> y2 の方向にする

			if(y1 > y2)
			{
				y1 = rcb.y1, y2 = rcb.y2;
				tmp = x1, x1 = x2, x2 = tmp;
			}

			inc = ((int64_t)(x2 - x1) << 16) / dy;
			f = (int64_t)x1 << 16;

			//Y クリッピング

			if(y1 < rc->y1)
			{
				f += inc * (rc->y1 - y1);
				y1 = rc->y1;
			}

			if(y2 > rc->y2) y2 = rc->y2;

			//

			tmp = (f + (1<<15)) >> 16;

			if(x1 < x2)
			{
				//----- X が右方向

				//開始位置が範囲外

				if(tmp > rc->x2) return;

				//範囲内まで進める

				while(tmp < rc->x1)
				{
					y1++;
					f += inc;

					tmp = (f + (1<<15)) >> 16;
				}

				//描画

				for(; y1 <= y2; y1++, f += inc)
				{
					tmp = (f + (1<<15)) >> 16;
					if(tmp > rc->x2) break;

					mPixbufSetPixel_blend128(p, tmp, y1, col);
				}
			}
			else
			{
				//----- X が左方向

				//開始位置が範囲外

				if(tmp < rc->x1) return;

				//範囲内まで進める

				while(tmp > rc->x2)
				{
					y1++;
					f += inc;

					tmp = (f + (1<<15)) >> 16;
				}

				//描画

				for(; y1 <= y2; y1++, f += inc)
				{
					tmp = (f + (1<<15)) >> 16;
					if(tmp < rc->x1) break;

					mPixbufSetPixel_blend128(p, tmp, y1, col);
				}
			}
		}
	}
}

/** クリッピング付き破線直線描画
 *
 * @return (x2,y2) の位置のパターン番号 */

static int _drawline_dash_clip(mPixbuf *p,int x1,int y1,int x2,int y2,int pat,mRect *rcclip)
{
	int dx,dy,tmp,pat_end,dir;
	int64_t f,inc;
	mPoint min,max;
	mPixCol col[4];
	uint8_t *pd;

	col[0] = col[1] = 0;
	col[2] = col[3] = MSYSCOL(WHITE);

	//描画座標の最小、最大値

	if(x1 < x2)
		min.x = x1, max.x = x2;
	else
		min.x = x2, max.x = x1;

	if(y1 < y2)
		min.y = y1, max.y = y2;
	else
		min.y = y2, max.y = y1;

	//

	if(x1 == x2 && y1 == y2)
	{
		//====== 1px

		if(rcclip->x1 <= x1 && x1 <= rcclip->x2 && rcclip->y1 <= y1 && y1 <= rcclip->y2)
			mPixbufSetPixel(p, x1, y1, col[pat]);

		return pat;
	}
	else if(y1 == y2)
	{
		//====== 横線

		pat_end = (pat + max.x - min.x) & 3;

		if(min.x <= rcclip->x2 && max.x >= rcclip->x1
			&& y1 >= rcclip->y1 && y1 <= rcclip->y2)
		{
			dir = (x1 < x2)? 1: -1;
		
			x1 = min.x, x2 = max.x;
			if(dir < 0) pat = pat_end;

			if(x1 < rcclip->x1)
			{
				pat = (pat + (rcclip->x1 - x1) * dir) & 3;
				x1 = rcclip->x1;
			}
			
			if(x2 > rcclip->x2) x2 = rcclip->x2;

			pd = mPixbufGetBufPtFast(p, x1, y1);

			for(dx = x2 - x1 + 1; dx; dx--, pd += p->bpp, pat = (pat + dir) & 3)
				(p->setbuf)(pd, col[pat]);
		}

		return pat_end;
	}
	else if(x1 == x2)
	{
		//====== 縦線

		pat_end = (pat + max.y - min.y) & 3;

		if(x1 >= rcclip->x1 && x1 <= rcclip->x2
			&& min.y <= rcclip->y2 && max.y >= rcclip->y1)
		{
			dir = (y1 < y2)? 1: -1;
		
			y1 = min.y, y2 = max.y;
			if(dir < 0) pat = pat_end;

			if(y1 < rcclip->y1)
			{
				pat = (pat + (rcclip->y1 - y1) * dir) & 3;
				y1 = rcclip->y1;
			}
			
			if(y2 > rcclip->y2) y2 = rcclip->y2;

			pd = mPixbufGetBufPtFast(p, x1, y1);

			for(dy = y2 - y1 + 1; dy; dy--, pd += p->pitch_dir, pat = (pat + dir) & 3)
				(p->setbuf)(pd, col[pat]);
		}

		return pat_end;
	}
	else
	{
		//====== 斜め

		dx = max.x - min.x;
		dy = max.y - min.y;

		if(dx > dy)
			pat_end = (pat + max.x - min.x) & 3;
		else
			pat_end = (pat + max.y - min.y) & 3;

		//範囲外

		if(min.x > rcclip->x2 || max.x < rcclip->x1
			|| min.y > rcclip->y2 || max.y < rcclip->y1)
			return pat_end;

		//

		if(dx > dy)
		{
			//----- 横長

			dir = (x1 < x2)? 1: -1;

			//x1 -> x2 の方向にする

			if(dir < 0)
			{
				x1 = min.x, x2 = max.x;
				tmp = y1, y1 = y2, y2 = tmp;
				pat = pat_end;
			}

			inc = ((int64_t)(y2 - y1) << 16) / dx;
			f = (int64_t)y1 << 16;

			//X クリッピング

			if(x1 < rcclip->x1)
			{
				pat = (pat + (rcclip->x1 - x1) * dir) & 3;
				f += inc * (rcclip->x1 - x1);
				x1 = rcclip->x1;
			}

			if(x2 > rcclip->x2) x2 = rcclip->x2;

			//

			tmp = (f + (1<<15)) >> 16;

			if(y1 < y2)
			{
				//----- Y が下方向

				//開始位置が範囲外

				if(tmp > rcclip->y2) return pat_end;

				//範囲内まで進める

				while(tmp < rcclip->y1)
				{
					x1++;
					f += inc;
					pat = (pat + dir) & 3;

					tmp = (f + (1<<15)) >> 16;
				}

				//描画

				for(; x1 <= x2; x1++, f += inc, pat = (pat + dir) & 3)
				{
					tmp = (f + (1<<15)) >> 16;
					if(tmp > rcclip->y2) break;

					mPixbufSetPixel(p, x1, tmp, col[pat]);
				}
			}
			else
			{
				//----- Y が上方向

				//開始位置が範囲外

				if(tmp < rcclip->y1) return pat_end;

				//範囲内まで進める

				while(tmp > rcclip->y2)
				{
					x1++;
					f += inc;
					pat = (pat + dir) & 3;

					tmp = (f + (1<<15)) >> 16;
				}

				//描画

				for(; x1 <= x2; x1++, f += inc, pat = (pat + dir) & 3)
				{
					tmp = (f + (1<<15)) >> 16;
					if(tmp < rcclip->y1) break;

					mPixbufSetPixel(p, x1, tmp, col[pat]);
				}
			}
		}
		else
		{
			//------ 縦長

			dir = (y1 < y2)? 1: -1;

			//y1 -> y2 の方向にする

			if(y1 > y2)
			{
				y1 = min.y, y2 = max.y;
				tmp = x1, x1 = x2, x2 = tmp;
				pat = pat_end;
			}

			inc = ((int64_t)(x2 - x1) << 16) / dy;
			f = (int64_t)x1 << 16;

			//Y クリッピング

			if(y1 < rcclip->y1)
			{
				pat = (pat + (rcclip->y1 - y1) * dir) & 3;
				f += inc * (rcclip->y1 - y1);
				y1 = rcclip->y1;
			}

			if(y2 > rcclip->y2) y2 = rcclip->y2;

			//

			tmp = (f + (1<<15)) >> 16;

			if(x1 < x2)
			{
				//----- X が右方向

				//開始位置が範囲外

				if(tmp > rcclip->x2) return pat_end;

				//範囲内まで進める

				while(tmp < rcclip->x1)
				{
					y1++;
					f += inc;
					pat = (pat + dir) & 3;

					tmp = (f + (1<<15)) >> 16;
				}

				//描画

				for(; y1 <= y2; y1++, f += inc, pat = (pat + dir) & 3)
				{
					tmp = (f + (1<<15)) >> 16;
					if(tmp > rcclip->x2) break;

					mPixbufSetPixel(p, tmp, y1, col[pat]);
				}
			}
			else
			{
				//----- X が左方向

				//開始位置が範囲外

				if(tmp < rcclip->x1) return pat_end;

				//範囲内まで進める

				while(tmp > rcclip->x2)
				{
					y1++;
					f += inc;
					pat = (pat + dir) & 3;

					tmp = (f + (1<<15)) >> 16;
				}

				//描画

				for(; y1 <= y2; y1++, f += inc, pat = (pat + dir) & 3)
				{
					tmp = (f + (1<<15)) >> 16;
					if(tmp < rcclip->x1) break;

					mPixbufSetPixel(p, tmp, y1, col[pat]);
				}
			}
		}

		return pat_end;
	}
}


//===========================
//
//===========================


/** 白黒破線の十字線描画
 *
 * キャンバスビューのルーペ時。
 *
 * @param x    左側の垂直線の位置
 * @param y    上側の水平線の位置
 * @param size ２つの線の間隔
 * @param box  描画範囲 */

void pixbufDraw_cross_dash(mPixbuf *pixbuf,int x,int y,int size,mBox *boxarea)
{
	mPixCol col[2];
	mBox box;
	uint8_t *pd;
	int i,n;

	if(!mPixbufGetClipBox_box(pixbuf, &box, boxarea))
		return;

	col[0] = 0;
	col[1] = MSYSCOL(WHITE);

	//水平線、上 (実際には y - 1 の位置)

	n = y - 1;

	if(box.y <= n && n < box.y + box.h)
	{
		pd = mPixbufGetBufPtFast(pixbuf, box.x, n);
		n = box.x;

		for(i = box.w; i; i--, pd += pixbuf->bpp, n++)
		{
			if(n < x || n >= x + size)
				(pixbuf->setbuf)(pd, col[(n >> 1) & 1]);
		}
	}

	//水平線、下

	n = y + size;

	if(box.y <= n && n < box.y + box.h)
	{
		pd = mPixbufGetBufPtFast(pixbuf, box.x, n);
		n = box.x;

		for(i = box.w; i; i--, pd += pixbuf->bpp, n++)
		{
			if(n < x || n >= x + size)
				(pixbuf->setbuf)(pd, col[(n >> 1) & 1]);
		}
	}

	//垂直線、左

	n = x - 1;

	if(box.x <= n && n < box.x + box.w)
	{
		pd = mPixbufGetBufPtFast(pixbuf, n, box.y);
		n = box.y;

		for(i = box.h; i; i--, pd += pixbuf->pitch_dir, n++)
		{
			if(n < y || n >= y + size)
				(pixbuf->setbuf)(pd, col[(n >> 1) & 1]);
		}
	}

	//垂直線、右

	n = x + size;

	if(box.x <= n && n < box.x + box.w)
	{
		pd = mPixbufGetBufPtFast(pixbuf, n, box.y);
		n = box.y;

		for(i = box.h; i; i--, pd += pixbuf->pitch_dir, n++)
		{
			if(n < y || n >= y + size)
				(pixbuf->setbuf)(pd, col[(n >> 1) & 1]);
		}
	}
}

/** 白黒破線の矩形枠描画 (フィルタプレビュー用) */

void pixbufDraw_dashBox_mono(mPixbuf *pixbuf,int x,int y,int w,int h)
{
	mPixCol col[2];
	int i,n,pos = 0;

	col[0] = 0;
	col[1] = MSYSCOL(WHITE);

	//上

	for(i = 0; i < w; i++, pos++)
		mPixbufSetPixel(pixbuf, x + i, y, col[(pos >> 1) & 1]);

	//右

	n = x + w - 1;

	for(i = 1; i < h; i++, pos++)
		mPixbufSetPixel(pixbuf, n, y + i, col[(pos >> 1) & 1]);

	//下

	n = y + h - 1;

	for(i = w - 2; i >= 0; i--, pos++)
		mPixbufSetPixel(pixbuf, x + i, n, col[(pos >> 1) & 1]);

	//左

	for(i = h - 2; i > 0; i--, pos++)
		mPixbufSetPixel(pixbuf, x, y + i, col[(pos >> 1) & 1]);
}

/** 選択範囲の枠を描画 */

void pixbufDrawSelectBox(mPixbuf *pixbuf,mBox *boximg,CanvasDrawInfo *info)
{
	mRect rcclip;
	mPoint pt[4];
	double x1,y1,x2,y2;
	int pat;

	//描画先のクリッピング範囲 (rect)
	
	mRectSetByBox(&rcclip, &info->boxdst);

	//

	x1 = boximg->x, y1 = boximg->y;
	x2 = boximg->x + boximg->w, y2 = boximg->y + boximg->h;

	if(info->param->rd == 0)
	{
		//----- 回転なし

		CanvasDrawInfo_imageToarea_pt(info, x1, y1, pt);
		CanvasDrawInfo_imageToarea_pt(info, x2, y2, pt + 1);

		//左上を -1px

		pt[0].y--;

		if(info->mirror)
			pt[0].x++;
		else
			pt[0].x--;

		//描画

		pat = _drawline_dash_clip(pixbuf, pt[0].x, pt[0].y, pt[1].x, pt[0].y, 0, &rcclip);
		pat = _drawline_dash_clip(pixbuf, pt[1].x, pt[0].y, pt[1].x, pt[1].y, pat, &rcclip);
		pat = _drawline_dash_clip(pixbuf, pt[1].x, pt[1].y, pt[0].x, pt[1].y, pat, &rcclip);
		_drawline_dash_clip(pixbuf, pt[0].x, pt[1].y, pt[0].x, pt[0].y, pat, &rcclip);
	}
	else
	{
		//----- 回転あり

		//左上を -1px するため、イメージ座標で 1px に相当する値を引く

		x1 -= info->param->scalediv;
		y1 -= info->param->scalediv;
	
		CanvasDrawInfo_imageToarea_pt(info, x1, y1, pt);
		CanvasDrawInfo_imageToarea_pt(info, x2, y1, pt + 1);
		CanvasDrawInfo_imageToarea_pt(info, x2, y2, pt + 2);
		CanvasDrawInfo_imageToarea_pt(info, x1, y2, pt + 3);

		//描画

		pat = _drawline_dash_clip(pixbuf, pt[0].x, pt[0].y, pt[1].x, pt[1].y, 0, &rcclip);
		pat = _drawline_dash_clip(pixbuf, pt[1].x, pt[1].y, pt[2].x, pt[2].y, pat, &rcclip);
		pat = _drawline_dash_clip(pixbuf, pt[2].x, pt[2].y, pt[3].x, pt[3].y, pat, &rcclip);
		_drawline_dash_clip(pixbuf, pt[3].x, pt[3].y, pt[0].x, pt[0].y, pat, &rcclip);
	}
}

/** キャンバスにグリッド線描画
 *
 * @param boxdst 描画先の範囲
 * @param boximg boxdst の範囲に相当するイメージ座標での範囲
 * @param rcgrid グリッドを描画するイメージ範囲 (NULL で全体) */

void pixbufDrawGrid(mPixbuf *pixbuf,mBox *boxdst,mBox *boximg,
	int gridw,int gridh,uint32_t col,mRect *rcgrid,CanvasDrawInfo *info)
{
	mRect rcclip;
	int top,bottom,i;
	double inc[4],x1,y1,x2,y2,incx,incy;
	mBool inarea;

	//描画先のクリッピング範囲 (rect)
	
	mRectSetByBox(&rcclip, boxdst);

	//イメージ座標 +1 時のキャンバス座標変化値

	CanvasDrawInfo_getImageIncParam(info, inc);

	//--------- 縦線

	top = boximg->x;
	bottom = boximg->x + boximg->w - 1;
	inarea = TRUE;

	if(rcgrid)
	{
		//グリッド範囲あり
		
		if(top > rcgrid->x2 || bottom < rcgrid->x1)
			inarea = FALSE;
		else
		{
			if(top < rcgrid->x1) top = rcgrid->x1;
			if(bottom > rcgrid->x2) bottom = rcgrid->x2;

			top = (top - rcgrid->x1) / gridw * gridw + rcgrid->x1;
			bottom = (bottom - rcgrid->x1) / gridw * gridw + rcgrid->x1;

			CanvasDrawInfo_imageToarea(info, top, rcgrid->y1, &x1, &y1);
			CanvasDrawInfo_imageToarea(info, top, rcgrid->y2, &x2, &y2);
		}
	}
	else
	{
		//全体
		
		top = top / gridw * gridw;
		bottom = bottom / gridw * gridw;

		CanvasDrawInfo_imageToarea(info, top, 0, &x1, &y1);
		CanvasDrawInfo_imageToarea(info, top, info->imgh, &x2, &y2);
	}

	//描画

	if(inarea)
	{
		incx = inc[0] * gridw;
		incy = inc[1] * gridw;

		for(i = top; i <= bottom; i += gridw)
		{
			_drawline_blend_clip(pixbuf, x1, y1, x2, y2, col, &rcclip);

			x1 += incx, y1 += incy;
			x2 += incx, y2 += incy;
		}
	}

	//--------- 横線

	top = boximg->y;
	bottom = boximg->y + boximg->h - 1;
	inarea = TRUE;

	if(rcgrid)
	{
		//グリッド範囲あり
		
		if(top > rcgrid->y2 || bottom < rcgrid->y1)
			inarea = FALSE;
		else
		{
			if(top < rcgrid->y1) top = rcgrid->y1;
			if(bottom > rcgrid->y2) bottom = rcgrid->y2;

			top = (top - rcgrid->y1) / gridh * gridh + rcgrid->y1;
			bottom = (bottom - rcgrid->y1) / gridh * gridh + rcgrid->y1;

			CanvasDrawInfo_imageToarea(info, rcgrid->x1, top, &x1, &y1);
			CanvasDrawInfo_imageToarea(info, rcgrid->x2, top, &x2, &y2);
		}
	}
	else
	{
		//全体
		
		top = top / gridh * gridh;
		bottom = bottom / gridh * gridh;

		CanvasDrawInfo_imageToarea(info, 0, top, &x1, &y1);
		CanvasDrawInfo_imageToarea(info, info->imgw, top, &x2, &y2);
	}

	//描画

	if(inarea)
	{
		incx = inc[2] * gridh;
		incy = inc[3] * gridh;

		for(i = top; i <= bottom; i += gridh)
		{
			_drawline_blend_clip(pixbuf, x1, y1, x2, y2, col, &rcclip);

			x1 += incx, y1 += incy;
			x2 += incx, y2 += incy;
		}
	}
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * TileImage
 *
 * RGBA タイプ
 *****************************************/
/*
 * R-G-B-A の順で 1px ごとに並んでいる。
 */

#include <string.h>

#include "mDef.h"

#include "defTileImage.h"
#include "TileImage.h"
#include "TileImage_pv.h"



/** タイルと px 位置からバッファ位置取得 */

uint8_t *__TileImage_RGBA_getPixelBuf_fromTile(TileImage *p,uint8_t *tile,int x,int y)
{
	x = (x - p->offx) & 63;
	y = (y - p->offy) & 63;

	return tile + (((y << 6) + x) << 2);
}

/** タイルと px 位置から色取得 */

void __TileImage_RGBA_getPixelCol_fromTile(TileImage *p,uint8_t *tile,int x,int y,PixelRGBA *pix)
{
	x = (x - p->offx) & 63;
	y = (y - p->offy) & 63;

	tile += ((y << 6) + x) << 2;

	pix->r = tile[0];
	pix->g = tile[1];
	pix->b = tile[2];
	pix->a = tile[3];
}

/** 色をセット */

void __TileImage_RGBA_setPixel(TileImage *p,uint8_t *buf,int x,int y,PixelRGBA *pix)
{
	*((PixelRGBA *)buf) = *pix;
}

/** タイルがすべて透明か */

mBool __TileImage_RGBA_isTransparentTile(uint8_t *tile)
{
	int i;

	tile += 3;

	for(i = 64 * 64; i; i--, tile += 4)
	{
		if(*tile) return FALSE;
	}

	return TRUE;
}

/** ImageBuf24 に合成 */

void __TileImage_RGBA_blendTile(TileImageBlendInfo *info)
{
	int pitchs,ix,iy,opacity,a,sr,sg,sb,dr,dg,db;
	uint8_t *ps,*pd,*blendtbl,use_blendtable;

	pd = info->dst;
	ps = info->tile + (((info->sy << 6) + info->sx) << 2);
	pitchs = (64 - info->w) << 2;
	opacity = info->opacity;

	use_blendtable = info->use_blendtable;
	blendtbl = g_tileimage_work.blendbuf;

	//

	for(iy = info->h; iy; iy--)
	{
		for(ix = info->w; ix; ix--, ps += 4, pd += 3)
		{
			a = ps[3];
			if(a == 0) continue;
			
			sr = ps[0], sg = ps[1], sb = ps[2];
			dr = pd[0], dg = pd[1], db = pd[2];

			a = a * opacity >> 7;

			//色合成 ("通常"の場合はそのまま)

			if(use_blendtable)
			{
				sr = blendtbl[(sr << 8) + dr];
				sg = blendtbl[(sg << 8) + dg];
				sb = blendtbl[(sb << 8) + db];
			}

			//アルファ合成

			if(a == 255)
			{
				pd[0] = sr;
				pd[1] = sg;
				pd[2] = sb;
			}
			else if(a)
			{
				pd[0] = (sr - dr) * a / 255 + dr;
				pd[1] = (sg - dg) * a / 255 + dg;
				pd[2] = (sb - db) * a / 255 + db;
			}
		}

		ps += pitchs;
		pd += info->pitch_dst;
	}
}

/** Y1行の RGBA データをセット
 *
 * 画像読み込み時。タイル配列は width x height のサイズになっている。 */

mBool __TileImage_RGBA_setRow_fromRGBA(TileImage *p,
	int y,uint8_t *buf,int width,int height,mBool bottomup)
{
	uint8_t **pptile,**pp;
	int i,rx,pos,tiley;

	pptile = p->ppbuf + (y >> 6) * p->tilew;
	tiley = y & 63;

	//y がタイルの先頭行なら、横一列のタイルを確保

	if((!bottomup && tiley == 0) || (bottomup && (y == height - 1 || tiley == 63)))
	{
		for(i = p->tilew, pp = pptile; i; i--, pp++)
		{
			*pp = TileImage_allocTile(p, TRUE);
			if(!(*pp)) return FALSE;
		}
	}

	//タイル単位でコピー

	pos = tiley << (6 + 2);

	for(i = p->tilew, rx = width, pp = pptile; i; i--, pp++, rx -= 64)
	{
		memcpy(*pp + pos, buf, (rx < 64)? (rx << 2): 64 * 4);

		buf += 64 * 4;
	}

	//y がタイルの終端なら、透明タイルを解放

	if((!bottomup && (tiley == 63 || y == height - 1)) || (bottomup && tiley == 0))
	{
		for(i = p->tilew, pp = pptile; i; i--, pp++)
		{
			if(__TileImage_RGBA_isTransparentTile(*pp))
				TileImage_freeTile(pp);
		}
	}

	return TRUE;
}

/** タイルイメージを左右反転 */

void __TileImage_RGBA_tileReverseHorz(uint8_t *tile)
{
	uint32_t *p1,*p2,c;
	int ix,iy;

	p1 = (uint32_t *)tile;
	p2 = p1 + 63;

	for(iy = 64; iy; iy--)
	{
		for(ix = 32; ix; ix--, p1++, p2--)
		{
			c = *p1,
			*p1 = *p2;
			*p2 = c;
		}

		p1 += 64 - 32;
		p2 += 64 + 32;
	}
}

/** タイルイメージを上下反転 */

void __TileImage_RGBA_tileReverseVert(uint8_t *tile)
{
	uint32_t *p1,*p2,c;
	int ix,iy;

	p1 = (uint32_t *)tile;
	p2 = p1 + 63 * 64;

	for(iy = 32; iy; iy--, p2 -= 64 * 2)
	{
		for(ix = 64; ix; ix--, p1++, p2++)
		{
			c = *p1,
			*p1 = *p2;
			*p2 = c;
		}
	}
}


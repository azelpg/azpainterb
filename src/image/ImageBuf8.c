/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * ImageBuf8
 *
 * 8bit イメージ
 *****************************************/
/*
 * - 確保バッファサイズは 4byte 単位。
 * - テクスチャ画像で使用される。
 */


#include <string.h>

#include "mDef.h"
#include "mPixbuf.h"

#include "ImageBuf8.h"
#include "ImageBuf24.h"


#define _TOGRAY(r,g,b)  ((r * 77 + g * 150 + b * 29) >> 8)



/** 解放 */

void ImageBuf8_free(ImageBuf8 *p)
{
	if(p)
	{
		mFree(p->buf);
		mFree(p);
	}
}

/** 作成 */

ImageBuf8 *ImageBuf8_new(int w,int h)
{
	ImageBuf8 *p;
	uint32_t size;

	p = (ImageBuf8 *)mMalloc(sizeof(ImageBuf8), TRUE);
	if(!p) return NULL;

	size = (w * h + 3) & (~3);

	p->buf = (uint8_t *)mMalloc(size, FALSE);
	if(!p->buf)
	{
		mFree(p);
		return NULL;
	}

	p->w = w;
	p->h = h;
	p->bufsize = size;

	return p;
}

/** テクスチャ画像として色を取得 */

uint8_t ImageBuf8_getPixel_forTexture(ImageBuf8 *p,int x,int y)
{
	if(x < 0)
		x += (-x / p->w + 1) * p->w;

	if(y < 0)
		y += (-y / p->h + 1) * p->h;

	return *(p->buf + (y % p->h) * p->w + (x % p->w));
}

/** テクスチャ用、色ビット数とパレットを取得
 *
 * @param palbuf 最大4色分のパレットデータが入る */

int ImageBuf8_getColorBits(ImageBuf8 *p,uint8_t *palbuf)
{
	uint8_t flag[32],*ps,f;
	int i,pos,cnt = 0;

	//色数計算

	memset(flag, 0, 32);

	ps = p->buf;

	for(i = p->w * p->h; i; i--, ps++)
	{
		pos = *ps >> 3;
		f = 1 << (*ps & 7);

		if(!(flag[pos] & f))
		{
			flag[pos] |= f;

			if(cnt < 4)
				palbuf[cnt] = *ps;
			
			cnt++;
		}
	}

	//ビット数

	if((cnt == 1 && (flag[0] == 1 || flag[31] == 0x80))
		|| (cnt == 2 && flag[0] == 1 && flag[31] == 0x80))
		//白黒
		return 1;
	else if(cnt <= 4)
	{
		//パレット付き4色

		if(cnt != 4) memset(palbuf + cnt, 0, 4 - cnt);
		return 2;
	}
	else
		return 8;

	return cnt;
}

/** ImageBuf24 からテクスチャ画像として作成 */

ImageBuf8 *ImageBuf8_createFromImageBuf24(ImageBuf24 *src)
{
	ImageBuf8 *img;
	uint8_t *pd,*ps;
	uint32_t i;

	img = ImageBuf8_new(src->w, src->h);
	if(!img) return NULL;

	//変換

	pd = img->buf;
	ps = src->buf;

	for(i = src->w * src->h; i; i--, ps += 3)
		*(pd++) = 255 - _TOGRAY(ps[0], ps[1], ps[2]);

	return img;
}

/** mPixbuf にテクスチャプレビュー描画 */

void ImageBuf8_drawTexturePreview(ImageBuf8 *p,mPixbuf *pixbuf,int x,int y,int w,int h)
{
	mBox box;
	int ix,iy,sw,sh,pitchd,bpp,fx,fy,fxleft;
	uint8_t *pd,*psY;

	if(!mPixbufGetClipBox_d(pixbuf, &box, x, y, w, h))
		return;

	sw = p->w, sh = p->h;

	pd = mPixbufGetBufPtFast(pixbuf, box.x, box.y);
	bpp = pixbuf->bpp;
	pitchd = pixbuf->pitch_dir - box.w * bpp;

	fxleft = (box.x - x) % sw;
	fy = (box.y - y) % sh;

	psY = p->buf + fy * sw;

	//

	for(iy = box.h; iy; iy--)
	{
		fx = fxleft;

		for(ix = box.w; ix; ix--, pd += bpp)
		{
			(pixbuf->setbuf)(pd, mGraytoPix(255 - *(psY + fx)));

			fx++;
			if(fx == sw) fx = 0;
		}

		fy++;
		if(fy == sh)
			fy = 0, psY = p->buf;
		else
			psY += sw;

		pd += pitchd;
	}
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * TileImage
 *
 * ブラシ描画
 *****************************************/
/*
 * [!] 描画前に TileImageDrawInfo::pixdraw に描画色を指定しておく。
 */

#include <math.h>

#include "mDef.h"

#include "TileImage.h"
#include "TileImageDrawInfo.h"

#include "BrushDrawParam.h"


//-----------------

#define _CURVE_NUM  10

typedef struct
{
	double x,y,pressure;
}DrawPoint;

typedef union
{
	struct { double r,g,b,a; };	//a = 0.0 - 1.0
	double ar[4];
}PixelRGBA_double;

//-----------------
/* 作業用データ */

typedef struct
{
	double t,lastx,lasty,lastpress;

	DrawPoint curve_pt[4];
	double curve_param[_CURVE_NUM][4];

	PixelRGBA_double pixwater,	//水彩色
		pixwater_draw;			//水彩、開始時の描画色
}DrawBrushWork;

static DrawBrushWork g_work;

//-----------------

#define _BRUSHPARAM  (g_tileimage_dinfo.brushparam)

//-----------------

static void _drawbrush_point(TileImage *p,double x,double y,double radius,double opacity);

//-----------------



//==================================
// sub
//==================================


/** 水彩時、指定位置から半径内の色の平均色を double で取得 */

static void _water_get_average_color(TileImage *p,
	double x,double y,double radius,PixelRGBA_double *pixdst)
{
	int x1,y1,x2,y2,cnt,ix,iy;
	double rd,r,g,b,a,dx,dy,dd;
	PixelRGBA pix;

	x1 = floor(x - radius);
	y1 = floor(y - radius);
	x2 = floor(x + radius);
	y2 = floor(y + radius);

	rd = radius * radius;

	r = g = b = a = 0;
	cnt = 0;

	for(iy = y1; iy <= y2; iy++)
	{
		dy = iy - y;
		dy *= dy;

		for(ix = x1; ix <= x2; ix++)
		{
			dx = ix - x;

			if(dx * dx + dy < rd)
			{
				TileImage_getPixel_clip(p, ix, iy, &pix);

				dd = pix.a / 255.0;

				r += pix.r * dd;
				g += pix.g * dd;
				b += pix.b * dd;
				a += dd;

				cnt++;
			}
		}
	}

	//平均

	if(a == 0 || cnt == 0)
		pixdst->r = pixdst->g = pixdst->b = pixdst->a = 0;
	else
	{
		dd = 1.0 / a;

		pixdst->r = r * dd;
		pixdst->g = g * dd;
		pixdst->b = b * dd;
		pixdst->a = a / cnt;
	}
}

/** 水彩の混色 */
/*
 * やまかわ氏の "gimp-painter-" (MixBrush 改造版) のソースを参考にしています。
 * http://sourceforge.jp/projects/gimp-painter/releases/?package_id=6799
 */

static void _water_mix_color(PixelRGBA_double *pixdst,
	PixelRGBA_double *pix1,double op1,
    PixelRGBA_double *pix2,double op2,mBool alpha)
{
    double a1,a2;

    a1 = pix1->a * op1;
    a2 = pix2->a * (1.0 - a1) * op2;

    if(a1 == 0)
    {
        *pixdst = *pix2;
        pixdst->a = a2;
    }
    else if(a2 == 0)
    {
        *pixdst = *pix1;
        pixdst->a = a1;
    }
    else
    {
        pixdst->a = a1 + a2;

        a1 = a1 / (a1 + a2);
        a2 = 1.0 - a1;

        pixdst->r = pix1->r * a1 + pix2->r * a2;
        pixdst->g = pix1->g * a1 + pix2->g * a2;
        pixdst->b = pix1->b * a1 + pix2->b * a2;
    }

    if(alpha)
    {
        op1 = pow(op1, 1.35);
        op2 = pow(op2, 1.15);
        a1  = op1 + op2;

        if(a1 == 0)
            pixdst->a = 0;
        else
        {
            a2 = op1 / a1;
            pixdst->a = (pix1->a * a2 + pix2->a * (1.0 - a2)) * (op1 + (1.0 - op1) * op2);
        }
    }
}

/** 水彩、描画色の計算 */

static void _water_calc_drawcol(TileImage *p,double x,double y,double radius,
	double *popacity,PixelRGBA *pixdst)
{
	BrushDrawParam *param = _BRUSHPARAM;
	PixelRGBA_double pix1,pix2;
	int i,n;
	double opa;

	//描画色 + 前回の色 => pix1

	_water_mix_color(&pix1,
		&g_work.pixwater_draw, param->water[0],
		&g_work.pixwater, 1, FALSE);

	//pix1 + 描画先の色 => pixwater

	_water_get_average_color(p, x, y, radius, &pix2);
	
	_water_mix_color(&g_work.pixwater,
		&pix1, param->water[2],
		&pix2, param->water[1], TRUE);

	//結果の描画色

	for(i = 0; i < 3; i++)
	{
		n = round(g_work.pixwater.ar[i]);

		if(n < 0) n = 0;
		else if(n > 255) n = 255;

		pixdst->ar[i] = n;
	}

	//濃度 (*popacity は 0-255)
	/* pixwater のアルファ値で制限 */

	opa = g_work.pixwater.a * 255;

	if(opa < *popacity)
		*popacity = opa;
}


//==================================
//
//==================================


/** 曲線補間用パラメータ初期化 */

void __TileImage_initCurve()
{
	int i;
	double t,tt,t1,t2,t3;

	tt = 1.0 / 6.0;

	for(i = 0, t = 1.0 / _CURVE_NUM; i < _CURVE_NUM; i++, t += 1.0 / _CURVE_NUM)
	{
		t1 = 4.0 - (t + 3.0);
		t2 = t + 2.0;
		t3 = t + 1.0;

		g_work.curve_param[i][0] = tt * t1 * t1 * t1;
		g_work.curve_param[i][1] = tt * ( 3 * t2 * t2 * t2 - 24 * t2 * t2 + 60 * t2 - 44);
		g_work.curve_param[i][2] = tt * (-3 * t3 * t3 * t3 + 12 * t3 * t3 - 12 * t3 + 4);
		g_work.curve_param[i][3] = tt * t * t * t;
	}
}

/** ブラシ自由線描画の開始 */

void TileImage_beginDrawBrush_free(TileImage *p,double x,double y,double pressure)
{
	g_work.lastx = x;
	g_work.lasty = y;
	g_work.lastpress = pressure;
	g_work.t = 0;

	//曲線補間

	if(_BRUSHPARAM->flags & BRUSHDP_F_CURVE)
	{
		g_work.curve_pt[0].x = x;
		g_work.curve_pt[0].y = y;
		g_work.curve_pt[0].pressure = pressure;
	
		g_work.curve_pt[1] = g_work.curve_pt[0];
		g_work.curve_pt[2] = g_work.curve_pt[0];
	}

	//水彩

	if(_BRUSHPARAM->flags & BRUSHDP_F_WATER)
	{
		PixelRGBA_double *pix = &g_work.pixwater_draw;

		//開始時の描画色

		pix->r = g_tileimage_dinfo.pixdraw.r;
		pix->g = g_tileimage_dinfo.pixdraw.g;
		pix->b = g_tileimage_dinfo.pixdraw.b;
		pix->a = 1;

		//描画先の平均色

		_water_get_average_color(p, x, y, _BRUSHPARAM->radius, &g_work.pixwater);
	}
}


//================================
// 線の描画
//================================


/** 曲線補間の線描画 */

static void _drawbrush_curveline(TileImage *p)
{
	int i;
	double lastx,lasty,x,y,press_st,press_ed;
	double t1,t2,t3,t4;
	DrawPoint *pt = g_work.curve_pt;

	lastx = g_work.lastx;
	lasty = g_work.lasty;
	press_st = g_work.lastpress;

	//

	for(i = 0; i < _CURVE_NUM; i++)
	{
		t1 = g_work.curve_param[i][0];
		t2 = g_work.curve_param[i][1];
		t3 = g_work.curve_param[i][2];
		t4 = g_work.curve_param[i][3];

		//

		x = pt[0].x * t1 + pt[1].x * t2 + pt[2].x * t3 + pt[3].x * t4;
		y = pt[0].y * t1 + pt[1].y * t2 + pt[2].y * t3 + pt[3].y * t4;

		press_ed = pt[0].pressure * t1 + pt[1].pressure * t2 + pt[2].pressure * t3 + pt[3].pressure * t4;

		g_work.t = TileImage_drawBrushLine(p,
			lastx, lasty, x, y, press_st, press_ed, g_work.t);

		//

		lastx = x;
		lasty = y;
		press_st = press_ed;
	}

	//

	g_work.lastx = x;
	g_work.lasty = y;
	g_work.lastpress = press_ed;
}

/** ブラシで自由線描画 */

void TileImage_drawBrushFree(TileImage *p,double x,double y,double pressure)
{
	if(_BRUSHPARAM->flags & BRUSHDP_F_CURVE)
	{
		//曲線補間

		g_work.curve_pt[3].x = x;
		g_work.curve_pt[3].y = y;
		g_work.curve_pt[3].pressure = pressure;

		_drawbrush_curveline(p);

		for(int i = 0; i < 3; i++)
			g_work.curve_pt[i] = g_work.curve_pt[i + 1];
	}
	else
	{
		//線形補間

		g_work.t = TileImage_drawBrushLine(p,
			g_work.lastx, g_work.lasty, x, y,
			g_work.lastpress, pressure, g_work.t);

		g_work.lastx = x;
		g_work.lasty = y;
		g_work.lastpress = pressure;
	}
}


//=====================================
// 線を引く
//=====================================


/** ブラシで線を引く (線形補間)
 *
 * @return 次の t 値が返る */

double TileImage_drawBrushLine(TileImage *p,
	double x1,double y1,double x2,double y2,
	double press_st,double press_ed,double t_start)
{
	BrushDrawParam *param = _BRUSHPARAM;
	double len,t,dx,dy,ttm,dtmp;
	double press_len,radius_len,opa_len,press,radius,opacity;

	//線の長さ

	dx = x2 - x1;
	dy = y2 - y1;

	len = sqrt(dx * dx + dy * dy);
	if(len == 0) return t_start;

	//間隔を長さ 1.0 に対する値に

	ttm = param->interval / len;

	//各値の幅

	radius_len = 1.0 - param->min_size;
	opa_len    = 1.0 - param->min_opacity;
	press_len  = press_ed - press_st;

	//------------------

	t = t_start / len;

	while(t < 1.0)
	{
		//筆圧

		press = press_len * t + press_st;

		if(param->flags & BRUSHDP_F_PRESSURE_GAMMA)
			press = pow(press, param->pressure_gamma);

		//半径、濃度

		radius  = (press * radius_len + param->min_size) * param->radius;
		opacity = (press * opa_len + param->min_opacity) * param->opacity;

		//点描画

		_drawbrush_point(p, x1 + dx * t, y1 + dy * t, radius, opacity);

		//次へ

		dtmp = radius * ttm;
		if(dtmp < 0.0001) dtmp = 0.0001;

		t += dtmp;
	}

	//次回の開始 t 値 (px に変換)

	return len * (t - 1.0);
}

/** 入り抜き指定ありの直線 */

void TileImage_drawBrushLine_headtail(TileImage *p,
	double x1,double y1,double x2,double y2,uint32_t headtail)
{
	int head,tail;
	double t,dx,dy,d,xx,yy,xx2,yy2;

	head = headtail >> 16;
	tail = headtail & 0xffff;

	if(tail > 1000 - head)
		tail = 1000 - head;

	//両方 0 なら入り抜きなしの通常線

	if(head == 0 && tail == 0)
	{
		TileImage_drawBrushLine(p, x1, y1, x2, y2, 1, 1, 0);
		return;
	}

	//----------

	dx = x2 - x1;
	dy = y2 - y1;

	xx = x1, yy = y1;
	t = 0;

	//入り

	if(head)
	{
		d  = head * 0.001;
		xx = x1 + dx * d;
		yy = y1 + dy * d;

		t = TileImage_drawBrushLine(p, x1, y1, xx, yy, 0, 1, t);
	}

	//中間部分 (筆圧が最大の部分)

	if(tail != 1000 - head)
	{
		d   = (1000 - tail) * 0.001;
		xx2 = x1 + dx * d;
		yy2 = y1 + dy * d;

		t = TileImage_drawBrushLine(p, xx, yy, xx2, yy2, 1, 1, t);

		xx = xx2, yy = yy2;
	}

	//抜き

	if(tail)
		TileImage_drawBrushLine(p, xx, yy, x2, y2, 1, 0, t);
}

/** 四角形描画 */

void TileImage_drawBrushBox(TileImage *p,mDoublePoint *pt)
{
	TileImage_drawBrushLine(p, pt[0].x, pt[0].y, pt[1].x, pt[1].y, 1, 1, 0);
	TileImage_drawBrushLine(p, pt[1].x, pt[1].y, pt[2].x, pt[2].y, 1, 1, 0);
	TileImage_drawBrushLine(p, pt[2].x, pt[2].y, pt[3].x, pt[3].y, 1, 1, 0);
	TileImage_drawBrushLine(p, pt[3].x, pt[3].y, pt[0].x, pt[0].y, 1, 1, 0);
}


//=====================================
// 点の描画
//=====================================

#define FIXF_BIT 10
#define FIXF_VAL (1<<FIXF_BIT)


/** 円形 (硬い) */

static void _drawbrush_point_hard(TileImage *p,
	double drawx,double drawy,double radius,double opacity,PixelRGBA *pix)
{
	int subnum,x1,y1,x2,y2,ix,iy,px,py,antialias;
	int xtbl[11],ytbl[11],subpos[11],rr,n,fx,fy,fx_left;
	int64_t fpos;
	double ddiv;
	PixelRGBA pixdraw;
	void (*drawpix)(TileImage *,int,int,PixelRGBA *) = g_tileimage_dinfo.funcDrawPixel;

	//半径の大きさによりサブピクセル数調整

	if(radius < 3) subnum = 11;
	else if(radius < 15) subnum = 5;
	else subnum = 3;

	//px 範囲

	x1 = (int)floor(drawx - radius);
	y1 = (int)floor(drawy - radius);
	x2 = (int)floor(drawx + radius);
	y2 = (int)floor(drawy + radius);

	//座標は固定少数点数にする

	fx_left = floor((x1 - drawx) * FIXF_VAL);
	fy = floor((y1 - drawy) * FIXF_VAL);
	rr = floor(radius * radius * FIXF_VAL);
	ddiv = 1.0 / (subnum * subnum);

	pixdraw = *pix;
	antialias = (_BRUSHPARAM->flags & BRUSHDP_F_ANTIALIAS);

	for(ix = 0; ix < subnum; ix++)
		subpos[ix] = (ix << FIXF_BIT) / subnum;

	//------------------

	for(py = y1; py <= y2; py++, fy += FIXF_VAL)
	{
		//Y テーブル

		for(iy = 0; iy < subnum; iy++)
		{
			fpos = fy + subpos[iy];
			ytbl[iy] = fpos * fpos >> FIXF_BIT;
		}

		//

		for(px = x1, fx = fx_left; px <= x2; px++, fx += FIXF_VAL)
		{
			//X テーブル

			for(ix = 0; ix < subnum; ix++)
			{
				fpos = fx + subpos[ix];
				xtbl[ix] = fpos * fpos >> FIXF_BIT;
			}

			//オーバーサンプリングで円の内外判定

			n = 0;

			for(iy = 0; iy < subnum; iy++)
			{
				for(ix = 0; ix < subnum; ix++)
				{
					if(xtbl[ix] + ytbl[iy] < rr) n++;
				}
			}

			//1px 点を打つ

			if(n)
			{
				n = (int)(opacity * (n * ddiv) + 0.5);
				if(n)
				{
					if(antialias)
						pixdraw.a = n;
					else
						pixdraw.a = (int)(opacity + 0.5);

					(drawpix)(p, px, py, &pixdraw);
				}
			}
		}
	}
}

/** 円形 (柔らかい1) */

static void _drawbrush_point_soft1(TileImage *p,
	double drawx,double drawy,double radius,double opacity,PixelRGBA *pix)
{
	int subnum,x1,y1,x2,y2,ix,iy,px,py,c,antialias;
	double dsubadd,dsumdiv,dtmp,dsum,rrdiv,ytbl[11],xtbl[11];
	PixelRGBA pixdraw;
	void (*drawpix)(TileImage *,int,int,PixelRGBA *) = g_tileimage_dinfo.funcDrawPixel;

	//半径の大きさによりサブピクセル数調整

	if(radius < 3) subnum = 11;
	else if(radius < 15) subnum = 5;
	else if(radius < 40) subnum = 3;
	else subnum = 1;

	//px 範囲

	x1 = (int)floor(drawx - radius);
	y1 = (int)floor(drawy - radius);
	x2 = (int)floor(drawx + radius);
	y2 = (int)floor(drawy + radius);

	//

	rrdiv = 1.0 / (radius * radius);
	dsubadd = 1.0 / subnum;
	dsumdiv = 1.0 / (subnum * subnum);

	pixdraw = *pix;
	antialias = (_BRUSHPARAM->flags & BRUSHDP_F_ANTIALIAS);

	//------------------

	for(py = y1; py <= y2; py++)
	{
		//Y テーブル

		for(iy = 0, dtmp = py - drawy; iy < subnum; iy++, dtmp += dsubadd)
			ytbl[iy] = dtmp * dtmp;

		//

		for(px = x1; px <= x2; px++)
		{
			//X テーブル

			for(ix = 0, dtmp = px - drawx; ix < subnum; ix++, dtmp += dsubadd)
				xtbl[ix] = dtmp * dtmp;

			//オーバーサンプリング

			dsum = 0;

			for(iy = 0; iy < subnum; iy++)
			{
				for(ix = 0; ix < subnum; ix++)
				{
					dtmp = (xtbl[ix] + ytbl[iy]) * rrdiv;

					if(dtmp < 1)
					{
						//半径 0.3 以内は最大濃度、それ以上は 0.0-1.0 に置換
					
						if(dtmp < 0.3)
							dtmp = 0;
						else
							dtmp = (dtmp - 0.3) * (1.0 / 0.7);
						
						dsum += 1.0 - dtmp;
					}
				}
			}

			//1px 点を打つ

			if(dsum != 0)
			{
				c = (int)(opacity * (dsum * dsumdiv) + 0.5);
				if(c)
				{
					if(antialias)
						pixdraw.a = c;
					else
						pixdraw.a = (int)(opacity + 0.5);

					(drawpix)(p, px, py, &pixdraw);
				}
			}
		}
	}
}

/** 円形 (柔らかい2) */

static void _drawbrush_point_soft2(TileImage *p,
	double drawx,double drawy,double radius,double opacity,PixelRGBA *pix)
{
	int subnum,x1,y1,x2,y2,ix,iy,px,py,c,antialias;
	double dsubadd,dsumdiv,dtmp,dsum,rrdiv,ytbl[11],xtbl[11];
	PixelRGBA pixdraw;
	void (*drawpix)(TileImage *,int,int,PixelRGBA *) = g_tileimage_dinfo.funcDrawPixel;

	//半径の大きさによりサブピクセル数調整

	if(radius < 3) subnum = 11;
	else if(radius < 15) subnum = 5;
	else if(radius < 40) subnum = 3;
	else subnum = 1;

	//px 範囲

	x1 = (int)floor(drawx - radius);
	y1 = (int)floor(drawy - radius);
	x2 = (int)floor(drawx + radius);
	y2 = (int)floor(drawy + radius);

	//

	rrdiv = 1.0 / (radius * radius);
	dsubadd = 1.0 / subnum;
	dsumdiv = 1.0 / (subnum * subnum);

	pixdraw = *pix;
	antialias = (_BRUSHPARAM->flags & BRUSHDP_F_ANTIALIAS);

	//------------------

	for(py = y1; py <= y2; py++)
	{
		//Y テーブル

		for(iy = 0, dtmp = py - drawy; iy < subnum; iy++, dtmp += dsubadd)
			ytbl[iy] = dtmp * dtmp;

		//

		for(px = x1; px <= x2; px++)
		{
			//X テーブル

			for(ix = 0, dtmp = px - drawx; ix < subnum; ix++, dtmp += dsubadd)
				xtbl[ix] = dtmp * dtmp;

			//オーバーサンプリング

			dsum = 0;

			for(iy = 0; iy < subnum; iy++)
			{
				for(ix = 0; ix < subnum; ix++)
				{
					//半径をそのまま濃度に
					
					dtmp = (xtbl[ix] + ytbl[iy]) * rrdiv;

					if(dtmp < 1)
						dsum += 1.0 - dtmp;
				}
			}

			//1px 点を打つ

			if(dsum != 0)
			{
				c = (int)(opacity * (dsum * dsumdiv) + 0.5);
				if(c)
				{
					if(antialias)
						pixdraw.a = c;
					else
						pixdraw.a = (int)(opacity + 0.5);

					(drawpix)(p, px, py, &pixdraw);
				}
			}
		}
	}
}

/** 円形 (柔らかい3) */

static void _drawbrush_point_soft3(TileImage *p,
	double drawx,double drawy,double radius,double opacity,PixelRGBA *pix)
{
	int subnum,x1,y1,x2,y2,ix,iy,px,py,c,antialias;
	double dsubadd,dsumdiv,dtmp,dsum,rrdiv,ytbl[11],xtbl[11];
	PixelRGBA pixdraw;
	void (*drawpix)(TileImage *,int,int,PixelRGBA *) = g_tileimage_dinfo.funcDrawPixel;

	//半径の大きさによりサブピクセル数調整

	if(radius < 3) subnum = 11;
	else if(radius < 15) subnum = 5;
	else if(radius < 40) subnum = 3;
	else subnum = 1;

	//px 範囲

	x1 = (int)floor(drawx - radius);
	y1 = (int)floor(drawy - radius);
	x2 = (int)floor(drawx + radius);
	y2 = (int)floor(drawy + radius);

	//

	rrdiv = 1.0 / (radius * radius);
	dsubadd = 1.0 / subnum;
	dsumdiv = 1.0 / (subnum * subnum);

	pixdraw = *pix;
	antialias = (_BRUSHPARAM->flags & BRUSHDP_F_ANTIALIAS);

	//------------------

	for(py = y1; py <= y2; py++)
	{
		//Y テーブル

		for(iy = 0, dtmp = py - drawy; iy < subnum; iy++, dtmp += dsubadd)
			ytbl[iy] = dtmp * dtmp;

		//

		for(px = x1; px <= x2; px++)
		{
			//X テーブル

			for(ix = 0, dtmp = px - drawx; ix < subnum; ix++, dtmp += dsubadd)
				xtbl[ix] = dtmp * dtmp;

			//オーバーサンプリング

			dsum = 0;

			for(iy = 0; iy < subnum; iy++)
			{
				for(ix = 0; ix < subnum; ix++)
				{
					dtmp = (xtbl[ix] + ytbl[iy]) * rrdiv;

					if(dtmp < 1)
					{
						dtmp = 1.0 - dtmp;
					
						dtmp = dtmp - dtmp * 0.25;
						if(dtmp < 0) dtmp = 0;
						
						dsum += dtmp;
					}
				}
			}

			//1px 点を打つ

			if(dsum != 0)
			{
				c = (int)(opacity * (dsum * dsumdiv) + 0.5);
				if(c)
				{
					if(antialias)
						pixdraw.a = c;
					else
						pixdraw.a = (int)(opacity + 0.5);

					(drawpix)(p, px, py, &pixdraw);
				}
			}
		}
	}
}

/** ブラシ、点の描画 */

void _drawbrush_point(TileImage *p,
	double x,double y,double radius,double opacity)
{
	PixelRGBA pix;
	void (*drawpoint[4])(TileImage *p,double,double,double,double,PixelRGBA *) =
	{
		_drawbrush_point_hard, _drawbrush_point_soft1,
		_drawbrush_point_soft2, _drawbrush_point_soft3
	};

	if(opacity != 0 && radius > 0.05)
	{
		pix = g_tileimage_dinfo.pixdraw;
		
		//水彩、描画色の計算

		if(_BRUSHPARAM->flags & BRUSHDP_F_WATER)
		{
			_water_calc_drawcol(p, x, y, radius, &opacity, &pix);

			if(opacity == 0) return;
		}

		//
	
		(drawpoint[_BRUSHPARAM->shape])(p, x, y, radius, opacity, &pix);
	}
}

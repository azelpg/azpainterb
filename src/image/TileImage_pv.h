/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/********************************
 * TileImage 内部処理
 ********************************/

#ifndef TILEIMAGE_PV_H
#define TILEIMAGE_PV_H

typedef struct _TileImage  TileImage;
typedef union  _PixelRGBA  PixelRGBA;


/* 作業用データ */

typedef struct
{
	uint8_t *blendbuf;		//色合成テーブル (256*256)
	int blend_curmode;		//現在バッファにセットされている合成モード

	PixelRGBA *finger_buf;	//指先用バッファ
	int finger_cursize;		//指先用バッファの現在サイズ

	int dotpen_subx,		//ドット形状描画時のサブ位置
		dotpen_suby;
}TileImageWorkData;

extern TileImageWorkData g_tileimage_work;

/* 合成用情報 */

typedef struct
{
	uint8_t *tile,*dst;
	int sx,sy,dx,dy,w,h,pitch_dst,opacity;
	uint8_t use_blendtable;
}TileImageBlendInfo;


/* TileImage_pv.c */

TileImage *__TileImage_create(int type,int tilew,int tileh);

mBool __TileImage_allocTileBuf(TileImage *p);
uint8_t **__TileImage_allocTileBuf_new(int w,int h,mBool clear);

mBool __TileImage_resizeTileBuf(TileImage *p,int movx,int movy,int neww,int newh);
mBool __TileImage_resizeTileBuf_clone(TileImage *p,TileImage *src);

void __TileImage_setBlendInfo(TileImageBlendInfo *info,int px,int py,mRect *rcclip);

/* TileImage_pixel.c */

uint8_t *__TileImage_getPixelBuf_new(TileImage *p,int x,int y);

/* TileImage_blendcolor.c */

mBool __TileImage_setBlendColorTable(int mode);
void __TileImage_getBlendColor(int mode,PixelRGBA *src,PixelRGBA *dst);

#endif

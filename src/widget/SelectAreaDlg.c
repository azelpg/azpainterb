/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * 選択範囲、座標指定ダイアログ
 *****************************************/

#include <stdlib.h>

#include "mDef.h"
#include "mWidget.h"
#include "mDialog.h"
#include "mContainer.h"
#include "mWindow.h"
#include "mLabel.h"
#include "mLineEdit.h"
#include "mTrans.h"
#include "mEvent.h"
#include "mRectBox.h"

#include "defDraw.h"
#include "defMacros.h"

#include "draw_select.h"

#include "trgroup.h"


//-----------------------

typedef struct
{
	mWidget wg;
	mContainerData ct;
	mWindowData win;
	mDialogData dlg;

	int val[6];
	mLineEdit *edit[6];
}_select_dlg;

//-----------------------

enum
{
	WID_EDIT_SX = 100,
	WID_EDIT_SY,
	WID_EDIT_EX,
	WID_EDIT_EY,
	WID_EDIT_W,
	WID_EDIT_H
};

//-----------------------


/** エディット値の変更時 */

static void _change_edit(_select_dlg *p,int no)
{
	p->val[no] = mLineEditGetNum(p->edit[no]);

	if(no < 4)
	{
		//位置から幅、高さセット
		
		p->val[4] = abs(p->val[2] - p->val[0]) + 1;
		p->val[5] = abs(p->val[3] - p->val[1]) + 1;

		mLineEditSetNum(p->edit[4], p->val[4]);
		mLineEditSetNum(p->edit[5], p->val[5]);
	}
	else
	{
		//幅、高さから位置 (x2,y2) をセット

		p->val[2] = (p->val[4])? p->val[0] + p->val[4] - 1: p->val[0];
		p->val[3] = (p->val[5])? p->val[1] + p->val[5] - 1: p->val[1];

		mLineEditSetNum(p->edit[2], p->val[2]);
		mLineEditSetNum(p->edit[3], p->val[3]);
	}
}

/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		if(ev->notify.id >= WID_EDIT_SX && ev->notify.id <= WID_EDIT_H
			&& ev->notify.type == MLINEEDIT_N_CHANGE)
		{
			_change_edit((_select_dlg *)wg, ev->notify.id - WID_EDIT_SX);
		}
	}
	
	return mDialogEventHandle_okcancel(wg, ev);
}

/** 作成 */

static _select_dlg *_dlg_create(mWindow *owner)
{
	_select_dlg *p;
	mWidget *ct;
	int i;
	mBox box;
	mLineEdit *edit;

	//作成

	p = (_select_dlg *)mDialogNew(sizeof(_select_dlg), owner, MWINDOW_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;
	p->ct.sepW = 18;

	//

	mContainerSetPadding_one(M_CONTAINER(p), 8);

	M_TR_G(TRGROUP_DLG_SELECT_AREA);

	mWindowSetTitle(M_WINDOW(p), M_TR_T(0));

	//初期値

	if(APP_DRAW->sel.boxsel.w)
		box = APP_DRAW->sel.boxsel;
	else
		box.x = box.y = box.w = box.h = 0;

	p->val[0] = box.x;
	p->val[1] = box.y;
	p->val[2] = (box.w)? box.x + box.w - 1: 0;
	p->val[3] = (box.h)? box.y + box.h - 1: 0;
	p->val[4] = box.w;
	p->val[5] = box.h;

	//------ ウィジェット

	ct = mContainerCreateGrid(M_WIDGET(p), 4, 6, 7, 0);

	for(i = 0; i < 6; i++)
	{
		mLabelCreate(ct, 0, MLF_MIDDLE | MLF_RIGHT, 0, M_TR_T(i + 1));
	
		edit = p->edit[i] = mLineEditCreate(ct, WID_EDIT_SX + i,
			MLINEEDIT_S_SPIN | MLINEEDIT_S_NOTIFY_CHANGE, 0, 0);

		mLineEditSetWidthByLen(edit, 7);
		mLineEditSetNumStatus(edit, 0, IMAGE_SIZE_MAX, 0);
		mLineEditSetNum(edit, p->val[i]);
	}

	//OK/cancel

	mContainerCreateOkCancelButton(M_WIDGET(p));

	return p;
}

/** 範囲決定 */

static void _set_select(_select_dlg *p)
{
	mRect rc;
	mBox box;

	rc.x1 = p->val[0], rc.y1 = p->val[1];
	rc.x2 = p->val[2], rc.y2 = p->val[3];

	mRectSwap(&rc);

	//現在範囲を解除

	drawSelect_setSelectBox(APP_DRAW, NULL);

	//1x1 の場合は範囲なし

	if(mRectClipBox_d(&rc, 0, 0, APP_DRAW->imgw, APP_DRAW->imgh)
		&& !(rc.x1 == rc.x2 && rc.y1 == rc.y2))
	{
		mBoxSetByRect(&box, &rc);

		drawSelect_setSelectBox(APP_DRAW, &box);
	}
}

/** ダイアログ実行 */

mBool SelectAreaDlg_run(mWindow *owner)
{
	_select_dlg *p;
	mBool ret;

	p = _dlg_create(owner);
	if(!p) return FALSE;

	mWindowMoveResizeShow_hintSize(M_WINDOW(p));

	ret = mDialogRun(M_DIALOG(p), FALSE);

	if(ret)
		_set_select(p);

	mWidgetDestroy(M_WIDGET(p));

	return ret;
}

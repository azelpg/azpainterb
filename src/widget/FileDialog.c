/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * FileDialog
 * 
 * ファイル選択ダイアログ
 *****************************************/

#include "mDef.h"
#include "mFileDialog.h"
#include "mWidget.h"
#include "mWindow.h"
#include "mContainer.h"
#include "mCheckButton.h"
#include "mTrans.h"

#include "defConfig.h"

#include "FileDialog.h"

#include "trgroup.h"


//-------------------------

typedef struct
{
	mWidget wg;
	mContainerData ct;
	mWindowData win;
	mDialogData dlg;
	mFileDialogData fdlg;

	mCheckButton *ck[2];
}_openfile;

//-------------------------



//*****************************
// 画像を開くダイアログ
//*****************************


/** OK/キャンセル時 */

static mBool _open_okcancel_handle(mFileDialog *fdlg,mBool bOK,const char *path)
{
	_openfile *p = (_openfile *)fdlg;
	uint8_t f = 0;

	f = 0;
	if(mCheckButtonIsChecked(p->ck[0])) f |= CONFIG_OPENIMAGE_F_IGNORE_ALPHA;
	if(mCheckButtonIsChecked(p->ck[1])) f |= CONFIG_OPENIMAGE_F_LEAVE_TRANS;

	APP_CONF->openimage_flags = f;

	return TRUE;
}

/** 拡張ウィジェット作成 */

static void _open_create_widget(_openfile *p)
{
	mWidget *ct;

	M_TR_G(TRGROUP_FILEDIALOG);

	ct = mContainerCreate(M_WIDGET(p), MCONTAINER_TYPE_VERT, 0, 3, 0);
	ct->margin.top = 10;

	p->ck[0] = mCheckButtonCreate(ct, 0, 0, 0, 0, M_TR_T(0),
		(APP_CONF->openimage_flags & CONFIG_OPENIMAGE_F_IGNORE_ALPHA));

	p->ck[1] = mCheckButtonCreate(ct, 0, 0, 0, 0, M_TR_T(1),
		(APP_CONF->openimage_flags & CONFIG_OPENIMAGE_F_LEAVE_TRANS));
}

/** 画像を開くダイアログ */

mBool FileDialog_openImage(mWindow *owner,const char *filter,
	const char *initdir,mStr *strfname)
{
	_openfile *p;

	//作成

	p = (_openfile *)mFileDialogNew(sizeof(_openfile),
			owner, 0, MFILEDIALOG_TYPE_OPENFILE);
	if(!p) return FALSE;

	p->fdlg.onOkCancel = _open_okcancel_handle;

	mFileDialogInit(M_FILEDIALOG(p), filter, 0, initdir, strfname);

	_open_create_widget(p);

	mFileDialogShow(M_FILEDIALOG(p));

	//実行

	return mDialogRun(M_DIALOG(p), TRUE);
}


/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * ドットペン/消しゴム用、スロット選択ウィジェット
 *****************************************/
/*
 * [通知] 選択変更時。param1:番号
 */

#include "mDef.h"
#include "mWidgetDef.h"
#include "mWidget.h"
#include "mEvent.h"
#include "mPixbuf.h"

#include "dataImagePattern.h"


//--------------------

typedef struct
{
	mWidget wg;

	int num,sel;
}DotPenSlot;

//--------------------

#define _EACHW  24
#define _HEIGHT 19

#define _COL_BKGND     0xf0f0f0
#define _COL_SEL_BKGND 0xffcccc
#define _COL_SEL_FRAME 0xff0000

//--------------------


/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_POINTER
		&& ev->pt.type == MEVENT_POINTER_TYPE_PRESS
		&& ev->pt.btt == M_BTT_LEFT)
	{
		//左ボタン押し

		DotPenSlot *p = (DotPenSlot *)wg;
		int no;

		no = ev->pt.x / _EACHW;
		if(no >= p->num) no = p->num - 1;

		if(no != p->sel)
		{
			p->sel = no;

			mWidgetUpdate(wg);

			mWidgetAppendEvent_notify(NULL, wg, 0, no, 0);
		}
	}

	return 1;
}

/** 描画 */

static void _draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	DotPenSlot *p = (DotPenSlot *)wg;
	int i,x;
	uint8_t m[2] = {1,255};

	//背景

	mPixbufFillBox(pixbuf, 0, 0, wg->w, wg->h, mRGBtoPix(_COL_BKGND));

	//枠

	mPixbufBox(pixbuf, 0, 0, wg->w, wg->h, 0);

	for(i = p->num, x = _EACHW; i > 1; i--, x += _EACHW)
		mPixbufLineV(pixbuf, x, 1, _HEIGHT - 2, 0);

	//選択

	x = p->sel * _EACHW;

	mPixbufFillBox(pixbuf, x + 1, 1,
		_EACHW - 1, _HEIGHT - 2, mRGBtoPix(_COL_SEL_BKGND));

	mPixbufBox(pixbuf, x, 0, _EACHW + 1, _HEIGHT, mRGBtoPix(_COL_SEL_FRAME));

	//番号

	x = 1 + (_EACHW - 5 - 2) / 2;

	for(i = p->num; i > 0; i--, x += _EACHW, m[0]++)
	{
		mPixbufDrawBitPatternSum(pixbuf, x, (_HEIGHT - 9) / 2,
			g_imgpat_number_5x9, IMGPAT_NUMBER_5x9_PATW, 9, 5, m, 0);
	}
}

/** 作成 */

mWidget *DockOption_dotpenslot_new(mWidget *parent,int id,int slotnum,int sel)
{
	DotPenSlot *p;

	p = (DotPenSlot *)mWidgetNew(sizeof(DotPenSlot), parent);

	p->num = slotnum;
	p->sel = sel;

	p->wg.id = id;
	p->wg.w = _EACHW * slotnum + 1;
	p->wg.h = _HEIGHT;
	p->wg.fLayout = MLF_FIX_WH;
	p->wg.fEventFilter |= MWIDGET_EVENTFILTER_POINTER;

	p->wg.event = _event_handle;
	p->wg.draw = _draw_handle;

	return (mWidget *)p;
}

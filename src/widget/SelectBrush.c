/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**********************************
 * ブラシ選択ウィジェット
 **********************************/

#include "mDef.h"
#include "mWidgetDef.h"
#include "mWidget.h"
#include "mEvent.h"
#include "mPixbuf.h"
#include "mFont.h"
#include "mSysCol.h"
#include "mMenu.h"
#include "mTrans.h"

#include "BrushItem.h"
#include "BrushList.h"

#include "trgroup.h"


//-----------------------

typedef struct _SelectBrush
{
	mWidget wg;

	int listno;
	BrushItem *item;
}SelectBrush;

//-----------------------

mBool BrushListDlg_run(mWindow *owner,int listno);

//-----------------------


//=========================
// sub
//=========================


/** 選択アイテム変更時
 *
 * p->item に新しいアイテムがセットされている */

static void _change_sel(SelectBrush *p)
{
	BrushList_setSelectItem(p->listno, p->item);

	mWidgetAppendEvent_notify(NULL, M_WIDGET(p), 0, 0, 0);
	mWidgetUpdate(M_WIDGET(p));
}

/** リスト編集 */

static void _cmd_edit_list(SelectBrush *p)
{
	if(BrushListDlg_run(p->wg.toplevel, p->listno))
	{
		//OK 時はリストデータが置き換わっているので、常に選択も変更
	
		p->item = BrushList_getSelectItem(p->listno);

		_change_sel(p);
	}
}

/** メニュー実行 */

static void _run_menu(SelectBrush *p)
{
	mMenu *menu;
	mMenuItemInfo *mi,info;
	BrushItem *item;
	mBox box;

	item = BrushList_getTopItem(p->listno);

	//メニュー

	menu = mMenuNew();

	mMenuAddText_static(menu, 0, M_TR_T2(TRGROUP_DOCKOPT_BRUSH, 200));
	if(item) mMenuAddSep(menu);

	mMemzero(&info, sizeof(mMenuItemInfo));
	info.id = 1;

	for( ; item; item = BRUSHITEM(item->i.next))
	{
		info.label = item->name;
		info.param1 = (intptr_t)item;
		
		mMenuAdd(menu, &info);
	}

	mWidgetGetRootBox(M_WIDGET(p), &box);

	mi = mMenuPopup(menu, NULL, box.x, box.y + box.h, 0);

	mMenuDestroy(menu);

	//-------

	if(!mi) return;

	if(mi->id == 0)
		//編集
		_cmd_edit_list(p);
	else
	{
		//選択変更

		p->item = BRUSHITEM(mi->param1);

		_change_sel(p);
	}
}


//=========================
//
//=========================


/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_POINTER && ev->pt.type == MEVENT_POINTER_TYPE_PRESS)
	{
		if(ev->pt.btt == M_BTT_RIGHT
			|| (ev->pt.btt == M_BTT_LEFT && (ev->pt.state & M_MODS_CTRL)))
		{
			//右ボタン or Ctrl+左 でリスト編集

			_cmd_edit_list((SelectBrush *)wg);
		}
		else if(ev->pt.btt == M_BTT_LEFT)
		{
			//左ボタン : メニュー
			
			_run_menu((SelectBrush *)wg);
		}
	}
	
	return 1;
}

/** 描画 */

static void _draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	SelectBrush *p = (SelectBrush *)wg;
	mFont *font;

	font = mWidgetGetFont(wg);

	//枠

	mPixbufBox(pixbuf, 0, 0, wg->w, wg->h, MSYSCOL(FRAME_DARK));

	//背景

	mPixbufFillBox(pixbuf, 1, 1, wg->w - 2, wg->h - 2, MSYSCOL(FACE_LIGHTEST));

	//矢印

	mPixbufDrawArrowDown(pixbuf, wg->w - 7, (wg->h - 3) / 2, MSYSCOL(TEXT));

	//名前

	if(p->item && p->item->name
		&& mPixbufSetClipBox_d(pixbuf, 3, 0, wg->w - 3 - 12, wg->h))
	{
		mFontDrawText(font, pixbuf, 3, 3, p->item->name, -1, MSYSCOL_RGB(TEXT));
	}
}

/** 計算 */

static void _calchint_handle(mWidget *wg)
{
	wg->hintW = 50;
	wg->hintH = mWidgetGetFontHeight(wg) + 6;
}

/** 作成 */

SelectBrush *SelectBrush_new(mWidget *parent,int id,int listno,BrushItem *item)
{
	SelectBrush *p;

	p = (SelectBrush *)mWidgetNew(sizeof(SelectBrush), parent);
	if(!p) return NULL;

	p->wg.id = id;
	p->wg.fEventFilter |= MWIDGET_EVENTFILTER_POINTER;
	p->wg.fLayout = MLF_EXPAND_W;
	p->wg.calcHint = _calchint_handle;
	p->wg.draw = _draw_handle;
	p->wg.event = _event_handle;

	p->listno = listno;
	p->item = item;

	return p;
}

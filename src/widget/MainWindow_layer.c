/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * MainWindow
 *
 * レイヤのコマンド
 *****************************************/

#include "mDef.h"
#include "mStr.h"
#include "mTrans.h"
#include "mSysDialog.h"

#include "defWidgets.h"
#include "defConfig.h"
#include "defDraw.h"
#include "AppErr.h"

#include "MainWindow.h"
#include "PopupThread.h"
#include "FileDialog.h"
#include "Docks_external.h"

#include "LayerList.h"
#include "LayerItem.h"
#include "TileImage.h"

#include "draw_layer.h"

#include "trgroup.h"
#include "trid_word.h"
#include "trid_mainmenu.h"


//-------------------

/* LayerCombineDlg.c */
int LayerCombineDlg_run(mWindow *owner,mBool folder,mBool have_checked_layer);

//-------------------



/** ファイルから読み込み */

static void _load_file(MainWindow *p)
{
	mStr str = MSTR_INIT;
	int err;

	if(FileDialog_openImage(M_WINDOW(p),
		"BMP/PNG/GIF/JPEG/APD(v3)\t*.bmp;*.png;*.gif;*.jpg;*.jpeg;*.apd\tAll Files\t*",
		APP_CONF->strLayerFileDir.buf, &str))
	{
		//フォルダ記録

		mStrPathGetDir(&APP_CONF->strLayerFileDir, str.buf);

		//読み込み

		err = drawLayer_newLayer_file(APP_DRAW, str.buf,
			APP_CONF->openimage_flags & CONFIG_OPENIMAGE_F_IGNORE_ALPHA,
			APP_CONF->openimage_flags & CONFIG_OPENIMAGE_F_LEAVE_TRANS, NULL);

		if(err != APPERR_OK)
			MainWindow_apperr(err, NULL);
	}

	mStrFree(&str);
}

/** レイヤ名変更 */

void MainWindow_layer_rename(MainWindow *p,LayerItem *pi)
{
	mStr str = MSTR_INIT;

	M_TR_G(TRGROUP_WORD);

	mStrSetText(&str, pi->name);

	if(mSysDlgInputText(M_WINDOW(p),
		M_TR_T(TRID_WORD_RENAME), M_TR_T(TRID_WORD_NAME), &str, 0))
	{
		mStrLimitBytes(&str, 1024);
	
		mStrdup_ptr(&pi->name, str.buf);

		DockLayer_update_layer(pi);
	}

	mStrFree(&str);
}

/** 複数レイヤ結合 */

static void _combine_multi(MainWindow *p)
{
	int ret;

	ret = LayerCombineDlg_run(M_WINDOW(p),
		LAYERITEM_IS_FOLDER(APP_DRAW->curlayer),
		LayerList_haveCheckedLayer(APP_DRAW->layerlist));

	if(ret != -1)
		drawLayer_combineMulti(APP_DRAW, ret & 3, ((ret & (1<<2)) != 0));
}


//==============================
// ファイルに出力
//==============================


typedef struct
{
	const char *filename;
	int type;
}_thdata_savefile;


/** [スレッド] ファイルに出力 */

static int _thread_save_file(mPopupProgress *prog,void *data)
{
	_thdata_savefile *p = (_thdata_savefile *)data;
	mBox box;

	if(p->type == 0)
		//apd
		return LayerItem_saveAPD_single(APP_DRAW->curlayer, p->filename, prog);
	else
	{
		//png

		box.x = box.y = 0;
		box.w = APP_DRAW->imgw;
		box.h = APP_DRAW->imgh;
		
		return TileImage_savePNG_rgba(APP_DRAW->curlayer->img,
				p->filename, APP_DRAW->imgdpi, &box, prog);
	}
}

/** ファイルに出力 */

static void _save_file(MainWindow *p)
{
	mStr str = MSTR_INIT;
	_thdata_savefile dat;
	int type;

	//フォルダは除く

	if(LAYERITEM_IS_FOLDER(APP_DRAW->curlayer)) return;

	//初期ファイル名 (レイヤ名から)

	mStrSetText(&str, APP_DRAW->curlayer->name);
	mStrPathReplaceDisableChar(&str, '_');

	//ファイル名取得

	if(mSysDlgSaveFile(M_WINDOW(p),
		"AzPainter file v3 (*.apd)\t*.apd\tPNG file (*.png)\t*.png",
		0, APP_CONF->strLayerFileDir.buf, 0, &str, &type))
	{
		//拡張子

		mStrPathSetExt(&str, (type == 0)? "apd": "png");

		//ディレクトリ記録

		mStrPathGetDir(&APP_CONF->strLayerFileDir, str.buf);

		//保存

		dat.filename = str.buf;
		dat.type = type;

		if(PopupThread_run(&dat, _thread_save_file) != 1)
			MainWindow_apperr(APPERR_FAILED, NULL);
	}

	mStrFree(&str);
}


//========================


/** コマンド処理 */

void MainWindow_layercmd(MainWindow *p,int id)
{
	switch(id)
	{
		//----- ツールバー
		
		//上へ移動
		case TRMENU_LAYER_TB_MOVE_UP:
			drawLayer_moveUpDown(APP_DRAW, TRUE);
			break;
		//下へ移動
		case TRMENU_LAYER_TB_MOVE_DOWN:
			drawLayer_moveUpDown(APP_DRAW, FALSE);
			break;

		//------ メインメニュー
	
		//新規レイヤ
		case TRMENU_LAYER_NEW:
			drawLayer_newLayer(APP_DRAW, FALSE);
			break;
		//新規フォルダ
		case TRMENU_LAYER_NEW_FOLDER:
			drawLayer_newLayer(APP_DRAW, TRUE);
			break;
		//ファイルから新規レイヤ
		case TRMENU_LAYER_NEW_FROM_FILE:
			_load_file(p);
			break;
		//複製
		case TRMENU_LAYER_COPY:
			drawLayer_copy(APP_DRAW);
			break;
		//削除
		case TRMENU_LAYER_DELETE:
			drawLayer_delete(APP_DRAW, TRUE);
			break;
		//イメージ消去
		case TRMENU_LAYER_ERASE:
			drawLayer_erase(APP_DRAW);
			break;
		//名前変更
		case TRMENU_LAYER_RENAME:
			MainWindow_layer_rename(p, APP_DRAW->curlayer);
			break;

		//下レイヤに移す
		case TRMENU_LAYER_DROP:
			drawLayer_combine(APP_DRAW, TRUE);
			break;
		//下レイヤと結合
		case TRMENU_LAYER_COMBINE:
			drawLayer_combine(APP_DRAW, FALSE);
			break;
		//複数レイヤ結合
		case TRMENU_LAYER_COMBINE_MULTI:
			_combine_multi(p);
			break;
		//画像の統合
		case TRMENU_LAYER_BLEND_ALL:
			drawLayer_blendAll(APP_DRAW);
			break;

		//左右反転
		case TRMENU_LAYER_REV_HORZ:
			drawLayer_editFullImage(APP_DRAW, 0);
			break;
		//上下反転
		case TRMENU_LAYER_REV_VERT:
			drawLayer_editFullImage(APP_DRAW, 1);
			break;

		//すべて表示
		case TRMENU_LAYER_VIEW_ALL_SHOW:
			drawLayer_showAll(APP_DRAW, 1);
			break;
		//すべて非表示
		case TRMENU_LAYER_VIEW_ALL_HIDE:
			drawLayer_showAll(APP_DRAW, 0);
			break;
		//カレントレイヤのみ表示
		case TRMENU_LAYER_VIEW_ONLY_CURRENT:
			drawLayer_showAll(APP_DRAW, 2);
			break;
		//チェックレイヤの表示反転
		case TRMENU_LAYER_VIEW_REV_CHECKED:
			drawLayer_showRevChecked(APP_DRAW);
			break;
		//フォルダを除くレイヤの表示反転
		case TRMENU_LAYER_VIEW_REV_IMAGE:
			drawLayer_showRevImage(APP_DRAW);
			break;

		//ロック解除
		case TRMENU_LAYER_FLAG_OFF_LOCK:
			drawLayer_allFlagsOff(APP_DRAW, LAYERITEM_F_LOCK);
			break;
		//塗りつぶし判定元解除
		case TRMENU_LAYER_FLAG_OFF_FILL_REF:
			drawLayer_allFlagsOff(APP_DRAW, LAYERITEM_F_FILLREF);
			break;
		//チェック解除
		case TRMENU_LAYER_FLAG_OFF_CHECKED:
			drawLayer_allFlagsOff(APP_DRAW, LAYERITEM_F_CHECKED);
			break;

		//ファイルに出力
		case TRMENU_LAYER_OUTPUT:
			_save_file(p);
			break;
	}
}

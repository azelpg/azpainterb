/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************************
 * ブラシリスト編集ダイアログ
 ************************************************/

#include "mDef.h"
#include "mWidget.h"
#include "mContainer.h"
#include "mWindow.h"
#include "mDialog.h"
#include "mButton.h"
#include "mLineEdit.h"
#include "mListView.h"
#include "mEvent.h"
#include "mTrans.h"
#include "mList.h"
#include "mStr.h"

#include "BrushList.h"
#include "BrushItem.h"

#include "trgroup.h"


//----------------------

typedef struct
{
	mWidget wg;
	mContainerData ct;
	mWindowData win;
	mDialogData dlg;

	int listno;
	mList blist;

	mListView *list;
	mLineEdit *edit_name;
}_brushlist_dlg;

//----------------------

enum
{
	WID_LIST = 100,
	WID_EDIT_NAME,
	WID_BTT_ADD,
	WID_BTT_COPY,
	WID_BTT_DEL,
	WID_BTT_MOVE_UP,
	WID_BTT_MOVE_DOWN
};

//----------------------


//===========================
// sub
//===========================


/** 選択アイテム取得 */

static mBool _get_select(_brushlist_dlg *p,mListViewItem **ret_lvi,BrushItem **ret_bi)
{
	mListViewItem *lvi;

	lvi = mListViewGetFocusItem(p->list);
	if(!lvi) return FALSE;
	
	if(ret_lvi) *ret_lvi = lvi;
	*ret_bi = BRUSHITEM(lvi->param);

	return TRUE;
}

/** 名前変更 */

static void _item_rename(_brushlist_dlg *p)
{
	mStr str = MSTR_INIT;
	mListViewItem *lvi;
	BrushItem *bi;

	if(!_get_select(p, &lvi, &bi)) return;

	mLineEditGetTextStr(p->edit_name, &str);

	//空文字列は無効

	if(!mStrIsEmpty(&str))
	{
		mStrdup_ptr(&bi->name, str.buf);

		mListViewSetItemText(p->list, lvi, bi->name);
	}

	mStrFree(&str);
}

/** コマンド処理 */

static void _run_command(_brushlist_dlg *p,int id)
{
	BrushItem *bi;
	mListViewItem *lvi;

	//選択アイテム

	if(id != WID_BTT_ADD)
	{
		lvi = mListViewGetFocusItem(p->list);
		if(!lvi) return;

		bi = BRUSHITEM(lvi->param);
	}

	//

	switch(id)
	{
		//追加
		case WID_BTT_ADD:
			bi = BrushList_edit_newItem(&p->blist, p->listno, NULL);
			if(bi)
			{
				lvi = mListViewAddItem(p->list, bi->name, -1, 0, (intptr_t)bi);

				mListViewSetFocusItem(p->list, lvi);

				mLineEditSetText(p->edit_name, bi->name);
			}
			break;
		//複製
		case WID_BTT_COPY:
			bi = BrushList_edit_newItem(&p->blist, p->listno, bi);
			if(bi)
			{
				lvi = mListViewInsertItem(p->list, M_LISTVIEWITEM(lvi->i.next),
					bi->name, -1, 0, (intptr_t)bi);

				mListViewSetFocusItem(p->list, lvi);
			}
			break;
		//削除
		case WID_BTT_DEL:
			mListDelete(&p->blist, M_LISTITEM(bi));

			lvi = mListViewDeleteItem_sel(p->list, lvi);

			mListViewSetFocusItem(p->list, lvi);

			if(lvi)
				mLineEditSetText(p->edit_name, BRUSHITEM(lvi->param)->name);
			break;
		//移動
		case WID_BTT_MOVE_UP:
		case WID_BTT_MOVE_DOWN:
			mListMoveUpDown(&p->blist, M_LISTITEM(bi), (id == WID_BTT_MOVE_UP));
			
			mListViewMoveItem_updown(p->list, lvi, (id == WID_BTT_MOVE_DOWN));
			break;
	}
}


//===========================
//
//===========================


/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		_brushlist_dlg *p = (_brushlist_dlg *)wg;
	
		switch(ev->notify.id)
		{
			case WID_LIST:
				//選択変更時、名前セット
				if(ev->notify.type == MLISTVIEW_N_CHANGE_FOCUS)
					mLineEditSetText(p->edit_name, BRUSHITEM(ev->notify.param2)->name);
				break;
			case WID_EDIT_NAME:
				//Enter で名前変更
				if(ev->notify.type == MLINEEDIT_N_ENTER)
					_item_rename(p);
				break;

			case WID_BTT_ADD:
			case WID_BTT_COPY:
			case WID_BTT_DEL:
			case WID_BTT_MOVE_UP:
			case WID_BTT_MOVE_DOWN:
				_run_command(p, ev->notify.id);
				break;
		}
	}
	
	return mDialogEventHandle_okcancel(wg, ev);
}

/** 作成 */

static _brushlist_dlg *_dlg_create(mWindow *owner,int listno)
{
	_brushlist_dlg *p;
	mWidget *cth,*ctv;
	mListView *list;
	BrushItem *item;
	int i;

	p = (_brushlist_dlg *)mDialogNew(sizeof(_brushlist_dlg), owner, MWINDOW_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;

	p->listno = listno;

	//リストデータを複製

	if(!BrushList_dup(listno, &p->blist))
	{
		mWidgetDestroy(M_WIDGET(p));
		return NULL;
	}

	//

	mContainerSetPadding_one(M_CONTAINER(p), 8);
	p->ct.sepW = 18;

	M_TR_G(TRGROUP_DLG_BRUSHLIST);

	mWindowSetTitle(M_WINDOW(p), M_TR_T(0));

	//========= ウィジェット

	cth = mContainerCreate(M_WIDGET(p), MCONTAINER_TYPE_HORZ, 0, 5, MLF_EXPAND_WH);

	//------ リスト + 名前

	ctv = mContainerCreate(cth, MCONTAINER_TYPE_VERT, 0, 7, MLF_EXPAND_WH);

	//リスト

	list = mListViewNew(0, ctv, MLISTVIEW_S_AUTO_WIDTH,
		MSCROLLVIEW_S_HORZVERT | MSCROLLVIEW_S_FRAME);

	p->list = list;

	list->wg.id = WID_LIST;
	list->wg.fLayout = MLF_EXPAND_WH;

	mWidgetSetInitSize_fontHeight(M_WIDGET(list), 16, 15);

	for(item = BRUSHITEM(p->blist.top); item; item = BRUSHITEM(item->i.next))
		mListViewAddItem(list, item->name, -1, 0, (intptr_t)item);

	//名前

	p->edit_name = mLineEditCreate(ctv, WID_EDIT_NAME, MLINEEDIT_S_NOTIFY_ENTER, MLF_EXPAND_W, 0);

	//---------- ボタン

	ctv = mContainerCreate(cth, MCONTAINER_TYPE_VERT, 0, 2, MLF_EXPAND_H);

	for(i = 0; i < 5; i++)
		mButtonCreate(ctv, WID_BTT_ADD + i, 0, MLF_EXPAND_W, 0, M_TR_T(1 + i));

	//OK/cancel

	mContainerCreateOkCancelButton(M_WIDGET(p));

	return p;
}

/** 実行 */

mBool BrushListDlg_run(mWindow *owner,int listno)
{
	_brushlist_dlg *p;
	mBool ret;
	BrushItem *item;

	p = _dlg_create(owner, listno);
	if(!p) return FALSE;

	mWindowMoveResizeShow_hintSize(M_WINDOW(p));

	ret = mDialogRun(M_DIALOG(p), FALSE);

	if(!ret)
		mListDeleteAll(&p->blist);
	else
	{
		//OK
		
		if(!_get_select(p, NULL, &item))
			item = NULL;
	
		BrushList_replace(listno, &p->blist, item);
	}

	mWidgetDestroy(M_WIDGET(p));

	return ret;
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************************
 * ブラシ詳細設定ダイアログ
 ************************************************/

#include "mDef.h"
#include "mDialog.h"
#include "mWindow.h"
#include "mWidget.h"
#include "mContainer.h"
#include "mLabel.h"
#include "mCheckButton.h"
#include "mArrowButton.h"
#include "mComboBox.h"
#include "mEvent.h"
#include "mTrans.h"
#include "mUtilStr.h"

#include "defPixelMode.h"

#include "ValueBar.h"
#include "PressureWidget.h"

#include "BrushList.h"
#include "BrushItem.h"

#include "trgroup.h"
#include "trid_word.h"


//----------------------

typedef struct
{
	mWidget wg;
	mContainerData ct;
	mWindowData win;
	mDialogData dlg;

	mComboBox *cb_rdmin,
		*cb_rdmax,
		*cb_shape,
		*cb_pixmode;
	ValueBar *bar_minsize,
		*bar_minopa,
		*bar_itv;
	PressureWidget *presswg;
	mCheckButton *ckbtt[2];
}_detail_dlg;

//----------------------

enum
{
	WID_PRESSURE_UP = 100,
	WID_PRESSURE_DOWN
};

enum
{
	TRID_RADIUS_MIN = 1,
	TRID_RADIUS_MAX,
	TRID_SHAPE,
	TRID_PIXMODE,
	TRID_MIN_SIZE,
	TRID_MIN_OPACITY,
	TRID_INTERVAL,
	TRID_PRESSURE,
	TRID_ANTIALIAS,
	TRID_CURVE,

	TRID_MIN = 100,
	TRID_MAX,

	TRID_SHAPE_TOP = 200
};

//----------------------


/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY && ev->notify.id >= WID_PRESSURE_UP)
	{
		//筆圧カーブを上下へ

		_detail_dlg *p = (_detail_dlg *)wg;
		int val;

		val = PressureWidget_getValue(p->presswg);

		if(ev->notify.id == WID_PRESSURE_UP)
		{
			if(val > BRUSHITEM_PRESSURE_MIN)
				val--;
		}
		else
		{
			if(val < BRUSHITEM_PRESSURE_MAX)
				val++;
		}

		PressureWidget_setValue(p->presswg, val);

		return 1;
	}
	
	return mDialogEventHandle_okcancel(wg, ev);
}

/** ラベル+コンボボックス 作成 */

static mComboBox *_create_combobox(mWidget *parent,int trid)
{
	mLabelCreate(parent, 0, MLF_RIGHT | MLF_MIDDLE, 0, M_TR_T(trid));

	return mComboBoxCreate(parent, 0, 0, MLF_EXPAND_W, 0);
}

/** コンボボックス、アイテム追加後の初期化 */

static void _init_combobox(mComboBox *cb,int val)
{
	mComboBoxSetWidthAuto(cb);
	mComboBoxSetSel_findParam_notfind(cb, val, 0);
}

/** ラベル+バー を作成 */

static ValueBar *_create_bar(mWidget *parent,int trid,int dig,int min,int max,int val)
{
	mLabelCreate(parent, MLABEL_S_RIGHT, MLF_MIDDLE | MLF_RIGHT, 0, M_TR_T(trid));

	return ValueBar_new(parent, 0, MLF_EXPAND_W | MLF_MIDDLE, dig, min, max, val);
}

/** 作成 */

static _detail_dlg *_dlg_create(mWindow *owner,int listno)
{
	_detail_dlg *p;
	mWidget *ct,*ct2,*ct3;
	mComboBox *cb;
	int i;
	char m[16];
	BrushItem *item;
	uint8_t pixmode[] = {
		PIXELMODE_BLEND_STROKE, PIXELMODE_BLEND_PIXEL,
		PIXELMODE_COMPARE_A, PIXELMODE_OVERWRITE_STYLE, 255
	},
		pixmode_erase[] = { PIXELMODE_BLEND_STROKE, PIXELMODE_BLEND_PIXEL, 255 };
	uint8_t *ppixmode;

	item = BrushList_getEditItem(listno);

	//

	p = (_detail_dlg *)mDialogNew(sizeof(_detail_dlg), owner, MWINDOW_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;

	//

	mContainerSetPadding_one(M_CONTAINER(p), 8);
	p->ct.sepW = 18;

	M_TR_G(TRGROUP_DLG_BRUSH_DETAIL);

	mWindowSetTitle(M_WINDOW(p), M_TR_T(0));

	//------ ウィジェット

	ct = mContainerCreateGrid(M_WIDGET(p), 2, 5, 7, MLF_EXPAND_W);

	//半径最小値

	p->cb_rdmin = cb = _create_combobox(ct, TRID_RADIUS_MIN);

	mComboBoxAddItem_static(cb, M_TR_T(TRID_MIN), 0);

	for(i = 50; i <= 300; )
	{
		mIntToStr(m, i);
		mComboBoxAddItem(cb, m, i * 10);

		i += (i == 50)? 50: 100;
	}

	_init_combobox(cb, item->radius_min);

	cb->wg.initW = 200;  //初期幅
	
	//半径最大値

	p->cb_rdmax = cb = _create_combobox(ct, TRID_RADIUS_MAX);

	mComboBoxAddItem_static(cb, M_TR_T(TRID_MAX), 0);

	for(i = 10; i <= BRUSHITEM_RADIUS_MAX / 10; )
	{
		mIntToStr(m, i);
		mComboBoxAddItem(cb, m, i * 10);

		if(i == 10) i = 50;
		else if(i == 50) i = 100;
		else i += 100;
	}

	_init_combobox(cb, item->radius_max);

	//形状

	if(listno != BRUSHLIST_BLUR)
	{
		p->cb_shape = cb = _create_combobox(ct, TRID_SHAPE);

		mComboBoxAddTrItems(cb, BRUSHITEM_SHAPE_NUM, TRID_SHAPE_TOP, 0);

		_init_combobox(cb, item->shape);
	}

	//塗り

	if(listno == BRUSHLIST_PEN || listno == BRUSHLIST_BRUSH || listno == BRUSHLIST_ERASE)
	{
		p->cb_pixmode = cb = _create_combobox(ct, TRID_PIXMODE);

		M_TR_G(TRGROUP_PIXELMODE);

		ppixmode = (listno == BRUSHLIST_ERASE)? pixmode_erase: pixmode;

		for(i = 0; ppixmode[i] != 255; i++)
			mComboBoxAddItem_static(cb, M_TR_T(ppixmode[i]), ppixmode[i]);

		_init_combobox(cb, item->pixmode);

		M_TR_G(TRGROUP_DLG_BRUSH_DETAIL);
	}

	//サイズ最小

	p->bar_minsize = _create_bar(ct, TRID_MIN_SIZE, 1, 0, 1000, item->min_size);

	//濃度最小

	if(listno != BRUSHLIST_BLUR)
		p->bar_minopa = _create_bar(ct, TRID_MIN_OPACITY, 1, 0, 1000, item->min_opacity);

	//間隔

	if(listno != BRUSHLIST_DODGE && listno != BRUSHLIST_BURN && listno != BRUSHLIST_BLUR)
	{
		p->bar_itv = _create_bar(ct, TRID_INTERVAL, 2,
			BRUSHITEM_INTERVAL_MIN, BRUSHITEM_INTERVAL_MAX, item->interval);
	}

	//筆圧

	mLabelCreate(ct, 0, MLF_RIGHT, 0, M_TR_T(TRID_PRESSURE));

	ct2 = mContainerCreate(ct, MCONTAINER_TYPE_HORZ, 0, 10, 0);

	p->presswg = PressureWidget_new(ct2, 70, item->pressure_gamma);

	//矢印ボタン

	ct3 = mContainerCreate(ct2, MCONTAINER_TYPE_VERT, 0, 0, MLF_BOTTOM);

	mArrowButtonCreate(ct3, WID_PRESSURE_UP, MARROWBUTTON_S_UP, 0, 0);
	mArrowButtonCreate(ct3, WID_PRESSURE_DOWN, MARROWBUTTON_S_DOWN, 0, 0);

	//チェックボタン

	mWidgetNew(0, ct);

	ct2 = mContainerCreate(ct, MCONTAINER_TYPE_VERT, 0, 5, 0);

	if(listno != BRUSHLIST_BLUR)
		p->ckbtt[0] = mCheckButtonCreate(ct2, 0, 0, 0, 0, M_TR_T(TRID_ANTIALIAS), item->flags & BRUSHITEM_F_ANTIALIAS);

	p->ckbtt[1] = mCheckButtonCreate(ct2, 0, 0, 0, 0, M_TR_T(TRID_CURVE), item->flags & BRUSHITEM_F_CURVE);

	//OK/cancel

	mContainerCreateOkCancelButton(M_WIDGET(p));

	return p;
}

/** 値を取得 */

static mBool _get_value(_detail_dlg *p,int listno)
{
	BrushItem *item;
	int min,max;
	mBool ret;

	item = BrushList_getEditItem(listno);

	//半径範囲

	min = mComboBoxGetItemParam(p->cb_rdmin, -1);
	max = mComboBoxGetItemParam(p->cb_rdmax, -1);

	if(max != 0 && max <= min) max = 0;

	ret = (min != item->radius_min || max != item->radius_max);

	item->radius_min = min;
	item->radius_max = max;

	//

	if(p->cb_shape)
		item->shape = mComboBoxGetSelItemIndex(p->cb_shape);

	if(p->cb_pixmode)
		item->pixmode = mComboBoxGetItemParam(p->cb_pixmode, -1);

	item->min_size = ValueBar_getPos(p->bar_minsize);

	if(p->bar_minopa)
		item->min_opacity = ValueBar_getPos(p->bar_minopa);

	if(p->bar_itv)
		item->interval = ValueBar_getPos(p->bar_itv);

	if(p->presswg)
		item->pressure_gamma = PressureWidget_getValue(p->presswg);

	//アンチエイリアス

	if(p->ckbtt[0])
	{
		item->flags &= ~BRUSHITEM_F_ANTIALIAS;

		if(mCheckButtonIsChecked(p->ckbtt[0]))
			item->flags |= BRUSHITEM_F_ANTIALIAS;
	}

	//曲線

	item->flags &= ~BRUSHITEM_F_CURVE;

	if(mCheckButtonIsChecked(p->ckbtt[1]))
		item->flags |= BRUSHITEM_F_CURVE;

	return ret;
}

/** 実行
 *
 * @return TRUE で半径の範囲が変更された */

mBool BrushDetailDlg_run(mWindow *owner,int listno)
{
	_detail_dlg *p;
	mBool ret = FALSE;

	p = _dlg_create(owner, listno);
	if(!p) return FALSE;

	mWindowMoveResizeShow_hintSize(M_WINDOW(p));

	if(mDialogRun(M_DIALOG(p), FALSE))
		ret = _get_value(p, listno);

	mWidgetDestroy(M_WIDGET(p));

	return ret;
}

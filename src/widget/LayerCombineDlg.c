/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * 複数レイヤ結合ダイアログ
 *****************************************/

#include "mDef.h"
#include "mWidget.h"
#include "mDialog.h"
#include "mContainer.h"
#include "mWindow.h"
#include "mWidgetBuilder.h"
#include "mCheckButton.h"
#include "mTrans.h"
#include "mEvent.h"

#include "trgroup.h"


//----------------------

typedef struct
{
	mWidget wg;
	mContainerData ct;
	mWindowData win;
	mDialogData dlg;

	mCheckButton *ck_target[3],
		*ck_new;
	mBool have_checked_layer;
}_combine_dlg;

//----------------------

static const char *g_wb_combine =
"ct#v:sep=4;"
  "ck#r:id=100:tr=1;"
  "ck#r:id=101:tr=2;"
  "ck#r:id=102:tr=3;"
  "ck:id=103:tr=4:mg=t5;"
  "lb#B:tr=5:mg=t8;";

//----------------------

enum
{
	WID_CK_TARGET = 100,
	WID_CK_NEW = 103
};

//----------------------


/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	_combine_dlg *p = (_combine_dlg *)wg;

	if(ev->type == MEVENT_NOTIFY && ev->notify.id == WID_CK_NEW
		&& ev->notify.type == MCHECKBUTTON_N_PRESS)
	{
		//新規レイヤに結合

		if(p->have_checked_layer)
		{
			mWidgetEnable(M_WIDGET(p->ck_target[2]), ev->notify.param1);

			/* チェックされたレイヤが ON の状態で "新規レイヤに結合" が OFF になった場合、
			 * 選択を "すべての表示レイヤ" に変更。 */
			
			if(!ev->notify.param1 && mCheckButtonIsChecked(p->ck_target[2]))
				mCheckButtonSetState(p->ck_target[0], 1);
		}

		return 1;
	}

	return mDialogEventHandle_okcancel(wg, ev);
}

/** 作成 */

static _combine_dlg *_combinedlg_create(mWindow *owner,mBool folder)
{
	_combine_dlg *p;
	int i;

	p = (_combine_dlg *)mDialogNew(sizeof(_combine_dlg), owner, MWINDOW_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;

	//

	mContainerSetPadding_one(M_CONTAINER(p), 8);

	p->ct.sepW = 18;

	M_TR_G(TRGROUP_DLG_LAYER_COMBINE);

	mWindowSetTitle(M_WINDOW(p), M_TR_T(0));

	//ウィジェット

	mWidgetBuilderCreateFromText(M_WIDGET(p), g_wb_combine);

	for(i = 0; i < 3; i++)
		p->ck_target[i] = (mCheckButton *)mWidgetFindByID(M_WIDGET(p), WID_CK_TARGET + i);

	p->ck_new = (mCheckButton *)mWidgetFindByID(M_WIDGET(p), WID_CK_NEW);

	//初期化

	if(!folder)
		mWidgetEnable(M_WIDGET(p->ck_target[1]), 0);

	mWidgetEnable(M_WIDGET(p->ck_target[2]), 0);

	mCheckButtonSetState(p->ck_target[0], 1);

	//OK/cancel

	mContainerCreateOkCancelButton(M_WIDGET(p));

	return p;
}

/** 複数レイヤ結合ダイアログ
 *
 * @param folder  対象がフォルダか
 * @param have_checked_layer チェックされたレイヤがあるか
 * @return -1 でキャンセル。0-1bit:対象、2bit:新規レイヤ */

int LayerCombineDlg_run(mWindow *owner,mBool folder,mBool have_checked_layer)
{
	_combine_dlg *p;
	int ret = -1;

	p = _combinedlg_create(owner, folder);
	if(!p) return FALSE;

	p->have_checked_layer = have_checked_layer;

	mWindowMoveResizeShow_hintSize(M_WINDOW(p));

	if(mDialogRun(M_DIALOG(p), FALSE))
	{
		ret = mCheckButtonGetGroupSelIndex(p->ck_target[0]);

		if(mCheckButtonIsChecked(p->ck_new)) ret |= 1<<2;
	}

	mWidgetDestroy(M_WIDGET(p));

	return ret;
}

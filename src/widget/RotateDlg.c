/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * 回転ダイアログ
 *****************************************/

#include "mDef.h"
#include "mWidget.h"
#include "mDialog.h"
#include "mContainer.h"
#include "mWindow.h"
#include "mLabel.h"
#include "mLineEdit.h"
#include "mComboBox.h"
#include "mTrans.h"
#include "mEvent.h"

#include "defConfig.h"

#include "AngleWidget.h"

#include "trgroup.h"


//-----------------------

typedef struct
{
	mWidget wg;
	mContainerData ct;
	mWindowData win;
	mDialogData dlg;

	mLineEdit *edit_angle;
	AngleWidget *anglewg;
	mComboBox *cb_type;
}_rotate_dlg;

//-----------------------

enum
{
	WID_EDIT_ANGLE = 100,
	WID_ANGLE,
	WID_CB_TYPE
};

enum
{
	TRID_ANGLE = 1,
	TRID_TYPE,
	TRID_TYPE_TOP = 100
};

//-----------------------


/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		_rotate_dlg *p = (_rotate_dlg *)wg;
	
		switch(ev->notify.id)
		{
			//角度 (エディット)
			case WID_EDIT_ANGLE:
				if(ev->notify.type == MLINEEDIT_N_CHANGE)
					AngleWidget_setAngle(p->anglewg, mLineEditGetInt(p->edit_angle));
				break;
			//角度ウィジェット
			case WID_ANGLE:
				mLineEditSetNum(p->edit_angle, ev->notify.param1);
				break;
		}
	}
	
	return mDialogEventHandle_okcancel(wg, ev);
}

/** 作成 */

static _rotate_dlg *_dlg_create(mWindow *owner)
{
	_rotate_dlg *p;
	mWidget *ct,*ct2;
	mLineEdit *edit;

	//作成

	p = (_rotate_dlg *)mDialogNew(sizeof(_rotate_dlg), owner, MWINDOW_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;
	p->ct.sepW = 18;

	//

	mContainerSetPadding_one(M_CONTAINER(p), 8);

	M_TR_G(TRGROUP_DLG_ROTATE);

	mWindowSetTitle(M_WINDOW(p), M_TR_T(0));

	//------ ウィジェット

	ct = mContainerCreateGrid(M_WIDGET(p), 2, 6, 7, 0);

	//角度

	mLabelCreate(ct, 0, MLF_RIGHT, 0, M_TR_T(TRID_ANGLE));

	ct2 = mContainerCreate(ct, MCONTAINER_TYPE_VERT, 0, 7, 0);

	edit = p->edit_angle = mLineEditCreate(ct2, WID_EDIT_ANGLE,
		MLINEEDIT_S_SPIN | MLINEEDIT_S_NOTIFY_CHANGE, 0, 0);

	mLineEditSetWidthByLen(edit, 6);
	mLineEditSetNumStatus(edit, -360, 360, 0);
	mLineEditSetNum(edit, 0);

	p->anglewg = AngleWidget_new(ct2, WID_ANGLE, 91);

	//補間方法

	mLabelCreate(ct, 0, MLF_RIGHT | MLF_MIDDLE, 0, M_TR_T(TRID_TYPE));

	p->cb_type = mComboBoxCreate(ct, WID_CB_TYPE, 0, 0, 0);

	mComboBoxAddTrItems(p->cb_type, 3, TRID_TYPE_TOP, 0);
	mComboBoxSetWidthAuto(p->cb_type);
	mComboBoxSetSel_index(p->cb_type, APP_CONF->rotate_type);

	//OK/cancel

	mContainerCreateOkCancelButton(M_WIDGET(p));

	return p;
}

/** ダイアログ実行 */

mBool RotateDlg_run(mWindow *owner,int *pangle,int *ptype)
{
	_rotate_dlg *p;
	mBool ret;

	p = _dlg_create(owner);
	if(!p) return FALSE;

	mWindowMoveResizeShow_hintSize(M_WINDOW(p));

	ret = mDialogRun(M_DIALOG(p), FALSE);

	if(ret)
	{
		*pangle = AngleWidget_getAngle(p->anglewg);
		*ptype = mComboBoxGetSelItemIndex(p->cb_type);

		APP_CONF->rotate_type = *ptype;
	}

	mWidgetDestroy(M_WIDGET(p));

	return ret;
}


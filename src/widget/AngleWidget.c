/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * AngleWidget
 *
 * 角度指定ウィジェット
 *****************************************/
/*
 * <通知>
 * param1: 角度
 */

#include <math.h>

#include "mDef.h"
#include "mWidgetDef.h"
#include "mWidget.h"
#include "mPixbuf.h"
#include "mEvent.h"
#include "mSysCol.h"


//-------------------------

typedef struct _AngleWidget
{
	mWidget wg;

	int angle,
		fdrag;
	double rd;
}AngleWidget;

//-------------------------



/** カーソル位置から角度セット */

static void _set_angle(AngleWidget *p,mEvent *ev)
{
	int ct,n;
	double rd;

	ct = p->wg.w / 2;

	rd = atan2(ev->pt.y - ct, ev->pt.x - ct);

	n = (int)(-rd / M_MATH_PI * 180);

	if(n != p->angle)
	{
		p->angle = n;
		p->rd = rd;

		mWidgetUpdate(M_WIDGET(p));

		mWidgetAppendEvent_notify(NULL, M_WIDGET(p), 0, n, 0);
	}
}

/** グラブ解除 */

static void _release_grab(AngleWidget *p)
{
	if(p->fdrag)
	{
		p->fdrag = 0;
		mWidgetUngrabPointer(M_WIDGET(p));
	}
}

/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	AngleWidget *p = (AngleWidget *)wg;

	switch(ev->type)
	{
		case MEVENT_POINTER:
			if(ev->pt.type == MEVENT_POINTER_TYPE_MOTION)
			{
				//移動

				if(p->fdrag)
					_set_angle(p, ev);
			}
			else if(ev->pt.type == MEVENT_POINTER_TYPE_PRESS)
			{
				//押し

				if(ev->pt.btt == M_BTT_LEFT && !p->fdrag)
				{
					_set_angle(p, ev);

					p->fdrag = 1;
					mWidgetGrabPointer(wg);
				}
			}
			else if(ev->pt.type == MEVENT_POINTER_TYPE_RELEASE)
			{
				//離し
				
				if(ev->pt.btt == M_BTT_LEFT)
					_release_grab(p);
			}
			break;
		
		case MEVENT_FOCUS:
			if(ev->focus.bOut)
				_release_grab(p);
			break;
		default:
			return FALSE;
	}

	return TRUE;
}

/** 描画 */

static void _draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	AngleWidget *p = (AngleWidget *)wg;
	int ct,r;

	//背景

	mPixbufFillBox(pixbuf, 0, 0, wg->w, wg->h, MSYSCOL(WHITE));

	//円の枠

	mPixbufEllipse(pixbuf, 0, 0, wg->w - 1, wg->h - 1, 0);

	//角度の線

	ct = wg->w / 2;
	r = ct - 1;

	mPixbufLine(pixbuf, ct, ct,
		round(ct + r * cos(p->rd)), round(ct + r * sin(p->rd)),
		mRGBtoPix(0xff0000));
}


//====================


/** 作成 */

AngleWidget *AngleWidget_new(mWidget *parent,int id,int size)
{
	AngleWidget *p;

	p = (AngleWidget *)mWidgetNew(sizeof(AngleWidget), parent);
	if(!p) return NULL;

	p->wg.id = id;
	p->wg.fEventFilter |= MWIDGET_EVENTFILTER_POINTER;
	p->wg.event = _event_handle;
	p->wg.draw = _draw_handle;
	p->wg.hintW = p->wg.hintH = size;

	p->rd = 0;

	return p;
}

/** 角度を取得 */

int AngleWidget_getAngle(AngleWidget *p)
{
	return p->angle;
}

/** 角度をセット */

mBool AngleWidget_setAngle(AngleWidget *p,int angle)
{
	while(angle < -180) angle += 360;
	while(angle > 180) angle -= 360;

	if(angle == p->angle)
		return FALSE;
	else
	{
		p->angle = angle;
		p->rd = -angle / 180.0 * M_MATH_PI;

		mWidgetUpdate(M_WIDGET(p));

		return TRUE;
	}
}

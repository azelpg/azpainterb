/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * [dock]テクスチャのウィジェット
 *
 * ON/OFF ボタン、プレビュー、ライブラリ一覧
 *****************************************/

#include "mDef.h"
#include "mWidget.h"
#include "mContainerDef.h"
#include "mContainer.h"
#include "mScrollBar.h"
#include "mSysCol.h"
#include "mPixbuf.h"
#include "mFont.h"
#include "mEvent.h"
#include "mMenu.h"
#include "mTrans.h"

#include "defDraw.h"

#include "ImageBuf8.h"
#include "TextureLibrary.h"

#include "trgroup.h"


//------------------

mBool DockTexture_loadTexture(const char *filename);
mBool DockTexture_addImage_toLibrary(const char *filename,mBool puterr);

//------------------



/*************************************
 * ON/OFFボタン
 *************************************/


/** 描画 */

static void _btt_draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	mFont *font;
	int h,w;

	font = mWidgetGetFont(wg);

	//枠

	mPixbufBox(pixbuf, 0, 0, wg->w, wg->h, 0);

	//背景、テキスト

	h = font->height + 2;

	if(APP_DRAW->tex.on)
	{
		//ON
		
		mPixbufFillBox(pixbuf, 1, 1, wg->w - 2, wg->h - h - 2, mRGBtoPix(0xF6D5DB));
		mPixbufFillBox(pixbuf, 1, wg->h - 1 - h, wg->w - 2, h, mRGBtoPix(0xFC4F6D));

		w = wg->param >> 16;

		mFontDrawText(font, pixbuf, (wg->w - w) / 2, wg->h - h,
			"ON", -1, 0xffffff);
	}
	else
	{
		//OFF
		
		mPixbufFillBox(pixbuf, 1, 1, wg->w - 2, h, mRGBtoPix(0x72AAEF));
		mPixbufFillBox(pixbuf, 1, 1 + h, wg->w - 2, wg->h - h - 2, mRGBtoPix(0xD1E5FD));

		w = wg->param & 0xffff;

		mFontDrawText(font, pixbuf, (wg->w - w) / 2, 2,
			"OFF", -1, 0xffffff);
	}
}

/** イベント */

static int _btt_event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_POINTER
		&& ev->pt.btt == M_BTT_LEFT
		&& (ev->pt.type == MEVENT_POINTER_TYPE_PRESS || ev->pt.type == MEVENT_POINTER_TYPE_DBLCLK))
	{
		APP_DRAW->tex.on ^= 1;

		mWidgetUpdate(wg);
	}
	
	return 1;
}

/** 計算 */

static void _btt_calchint_handle(mWidget *wg)
{
	mFont *font = mWidgetGetFont(wg);
	int won,woff;

	won = mFontGetTextWidth(font, "ON", -1);
	woff = mFontGetTextWidth(font, "OFF", -1);

	wg->param = (won << 16) | woff;

	wg->hintW = (woff + 10 < 40)? 40: woff + 10;
	wg->hintH = font->height + 2 + 2 + 12;
}

/** ON/OFFボタン 作成 */

mWidget *DockTexture_button_new(mWidget *parent)
{
	mWidget *p;

	p = mWidgetNew(0, parent);
	if(!p) return NULL;

	p->fEventFilter |= MWIDGET_EVENTFILTER_POINTER;
	p->fLayout = MLF_EXPAND_H;
	p->calcHint = _btt_calchint_handle;
	p->draw = _btt_draw_handle;
	p->event = _btt_event_handle;

	return p;
}



/*************************************
 * テクスチャプレビュー
 *************************************/


/** 描画 */

static void _prev_draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	//枠

	mPixbufDraw3DFrame(pixbuf, 0, 0, wg->w, wg->h, mGraytoPix(128), MSYSCOL(WHITE));

	//背景 or プレビュー

	if(APP_DRAW->tex.curimg)
	{
		ImageBuf8_drawTexturePreview(APP_DRAW->tex.curimg, pixbuf,
			1, 1, wg->w - 2, wg->h - 2);
	}
	else
		mPixbufFillBox(pixbuf, 1, 1, wg->w - 2, wg->h - 2, mGraytoPix(180));
}

/** D&D */

static int _prev_dnd_handle(mWidget *wg,char **files)
{
	if(DockTexture_loadTexture(*files))
		mWidgetUpdate(wg);

	return 1;
}

/** プレビュー 作成 */

mWidget *DockTexture_preview_new(mWidget *parent)
{
	mWidget *p;

	p = mWidgetNew(0, parent);
	if(!p) return NULL;

	p->fLayout = MLF_EXPAND_WH;
	p->hintW = 6;
	p->hintH = 24;
	p->fState |= MWIDGET_STATE_ENABLE_DROP;
	p->draw = _prev_draw_handle;
	p->onDND = _prev_dnd_handle;

	return p;
}



/*************************************
 * ライブラリ一覧
 *************************************/


typedef struct
{
	mWidget wg;

	mScrollBar *scr;
	int xnum,
		fpress;
	mPoint ptcur;
}_liblist_area;


#define _LIBLIST_COL_BKGND  mGraytoPix(0x99)
#define _LIBLIST_COL_SEL    mRGBtoPix(0xff0000)



//==========================
// sub
//==========================


/** スクロール情報セット */

static void _liblist_set_scroll(_liblist_area *p)
{
	int page;

	page = p->wg.h / APP_DRAW->tex.cellh;
	if(page <= 0) page = 1;

	mScrollBarSetStatus(p->scr, 0,
		(TextureLibrary_getNum(APP_DRAW->tex.libno) + p->xnum - 1) / p->xnum, page);
}

/** 更新 (データ数変更) */

static void _liblist_update(_liblist_area *p)
{
	_liblist_set_scroll(p);

	mWidgetUpdate(M_WIDGET(p));
}

/** カーソル位置からテクスチャ番号取得
 *
 * @param  pt  NULL 以外で X Y 位置がセットされる */

static int _get_texno_at_pt(_liblist_area *p,int x,int y,mPoint *pt)
{
	int no;

	if(x < 0 || y < 0) return -1;

	x = x / APP_DRAW->tex.cellw;
	y = y / APP_DRAW->tex.cellh + p->scr->sb.pos;

	if(x >= p->xnum) return -1;

	no = x + y * p->xnum;

	if(no >= TextureLibrary_getNum(APP_DRAW->tex.libno))
		return -1;

	if(pt)
	{
		pt->x = x * APP_DRAW->tex.cellw;
		pt->y = (y - p->scr->sb.pos) * APP_DRAW->tex.cellh;
	}

	return no;
}

/** カーソルを直接描画 */

static void _draw_cursor(_liblist_area *p,mPoint *pt,mBool erase)
{
	mPixbuf *pixbuf;
	mBox box;

	pixbuf = mWidgetBeginDirectDraw(M_WIDGET(p));
	if(pixbuf)
	{
		box.x = pt->x;
		box.y = pt->y;
		box.w = APP_DRAW->tex.cellw + 1;
		box.h = APP_DRAW->tex.cellh + 1;
	
		mPixbufBox(pixbuf, box.x, box.y, box.w, box.h,
			(erase)? _LIBLIST_COL_BKGND: _LIBLIST_COL_SEL);
	
		mWidgetEndDirectDraw(M_WIDGET(p), pixbuf);

		mWidgetUpdateBox_box(M_WIDGET(p), &box);
	}
}

/** テクスチャ削除 */

static void _liblist_cmd_delete(_liblist_area *p,int texno)
{
	TextureLibrary_delete(APP_DRAW->tex.libno, texno);

	_liblist_update(p);
}


//==========================
// ハンドラ
//==========================


/** メニュー */

static void _run_menu(_liblist_area *p,int texno,int rx,int ry,mPoint *pt)
{
	mMenu *menu;
	mMenuItemInfo *mi;
	int id;
	uint16_t dat[] = { 100, 0xffff };

	//カーソル

	_draw_cursor(p, pt, FALSE);

	//メニュー

	M_TR_G(TRGROUP_DOCK_TEXTURE);

	menu = mMenuNew();

	mMenuAddTrArray16(menu, dat);

	mi = mMenuPopup(menu, NULL, rx, ry, 0);
	id = (mi)? mi->id: -1;

	mMenuDestroy(menu);

	//カーソル消去

	_draw_cursor(p, pt, TRUE);

	//コマンド

	if(id == 100)
		_liblist_cmd_delete(p, texno);
}

/** 押し時 */

static void _event_press(_liblist_area *p,mEvent *ev)
{
	int no;
	mPoint pt;

	//パレット位置

	no = _get_texno_at_pt(p, ev->pt.x, ev->pt.y, &pt);
	if(no == -1) return;

	//ボタン

	if(ev->pt.btt == M_BTT_RIGHT)
	{
		//右ボタン : メニュー

		_run_menu(p, no, ev->pt.rootx, ev->pt.rooty, &pt);
	}
	else if(ev->pt.btt == M_BTT_LEFT)
	{
		//------ 左ボタン

		if(ev->pt.state & M_MODS_CTRL)
		{
			//+Ctrl : 削除

			_liblist_cmd_delete(p, no);
		}
		else
		{
			//選択

			ImageBuf8 *img;

			img = TextureLibrary_createImage(APP_DRAW->tex.libno, no);

			if(img)
			{
				ImageBuf8_free(APP_DRAW->tex.curimg);
				APP_DRAW->tex.curimg = img;
			
				mWidgetAppendEvent_notify(NULL, M_WIDGET(p), 0, 0, 0);
			}

			_draw_cursor(p, &pt, FALSE);

			p->fpress = 1;
			p->ptcur = pt;
			mWidgetGrabPointer(M_WIDGET(p));
		}
	}
}

/** グラブ解除 */

static void _grab_release(_liblist_area *p)
{
	if(p->fpress)
	{
		_draw_cursor(p, &p->ptcur, TRUE);
	
		p->fpress = 0;
		mWidgetUngrabPointer(M_WIDGET(p));
	}
}

/** イベント */

static int _liblist_event_handle(mWidget *wg,mEvent *ev)
{
	_liblist_area *p = (_liblist_area *)wg;

	switch(ev->type)
	{
		case MEVENT_POINTER:
			if(ev->pt.type == MEVENT_POINTER_TYPE_PRESS)
			{
				//押し
				
				if(!p->fpress)
					_event_press(p, ev);
			}
			else if(ev->pt.type == MEVENT_POINTER_TYPE_RELEASE)
			{
				//離し

				if(ev->pt.btt == M_BTT_LEFT)
					_grab_release(p);
			}
			break;

		//ホイール
		case MEVENT_SCROLL:
			if(ev->scr.dir == MEVENT_SCROLL_DIR_UP
				|| ev->scr.dir == MEVENT_SCROLL_DIR_DOWN)
			{
				mScrollBarMovePos(p->scr,
					(ev->scr.dir == MEVENT_SCROLL_DIR_UP)? -1: 1);

				mWidgetUpdate(wg);
			}
			break;

		case MEVENT_FOCUS:
			if(ev->focus.bOut)
				_grab_release(p);
			break;
	}
	
	return 1;
}

/** 描画 */

static void _liblist_draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	_liblist_area *p = (_liblist_area *)wg;
	int cellw,cellh,libno,tno,num,x,y,ix;

	libno = APP_DRAW->tex.libno;
	cellw = APP_DRAW->tex.cellw;
	cellh = APP_DRAW->tex.cellh;

	num = TextureLibrary_getNum(libno);

	//背景

	mPixbufFillBox(pixbuf, 0, 0, wg->w, wg->h, _LIBLIST_COL_BKGND);

	//テクスチャ

	tno = p->scr->sb.pos * p->xnum;
	x = y = 1;

	for(ix = 0; tno < num; tno++)
	{
		TextureLibrary_drawImage(libno, tno, pixbuf,
			x, y, cellw - 1, cellh - 1);

		ix++;
		if(ix == p->xnum)
		{
			x = 1;
			y += cellh;
			ix = 0;
		}
		else
			x += cellw;
	}
}

/** サイズ変更時 */

static void _liblist_onsize_handle(mWidget *wg)
{
	_liblist_area *p = (_liblist_area *)wg;

	p->xnum = wg->w / APP_DRAW->tex.cellw;
	if(p->xnum <= 0) p->xnum = 1;

	_liblist_set_scroll(p);
}

/** D&D */

static int _liblist_dnd_handle(mWidget *wg,char **files)
{
	for(; *files; files++)
		DockTexture_addImage_toLibrary(*files, FALSE);

	_liblist_update((_liblist_area *)wg);

	return 1;
}

/** スクロールバー ハンドラ */

static void _scrollbar_handle(mScrollBar *bar,int pos,int flags)
{
	if(flags & MSCROLLBAR_N_HANDLE_F_CHANGE)
		mWidgetUpdate((mWidget *)bar->wg.param);
}


//==========================
// main
//==========================


/** ライブラリ一覧 作成
 *
 * [!] 領域部分のウィジェットが返る */

mWidget *DockTexture_librarylist_new(mWidget *parent,int id)
{
	mWidget *ct;
	_liblist_area *area;
	mScrollBar *sb;

	ct = mContainerCreate(parent, MCONTAINER_TYPE_HORZ, 0, 0, MLF_EXPAND_WH);
	ct->draw = NULL;

	//エリア作成

	area = (_liblist_area *)mWidgetNew(sizeof(_liblist_area), ct);

	area->wg.id = id;
	area->wg.fEventFilter |= MWIDGET_EVENTFILTER_POINTER | MWIDGET_EVENTFILTER_SCROLL;
	area->wg.fLayout = MLF_EXPAND_WH;
	area->wg.fState |= MWIDGET_STATE_ENABLE_DROP;
	area->wg.event = _liblist_event_handle;
	area->wg.draw = _liblist_draw_handle;
	area->wg.onSize = _liblist_onsize_handle;
	area->wg.onDND = _liblist_dnd_handle;

	//スクロールバー

	sb = mScrollBarNew(0, ct, MSCROLLBAR_S_VERT);

	area->scr = sb;

	sb->wg.param = (intptr_t)area;
	sb->wg.fLayout = MLF_EXPAND_H;
	sb->sb.handle = _scrollbar_handle;

	return (mWidget *)area;
}

/** 更新 */

void DockTexture_librarylist_update(mWidget *wg,mBool scr_top)
{
	_liblist_area *p = (_liblist_area *)wg;

	p->xnum = wg->w / APP_DRAW->tex.cellw;
	if(p->xnum <= 0) p->xnum = 1;

	//スクロール

	_liblist_set_scroll(p);

	if(scr_top)
		mScrollBarSetPos(p->scr, 0);

	//

	mWidgetUpdate(wg);
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * PressureWidget
 *
 * 筆圧補正操作ウィジェット
 *****************************************/

#include <math.h>

#include "mDef.h"
#include "mWidgetDef.h"
#include "mWidget.h"
#include "mPixbuf.h"
#include "mEvent.h"
#include "mSysCol.h"
#include "mUtilStr.h"

#include "PressureWidget.h"


//-------------------------

struct _PressureWidget
{
	mWidget wg;

	int value;
	mBool press;
};

//-------------------------


//========================
// sub
//========================


/** ポインタ位置から値セット */

static void _setvalue_bypos(PressureWidget *p,int x,int y)
{
	int len,val,max;

	x -= p->wg.w >> 1;
	y -= p->wg.w >> 1;

	//中心からの距離
	len = (int)(sqrt(x * x + y * y) + 0.5);

	max = p->wg.w - 10;
	if(len > max) len = max;

	//値

	if(x < 0 || y < 0)
		val = 100 - (100 - 1) * len / max;
	else
		val = (600 - 100) * len / max + 100;

	//中央に近ければ 100 に補正

	if(val >= 98 && val <= 102) val = 100;

	//変更

	if(val != p->value)
	{
		p->value = val;

		mWidgetUpdate(M_WIDGET(p));
	}
}

/** グラブ解除 */

static void _release_grab(PressureWidget *p)
{
	if(p->press)
	{
		p->press = FALSE;
		mWidgetUngrabPointer(M_WIDGET(p));
	}
}


//========================


/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	PressureWidget *p = (PressureWidget *)wg;

	switch(ev->type)
	{
		case MEVENT_POINTER:
			if(ev->pt.type == MEVENT_POINTER_TYPE_MOTION)
			{
				//移動

				if(p->press)
					_setvalue_bypos(p, ev->pt.x, ev->pt.y);
			}
			else if(ev->pt.type == MEVENT_POINTER_TYPE_PRESS)
			{
				//押し

				if(ev->pt.btt == M_BTT_LEFT && !p->press)
				{
					p->press = TRUE;
					mWidgetGrabPointer(wg);

					_setvalue_bypos(p, ev->pt.x, ev->pt.y);
				}
			}
			else if(ev->pt.type == MEVENT_POINTER_TYPE_RELEASE)
			{
				//離し
				
				if(ev->pt.btt == M_BTT_LEFT)
					_release_grab(p);
			}
			break;
		
		case MEVENT_FOCUS:
			if(ev->focus.bOut)
				_release_grab(p);
			break;
		default:
			return FALSE;
	}

	return TRUE;
}

/** 描画 */

static void _draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	PressureWidget *p = (PressureWidget *)wg;
	int size,size2,lasty,i,x,y;
	double gamma;
	mPixCol col;
	char m[16];

	size = wg->w;
	size2 = size - 1;

	//背景

	mPixbufFillBox(pixbuf, 0, 0, size, size, MSYSCOL(WHITE));

	//基準線

	mPixbufLine(pixbuf, 0, size2, size2, 0, mRGBtoPix(0x808080));

	//枠

	mPixbufBox(pixbuf, 0, 0, size, size, 0);

	//曲線

	col = mRGBtoPix(0x0000ff);

	gamma = p->value * 0.01;
	lasty = size2;

	for(i = 1; i < size; i++)
	{
		y = size2 - (int)(pow((double)i / size2, gamma) * size2 + 0.5);

		mPixbufLine(pixbuf, i - 1, lasty, i, y, col);

		lasty = y;
	}

	//数値

	i = mFloatIntToStr(m, p->value, 2);

	if(p->value <= 100)
		x = size - 1 - 5 * i, y = size - 4 - 7;
	else
		x = y = 4;

	mPixbufDrawNumber_5x7(pixbuf, x, y, m, 0);
}


//====================


/** 作成 */

PressureWidget *PressureWidget_new(mWidget *parent,int size,int val)
{
	PressureWidget *p;

	p = (PressureWidget *)mWidgetNew(sizeof(PressureWidget), parent);
	if(!p) return NULL;

	p->wg.fEventFilter |= MWIDGET_EVENTFILTER_POINTER;
	p->wg.event = _event_handle;
	p->wg.draw = _draw_handle;
	p->wg.hintW = size;
	p->wg.hintH = size;

	p->value = val;

	return p;
}

/** 値を取得 */

int PressureWidget_getValue(PressureWidget *p)
{
	return p->value;
}

/** 値をセット */

void PressureWidget_setValue(PressureWidget *p,int val)
{
	if(val != p->value)
	{
		p->value = val;
		mWidgetUpdate(M_WIDGET(p));
	}
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DockTexture
 *
 * [dock] テクスチャ
 *****************************************/

#include "mDef.h"
#include "mWindowDef.h"
#include "mWidget.h"
#include "mContainer.h"
#include "mTab.h"
#include "mButton.h"
#include "mArrowButton.h"
#include "mDockWidget.h"
#include "mEvent.h"
#include "mSysDialog.h"
#include "mMessageBox.h"
#include "mStr.h"
#include "mMenu.h"
#include "mTrans.h"

#include "defWidgets.h"
#include "defConfig.h"
#include "defDraw.h"
#include "defMacros.h"
#include "AppErr.h"

#include "DockObject.h"
#include "MainWindow.h"
#include "ImageBuf8.h"
#include "TextureLibrary.h"
#include "draw_main.h"

#include "trgroup.h"


//------------------------

#define _DOCKTEXTURE(p)  ((DockTexture *)(p))
#define _DT_PTR          ((DockTexture *)APP_WIDGETS->dockobj[DOCKWIDGET_TEXTURE]);


typedef struct
{
	DockObject obj;

	mTab *tab;
	mWidget *texbtt,
		*preview,
		*menubtt,
		*liblist;
}DockTexture;

//------------------------

enum
{
	WID_TAB = 100,
	WID_BTT_LOAD,
	WID_MENUBTT,
	WID_LIBLIST
};

enum
{
	TRID_ADD,
	TRID_LOAD_LIB,
	TRID_SAVE_LIB,
	TRID_EMPTY,
	TRID_OPTION,

	TRID_MES_EMPTY = 1000,
	TRID_OPTION_TITLE,
	TRID_OPTION_MES
};

//------------------------

/* DockTexture_widget.c */

mWidget *DockTexture_button_new(mWidget *parent);
mWidget *DockTexture_preview_new(mWidget *parent);

mWidget *DockTexture_librarylist_new(mWidget *parent,int id);
void DockTexture_librarylist_update(mWidget *wg,mBool scr_top);

//------------------------


//===========================
// コマンド
//===========================


/** カレントにテクスチャ画像読み込み */

mBool DockTexture_loadTexture(const char *filename)
{
	ImageBuf8 *img;
	int err;

	img = drawTexture_loadImage(filename, &err);
	if(!img)
	{
		MainWindow_apperr(err, NULL);
		return FALSE;
	}
	else
	{
		ImageBuf8_free(APP_DRAW->tex.curimg);

		APP_DRAW->tex.curimg = img;
		APP_DRAW->tex.save_curimg = TRUE;

		return TRUE;
	}
}

/** ファイルを開いてテクスチャ読み込み */

static void _cmd_load_texture(DockTexture *p)
{
	mStr str = MSTR_INIT;

	if(mSysDlgOpenFile(DockObject_getOwnerWindow((DockObject *)p),
		FILEFILTER_NORMAL_IMAGE, 0,
		APP_CONF->strTextureFileDir.buf, 0, &str))
	{
		//ディレクトリ記録
		mStrPathGetDir(&APP_CONF->strTextureFileDir, str.buf);
	
		if(DockTexture_loadTexture(str.buf))
			mWidgetUpdate(p->preview);
	
		mStrFree(&str);
	}
}

/** ライブラリに画像追加 */

mBool DockTexture_addImage_toLibrary(const char *filename,mBool puterr)
{
	ImageBuf8 *img;
	int err;
	mBool ret;

	img = drawTexture_loadImage(filename, &err);
	if(!img)
	{
		if(puterr) MainWindow_apperr(err, NULL);
		ret = FALSE;
	}
	else
	{
		ret = TextureLibrary_append(APP_DRAW->tex.libno, img);
	
		ImageBuf8_free(img);
	}

	return ret;
}

/** (複数)ファイルを開いてライブラリに画像追加 */

static void _cmd_append_library(DockTexture *p)
{
	mStr str = MSTR_INIT,str2 = MSTR_INIT;
	int param = 0;

	if(!mSysDlgOpenFile(DockObject_getOwnerWindow((DockObject *)p),
		FILEFILTER_NORMAL_IMAGE, 0,
		APP_CONF->strTextureFileDir.buf, MSYSDLG_OPENFILE_F_MULTI_SEL, &str))
		return;

	//ディレクトリ記録

	mStrGetSplitText(&APP_CONF->strTextureFileDir, str.buf, '\t', 0);

	//追加

	while(mStrPathExtractMultiFiles(&str2, str.buf, &param))
	{
		if(DockTexture_addImage_toLibrary(str2.buf, TRUE))
			DockTexture_librarylist_update(p->liblist, FALSE);
	}

	mStrFree(&str);
	mStrFree(&str2);
}

/** ライブラリ読込 */

static void _cmd_load_library(DockTexture *p)
{
	mStr str = MSTR_INIT;

	if(mSysDlgOpenFile(DockObject_getOwnerWindow((DockObject *)p),
		"Texture Library (*.apbt)\t*.apbt\tAll Files (*)\t*\t", 0,
		APP_CONF->strTextureFileDir.buf, 0, &str))
	{
		//ディレクトリ記録
		mStrPathGetDir(&APP_CONF->strTextureFileDir, str.buf);

		if(!TextureLibrary_loadFile(str.buf, APP_DRAW->tex.libno))
			MainWindow_apperr(APPERR_LOAD, NULL);

		DockTexture_librarylist_update(p->liblist, TRUE);
	
		mStrFree(&str);
	}
}

/** ライブラリ保存 */

static void _cmd_save_library(DockTexture *p)
{
	mStr str = MSTR_INIT;

	if(mSysDlgSaveFile(DockObject_getOwnerWindow((DockObject *)p),
		"Texture Library (*.apbt)\t*.apbt\t", 0,
		APP_CONF->strTextureFileDir.buf, 0, &str, NULL))
	{
		//ディレクトリ記録
		mStrPathGetDir(&APP_CONF->strTextureFileDir, str.buf);

		mStrPathSetExt(&str, "apbt");

		if(!TextureLibrary_saveFile(str.buf, APP_DRAW->tex.libno))
			MainWindow_apperr(APPERR_FAILED, NULL);
	
		mStrFree(&str);
	}
}

/** 表示幅の設定 */

static void _cmd_option(DockTexture *p)
{
	mStr str = MSTR_INIT;
	int n[2] = {30,30};

	mStrSetFormat(&str, "%d,%d", APP_DRAW->tex.cellw, APP_DRAW->tex.cellh);

	if(mSysDlgInputText(DockObject_getOwnerWindow((DockObject *)p),
		M_TR_T(TRID_OPTION_TITLE), M_TR_T(TRID_OPTION_MES),
		&str, MSYSDLG_INPUTTEXT_F_NOEMPTY))
	{
		mStrToIntArray_range(&str, n, 2, 0, 8, 512);

		APP_DRAW->tex.cellw = n[0];
		APP_DRAW->tex.cellh = n[1];

		DockTexture_librarylist_update(p->liblist, FALSE);
	}

	mStrFree(&str);
}


//===========================
// イベント
//===========================


/** ライブラリメニュー */

static void _run_libmenu(DockTexture *p)
{
	mMenu *menu;
	mMenuItemInfo *pi;
	int id;
	mBox box;
	uint16_t dat[] = {
		TRID_ADD, 0xfffe, TRID_LOAD_LIB, TRID_SAVE_LIB, 0xfffe,
		TRID_EMPTY, 0xfffe, TRID_OPTION, 0xffff
	};

	//メニュー

	M_TR_G(TRGROUP_DOCK_TEXTURE);

	menu = mMenuNew();

	mMenuAddTrArray16(menu, dat);

	mWidgetGetRootBox(p->menubtt, &box);

	pi = mMenuPopup(menu, NULL, box.x + box.w, box.y + box.h, MMENU_POPUP_F_RIGHT);
	id = (pi)? pi->id: -1; 

	mMenuDestroy(menu);

	if(id == -1) return;

	//コマンド

	switch(id)
	{
		//追加
		case TRID_ADD:
			_cmd_append_library(p);
			break;
		//ライブラリ読込
		case TRID_LOAD_LIB:
			_cmd_load_library(p);
			break;
		//ライブラリ保存
		case TRID_SAVE_LIB:
			_cmd_save_library(p);
			break;
		//ライブラリを空にする
		case TRID_EMPTY:
			if(mMessageBox(DockObject_getOwnerWindow((DockObject *)p),
				NULL, M_TR_T(TRID_MES_EMPTY), MMESBOX_YESNO, MMESBOX_YES) == MMESBOX_YES)
			{
				TextureLibrary_empty(APP_DRAW->tex.libno);
				DockTexture_librarylist_update(p->liblist, FALSE);
			}
			break;
		//表示幅の設定
		case TRID_OPTION:
			_cmd_option(p);
			break;
	}
}

/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	DockTexture *p = _DOCKTEXTURE(wg->param);

	if(ev->type == MEVENT_NOTIFY)
	{
		switch(ev->notify.id)
		{
			//ライブラリ一覧 (テクスチャ変更時)
			case WID_LIBLIST:
				APP_DRAW->tex.save_curimg = TRUE;
				
				mWidgetUpdate(p->preview);
				break;
			//テクスチャ読み込みボタン
			case WID_BTT_LOAD:
				_cmd_load_texture(p);
				break;
			//ライブラリメニュー
			case WID_MENUBTT:
				_run_libmenu(p);
				break;
			//タブ切り替え
			case WID_TAB:
				if(ev->notify.type == MTAB_N_CHANGESEL)
				{
					APP_DRAW->tex.libno = ev->notify.param1;

					DockTexture_librarylist_update(p->liblist, TRUE);
				}
				break;
		}
	}

	return 1;
}


//===========================
// 作成
//===========================


/** mDockWidget ハンドラ : 内容作成 */

static mWidget *_dock_func_create(mDockWidget *dockwg,int id,mWidget *parent)
{
	DockTexture *p = _DT_PTR;
	mWidget *ct_top,*ct;
	int i;
	char m[6] = {' ',' ','1',' ',' ',0};

	//コンテナ (トップ)

	ct_top = mContainerCreate(parent, MCONTAINER_TYPE_VERT, 0, 0, MLF_EXPAND_WH);

	ct_top->event = _event_handle;
	ct_top->notifyTarget = MWIDGET_NOTIFYTARGET_SELF;
	ct_top->param = (intptr_t)p;
	ct_top->margin.top = ct_top->margin.bottom = 4;

	//------ 上部

	ct = mContainerCreate(ct_top, MCONTAINER_TYPE_HORZ, 0, 5, MLF_EXPAND_W);

	mWidgetSetMargin_b4(ct, M_MAKE_DW4(2,0,2,5));

	p->texbtt = DockTexture_button_new(ct);
	p->preview = DockTexture_preview_new(ct);

	mButtonCreate(ct, WID_BTT_LOAD, MBUTTON_S_REAL_WH, 0, 0, "...");

	//------ ライブラリ

	ct = mContainerCreate(ct_top, MCONTAINER_TYPE_HORZ, 0, 3, MLF_EXPAND_W);

	//タブ

	p->tab = mTabCreate(ct, WID_TAB, MTAB_S_TOP, MLF_EXPAND_W, 0);

	for(i = 0; i < TEXTURE_LIBRARY_NUM; i++)
	{
		m[2] = '1' + i;
		mTabAddItemText(p->tab, m);
	}

	mTabSetSel_index(p->tab, APP_DRAW->tex.libno);

	//メニューボタン

	p->menubtt = (mWidget *)mArrowButtonCreate(ct, WID_MENUBTT,
		MARROWBUTTON_S_DOWN, 0, M_MAKE_DW4(0,0,3,2));

	//一覧

	p->liblist = DockTexture_librarylist_new(ct_top, WID_LIBLIST);

	return ct_top;
}

/** 作成 */

void DockTexture_new(mDockWidgetState *state)
{
	DockObject *p;

	p = DockObject_new(DOCKWIDGET_TEXTURE, sizeof(DockTexture), 0, MWINDOW_S_ENABLE_DND,
			state, _dock_func_create);

	mDockWidgetCreateWidget(p->dockwg);
}

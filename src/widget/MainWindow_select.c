/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * MainWindow
 *
 * 選択範囲のコマンド
 *****************************************/

#include "mDef.h"
#include "mStr.h"
#include "mSysDialog.h"
#include "mMenu.h"
#include "mTrans.h"

#include "defMacros.h"
#include "defDraw.h"
#include "defConfig.h"
#include "AppErr.h"

#include "TileImage.h"
#include "LayerItem.h"

#include "MainWindow.h"
#include "FileDialog.h"
#include "PopupThread.h"
#include "dialog_canvas.h"

#include "draw_select.h"

#include "trid_mainmenu.h"


//--------------------

mBool SelectAreaDlg_run(mWindow *owner);
mBool RotateDlg_run(mWindow *owner,int *pangle,int *ptype);

//--------------------


/** 拡大縮小 */

static void _cmd_scale(MainWindow *p)
{
	CanvasScaleInfo info;
	mBox box;
	int ret;

	drawSelect_getSelectBox(APP_DRAW, &box);

	info.w = box.w;
	info.h = box.h;
	info.have_dpi = FALSE;

	if(CanvasScaleDlg_run(M_WINDOW(p), &info)
		&& (info.w != box.w || info.h != box.h))
	{
		ret = drawSelect_scaling(APP_DRAW, &box, info.type,
			info.w, info.h);

		if(ret)
			MainWindow_apperr(ret, NULL);
	}
}

/** 回転 */

static void _cmd_rotate(MainWindow *p)
{
	int angle,type;

	if(RotateDlg_run(M_WINDOW(p), &angle, &type) && angle != 0)
	{
		drawSelect_rotate(APP_DRAW, type, angle);
	}
}

/** ファイルから貼り付け */

static void _cmd_paste_file(MainWindow *p)
{
	mStr str = MSTR_INIT;
	int err;

	if(FileDialog_openImage(M_WINDOW(p),
		FILEFILTER_NORMAL_IMAGE, APP_CONF->strSelectFileDir.buf, &str))
	{
		//フォルダ記録

		mStrPathGetDir(&APP_CONF->strSelectFileDir, str.buf);

		//読み込み & 貼り付け

		err = drawSelect_pasteFromFile(APP_DRAW, str.buf,
			APP_CONF->openimage_flags & CONFIG_OPENIMAGE_F_IGNORE_ALPHA,
			APP_CONF->openimage_flags & CONFIG_OPENIMAGE_F_LEAVE_TRANS);

		if(err != APPERR_OK)
			MainWindow_apperr(err, NULL);
	}

	mStrFree(&str);
}

//==============================
// ファイルに出力
//==============================


typedef struct
{
	const char *filename;
	mBox box;
}_thdata_savefile;


/** [スレッド] ファイルに出力 */

static int _thread_save_file(mPopupProgress *prog,void *data)
{
	_thdata_savefile *p = (_thdata_savefile *)data;

	return TileImage_savePNG_rgba(APP_DRAW->curlayer->img,
			p->filename, APP_DRAW->imgdpi, &p->box, prog);
}

/** ファイルに出力 */

static void _cmd_save_file(MainWindow *p)
{
	mStr str = MSTR_INIT;
	_thdata_savefile dat;

	//フォルダは除く

	if(LAYERITEM_IS_FOLDER(APP_DRAW->curlayer)) return;

	//初期ファイル名 (レイヤ名から)

	mStrSetText(&str, APP_DRAW->curlayer->name);
	mStrPathReplaceDisableChar(&str, '_');

	//ファイル名取得

	if(mSysDlgSaveFile(M_WINDOW(p),
		"PNG file (*.png)\t*.png",
		0, APP_CONF->strSelectFileDir.buf, 0, &str, NULL))
	{
		//拡張子

		mStrPathSetExt(&str, "png");

		//ディレクトリ記録

		mStrPathGetDir(&APP_CONF->strSelectFileDir, str.buf);

		//保存

		dat.filename = str.buf;

		drawSelect_getSelectBox(APP_DRAW, &dat.box);

		if(PopupThread_run(&dat, _thread_save_file) != 1)
			MainWindow_apperr(APPERR_FAILED, NULL);
	}

	mStrFree(&str);
}


//=========================
//
//=========================


/** コマンド処理 */

void MainWindow_selectcmd(MainWindow *p,int id)
{
	mBool seltool = (APP_DRAW->tool.no == TOOL_SELECT);

	//(他ツール時も含む) 貼付けモード中は実行しない

	if(APP_DRAW->sel.mode) return;

	/* [!] 選択ツール中のみ実行、またはいつでも実行可能かに注意。 */

	switch(id)
	{
		//解除 (選択ツール時のみ)
		case TRMENU_SEL_RELEASE:
			if(seltool)
				drawSelect_setSelectBox(APP_DRAW, NULL);
			break;
		//すべて選択 (選択ツール時のみ)
		case TRMENU_SEL_ALL:
			if(seltool)
				drawSelect_allSelect(APP_DRAW);
			break;
		//座標指定
		case TRMENU_SEL_SETAREA:
			if(seltool)
				SelectAreaDlg_run(M_WINDOW(p));
			break;

		//コピー
		case TRMENU_SEL_COPY:
			if(seltool)
				drawSelect_copy_and_cut(APP_DRAW, FALSE);
			break;
		//切り取り
		case TRMENU_SEL_CUT:
			if(seltool)
				drawSelect_copy_and_cut(APP_DRAW, TRUE);
			break;
		//貼付け
		case TRMENU_SEL_PASTE:
			drawSelect_paste(APP_DRAW);
			break;

		//塗りつぶし
		case TRMENU_SEL_FILL:
			drawSelect_fill(APP_DRAW);
			break;
		//消去
		case TRMENU_SEL_ERASE:
			drawSelect_erase(APP_DRAW);
			break;
		//トリミング
		case TRMENU_SEL_TRIM:
			drawSelect_trim(APP_DRAW);
			break;

		//タイル状に並べる
		case TRMENU_SEL_ARRANGE_FULL:
		case TRMENU_SEL_ARRANGE_HORZ:
		case TRMENU_SEL_ARRANGE_VERT:
			if(seltool)
				drawSelect_arrange_tile(APP_DRAW, id - TRMENU_SEL_ARRANGE_FULL);
			break;

		//ファイルから貼付け
		case TRMENU_SEL_FILE_PASTE:
			_cmd_paste_file(p);
			break;
		//ファイルに出力
		case TRMENU_SEL_FILE_SAVE:
			_cmd_save_file(p);
			break;

		//左右反転
		case TRMENU_SEL_REV_HORZ:
			drawSelect_reverse_and_rotate90(APP_DRAW, 0);
			break;
		//上下反転
		case TRMENU_SEL_REV_VERT:
			drawSelect_reverse_and_rotate90(APP_DRAW, 1);
			break;
		//左に90度回転
		case TRMENU_SEL_ROTATE_LEFT:
			drawSelect_reverse_and_rotate90(APP_DRAW, 2);
			break;
		//右に90度回転
		case TRMENU_SEL_ROTATE_RIGHT:
			drawSelect_reverse_and_rotate90(APP_DRAW, 3);
			break;
		//拡大縮小
		case TRMENU_SEL_SCALE:
			_cmd_scale(p);
			break;
		//回転
		case TRMENU_SEL_ROTATE:
			_cmd_rotate(p);
			break;
	}
}

/** メニューのポップアップ表示時の有効/無効セット */

void MainWindow_popup_select(MainWindow *p,mMenu *menu)
{
	int i,notselmode,seltool,havesel;
	uint16_t id_seltool[] = {
		TRMENU_SEL_ALL, TRMENU_SEL_SETAREA, 0
	},
	id_always[] = {
		TRMENU_SEL_FILL, TRMENU_SEL_ERASE,
		TRMENU_SEL_FILE, TRMENU_SEL_REV_HORZ, TRMENU_SEL_REV_VERT,
		TRMENU_SEL_ROTATE_LEFT, TRMENU_SEL_ROTATE_RIGHT,
		TRMENU_SEL_SCALE, TRMENU_SEL_ROTATE, 0 
	},
	id_havesel[] = {
		TRMENU_SEL_RELEASE, TRMENU_SEL_COPY, TRMENU_SEL_CUT, TRMENU_SEL_TRIM,
		TRMENU_SEL_ARRANGE, 0
	};

	notselmode = (APP_DRAW->sel.mode == 0);
	seltool = (notselmode && APP_DRAW->tool.no == TOOL_SELECT);
	havesel = (seltool && APP_DRAW->sel.boxsel.w);

	//選択範囲ツール時のみ

	for(i = 0; id_seltool[i]; i++)
		mMenuSetEnable(menu, id_seltool[i], seltool);

	//いつでも可能

	for(i = 0; id_always[i]; i++)
		mMenuSetEnable(menu, id_always[i], notselmode);

	//範囲がある時のみ

	for(i = 0; id_havesel[i]; i++)
		mMenuSetEnable(menu, id_havesel[i], havesel);

	//貼り付け (貼り付けイメージがある)

	mMenuSetEnable(menu, TRMENU_SEL_PASTE, (notselmode && APP_DRAW->sel.img_selcopy));
}

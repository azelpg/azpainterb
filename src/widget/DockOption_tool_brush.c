/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************************
 * [dock]オプションのタブ内容処理
 * <ツール - ブラシ関連>
 ************************************************/

#include <string.h>

#include "mDef.h"
#include "mContainerDef.h"
#include "mWidget.h"
#include "mContainer.h"
#include "mLabel.h"
#include "mButton.h"
#include "mComboBox.h"
#include "mEvent.h"
#include "mPixbuf.h"
#include "mSysCol.h"
#include "mTrans.h"
#include "mUtilStr.h"

#include "defDraw.h"

#include "ValueBar.h"
#include "SelectBrush.h"
#include "DockOption_sub.h"

#include "BrushList.h"
#include "BrushItem.h"

#include "trgroup.h"
#include "trid_word.h"


//---------------------

typedef struct
{
	mWidget wg;
	mContainerData ct;

	int listno;

	ValueBar *bar_radius,
		*bar_opa,
		*bar_itv;
	mComboBox *cb_stype,
		*cb_sstr,
		*cb_water;
}_tab_brush;

//---------------------

enum
{
	WID_SELBRUSH = 100,
	WID_BTT_SAVE,
	WID_BTT_DETAIL,
	WID_BAR_RADIUS,
	WID_BAR_OPACITY,
	WID_BAR_INTERVAL,
	WID_CB_SMOOTHING_TYPE,
	WID_CB_SMOOTHING_STR,
	WID_CB_WATER
};

enum
{
	TRID_RADIUS,
	TRID_SMOOTHING,
	TRID_WATER,
	TRID_INTERVAL,
	TRID_DETAIL,

	TRID_SMOOTHING_TYPE_TOP = 100
};

//---------------------

mBool BrushDetailDlg_run(mWindow *owner,int listno);

//---------------------


//============================
// 保存ボタン
//============================


/** サイズ計算 */

static void _savebtt_calchint(mWidget *wg)
{
	wg->hintW = wg->hintH = 12 + 3 * 2;
}

/** 描画 */

static void _savebtt_draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	const uint8_t img[] =
	{
		0xff,0x0f,0x0f,0x0f,0xff,0x0f,0xff,0x0f,0xff,0x0f,0xff,0x0f,0x07,0x0e,0x07,0x0e,
		0x07,0x0e,0x07,0x0e,0x07,0x0e,0xff,0x0f
	};
	mBool press;

	press = mButtonIsPressed(M_BUTTON(wg));

	mButtonDrawBase(M_BUTTON(wg), pixbuf, press);

	mPixbufBltFrom1bit(pixbuf, 3 + press, 3 + press,
		img, 0, 0, 12, 12, 12,
		0, MSYSCOL(WHITE));
}


//============================
// タブ内容
//============================


/** 半径の範囲を変更 */

static void _set_radius_range(_tab_brush *p)
{
	BrushItem *item;
	int min,max,val;

	item = BrushList_getEditItem(p->listno);

	min = item->radius_min;
	max = item->radius_max;
	val = item->radius;

	if(min == 0) min = BRUSHITEM_RADIUS_MIN;
	if(max == 0) max = BRUSHITEM_RADIUS_MAX;

	if(val < min)
		val = min;
	else if(val > max)
		val = max;

	ValueBar_setStatus(p->bar_radius, min, max, val);

	//半径変更

	item->radius = val;
}

/** 値をセット */

static void _set_item_value(_tab_brush *p)
{
	int no = p->listno,min,max;
	BrushItem *item;

	item = BrushList_getEditItem(no);

	//半径

	min = item->radius_min;
	max = item->radius_max;

	if(min == 0) min = BRUSHITEM_RADIUS_MIN;
	if(max == 0) max = BRUSHITEM_RADIUS_MAX;

	ValueBar_setStatus(p->bar_radius, min, max, item->radius);

	//濃度

	ValueBar_setPos(p->bar_opa, item->opacity);

	//補正

	if(no == BRUSHLIST_PEN)
	{
		mComboBoxSetSel_index(p->cb_stype, item->smoothing_type);
		mComboBoxSetSel_index(p->cb_sstr, item->smoothing_str - 1);
	}

	//水彩

	if(no == BRUSHLIST_WATER)
		mComboBoxSetSel_index(p->cb_water, item->water_type);

	//間隔

	if(no == BRUSHLIST_DODGE || no == BRUSHLIST_BURN)
		ValueBar_setPos(p->bar_itv, item->interval);
}

/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		_tab_brush *p = (_tab_brush *)wg;
	
		switch(ev->notify.id)
		{
			//半径
			case WID_BAR_RADIUS:
				if(ev->notify.param2)
					BrushList_update_radius(p->listno, ev->notify.param1);
				break;
			//濃度
			case WID_BAR_OPACITY:
				if(ev->notify.param2)
					BrushList_update_opacity(p->listno, ev->notify.param1);
				break;
			//補正タイプ
			case WID_CB_SMOOTHING_TYPE:
				if(ev->notify.type == MCOMBOBOX_N_CHANGESEL)
					BrushList_update_smoothing_type(p->listno, ev->notify.param2);
				break;
			//補正強さ
			case WID_CB_SMOOTHING_STR:
				if(ev->notify.type == MCOMBOBOX_N_CHANGESEL)
					BrushList_update_smoothing_str(p->listno, ev->notify.param2);
				break;
			//水彩
			case WID_CB_WATER:
				if(ev->notify.type == MCOMBOBOX_N_CHANGESEL)
					BrushList_update_water_type(p->listno, ev->notify.param2);
				break;
			//間隔
			case WID_BAR_INTERVAL:
				if(ev->notify.param2)
					BrushList_update_interval(p->listno, ev->notify.param1);
				break;

			//ブラシ選択 (選択変更時)
			case WID_SELBRUSH:
				_set_item_value(p);
				break;
	
			//保存
			case WID_BTT_SAVE:
				BrushList_saveItemValue(p->listno);
				break;
			//詳細設定
			case WID_BTT_DETAIL:
				if(BrushDetailDlg_run(p->wg.toplevel, p->listno))
				{
					//半径の範囲変更

					_set_radius_range(p);
				}
				break;
		}
	}

	return 1;
}


/** ラベル作成 */

static void _create_label(mWidget *parent,int trid)
{
	mLabelCreate(parent, 0, MLF_MIDDLE | MLF_RIGHT, 0,
		(trid < 0)? M_TR_T2(TRGROUP_WORD, -trid): M_TR_T(trid));
}

/** 作成 */

mWidget *DockOption_createTab_tool_brush(mWidget *parent)
{
	_tab_brush *p;
	mWidget *ct,*ct2,*wg;
	mComboBox *cb;
	int tool,listno,i;
	char m[32];

	tool = APP_DRAW->tool.no;
	listno = BrushList_getListNo_fromTool(tool);

	//

	p = (_tab_brush *)DockOption_createMainContainer(sizeof(_tab_brush),
		parent, _event_handle);

	p->listno = listno;

	//----- 選択と保存ボタン

	ct = mContainerCreate(M_WIDGET(p), MCONTAINER_TYPE_HORZ, 0, 6, MLF_EXPAND_W | MLF_MIDDLE);
	ct->margin.bottom = 3;

	//選択

	SelectBrush_new(ct, WID_SELBRUSH, listno, BrushList_getSelectItem(listno));

	//保存ボタン

	wg = (mWidget *)mButtonCreate(ct, WID_BTT_SAVE, MLF_MIDDLE, 0, 0, NULL);
	wg->calcHint = _savebtt_calchint;
	wg->draw = _savebtt_draw_handle;

	//----- パラメータ

	M_TR_G(TRGROUP_DOCKOPT_BRUSH);

	ct = mContainerCreateGrid(M_WIDGET(p), 2, 4, 5, MLF_EXPAND_W);

	//半径

	_create_label(ct, TRID_RADIUS);

	p->bar_radius = ValueBar_new(ct, WID_BAR_RADIUS, MLF_EXPAND_W | MLF_MIDDLE, 1,
		BRUSHITEM_RADIUS_MIN, BRUSHITEM_RADIUS_MAX, BRUSHITEM_RADIUS_MIN);

	//濃度 or 強さ

	_create_label(ct, (tool == TOOL_BLUR)? -TRID_WORD_STRENGTH: -TRID_WORD_DENSITY);

	p->bar_opa = ValueBar_new(ct, WID_BAR_OPACITY, MLF_EXPAND_W | MLF_MIDDLE, 0,
		1, 255, 255);

	//[ペン] 補正

	if(tool == TOOL_PEN)
	{
		_create_label(ct, TRID_SMOOTHING);
	
		ct2 = mContainerCreate(ct, MCONTAINER_TYPE_HORZ, 0, 5, MLF_EXPAND_W);

		//タイプ

		cb = mComboBoxCreate(ct2, WID_CB_SMOOTHING_TYPE, 0, 0, 0);

		p->cb_stype = cb;

		mComboBoxAddTrItems(cb, 4, TRID_SMOOTHING_TYPE_TOP, 0);
		mComboBoxSetWidthAuto(cb);

		//強さ

		cb = mComboBoxCreate(ct2, WID_CB_SMOOTHING_STR, 0, MLF_EXPAND_W, 0);

		p->cb_sstr = cb;

		for(i = 1; i <= BRUSHITEM_SMOOTHING_STR_MAX; i++)
		{
			mIntToStr(m, i);
			mComboBoxAddItem(cb, m, i);
		}
	}

	//[覆い焼き/焼き込み] 間隔

	if(tool == TOOL_DODGE || tool == TOOL_BURN)
	{
		_create_label(ct, TRID_INTERVAL);

		p->bar_itv = ValueBar_new(ct, WID_BAR_INTERVAL, MLF_EXPAND_W | MLF_MIDDLE, 2,
			BRUSHITEM_INTERVAL_MIN, BRUSHITEM_INTERVAL_MAX, BRUSHITEM_INTERVAL_MIN);
	}

	//[水彩] 水彩タイプ

	if(tool == TOOL_WATER)
	{
		_create_label(ct, TRID_WATER);
	
		cb = mComboBoxCreate(ct, WID_CB_WATER, 0, MLF_EXPAND_W, 0);

		p->cb_water = cb;

		strcpy(m, "typeA");

		for(i = 0; i < 5; i++)
		{
			m[4] = 'A' + i;
			mComboBoxAddItem(cb, m, i);
		}
	}

	//詳細設定

	mButtonCreate(M_WIDGET(p), WID_BTT_DETAIL, 0, MLF_RIGHT, M_MAKE_DW4(0,3,0,0),
		M_TR_T(TRID_DETAIL));

	//-----------

	//値セット

	_set_item_value(p);

	return (mWidget *)p;
}

/** 外部からの半径値変更時 */

void DockOption_toolBrush_changeRadius(mWidget *wg,int radius)
{
	ValueBar_setPos(((_tab_brush *)wg)->bar_radius, radius);
}

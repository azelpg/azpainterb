/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * MainWindow
 *
 * ファイル関連コマンド
 *****************************************/

#include <string.h>

#include "mDef.h"
#include "mStr.h"
#include "mUtilFile.h"
#include "mMenu.h"
#include "mIconButtons.h"
#include "mMessageBox.h"
#include "mSysDialog.h"
#include "mTrans.h"

#include "defMacros.h"
#include "defDraw.h"
#include "defConfig.h"
#include "defMainWindow.h"
#include "defFileFormat.h"
#include "AppErr.h"

#define LOADERR_DEFINE
#include "defLoadErr.h"

#include "defMainWindow.h"
#include "MainWindow.h"
#include "MainWindow_pv.h"

#include "FileDialog.h"
#include "PopupThread.h"
#include "Undo.h"

#include "draw_main.h"
#include "draw_file.h"

#include "trgroup.h"
#include "trid_mainmenu.h"
#include "trid_message.h"


//----------------------

mBool NewImageDialog_run(mWindow *owner,mSize *size,int *dpi);
mBool SaveOptionDlg_run(mWindow *owner,int format);

//----------------------



//============================
// FileFormat
//============================


/** ヘッダからファイルフォーマット取得 */

int FileFormat_getbyFileHeader(const char *filename)
{
	uint8_t d[8],png[8] = {0x89,0x50,0x4e,0x47,0x0d,0x0a,0x1a,0x0a};

	if(mReadFileHead(filename, d, 8))
	{
		if(memcmp(d, "AZPDATA", 7) == 0)
		{
			//APD

			if(d[7] < 2)
				return FILEFORMAT_APD_v1v2;
			else if(d[7] == 2)
				return FILEFORMAT_APD_v3;
			else
				return FILEFORMAT_UNKNOWN;
		}
		else if(memcmp(d, "AZDWDAT", 7) == 0)
			return FILEFORMAT_ADW;

		else if(memcmp(d, "8BPS", 4) == 0)
			return FILEFORMAT_PSD;

		else if(memcmp(d, png, 8) == 0)
			return FILEFORMAT_PNG;

		else if(d[0] == 0xff && d[1] == 0xd8)
			return FILEFORMAT_JPEG;

		else if(d[0] == 'G' && d[1] == 'I' && d[2] == 'F')
			return FILEFORMAT_GIF;

		else if(d[0] == 'B' && d[1] == 'M')
			return FILEFORMAT_BMP;
	}

	return FILEFORMAT_UNKNOWN;
}

/** 指定フォーマットが BMP などの通常イメージか */

mBool FileFormat_isNormalImage(int format)
{
	return (format != FILEFORMAT_UNKNOWN && format < FILEFORMAT_NOT_NORMAL_TOP);
}

/** APD フォーマットか */

mBool FileFormat_isAPD(int format)
{
	return (format == FILEFORMAT_APD
		|| format == FILEFORMAT_APD_v1v2 || format == FILEFORMAT_APD_v3);
}


//============================
// sub
//============================


/** 開く/保存ダイアログ時の初期ディレクトリ取得 */

static void _get_filedialog_dir(MainWindow *p,mStr *strdst,int recentno,mBool save)
{
	if(recentno == MAINWINDOW_RECENTNO_EDITFILE_DIR)
		//編集中のファイルと同じディレクトリ
		mStrPathGetDir(strdst, p->strFilename.buf);
	else if(save)
		mStrCopy(strdst, APP_CONF->strRecentSaveDir + recentno);
	else
		mStrCopy(strdst, APP_CONF->strRecentOpenDir + recentno);
}

/** 最近使ったファイルに追加 */

static void _add_recent_file(MainWindow *p,const char *filename)
{
	mStrArrayAddRecent(APP_CONF->strRecentFile, CONFIG_RECENTFILE_NUM, filename, TRUE);

	//メニュー

	MainWindow_setRecentFileMenu(p);
}

/** ファイル履歴のメニューをセット */

void MainWindow_setRecentFileMenu(MainWindow *p)
{
	mMenu *menu = p->menu_recentfile;

	//削除

	mMenuDeleteAll(menu);

	//ファイル

	mMenuSetStrArray(menu, MAINWINDOW_CMDID_RECENTFILE,
		APP_CONF->strRecentFile, CONFIG_RECENTFILE_NUM);

	//消去

	if(!mStrIsEmpty(APP_CONF->strRecentFile))
	{
		mMenuAddSep(menu);

		mMenuAddText_static(menu, TRMENU_FILE_RECENTFILE_CLEAR,
			M_TR_T2(TRGROUP_MAINMENU, TRMENU_FILE_RECENTFILE_CLEAR));
	}
}

/** 開く/保存ドロップメニューにディレクトリリスト追加 */

static void _add_dropmenu_dirlist(MainWindow *p,mMenu *menu,mBool save)
{
	mStr str = MSTR_INIT,*str_array;

	str_array = (save)? APP_CONF->strRecentSaveDir: APP_CONF->strRecentOpenDir;

	//編集中のファイルと同じディレクトリ

	if(!mStrIsEmpty(&p->strFilename))
	{
		mStrPathGetDir(&str, p->strFilename.buf);
	
		mMenuAddText_copy(menu, MAINWINDOW_RECENTNO_EDITFILE_DIR, str.buf);

		mStrFree(&str);

		if(!mStrIsEmpty(str_array))
			mMenuAddSep(menu);
	}

	//履歴

	mMenuAddStrArray(menu, 0, str_array, CONFIG_RECENTDIR_NUM);
}

/** ツールバーの開く/保存ドロップメニュー
 *
 * @return 選択された履歴の番号。-1 でキャンセル、1000 で編集中のファイルと同じディレクトリ。 */

int MainWindow_runMenu_toolbarDrop_opensave(MainWindow *p,mBool save)
{
	mMenu *menu;
	mMenuItemInfo *mi;
	mBox box;
	int no;

	menu = mMenuNew();

	_add_dropmenu_dirlist(p, menu, save);

	if(mMenuGetNum(menu) == 0)
		//項目が一つもない時
		no = -1;
	else
	{
		mIconButtonsGetItemBox(M_ICONBUTTONS(p->toolbar),
			(save)? TRMENU_FILE_SAVE_AS: TRMENU_FILE_OPEN, &box, TRUE);

		mi = mMenuPopup(menu, NULL, box.x, box.y + box.h, 0);
		no = (mi)? mi->id: -1;
	}

	mMenuDestroy(menu);

	return no;
}

/** 複製保存のドロップメニュー
 *
 * @return 選択された履歴の番号。-1 でキャンセル、1000 で編集中のファイルと同じディレクトリ。 */

int MainWindow_runMenu_toolbarDrop_savedup(MainWindow *p)
{
	mMenu *menu,*sub;
	mMenuItemInfo *mi;
	mBox box;
	int no,i;
	static const char *format_str[] = {
		"APD (AzPainter)", "PSD (PhotoShop)", "PNG", "JPEG", "GIF", "BMP"
	};
	uint8_t format_ar[] = {
		0, FILEFORMAT_APD, FILEFORMAT_PSD, FILEFORMAT_PNG, FILEFORMAT_JPEG,
		FILEFORMAT_GIF, FILEFORMAT_BMP
	};

	M_TR_G(TRGROUP_DROPMENU_SAVEDUP);

	//保存形式サブメニュー

	sub = mMenuNew();

	mMenuAddNormal(sub, 10000, M_TR_T(1), 0, MMENUITEM_F_RADIO);

	for(i = 0; i < 6; i++)
		mMenuAddNormal(sub, 10001 + i, format_str[i], 0, MMENUITEM_F_RADIO);

	for(i = 0; i < 7; i++)
	{
		if(APP_CONF->savedup_format == format_ar[i])
		{
			mMenuSetCheck(sub, 10000 + i, 1);
			break;
		}
	}

	//main

	menu = mMenuNew();

	mMenuAddSubmenu(menu, 100, M_TR_T(0), sub);
	mMenuAddSep(menu);

	_add_dropmenu_dirlist(p, menu, TRUE);
	
	//

	mIconButtonsGetItemBox(M_ICONBUTTONS(p->toolbar),
		TRMENU_FILE_SAVE_DUP, &box, TRUE);

	mi = mMenuPopup(menu, NULL, box.x, box.y + box.h, 0);
	no = (mi)? mi->id: -1;

	mMenuDestroy(menu);

	//保存形式変更

	if(no >= 10000)
	{
		APP_CONF->savedup_format = format_ar[no - 10000];
		return -1;
	}

	return no;
}

/** ツールバーのファイル履歴メニュー */

void MainWindow_runMenu_toolbar_recentfile(MainWindow *p)
{
	mBox box;

	//履歴が一つもない (先頭が空)

	if(mStrIsEmpty(APP_CONF->strRecentFile)) return;

	//メニュー (COMMAND イベントを送る)

	mIconButtonsGetItemBox(M_ICONBUTTONS(p->toolbar),
		TRMENU_FILE_RECENTFILE, &box, TRUE);

	mMenuPopup(p->menu_recentfile, M_WIDGET(p), box.x, box.y + box.h, 0);
}


//============================
// 新規作成
//============================


/** 新規作成 */

void MainWindow_newImage(MainWindow *p)
{
	mSize size;
	int dpi;

	//ダイアログ

	if(!NewImageDialog_run(M_WINDOW(p), &size, &dpi))
		return;

	//保存確認

	if(!MainWindow_confirmSave(p)) return;

	//新規

	if(!drawImage_new(APP_DRAW, size.w, size.h, dpi, TRUE))
	{
		MainWindow_apperr(APPERR_ALLOC, NULL);
		
		drawImage_new(APP_DRAW, 300, 300, dpi, TRUE);
	}

	//更新

	MainWindow_updateNewCanvas(p, "");
}


//============================
// ファイルを開く
//============================


typedef struct
{
	const char *filename;
	char *errmes;	//確保されたエラー文字列
	int format,
		loaderr;
}_thread_openfileinfo;


/** 読み込みスレッド処理 */

static int _thread_load(mPopupProgress *prog,void *data)
{
	_thread_openfileinfo *p = (_thread_openfileinfo *)data;
	int err = APPERR_LOAD;

	switch(p->format)
	{
		//APD v3
		case FILEFORMAT_APD_v3:
			if(drawFile_load_apd_v3(APP_DRAW, p->filename, prog))
				err = APPERR_OK;
			break;
	
		//ADW/APD v1,v2
		case FILEFORMAT_ADW:
		case FILEFORMAT_APD_v1v2:
			if(p->format == FILEFORMAT_ADW)
				err = drawFile_load_adw(p->filename, prog);
			else
				err = drawFile_load_apd_v1_v2(p->filename, prog);

			if(err == LOADERR_OK)
				err = APPERR_OK;
			else
			{
				p->loaderr = err;
				err = APPERR_LOAD;
			}
			break;

		//PSD
		case FILEFORMAT_PSD:
			if(drawFile_load_psd(APP_DRAW, p->filename, prog, &p->errmes))
				err = APPERR_OK;
			break;

		//画像ファイル
		default:
			err = drawImage_loadFile(APP_DRAW, p->filename, p->format,
				APP_CONF->openimage_flags & CONFIG_OPENIMAGE_F_IGNORE_ALPHA,
				APP_CONF->openimage_flags & CONFIG_OPENIMAGE_F_LEAVE_TRANS,
				prog, &p->errmes);
			break;
	}

	return err;
}

/** ファイルを開く (ダイアログ)
 *
 * @param recentno  ディレクトリ履歴番号 (0 で最新の履歴) */

void MainWindow_openFile(MainWindow *p,int recentno)
{
	mStr str = MSTR_INIT,strdir = MSTR_INIT;
	int ret;

	//ファイル名

	_get_filedialog_dir(p, &strdir, recentno, FALSE);

	ret = FileDialog_openImage(M_WINDOW(p),
		"Image Files (ADW/APD/PSD/BMP/PNG/JPEG/GIF)\t*.adw;*.apd;*.psd;*.bmp;*.png;*.jpg;*.jpeg;*.gif\t"
		"AzPainter File (*.apd)\t*.apd\t"
		"All Files\t*",
		strdir.buf, &str);

	mStrFree(&strdir);

	if(!ret) return;

	//保存確認後、読み込み

	if(MainWindow_confirmSave(p))
		MainWindow_loadImage(p, str.buf);

	mStrFree(&str);
}

/** 画像読み込み処理 */

mBool MainWindow_loadImage(MainWindow *p,const char *filename)
{
	_thread_openfileinfo dat;
	int err,format;

	//ヘッダからフォーマット取得

	format = FileFormat_getbyFileHeader(filename);

	if(format == FILEFORMAT_UNKNOWN)
	{
		MainWindow_apperr(APPERR_UNSUPPORTED_FORMAT, NULL);
		return FALSE;
	}

	//読み込み

	dat.filename = filename;
	dat.format = format;
	dat.errmes = NULL;
	dat.loaderr = LOADERR_OK;

	err = PopupThread_run(&dat, _thread_load);
	if(err == -1) return FALSE;

	//結果

	if(err == APPERR_OK)
	{
		//---- 成功

		mStr str = MSTR_INIT;

		//フォーマット

		if(FileFormat_isAPD(format))
			format = FILEFORMAT_APD;

		p->fileformat = format;

		//ファイル履歴追加
		/* filename が履歴内の文字列の場合があるので、以降
		 * ファイル名を参照する場合は、ファイル履歴の先頭を参照すること。 */

		_add_recent_file(p, filename);

		//ディレクトリ履歴

		mStrPathGetDir(&str, APP_CONF->strRecentFile[0].buf);

		mStrArrayAddRecent(APP_CONF->strRecentOpenDir, CONFIG_RECENTDIR_NUM, str.buf, TRUE);

		mStrFree(&str);

		//更新

		MainWindow_updateNewCanvas(p, APP_CONF->strRecentFile[0].buf);

		return TRUE;
	}
	else
	{
		//---- 失敗

		//エラーメッセージ

		if(dat.errmes)
		{
			MainWindow_apperr(err, dat.errmes);
			mFree(dat.errmes);
		}
		else if(dat.loaderr != LOADERR_OK)
			MainWindow_apperr(err, g_loaderr_str[dat.loaderr]);

		/* レイヤが一つもなければ新規作成。
		 * カレントレイヤが指定されていなければ途中まで読み込まれた */

		if(drawImage_onLoadError(APP_DRAW))
			MainWindow_updateNewCanvas(p, filename);
		   
		return FALSE;
	}
}


//============================
// ファイル保存 sub
//============================


enum
{
	FILETYPE_APD,
	FILETYPE_PSD,
	FILETYPE_PNG,
	FILETYPE_JPEG,
	FILETYPE_GIF,
	FILETYPE_BMP
};


/** フォーマットから拡張子セット */

static void _save_set_ext(mStr *str,int format)
{
	const char *ext = NULL;

	switch(format)
	{
		case FILEFORMAT_APD:
		case FILEFORMAT_APD_v1v2:
		case FILEFORMAT_APD_v3:
			ext = "apd";
			break;
		case FILEFORMAT_PSD:
			ext = "psd";
			break;
		case FILEFORMAT_PNG:
			ext = "png";
			break;
		case FILEFORMAT_JPEG:
			ext = "jpg";
			break;
		case FILEFORMAT_GIF:
			ext = "gif";
			break;
		case FILEFORMAT_BMP:
			ext = "bmp";
			break;
	}

	if(ext)
		mStrPathSetExt(str, ext);
}

/** フォーマット番号をダイアログの種類番号に変換 */

static int _save_format_to_type(int format)
{
	switch(format)
	{
		case FILEFORMAT_APD:
		case FILEFORMAT_APD_v1v2:
		case FILEFORMAT_APD_v3:
			return FILETYPE_APD;
		case FILEFORMAT_PSD:
			return FILETYPE_PSD;
		case FILEFORMAT_PNG:
			return FILETYPE_PNG;
		case FILEFORMAT_JPEG:
			return FILETYPE_JPEG;
		case FILEFORMAT_GIF:
			return FILETYPE_GIF;
		case FILEFORMAT_BMP:
			return FILETYPE_BMP;
	}

	return FILETYPE_APD;
}

/** 上書き保存時のメッセージ
 *
 * @return [0]キャンセル [1]上書き保存 [2]別名保存 */

static int _save_message(MainWindow *p)
{
	uint32_t ret;

	//ADW 形式での上書き保存は不可 => 別名保存

	if(p->fileformat == FILEFORMAT_ADW)
		return 2;

	//APD 以外の場合、APD で保存するか確認

	if(!FileFormat_isAPD(p->fileformat)
		&& (APP_CONF->optflags & CONFIG_OPTF_MES_SAVE_APD))
	{
		ret = mMessageBox(M_WINDOW(p), NULL, M_TR_T2(TRGROUP_MESSAGE, TRID_MES_SAVE_APD),
			MMESBOX_YESNO | MMESBOX_NOTSHOW, MMESBOX_YES);

		//メッセージ表示しない

		if(ret & MMESBOX_NOTSHOW)
			APP_CONF->optflags ^= CONFIG_OPTF_MES_SAVE_APD;

		return (ret & MMESBOX_YES)? 2: 1;
	}

	//上書き保存メッセージ

	if(APP_CONF->optflags & CONFIG_OPTF_MES_SAVE_OVERWRITE)
	{
		mStr str = MSTR_INIT;

		mStrPathGetFileName(&str, p->strFilename.buf);
		mStrAppendText(&str, "\n\n");
		mStrAppendText(&str, M_TR_T2(TRGROUP_MESSAGE, TRID_MES_SAVE_OVERWRITE));

		ret = mMessageBox(M_WINDOW(p), NULL, str.buf,
			MMESBOX_SAVE | MMESBOX_CANCEL | MMESBOX_NOTSHOW, MMESBOX_SAVE);

		mStrFree(&str);

		//メッセージ表示しない

		if(ret & MMESBOX_NOTSHOW)
			APP_CONF->optflags ^= CONFIG_OPTF_MES_SAVE_OVERWRITE;

		//キャンセル

		if(!(ret & MMESBOX_SAVE))
			return 0;
	}

	return 1;
}

/** ファイル名とフォーマットを取得
 *
 * @return 保存フォーマット。-1 でキャンセル、-2 で上書き保存 */

static int _save_get_path_and_format(MainWindow *p,
	mStr *strpath,int savetype,int recentno)
{
	int ret,type,format;
	mStr strdir = MSTR_INIT;
	uint8_t type_to_format[] = {
		FILEFORMAT_APD, FILEFORMAT_PSD, FILEFORMAT_PNG,
		FILEFORMAT_JPEG, FILEFORMAT_GIF, FILEFORMAT_BMP
	};

	//-------- 上書き保存
	/* 新規状態、または上書きできない場合は別名保存へ */

	if(savetype == MAINWINDOW_SAVEFILE_OVERWRITE && !mStrIsEmpty(&p->strFilename))
	{
		ret = _save_message(p);

		if(ret == 0)
			//キャンセル
			return -1;
		else if(ret == 1)
		{
			//上書き保存

			mStrCopy(strpath, &p->strFilename);
			return -2;
		}
	}

	//------- 別名保存、複製保存

	//初期ファイル名

	if(!mStrIsEmpty(&p->strFilename))
		mStrPathGetFileNameNoExt(strpath, p->strFilename.buf);

	//初期種類

	type = FILETYPE_APD;

	if(savetype == MAINWINDOW_SAVEFILE_DUP)
	{
		//複製時
		
		if(APP_CONF->savedup_format == 0)
		{
			//現在の編集ファイルと同じ形式 (新規なら APD)
		
			if(!mStrIsEmpty(&p->strFilename))
				type = _save_format_to_type(p->fileformat);
		}
		else
			//指定フォーマット
			type = _save_format_to_type(APP_CONF->savedup_format);
	}

	//ダイアログ

	_get_filedialog_dir(p, &strdir, recentno, TRUE);

	ret = mSysDlgSaveFile(M_WINDOW(p),
		"AzPainter (*.apd)\t*.apd\t"
		"PhotoShop (*.psd)\t*.psd\t"
		"PNG (*.png)\t*.png\t"
		"JPEG (*.jpg)\t*.jpg\t"
		"GIF (*.gif)\t*.gif\t"
		"BMP (*.bmp)\t*.bmp\t",
		type, strdir.buf, 0, strpath, &type);

	mStrFree(&strdir);

	if(!ret) return -1;

	//フォーマット取得

	format = type_to_format[type];

	//拡張子セット

	_save_set_ext(strpath, format);

	return format;
}


//============================
// ファイル保存 main
//============================


typedef struct
{
	const char *filename;
	int format;
}_thdata_save;


/** 保存スレッド処理
 *
 * @return 0:失敗 1:成功 2:GIF保存不可 */

static int _thread_save(mPopupProgress *prog,void *data)
{
	_thdata_save *p = (_thdata_save *)data;
	int ret;

	switch(p->format)
	{
		//APD v3
		case FILEFORMAT_APD:
			ret = drawFile_save_apd_v3(APP_DRAW, p->filename, prog);
			break;

		//PSD
		case FILEFORMAT_PSD:
			drawImage_blendImage_real(APP_DRAW);
			
			if(APP_CONF->save.psd_type == 0)
				ret = drawFile_save_psd_layer(APP_DRAW, p->filename, prog);
			else
			{
				ret = drawFile_save_psd_image(APP_DRAW, APP_CONF->save.psd_type,
					p->filename, prog);
			}

			drawUpdate_blendImage_full(APP_DRAW);
			break;

		//PNG (アルファチャンネル付き)
		case FILEFORMAT_PNG:
			if(APP_CONF->save.flags & CONFIG_SAVEOPTION_F_PNG_ALPHA_CHANNEL)
			{
				ret = drawFile_save_image(APP_DRAW, p->filename, FILEFORMAT_PNG_ALPHA, prog);
				break;
			}
			
			/* アルファチャンネルなしなら、以下の処理へ */

		//通常画像ファイル
		default:
			//合成イメージ
			drawImage_blendImage_real(APP_DRAW);

			//保存
			ret = drawFile_save_image(APP_DRAW, p->filename, p->format, prog);

			//blendimg 元に戻す
			drawUpdate_blendImage_full(APP_DRAW);
			break;
	}

	return ret;
}

/** ファイル保存
 *
 * @param savetype [0]上書き保存 [1]別名保存 [2]複製保存
 * @param recentno ディレクトリ履歴番号
 * @param FALSE でキャンセルされた */

mBool MainWindow_saveFile(MainWindow *p,int savetype,int recentno)
{
	mStr str = MSTR_INIT,strdir = MSTR_INIT;
	int format,ret;
	_thdata_save dat;
	PixelRGB pix;

	//パスとフォーマット取得

	ret = _save_get_path_and_format(p, &str, savetype, recentno);
	if(ret == -1)
	{
		mStrFree(&str);
		return FALSE;
	}

	format = (ret == -2)? p->fileformat: ret;

	//保存設定
	/* 上書き保存時、一度でも保存されていた場合は、前回の設定をそのまま使う。
	 * 開いた後一度も保存されていない場合は設定を行う。 */

	if(format != FILEFORMAT_BMP && format != FILEFORMAT_APD
		&& !(ret == -2 && p->saved))
	{
		//透過色
		/* 一度も保存されていない場合は読み込み時の透過色をデフォルトに。
		 * 読み込み時に透過色がなければ、(0,0) の色をデフォルトに。
		 * 
		 * [!] 読み込み時は、デフォルトで透過色を透明に置き換えているので、
		 *     元ファイルに透過色があってもなくても、デフォルトは OFF にしておく。 */

		if(!p->saved)
		{
			APP_CONF->save.tpcol = APP_DRAW->load_tpcol;

			if(APP_DRAW->load_tpcol < 0)
			{
				drawImage_getBlendColor_atPoint(APP_DRAW, 0, 0, &pix);
				APP_CONF->save.tpcol = M_RGB(pix.r, pix.g, pix.b);
			}

			APP_CONF->save.tpcol |= 1<<31;
		}
	
		//ダイアログ
	
		if(!SaveOptionDlg_run(M_WINDOW(p), format))
		{
			mStrFree(&str);
			return FALSE;
		}
	}

	//スレッド

	dat.filename = str.buf;
	dat.format = format;

	ret = PopupThread_run(&dat, _thread_save);

	//失敗

	if(ret != 1)
	{
		MainWindow_apperr((ret == 2)? APPERR_SAVEGIF_OVER_256: APPERR_FAILED, NULL);
		
		mStrFree(&str);
		return TRUE;
	}

	//----- 成功

	//編集ファイル情報

	if(savetype != MAINWINDOW_SAVEFILE_DUP)
	{
		mStrCopy(&p->strFilename, &str);

		p->fileformat = format;
		p->saved = TRUE;

		//タイトル

		MainWindow_setTitle(p);

		//undo データ変更フラグ OFF

		Undo_clearUpdateFlag();
	}

	//ファイル履歴

	_add_recent_file(p, str.buf);
	
	//ディレクトリ履歴

	mStrPathGetDir(&strdir, str.buf);

	mStrArrayAddRecent(APP_CONF->strRecentSaveDir, CONFIG_RECENTDIR_NUM, strdir.buf, TRUE);

	//

	mStrFree(&str);
	mStrFree(&strdir);

	return TRUE;
}


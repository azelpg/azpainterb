/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * グリッド設定ダイアログ
 *****************************************/

#include "mDef.h"
#include "mStr.h"
#include "mList.h"
#include "mWidget.h"
#include "mContainer.h"
#include "mWindow.h"
#include "mDialog.h"
#include "mButton.h"
#include "mColorButton.h"
#include "mCheckButton.h"
#include "mLineEdit.h"
#include "mListView.h"
#include "mWidgetBuilder.h"
#include "mEvent.h"
#include "mTrans.h"
#include "mSysDialog.h"

#include "defConfig.h"
#include "defGridData.h"

#include "ConfigData.h"

#include "trgroup.h"


//------------------------

enum
{
	//リスト
	TRID_BTT_EDIT = 100,
	TRID_BTT_ADD,
	TRID_BTT_DEL,
	TRID_BTT_UP,
	TRID_BTT_DOWN,
	TRID_BTT_LOAD,
	TRID_BTT_SAVE,

	//項目
	WID_ITEM_TOP = 200,
	
	WID_LIST = 1000
};

//ウィジェット番号
enum
{
	WNO_ITEM_W = 0,
	WNO_ITEM_H,
	WNO_ITEM_COL,
	WNO_ITEM_OPACITY,
	WNO_ITEM_CK_ZOOM,
	WNO_ITEM_ZOOM_TOP,
	WNO_ITEM_ZOOM_END,
	WNO_ITEM_CK_AREA,
	WNO_ITEM_X1,
	WNO_ITEM_Y1,
	WNO_ITEM_X2,
	WNO_ITEM_Y2,

	ITEM_WIDGET_NUM
};

//------------------------

typedef struct
{
	mWidget wg;
	mContainerData ct;
	mWindowData win;
	mDialogData dlg;

	mListView *lv;

	mList list;	//編集用データリスト
}_maindlg;

typedef struct
{
	mWidget wg;
	mContainerData ct;
	mWindowData win;
	mDialogData dlg;

	mWidget *pwg[ITEM_WIDGET_NUM];
}_itemdlg;

//------------------------

static const char *g_builder =
"ct#v:sep=9:mg=b18;"
  "ct#g4:sep=7,7;"
    "lb:lf=mr:tr=200;"
    "le#s:id=200:wlen=6;"
    "lb:lf=mr:tr=201;"
    "le#s:id=201:wlen=6;"
    "lb:lf=mr:tr=202;"
    "colbt#d:id=202:lf=w;"
    "lb:lf=mr:tr=203;"
    "le#s:id=203:wlen=6;"
  "-;"
  "ck:id=204:tr=204:mg=t3;"
  "ct#h:sep=4:pd=l16;"
    "le:id=205:wlen=6;"
    "lb:tr=205:lf=m;"
    "le:id=206:wlen=6;"
    "lb:tr=206:lf=m;"
  "-;"
  "ck:id=207:tr=207:mg=t3;"
  "ct#g4:sep=5,7;"
    "lb:lf=mr:tr=208;"
    "le:id=208:wlen=7;"
    "lb:lf=mr:tr=209;"
    "le:id=209:wlen=7;"
    "lb:lf=mr:tr=210;"
    "le:id=210:wlen=7;"
    "lb:lf=mr:tr=211;"
    "le:id=211:wlen=7;"
;

//------------------------

#define _ITEM(p)  ((GridListData *)(p))

//------------------------



/********************************
 * 項目ダイアログ
 ********************************/


/** イベント */

static int _item_event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		_itemdlg *p = (_itemdlg *)wg;
		int i,f;

		switch(ev->notify.id)
		{
			//表示倍率のチェック
			case WID_ITEM_TOP + WNO_ITEM_CK_ZOOM:
				if(ev->notify.type == MCHECKBUTTON_N_PRESS)
				{
					f = (ev->notify.param1 != 0);

					for(i = 0; i < 2; i++)
						mWidgetEnable(p->pwg[WNO_ITEM_ZOOM_TOP + i], f);
				}
				break;
			//"表示範囲を設定" のチェック
			case WID_ITEM_TOP + WNO_ITEM_CK_AREA:
				if(ev->notify.type == MCHECKBUTTON_N_PRESS)
				{
					f = (ev->notify.param1 != 0);

					for(i = 0; i < 4; i++)
						mWidgetEnable(p->pwg[WNO_ITEM_X1 + i], f);
				}
				break;
		}
	}

	return mDialogEventHandle_okcancel(wg, ev);
}

/** ダイアログ作成 */

static _itemdlg *_create_itemdlg(mWindow *owner,GridListData *item)
{
	_itemdlg *p;
	int i,f;

	p = (_itemdlg *)mDialogNew(sizeof(_itemdlg), owner, MWINDOW_S_DIALOG_NORMAL);
	if(!p) return FALSE;
	
	p->wg.event = _item_event_handle;

	//

	M_TR_G(TRGROUP_DLG_GRID_OPT);

	mContainerSetPadding_one(M_CONTAINER(p), 8);

	mWindowSetTitle(M_WINDOW(p), M_TR_T(0));

	//----- ウィジェット

	mWidgetBuilderCreateFromText(M_WIDGET(p), g_builder);

	for(i = 0; i < ITEM_WIDGET_NUM; i++)
		p->pwg[i] = mWidgetFindByID(M_WIDGET(p), WID_ITEM_TOP + i);

	//----- セット

	//幅、高さ

	for(i = 0; i < 2; i++)
		mLineEditSetNumStatus(M_LINEEDIT(p->pwg[i]), GRID_MIN_SIZE, GRID_MAX_SIZE, 0);

	mLineEditSetNum(M_LINEEDIT(p->pwg[WNO_ITEM_W]), item->w);
	mLineEditSetNum(M_LINEEDIT(p->pwg[WNO_ITEM_H]), item->h);

	//色

	mColorButtonSetColor(M_COLORBUTTON(p->pwg[WNO_ITEM_COL]), item->col);

	//濃度

	mLineEditSetNumStatus(M_LINEEDIT(p->pwg[WNO_ITEM_OPACITY]), 1, 100, 0);
	mLineEditSetNum(M_LINEEDIT(p->pwg[WNO_ITEM_OPACITY]), (int)((item->col >> 24) * 100.0 / 128.0 + 0.5));

	//表示倍率

	f = item->flags & GRIDDATA_F_HAVE_ZOOM;

	if(f)
		mCheckButtonSetState(M_CHECKBUTTON(p->pwg[WNO_ITEM_CK_ZOOM]), 1);

	for(i = 0; i < 2; i++)
	{
		mLineEditSetNumStatus(M_LINEEDIT(p->pwg[WNO_ITEM_ZOOM_TOP + i]), 0, 5000, 0);

		if(!f) mWidgetEnable(p->pwg[WNO_ITEM_ZOOM_TOP + i], FALSE);
	}

	mLineEditSetNum(M_LINEEDIT(p->pwg[WNO_ITEM_ZOOM_TOP]), item->zoom_min / 10);
	mLineEditSetNum(M_LINEEDIT(p->pwg[WNO_ITEM_ZOOM_END]), item->zoom_max / 10);

	//範囲

	f = item->flags & GRIDDATA_F_HAVE_AREA;

	if(f)
		mCheckButtonSetState(M_CHECKBUTTON(p->pwg[WNO_ITEM_CK_AREA]), 1);

	for(i = 0; i < 4; i++)
	{
		mLineEditSetNumStatus(M_LINEEDIT(p->pwg[WNO_ITEM_X1 + i]), 0, 20000, 0);
		mLineEditSetNum(M_LINEEDIT(p->pwg[WNO_ITEM_X1 + i]), item->area[i]);

		if(!f) mWidgetEnable(p->pwg[WNO_ITEM_X1 + i], FALSE);
	}

	//----- OK/Cancel

	mContainerCreateOkCancelButton(M_WIDGET(p));

	return p;
}

/** 項目設定ダイアログ実行 */

static mBool _itemdlg_run(mWindow *owner,GridListData *item)
{
	_itemdlg *p;
	mBool ret;
	uint32_t n;

	p = _create_itemdlg(owner, item);
	if(!p) return FALSE;

	mWindowMoveResizeShow_hintSize(M_WINDOW(p));

	ret = mDialogRun(M_DIALOG(p), FALSE);

	if(ret)
	{
		item->flags &= ~(GRIDDATA_F_HAVE_ZOOM | GRIDDATA_F_HAVE_AREA);

		//濃度
		
		n = (mLineEditGetNum(M_LINEEDIT(p->pwg[WNO_ITEM_OPACITY])) * 128.0 / 100 + 0.5);

		//幅、高さ、色
	
		item->w = mLineEditGetNum(M_LINEEDIT(p->pwg[WNO_ITEM_W]));
		item->h = mLineEditGetNum(M_LINEEDIT(p->pwg[WNO_ITEM_H]));
		item->col = mColorButtonGetColor(M_COLORBUTTON(p->pwg[WNO_ITEM_COL])) | (n << 24);

		//表示倍率

		item->zoom_min = mLineEditGetNum(M_LINEEDIT(p->pwg[WNO_ITEM_ZOOM_TOP])) * 10;
		item->zoom_max = mLineEditGetNum(M_LINEEDIT(p->pwg[WNO_ITEM_ZOOM_END])) * 10;

		if(mCheckButtonIsChecked(M_CHECKBUTTON(p->pwg[WNO_ITEM_CK_ZOOM]))
			&& item->zoom_min <= item->zoom_max)
			item->flags |= GRIDDATA_F_HAVE_ZOOM;

		//範囲 (範囲が正しくなければ、範囲なしにする)

		for(n = 0; n < 4; n++)
			item->area[n] = mLineEditGetNum(M_LINEEDIT(p->pwg[WNO_ITEM_X1 + n]));

		if(mCheckButtonIsChecked(M_CHECKBUTTON(p->pwg[WNO_ITEM_CK_AREA]))
			&& item->area[0] <= item->area[1]
			&& item->area[2] <= item->area[3])
			item->flags |= GRIDDATA_F_HAVE_AREA;
	}

	mWidgetDestroy(M_WIDGET(p));

	return ret;
}



/********************************
 * メインダイアログ (リスト)
 ********************************/


//======================
// sub
//======================


/** 現在の選択データ取得 */

static GridListData *_get_sel_item(_maindlg *p)
{
	mListViewItem *pi;

	pi = mListViewGetFocusItem(p->lv);

	return (pi)? _ITEM(pi->param): NULL;
}

/** リストの文字列取得 */

static void _get_list_text(mStr *str,GridListData *item)
{
	mStrSetFormat(str, "%dx%d #%06X:%d",
		item->w, item->h, item->col & 0xffffff,
		(int)((item->col >> 24) * 100.0 / 128.0 + 0.5));

	if(item->flags & GRIDDATA_F_HAVE_AREA)
	{
		mStrAppendFormat(str, " (%d,%d)-(%d,%d)",
			item->area[0], item->area[1], item->area[2], item->area[3]);
	}
}

/** リストをセット */

static void _set_list(_maindlg *p)
{
	mStr str = MSTR_INIT;
	GridListData *pi;

	for(pi = _ITEM(p->list.top); pi; pi = _ITEM(pi->i.next))
	{
		_get_list_text(&str, pi);

		mListViewAddItem(p->lv, str.buf, -1,
			(pi->flags & GRIDDATA_F_ON)? MLISTVIEW_ITEM_F_CHECKED: 0, (intptr_t)pi);
	}

	mStrFree(&str);
}


//======================
// コマンド
//======================


/** 編集 */

static void _cmd_edit(_maindlg *p)
{
	GridListData *pi;
	mStr str = MSTR_INIT;

	pi = _get_sel_item(p);
	if(!pi) return;

	if(_itemdlg_run(M_WINDOW(p), pi))
	{
		//リストのテキスト変更
		
		_get_list_text(&str, pi);

		mListViewSetItemText(p->lv, NULL, str.buf);

		mStrFree(&str);
	}
}

/** 追加 */

static void _cmd_add(_maindlg *p)
{
	GridListData item,*pi;
	mStr str = MSTR_INIT;

	if(p->list.num >= GRID_MAX_NUM) return;

	mMemzero(&item, sizeof(GridListData));

	item.w = item.h = 16;
	item.col = 0x0000ff | (32 << 24);
	item.flags = GRIDDATA_F_ON;

	if(_itemdlg_run(M_WINDOW(p), &item))
	{
		//データに追加

		pi = (GridListData *)mMemdup(&item, sizeof(GridListData));
		if(!pi) return;

		mListAppend(&p->list, M_LISTITEM(pi));

		//リストに追加
		
		_get_list_text(&str, pi);

		mListViewAddItem(p->lv, str.buf, -1, MLISTVIEW_ITEM_F_CHECKED, (intptr_t)pi);
	
		mStrFree(&str);
	}
}

/** 削除 */

static void _cmd_del(_maindlg *p)
{
	GridListData *pi;

	pi = _get_sel_item(p);
	if(pi)
	{
		//データ削除

		mListDelete(&p->list, M_LISTITEM(pi));

		//リスト項目削除

		mListViewDeleteItem_sel(p->lv, NULL);
	}
}

/** 上下移動 */

static void _cmd_updown(_maindlg *p,mBool down)
{
	GridListData *pi;

	pi = _get_sel_item(p);
	if(!pi) return;

	//データ

	mListMoveUpDown(&p->list, M_LISTITEM(pi), !down);

	//リスト
	
	mListViewMoveItem_updown(p->lv, NULL, down);
}

/** 読み込み */

static void _cmd_load(_maindlg *p)
{
	mStr str = MSTR_INIT;

	if(mSysDlgOpenFile(M_WINDOW(p),
		"Grid File (*.conf)\t*.conf\t"
		"All Files (*)\t*\t",
		0, APP_CONF->strGridFileDir.buf, 0, &str))
	{
		//ディレクトリ保存
		mStrPathGetDir(&APP_CONF->strGridFileDir, str.buf);

		//クリア
		mListDeleteAll(&p->list);
		mListViewDeleteAllItem(p->lv);
		
		ConfigData_loadGrid(&p->list, str.buf);

		mStrFree(&str);

		_set_list(p);
	}
}

/** 保存 */

static void _cmd_save(_maindlg *p)
{
	mStr str = MSTR_INIT;

	if(mSysDlgSaveFile(M_WINDOW(p), "Grid File (*.conf)\t*.conf\t",
		0, APP_CONF->strGridFileDir.buf, 0, &str, NULL))
	{
		mStrPathSetExt(&str, "conf");
	
		//ディレクトリ保存
		mStrPathGetDir(&APP_CONF->strGridFileDir, str.buf);
	
		ConfigData_saveGrid(&p->list, str.buf);

		mStrFree(&str);
	}
}


//====================


/** イベント */

static int _main_event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		_maindlg *p = (_maindlg *)wg;
	
		switch(ev->notify.id)
		{
			//リスト
			case WID_LIST:
				if(ev->notify.type == MLISTVIEW_N_ITEM_CHECK)
					//チェックのON/OFF
					_ITEM(ev->notify.param2)->flags ^= GRIDDATA_F_ON;
				else if(ev->notify.type == MLISTVIEW_N_ITEM_DBLCLK)
					//ダブルクリック : 編集
					_cmd_edit(p);
				break;

			//編集
			case TRID_BTT_EDIT:
				_cmd_edit(p);
				break;
			//追加
			case TRID_BTT_ADD:
				_cmd_add(p);
				break;
			//削除
			case TRID_BTT_DEL:
				_cmd_del(p);
				break;
			//上へ
			case TRID_BTT_UP:
				_cmd_updown(p, FALSE);
				break;
			//下へ
			case TRID_BTT_DOWN:
				_cmd_updown(p, TRUE);
				break;
			//読み込み
			case TRID_BTT_LOAD:
				_cmd_load(p);
				break;
			//保存
			case TRID_BTT_SAVE:
				_cmd_save(p);
				break;
		}
	}

	return mDialogEventHandle_okcancel(wg, ev);
}

/** 作成 */

static _maindlg *_create_maindlg(mWindow *owner)
{
	_maindlg *p;
	mWidget *ct;
	mListView *lv;
	int i;

	//作成
	
	p = (_maindlg *)mDialogNew(sizeof(_maindlg), owner, MWINDOW_S_DIALOG_NORMAL);
	if(!p) return NULL;
	
	p->wg.event = _main_event_handle;

	//リストデータを複製

	mListDup(&p->list, &APP_CONF->grid_list, sizeof(GridListData));

	//

	M_TR_G(TRGROUP_DLG_GRID_OPT);

	mContainerSetPadding_one(M_CONTAINER(p), 8);
	p->ct.sepW = 15;

	mWindowSetTitle(M_WINDOW(p), M_TR_T(0));

	//------ ウィジェット

	ct = mContainerCreate(M_WIDGET(p), MCONTAINER_TYPE_HORZ, 0, 12, MLF_EXPAND_WH);

	//リスト

	p->lv = lv = mListViewNew(0, ct,
		MLISTVIEW_S_CHECKBOX | MLISTVIEW_S_AUTO_WIDTH,
		MSCROLLVIEW_S_HORZVERT | MSCROLLVIEW_S_FIX_VERT | MSCROLLVIEW_S_FRAME);

	lv->wg.id = WID_LIST;
	lv->wg.fLayout = MLF_EXPAND_WH;

	mWidgetSetInitSize_fontHeight(M_WIDGET(lv), 18, 12);

	_set_list(p);

	//ボタン

	ct = mContainerCreate(ct, MCONTAINER_TYPE_VERT, 0, 0, 0);

	for(i = 0; i < 7; i++)
	{
		mButtonCreate(ct, TRID_BTT_EDIT + i, 0, MLF_EXPAND_W,
			(i == 0 || i == 4)? M_MAKE_DW4(0,0,0,7): 0, M_TR_T(TRID_BTT_EDIT + i));
	}

	//OK/Cancel

	mContainerCreateOkCancelButton(M_WIDGET(p));

	return p;
}


//=====================


/** グリッド設定ダイアログ実行 */

mBool GridOptionDlg_run(mWindow *owner)
{
	_maindlg *p;
	mBool ret;

	p = _create_maindlg(owner);
	if(!p) return FALSE;

	mWindowMoveResizeShow_hintSize(M_WINDOW(p));

	ret = mDialogRun(M_DIALOG(p), FALSE);

	if(!ret)
		mListDeleteAll(&p->list);
	else
	{
		//リストを置き換え
		
		mListDeleteAll(&APP_CONF->grid_list);

		APP_CONF->grid_list = p->list;
	}

	mWidgetDestroy(M_WIDGET(p));

	return ret;
}

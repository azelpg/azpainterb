/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * StatusBar
 * 
 * ステータスバー
 *****************************************/
/*
 * ステータスバーが非表示の場合、ウィジェットは削除される。
 * 作成されていなければ、APP_WIDGETS->statusbar は NULL。
 */

#include <stdio.h>

#include "mDef.h"
#include "mContainerDef.h"
#include "mWidget.h"
#include "mContainer.h"
#include "mLabel.h"
#include "mTrans.h"
#include "mStr.h"

#include "defConfig.h"
#include "defWidgets.h"
#include "defDraw.h"

#include "StatusBar.h"

#include "trgroup.h"


//----------------------

struct _StatusBar
{
	mWidget wg;
	mContainerData ct;

	mLabel *label_curpos,
		*label_imginfo,
		*label_help;
	mPoint curpos;

	mStr str;
};

//----------------------

enum
{
	TRID_DOTPEN_FREE,
	TRID_BRUSH_FREE,
	TRID_FINGER,
	TRID_FILL,
	TRID_TEXT,
	TRID_MOVE,
	TRID_SELECT,
	TRID_SEL_MOVECOPY,
	TRID_SEL_REPLACE,
	TRID_STAMP,
	TRID_SPOIT,
	TRID_LINE45,
	TRID_DRAWTYPE_BOX,
	TRID_DRAWTYPE_CIRCLE,
	TRID_DRAWTYPE_SUCCLINE,
	TRID_DRAWTYPE_CONCLINE,
	TRID_DRAWTYPE_BEZIER,
	TRID_POLYGON
};

//----------------------


/** 破棄ハンドラ */

static void _destroy_handle(mWidget *wg)
{
	mStrFree(&((StatusBar *)wg)->str);
}

/** ステータスバー作成 */

void StatusBar_new()
{
	StatusBar *p;

	p = (StatusBar *)mContainerNew(sizeof(StatusBar), M_WIDGET(APP_WIDGETS->mainwin));

	APP_WIDGETS->statusbar = p;

	//

	p->wg.fLayout = MLF_EXPAND_W;
	p->wg.draw = mWidgetHandleFunc_draw_drawBkgnd;
	p->wg.drawBkgnd = mWidgetHandleFunc_drawBkgnd_fillFace;
	p->wg.destroy = _destroy_handle;
	p->ct.sepW = 3;

	mContainerSetType(M_CONTAINER(p), MCONTAINER_TYPE_HORZ, 0);
	mContainerSetPadding_one(M_CONTAINER(p), 1);

	//ラベル

	p->label_curpos = mLabelCreate(M_WIDGET(p), MLABEL_S_BORDER | MLABEL_S_CENTER, 0, 0, NULL);
	p->label_imginfo = mLabelCreate(M_WIDGET(p), MLABEL_S_BORDER, 0, 0, NULL);
	p->label_help = mLabelCreate(M_WIDGET(p), 0, MLF_MIDDLE | MLF_EXPAND_W, M_MAKE_DW4(2,0,0,0), NULL);

	//カーソル位置は固定サイズ

	mWidgetSetHintOverW_fontTextWidth(M_WIDGET(p->label_curpos), "-99999, -99999");

	//セット (初期時。または非表示 => 表示に切り替わった時に再設定)

	StatusBar_setImageInfo();
	StatusBar_setHelp_tool();
}

/** 表示状態切り替え */

void StatusBar_toggleVisible()
{
	if(APP_WIDGETS->statusbar)
	{
		//非表示時は、削除
		
		mWidgetDestroy(M_WIDGET(APP_WIDGETS->statusbar));
		APP_WIDGETS->statusbar = NULL;
	}
	else
	{
		//表示
		
		StatusBar_new();
	}

	APP_CONF->fView ^= CONFIG_VIEW_F_STATUSBAR;

	mWidgetReLayout(M_WIDGET(APP_WIDGETS->mainwin));
}

/** プログレスバーを表示する位置を取得
 *
 * @param pt  Y は下端
 * @return FALSE でステータスバーは非表示 */

mBool StatusBar_getProgressBarPos(mPoint *pt)
{
	StatusBar *p = APP_WIDGETS->statusbar;
	mBox box;

	if(p)
	{
		mWidgetGetRootBox(M_WIDGET(p->label_help), &box);
		pt->x = box.x;

		mWidgetGetRootBox(M_WIDGET(p), &box);
		pt->y = box.y + box.h;

		return TRUE;
	}

	return FALSE;
}


/** カーソル位置セット */

void StatusBar_setCursorPos(mPoint *pt)
{
	StatusBar *p = APP_WIDGETS->statusbar;

	if(p && (p->curpos.x != pt->x || p->curpos.y != pt->y))
	{
		mStrSetFormat(&p->str, "%d, %d", pt->x, pt->y);

		mLabelSetText(p->label_curpos, p->str.buf);

		p->curpos = *pt;
	}
}

/** 画像情報セット */

void StatusBar_setImageInfo()
{
	StatusBar *p = APP_WIDGETS->statusbar;

	if(p)
	{
		mStrSetFormat(&p->str, "%d x %d %d DPI",
			APP_DRAW->imgw, APP_DRAW->imgh, APP_DRAW->imgdpi);

		mLabelSetText(p->label_imginfo, p->str.buf);

		mWidgetReLayout(M_WIDGET(p));
	}
}

/** ツールのヘルプセット */

void StatusBar_setHelp_tool()
{
	StatusBar *p = APP_WIDGETS->statusbar;
	int no,sub,id = -1;
	char drawtype[] = {
		TRID_LINE45, TRID_DRAWTYPE_BOX, TRID_DRAWTYPE_CIRCLE,
		TRID_DRAWTYPE_SUCCLINE, TRID_DRAWTYPE_CONCLINE, TRID_DRAWTYPE_BEZIER,
		TRID_DRAWTYPE_BOX, -1
	},
	fillpoly[] = {
		TRID_DRAWTYPE_BOX, TRID_DRAWTYPE_CIRCLE, TRID_POLYGON, -1
	};

	if(!p) return;

	no = APP_DRAW->tool.no;
	sub = APP_DRAW->tool.subno[no];

	switch(no)
	{
		case TOOL_DOTPEN:
		case TOOL_DOTPEN_ERASE:
			id = (sub == TOOLSUB_DRAW_FREE)? TRID_DOTPEN_FREE: drawtype[sub - 1];
			break;
		case TOOL_PEN:
		case TOOL_BRUSH:
		case TOOL_BRUSH_ERASE:
		case TOOL_DODGE:
		case TOOL_BURN:
		case TOOL_BLUR:
			id = (sub == TOOLSUB_DRAW_FREE)? TRID_BRUSH_FREE: drawtype[sub - 1];
			break;
		case TOOL_WATER:
			id = TRID_BRUSH_FREE;
			break;
		case TOOL_FINGER:
			id = TRID_FINGER;
			break;
		case TOOL_FILL_POLYGON:
		case TOOL_FILL_POLYGON_ERASE:
			id = fillpoly[sub];
			break;
		case TOOL_FILL:
			id = TRID_FILL;
			break;
		case TOOL_TEXT:
			id = TRID_TEXT;
			break;
		case TOOL_MOVE:
			id = TRID_MOVE;
			break;
		case TOOL_SELECT:
			id = TRID_SELECT;
			break;
		case TOOL_SEL_MOVE:
		case TOOL_SEL_COPY:
			id = TRID_SEL_MOVECOPY;
			break;
		case TOOL_SEL_REPLACE:
			id = TRID_SEL_REPLACE;
			break;
		case TOOL_STAMP:
			id = TRID_STAMP;
			break;
		case TOOL_GRADATION:
		case TOOL_CANVAS_ROTATE:
			id = TRID_LINE45;
			break;
		case TOOL_SPOIT:
			id = TRID_SPOIT;
			break;
	}

	if(id == -1)
		mLabelSetText(p->label_help, NULL);
	else
		mLabelSetText(p->label_help, M_TR_T2(TRGROUP_STATUSBAR_HELP, id));
}

/** ヘルプ欄に矩形選択時の範囲をセット */

void StatusBar_setHelp_imgbox()
{
	StatusBar *p = APP_WIDGETS->statusbar;
	int x1,y1,x2,y2;

	if(p)
	{
		x1 = APP_DRAW->w.pttmp[0].x;
		y1 = APP_DRAW->w.pttmp[0].y;
		x2 = APP_DRAW->w.pttmp[1].x;
		y2 = APP_DRAW->w.pttmp[1].y;
	
		mStrSetFormat(&p->str, "(%d, %d) - (%d, %d) : %d x %d",
			x1, y1, x2, y2, x2 - x1 + 1, y2 - y1 + 1);

		mLabelSetText(p->label_help, p->str.buf);
	}
}

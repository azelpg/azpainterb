/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * MainWindow
 *
 * コマンド関連
 *****************************************/

#include "mDef.h"
#include "mGui.h"
#include "mWidget.h"
#include "mIconButtons.h"
#include "mDockWidget.h"

#include "defConfig.h"
#include "defDraw.h"
#include "defWidgets.h"
#include "defCanvasKeyID.h"
#include "AppErr.h"

#include "LayerItem.h"
#include "Undo.h"
#include "AppResource.h"

#include "defMainWindow.h"
#include "MainWindow.h"
#include "MainWindow_pv.h"

#include "MainWinCanvas.h"
#include "DockObject.h"
#include "Docks_external.h"
#include "dialog_canvas.h"
#include "EnvOptDlg.h"

#include "draw_main.h"
#include "draw_layer.h"


//---------------

mBool PanelLayoutDlg_run(mWindow *owner);

//---------------


//=======================
// 表示関連
//=======================


/** 各ペインをレイアウト */

void MainWindow_pane_layout(MainWindow *p)
{
	char *pc;
	mWidget *wg,*next;
	int i,no,pane_have[3],spno,pane_cnt;

	//各ペイン、格納するパネルがあるか

	mMemzero(pane_have, sizeof(int) * 3);

	for(i = 0; i < DOCKWIDGET_NUM; i++)
	{
		no = DockObject_getPaneNo_fromNo(i);
		pane_have[no] = 1;
	}

	//リンクを外す

	for(wg = p->ct_main->first; wg; wg = next)
	{
		next = wg->next;
		mWidgetTreeRemove(wg);
	}

	//再リンク

	spno = 0;
	pane_cnt = 0;

	for(pc = APP_CONF->pane_layout; *pc; pc++)
	{
		no = *pc - '0';

		//パネルがないペインは除外

		if(no == 0 || pane_have[no - 1])
		{
			//キャンバス/ペイン
			
			wg = (no == 0)? (mWidget *)APP_WIDGETS->canvas: APP_WIDGETS->pane[no - 1];

			mWidgetTreeAppend(wg, p->ct_main);

			if(no != 0) pane_cnt++;

			//スプリッター

			if(spno < 3)
				mWidgetTreeAppend(p->splitter[spno++], p->ct_main);
		}
	}

	//非表示のペインがある場合、最後に余分なスプリッターが付くので、外す

	if(pane_cnt != 3)
		mWidgetTreeRemove(p->ct_main->last);
}

/** ペインとスプリッターの表示反転 */

void MainWindow_toggle_show_pane_splitter(MainWindow *p)
{
	int i;

	//切り離されているものは除外

	for(i = 0; i < 3; i++)
	{
		if(APP_WIDGETS->pane[i]->parent)
			mWidgetShow(APP_WIDGETS->pane[i], -1);

		if(p->splitter[i]->parent)
			mWidgetShow(p->splitter[i], -1);
	}
}

/** パネルすべてを表示/非表示 */

void MainWindow_cmd_show_panel_all(MainWindow *p)
{
	int i;

	//各パレット表示状態反転

	for(i = 0; i < DOCKWIDGET_NUM; i++)
	{
		if(APP_WIDGETS->dockobj[i])
			mDockWidgetSetVisible(APP_WIDGETS->dockobj[i]->dockwg, -1);
	}

	//ペインとスプリッターの表示反転

	MainWindow_toggle_show_pane_splitter(p);

	mWidgetReLayout(M_WIDGET(p));

	//

	APP_CONF->fView ^= CONFIG_VIEW_F_DOCKS;
}

/** 各パネルの表示状態を反転 */

void MainWindow_cmd_show_panel_toggle(int no)
{
	if(APP_CONF->fView & CONFIG_VIEW_F_DOCKS)
		mDockWidgetShow(APP_WIDGETS->dockobj[no]->dockwg, -1);
}


//============================
// ダイアログ
//============================


/** キャンバスサイズ変更 */

void MainWindow_cmd_resizeCanvas(MainWindow *p)
{
	CanvasResizeInfo info;
	int sw,sh,topx,topy,n;

	sw = APP_DRAW->imgw;
	sh = APP_DRAW->imgh;

	//ダイアログ

	info.w = sw;
	info.h = sh;

	if(!CanvasResizeDlg_run(M_WINDOW(p), &info))
		return;

	//サイズ変更なし

	if(info.w == sw && info.h == sh) return;

	//左上位置

	n = info.align % 3;

	if(n == 0) topx = 0;
	else if(n == 1) topx = (info.w - sw) >> 1;
	else topx = info.w - sw;

	n = info.align / 3;

	if(n == 0) topy = 0;
	else if(n == 1) topy = (info.h - sh) >> 1;
	else topy = info.h - sh;

	//実行

	if(!drawImage_resizeCanvas(APP_DRAW, info.w, info.h, topx, topy, info.crop))
		MainWindow_apperr(APPERR_ALLOC, NULL);

	MainWindow_updateNewCanvas(p, NULL);
}

/** キャンバス拡大縮小 */

void MainWindow_cmd_scaleCanvas(MainWindow *p)
{
	CanvasScaleInfo info;

	//ダイアログ

	info.w = APP_DRAW->imgw;
	info.h = APP_DRAW->imgh;
	info.dpi = APP_DRAW->imgdpi;
	info.have_dpi = TRUE;

	if(!CanvasScaleDlg_run(M_WINDOW(p), &info))
		return;

	//サイズ変更なし

	if(info.w == APP_DRAW->imgw && info.h == APP_DRAW->imgh)
		return;

	//実行

	if(!drawImage_scaleCanvas(APP_DRAW, info.w, info.h, info.dpi, info.type))
		MainWindow_apperr(APPERR_ALLOC, NULL);
	
	MainWindow_updateNewCanvas(p, NULL);
}

/** 環境設定 */

void MainWindow_cmd_envoption(MainWindow *p)
{
	int f;

	f = EnvOptionDlg_run(M_WINDOW(p));

	//テーマ

	if(f & ENVOPTDLG_F_THEME)
	{
		mAppLoadThemeFile(APP_CONF->strThemeFile.buf);

		DockColor_changeTheme();
		DockColorPalette_changeTheme();
		DockColorWheel_changeTheme();
	}

	//キャンバス

	if(f & ENVOPTDLG_F_UPDATE_ALL)
		//イメージ全体変更 (チェック柄背景色変更)
		/* キャンバス背景色も変わっている場合あり */
		drawUpdate_all();
	else if(f & ENVOPTDLG_F_UPDATE_CANVAS)
	{
		//キャンバス背景色変更
		
		drawUpdate_canvasArea();
		DockCanvasView_update();
	}

	//描画カーソル変更

	if(f & ENVOPTDLG_F_CURSOR)
		MainWinCanvasArea_changeDrawCursor();

	//ツールバーボタン

	if(f & ENVOPTDLG_F_TOOLBAR_BTT)
		MainWindow_createToolBar(p, FALSE);

	//アイコンサイズ

	if(f & ENVOPTDLG_F_ICONSIZE)
	{
		//ツールバー
		mIconButtonsReplaceImageList(M_ICONBUTTONS(p->toolbar),
			AppResource_loadIconImage("toolbar.png", APP_CONF->iconsize_toolbar));

		//dock
		DockTool_changeIconSize();
		DockLayer_changeIconSize();
		DockCanvasView_changeIconSize();
		DockImageViewer_changeIconSize();

		mWidgetReLayout(M_WIDGET(p));
	}
}

/** パネル配置設定 */

void MainWindow_cmd_panelLayout(MainWindow *p)
{
	if(PanelLayoutDlg_run(M_WINDOW(p)))
	{
		//ペインの再配置
		
		MainWindow_pane_layout(p);

		//ペイン内のパネルを再配置
	
		DockObjects_relocate();

		//レイアウト

		mWidgetReLayout(p->ct_main);
	}
}


//=========================
// ほか
//=========================


/** アンドゥ/リドゥ */

void MainWindow_undoredo(MainWindow *p,mBool redo)
{
	UndoUpdateInfo info;
	mBool ret;

	if(!Undo_isHave(redo)) return;

	//実行 (ret は更新を行うかどうか。失敗したかではない)

	MainWinCanvasArea_setCursor_wait();

	ret = Undo_runUndoRedo(redo, &info);

	MainWinCanvasArea_restoreCursor();

	if(!ret) return;

	//更新

	switch(info.type)
	{
		//イメージ範囲
		case UNDO_UPDATE_RECT:
			drawUpdate_rect_imgcanvas_canvasview_fromRect(APP_DRAW, &info.rc);
			DockLayer_update_layer(info.layer);
			break;
		//イメージ範囲 + レイヤ一覧
		case UNDO_UPDATE_RECT_AND_LAYERLIST:
			drawUpdate_rect_imgcanvas_canvasview_fromRect(APP_DRAW, &info.rc);
			DockLayer_update_all();
			break;
		//イメージ範囲 + レイヤ一覧の指定レイヤのみ
		case UNDO_UPDATE_RECT_AND_LAYERLIST_ONE:
			drawUpdate_rect_imgcanvas_canvasview_fromRect(APP_DRAW, &info.rc);
			DockLayer_update_layer(info.layer);
			break;
		//イメージ範囲 + レイヤ一覧の指定レイヤとその下レイヤ (下レイヤに移す時)
		case UNDO_UPDATE_RECT_AND_LAYERLIST_TWO:
			drawUpdate_rect_imgcanvas_canvasview_fromRect(APP_DRAW, &info.rc);

			DockLayer_update_layer(info.layer);
			DockLayer_update_layer(LayerItem_getNext(info.layer));
			break;
		//レイヤ一覧の指定レイヤのみ
		case UNDO_UPDATE_LAYERLIST_ONE:
			DockLayer_update_layer(info.layer);
			break;
		//すべて + レイヤ一覧
		case UNDO_UPDATE_ALL_AND_LAYERLIST:
			drawUpdate_all_layer();
			break;
		//キャンバスサイズ変更
		case UNDO_UPDATE_CANVAS_RESIZE:
			drawImage_changeImageSize(APP_DRAW, info.rc.x1, info.rc.y1);
			MainWindow_updateNewCanvas(p, NULL);
			break;
	}
}

/** キャンバスキー押し時、ボタン操作時のコマンド */

void MainWindow_onCanvasKeyCommand(int cmdid)
{
	DrawData *p = APP_DRAW;

	if(cmdid >= CANVASKEY_CMD_TOOL && cmdid < CANVASKEY_CMD_TOOL + TOOL_NUM)
	{
		//ツール変更

		drawTool_setTool(p, cmdid - CANVASKEY_CMD_TOOL);
	}
	else if(cmdid >= CANVASKEY_CMD_DRAWTYPE && cmdid < CANVASKEY_CMD_DRAWTYPE + TOOLSUB_DRAW_NUM)
	{
		//描画タイプ変更 (描画系ツール時)

		if(drawTool_isHaveDrawType())
			drawTool_setToolSubtype(p, cmdid - CANVASKEY_CMD_DRAWTYPE);
	}
	else if(cmdid >= CANVASKEY_CMD_OTHER && cmdid < CANVASKEY_CMD_OTHER + CANVASKEY_CMD_OTHER_NUM)
	{
		//他コマンド

		cmdid -= CANVASKEY_CMD_OTHER;

		switch(cmdid)
		{
			//一つ上/下のレイヤをカレントに
			case 0:
			case 1:
				drawLayer_currentSelUpDown(p, (cmdid == 0));
				break;
			//カレントレイヤ表示/非表示
			case 2:
				drawLayer_revVisible(p, p->curlayer);
				DockLayer_update_curlayer(FALSE);
				break;
			//描画色/背景色切り替え
			case 3:
				drawColor_toggleDrawCol(p);
				DockColor_changeDrawColor();
				break;
			//直前のツールと切り替え
			case 4:
				drawTool_setTool(p, -1);
				break;
			//一段階拡大/縮小
			case 5:
			case 6:
				drawCanvas_zoomStep(APP_DRAW, (cmdid == 5));
				break;
			//元に戻す/やり直す
			case 7:
			case 8:
				MainWindow_undoredo(APP_WIDGETS->mainwin, (cmdid == 8));
				break;
		}
	}
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DockOption
 *
 * <ツール - ドットペン/ドットペン消しゴム>
 *****************************************/

#include "mDef.h"
#include "mContainerDef.h"
#include "mWidget.h"
#include "mContainer.h"
#include "mCheckButton.h"
#include "mComboBox.h"
#include "mEvent.h"
#include "mPixbuf.h"
#include "mTrans.h"

#include "defDraw.h"
#include "defDotPenStyle.h"
#include "defPixelMode.h"
#include "macroToolOpt.h"

#include "ValueBar.h"
#include "DockOption_sub.h"

#include "trgroup.h"
#include "trid_word.h"


//------------------

typedef struct
{
	mWidget wg;
	mContainerData ct;
	
	mWidget *slotwg;
	mComboBox *cb_pix,
		*cb_style;
	ValueBar *bar_size,
		*bar_opa;
	mCheckButton *ck_fine;
}_tab_dotpen;

//------------------

enum
{
	WID_SLOT = 100,
	WID_BAR_SIZE,
	WID_BAR_OPACITY,
	WID_CB_PIXMODE,
	WID_CB_STYLE,
	WID_CK_FINE,
};

//------------------



/** ウィジェットの値セット */

static void _set_widget_val(_tab_dotpen *p,uint32_t val)
{
	ValueBar_setPos(p->bar_size, DOTPEN_GET_SIZE(val));
	ValueBar_setPos(p->bar_opa, DOTPEN_GET_OPACITY(val));

	mComboBoxSetSel_index(p->cb_pix, DOTPEN_GET_PIXMODE(val));
	mComboBoxSetSel_index(p->cb_style, DOTPEN_GET_STYLE(val));

	mCheckButtonSetState(p->ck_fine, DOTPEN_IS_FINE(val));
}

/** イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		uint32_t *pv,*slot_top;

		//セット先

		if(APP_DRAW->tool.no == TOOL_DOTPEN)
		{
			slot_top = APP_DRAW->tool.dotpen_slot;
			pv = slot_top + APP_DRAW->tool.dotpen_sel;
		}
		else
		{
			slot_top = APP_DRAW->tool.dotpen_erase_slot;
			pv = slot_top + APP_DRAW->tool.dotpen_erase_sel;
		}

		//

		switch(ev->notify.id)
		{
			//スロット選択変更
			case WID_SLOT:
				if(APP_DRAW->tool.no == TOOL_DOTPEN)
					APP_DRAW->tool.dotpen_sel = ev->notify.param1;
				else
					APP_DRAW->tool.dotpen_erase_sel = ev->notify.param1;
				
				_set_widget_val((_tab_dotpen *)wg, slot_top[ev->notify.param1]);
				break;
			//サイズ
			case WID_BAR_SIZE:
				if(ev->notify.param2)
					DOTPEN_SET_SIZE(pv, ev->notify.param1);
				break;
			//濃度
			case WID_BAR_OPACITY:
				if(ev->notify.param2)
					DOTPEN_SET_OPACITY(pv, ev->notify.param1);
				break;
			//塗り
			case WID_CB_PIXMODE:
				if(ev->notify.type == MCOMBOBOX_N_CHANGESEL)
					DOTPEN_SET_PIXMODE(pv, ev->notify.param2);
				break;
			//形状
			case WID_CB_STYLE:
				if(ev->notify.type == MCOMBOBOX_N_CHANGESEL)
					DOTPEN_SET_STYLE(pv, ev->notify.param2);
				break;
			//細線
			case WID_CK_FINE:
				DOTPEN_TOGGLE_FINE(pv);
				break;
		}
	}

	return 1;
}

/** 作成 */

mWidget *DockOption_createTab_tool_dotpen(mWidget *parent)
{
	_tab_dotpen *p;
	mWidget *ct;
	uint32_t val;
	mBool erase;
	uint8_t pixmode[] = {
		PIXELMODE_BLEND_STROKE, PIXELMODE_BLEND_PIXEL,
		PIXELMODE_COMPARE_A, PIXELMODE_OVERWRITE_STYLE,
		PIXELMODE_OVERWRITE_SQUARE, 255
	},
		pixmode_erase[] = {
		PIXELMODE_BLEND_STROKE, PIXELMODE_BLEND_PIXEL, 255
	};

	erase = (APP_DRAW->tool.no == TOOL_DOTPEN_ERASE);

	//データ値

	if(erase)
		val = APP_DRAW->tool.dotpen_erase_slot[APP_DRAW->tool.dotpen_erase_sel];
	else
		val = APP_DRAW->tool.dotpen_slot[APP_DRAW->tool.dotpen_sel];

	//メインコンテナ

	p = (_tab_dotpen *)DockOption_createMainContainer(
			sizeof(_tab_dotpen), parent, _event_handle);

	//スロット

	p->slotwg = DockOption_dotpenslot_new(M_WIDGET(p), WID_SLOT,
		DOTPEN_SLOT_NUM,
		(erase)? APP_DRAW->tool.dotpen_erase_sel: APP_DRAW->tool.dotpen_sel);

	p->slotwg->margin.bottom = 3;

	//-------

	ct = mContainerCreateGrid(M_WIDGET(p), 2, 3, 5, MLF_EXPAND_W);

	//サイズ

	p->bar_size = DockOption_createSizeBar(ct, WID_BAR_SIZE,
		DOTPEN_STYLE_MAX_SIZE, DOTPEN_GET_SIZE(val));

	//濃度

	p->bar_opa = DockOption_createDensityBar(ct,
		WID_BAR_OPACITY, DOTPEN_GET_OPACITY(val));

	//塗り

	p->cb_pix = DockOption_createPixelModeCombo(ct, WID_CB_PIXMODE,
		(erase)? pixmode_erase: pixmode, DOTPEN_GET_PIXMODE(val));

	//形状

	p->cb_style = DockOption_createComboBox(ct, WID_CB_STYLE, -TRID_WORD_STYLE);

	M_TR_G(TRGROUP_DOTPEN_STYLE);

	mComboBoxAddTrItems(p->cb_style, DOTPEN_STYLE_NUM, 0, 0);
	mComboBoxSetSel_index(p->cb_style, DOTPEN_GET_STYLE(val));

	//細線

	mWidgetNew(0, ct);
	
	p->ck_fine = mCheckButtonCreate(ct, WID_CK_FINE, 0, 0, 0,
		M_TR_T2(TRGROUP_DOCKOPT_DOTPEN, 0), DOTPEN_IS_FINE(val));

	return (mWidget *)p;
}


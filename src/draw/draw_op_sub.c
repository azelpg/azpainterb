/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DrawData
 *
 * 操作 - サブ関数
 *****************************************/

#include <stdlib.h>
#include <math.h>

#include "mDef.h"
#include "mRectBox.h"
#include "mWidget.h"

#include "defWidgets.h"
#include "defConfig.h"
#include "defDraw.h"
#include "defPixelMode.h"
#include "defGridData.h"

#include "LayerItem.h"
#include "LayerList.h"
#include "BrushItem.h"
#include "BrushList.h"
#include "GradationList.h"

#include "TileImage.h"
#include "TileImageDrawInfo.h"
#include "MainWinCanvas.h"
#include "macroToolOpt.h"

#include "DotPenStyle.h"
#include "FillPolygon.h"
#include "Undo.h"

#include "draw_main.h"
#include "draw_calc.h"
#include "draw_op_def.h"
#include "draw_op_sub.h"



//---------------------------
/* 塗りタイプ関数 */

static void (*g_tileimg_colfunc[])(TileImage *,PixelRGBA *,PixelRGBA *,void *) = {
	TileImage_colfunc_normal, TileImage_colfunc_normal, TileImage_colfunc_compareA,
	TileImage_colfunc_overwrite, TileImage_colfunc_overwrite,

	TileImage_colfunc_normal, TileImage_colfunc_overwrite, TileImage_colfunc_erase
};

//---------------------------



/** 操作開始時の情報セット */

void drawOpSub_setOpInfo(DrawData *p,int optype,
	void (*motion)(DrawData *,uint32_t),mBool (*release)(DrawData *),int opsubtype)
{
	p->w.optype = optype;
	p->w.opsubtype = opsubtype;

	p->w.funcMotion = motion;
	p->w.funcRelease = release;
	p->w.funcPressInGrab = NULL;
	p->w.funcAction = NULL;
}

/** 現在のレイヤに描画可能かどうか
 *
 * @return 0 で可能 */

int drawOpSub_canDrawLayer(DrawData *p)
{
	if(LAYERITEM_IS_FOLDER(p->curlayer))
		//フォルダ
		return CANDRAWLAYER_FOLDER;
	else if(LayerItem_isLock_real(p->curlayer))
		//ロック (上位フォルダがロックありの場合もON)
		return CANDRAWLAYER_LOCK;
	else
		return CANDRAWLAYER_OK;
}

/** 現在のレイヤに描画可能かどうか (選択範囲系のモードも判定) */

int drawOpSub_canDrawLayer_andSelMode(DrawData *p)
{
	if(p->sel.mode)
		return CANDRAWLAYER_SELMODE;
	else
		return drawOpSub_canDrawLayer(p);
}

/** カレントレイヤがフォルダか */

mBool drawOpSub_isFolder_curlayer()
{
	return LAYERITEM_IS_FOLDER(APP_DRAW->curlayer);
}

/** 描画がドットペンタイプか (操作中時) */

mBool drawOpSub_isDotPenDraw()
{
	int no = APP_DRAW->w.optoolno;

	return (no == TOOL_DOTPEN || no == TOOL_DOTPEN_ERASE
		|| no == TOOL_FINGER);
}

/** グリッド吸着を行うか */

mBool drawOpSub_isAttachGrid()
{
	return ((APP_DRAW->draw_flags & DRAW_DRAWFLAGS_ATTACH_GRID)
		&& (APP_CONF->fView & CONFIG_VIEW_F_GRID)
		&& APP_CONF->grid_list.top);
}

/** 図形塗りつぶし時、ドット単位で描画するか (操作中時)
 *
 * アンチエイリアスが OFF で、キャンバス回転が０の場合、TRUE。 */

mBool drawOpSub_isPolyFillToolDraw_dot()
{
	uint32_t val;

	val = (APP_DRAW->w.optoolno == TOOL_FILL_POLYGON)?
		APP_DRAW->tool.opt_fillpoly: APP_DRAW->tool.opt_fillpoly_erase;

	return (APP_DRAW->canvas_angle == 0 && !FILLPOLY_IS_ANTIALIAS(val));
}


//========================


/** 領域の XOR 用に tileimgTmp を作成 */

mBool drawOpSub_createTmpImage_area(DrawData *p)
{
	p->tileimgTmp = TileImage_new(TILEIMAGE_COLTYPE_1BIT, p->szCanvas.w, p->szCanvas.h);

	return (p->tileimgTmp != NULL);
}

/** 作業用イメージを作成 [RGBA] (box の範囲で) */

TileImage *drawOpSub_createTileImage_box(mBox *box)
{
	mRect rc;

	mRectSetByBox(&rc, box);

	return TileImage_newFromRect(TILEIMAGE_COLTYPE_RGBA, &rc);
}

/** tileimgTmp を削除 */

void drawOpSub_freeTmpImage(DrawData *p)
{
	TileImage_free(p->tileimgTmp);
	p->tileimgTmp = NULL;
}

/** sel.img_movecopy を解放 */

void drawOpSub_freeSelMoveCopyImage(DrawData *p)
{
	TileImage_free(p->sel.img_movecopy);
	p->sel.img_movecopy = NULL;
}

/** FillPolygon を解放 */

void drawOpSub_freeFillPolygon(DrawData *p)
{
	FillPolygon_free(p->w.fillpolygon);
	p->w.fillpolygon = NULL;
}


//=============================
// 描画開始/終了
//=============================


/** 描画開始
 *
 * [!] この前に drawOpSub_setDrawInfo() を実行すること。
 *     p->w.opdraw_flags のフラグのセットが必要。 */

void drawOpSub_beginDraw(DrawData *p)
{
	TileImageDrawInfo *info = &g_tileimage_dinfo;

	//描画情報の初期化

	info->err = 0;

	TileImageDrawInfo_clearDrawRect();

	//現在のレイヤイメージ情報取得

	TileImage_getInfo(p->curlayer->img, &info->tileimginfo);

	//描画前保存用イメージ作成

	p->tileimgDraw = TileImage_createSame(p->tileimgDraw, p->curlayer->img, TILEIMAGE_COLTYPE_RGBA);
	info->img_save = p->tileimgDraw;
	
	if(!info->img_save) info->err = 1;

	//ブラシ描画のストローク重ね塗り用の濃度バッファイメージ作成

	if(p->w.opdraw_flags & DRAW_OPDRAW_F_BRUSH_STROKE)
	{
		p->tileimgDraw2 = TileImage_createSame(p->tileimgDraw2, p->curlayer->img, TILEIMAGE_COLTYPE_8BIT);
		info->img_brush_stroke = p->tileimgDraw2;

		if(!info->img_brush_stroke) info->err = 1;
	}
	else
		info->img_brush_stroke = NULL;
}

/** 描画終了
 *
 * @param rc  描画されたイメージ範囲
 * @param boximg 更新用イメージ範囲 (キャンバス範囲内調整済み) */

void drawOpSub_endDraw(DrawData *p,mRect *rc,mBox *boximg)
{
	if(g_tileimage_dinfo.err)
	{
		//エラー時はアンドゥクリア

		Undo_deleteAll();
	}
	else
	{
		//----- エラーなし
		
		//空タイルを解放

		TileImage_freeEmptyTiles_byUndo(p->curlayer->img, p->tileimgDraw);

		//undo

		Undo_addTilesImage(&g_tileimage_dinfo.tileimginfo, rc);
	}

	//タイル削除

	TileImage_freeAllTiles(p->tileimgDraw);
	TileImage_freeAllTiles(p->tileimgDraw2);
}

/** 単体描画開始 */

void drawOpSub_beginDraw_single(DrawData *p)
{
	MainWinCanvasArea_setCursor_wait();
	
	drawOpSub_beginDraw(p);
}

/** 単体描画終了 */

void drawOpSub_endDraw_single(DrawData *p)
{
	drawOpSub_finishDraw_single(p);

	MainWinCanvasArea_restoreCursor();
}


//=============================
// 更新/終了の共通処理
//=============================


/** 描画された範囲を全体の範囲に追加し、キャンバス更新
 *
 * [自由線/連続直線/集中線]
 *
 * @param btimer TRUE でタイマー更新セット。FALSE で直接更新。 */

void drawOpSub_addrect_and_update(DrawData *p,mBool btimer)
{
	mBox box;

	if(!mRectIsEmpty(&g_tileimage_dinfo.rcdraw))
	{
		//全体の範囲追加

		mRectUnion(&p->w.rcdraw, &g_tileimage_dinfo.rcdraw);

		//更新

		if(drawCalc_getImageBox_rect(p, &box, &g_tileimage_dinfo.rcdraw))
		{
			if(btimer)
				MainWinCanvasArea_setTimer_updateRect(&box);
			else
				drawUpdate_rect_imgcanvas(p, &box);
		}
	}
}

/** DrawData::w.rcdraw の範囲で描画終了処理を行う
 *
 * 複数描画時用。イメージとキャンバスは更新済みである。 */

void drawOpSub_finishDraw_workrect(DrawData *p)
{
	mBox box;

	if(!mRectIsEmpty(&p->w.rcdraw))
	{
		//イメージ範囲

		if(!drawCalc_getImageBox_rect(p, &box, &p->w.rcdraw))
			box.x = -1;

		//描画終了

		drawOpSub_endDraw(p, &p->w.rcdraw, &box);

		//更新

		MainWinCanvasArea_clearTimer_updateRect(TRUE);

		//[dock]キャンバスビュー更新
		/* 固定表示時は、ストローク描画後に更新 */

		drawUpdate_endDraw_box(p, &box);
	}
}

/** TileImageDrawInfo::rcdraw の範囲で描画終了処理を行う
 *
 * 単体描画時用。イメージとキャンバスの更新も行う。 */

void drawOpSub_finishDraw_single(DrawData *p)
{
	mBox box;

	if(!mRectIsEmpty(&g_tileimage_dinfo.rcdraw))
	{
		//イメージ範囲

		if(!drawCalc_getImageBox_rect(p, &box, &g_tileimage_dinfo.rcdraw))
			box.x = -1;

		//UNDO

		drawOpSub_endDraw(p, &g_tileimage_dinfo.rcdraw, &box);

		//更新

		if(box.x != -1)
		{
			drawUpdate_rect_imgcanvas(p, &box);
			drawUpdate_endDraw_box(p, &box);
		}
	}
}


//==============================
// 描画前のセット
//==============================


/** 1px 消しゴム */

static void _setdrawinfo_1px_erase(DrawData *p,TileImageDrawInfo *info)
{
	info->funcColor = TileImage_colfunc_erase;
	info->texture = NULL;

	p->w.ntmp[0] = 0;
}

/** ドットペン/ドットペン消しゴム */

static void _setdrawinfo_dotpen(DrawData *p,TileImageDrawInfo *info,
	mBool erase,mBool fillmode)
{
	uint32_t val;
	int pixmode;

	if(erase)
		val = p->tool.dotpen_erase_slot[p->tool.dotpen_erase_sel];
	else
		val = p->tool.dotpen_slot[p->tool.dotpen_sel];

	//サイズ、形状

	DotPenStyle_create(DOTPEN_GET_STYLE(val), DOTPEN_GET_SIZE(val));

	info->dotpen_size = DotPenStyle_get(&info->dotpen_buf);

	//濃度
	
	p->w.pixdraw.a = DOTPEN_GET_OPACITY(val);

	//塗り

	pixmode = DOTPEN_GET_PIXMODE(val);

	info->funcColor = (erase)? TileImage_colfunc_erase: g_tileimg_colfunc[pixmode];

	if(!fillmode)
	{
		if(pixmode == PIXELMODE_BLEND_STROKE)
			info->funcDrawPixel = TileImage_setPixel_draw_dotpen_stroke;
		else if(pixmode == PIXELMODE_OVERWRITE_SQUARE)
			info->funcDrawPixel = TileImage_setPixel_draw_dotpen_overwrite_square;
		else
			info->funcDrawPixel = TileImage_setPixel_draw_dotpen_direct;
	}

	//細線

	p->w.ntmp[0] = DOTPEN_IS_FINE(val);
}

/** ブラシ系 */

static void _setdrawinfo_brush(DrawData *p,TileImageDrawInfo *info,int tool,mBool fillmode)
{
	BrushItem *item;

	info->brushparam = BrushList_getDrawParam(tool, &item);
	info->pixdraw = p->w.pixdraw;

	//塗りつぶし時 (ntmp[0] にアンリエイリアスで描画するかをセット)

	if(fillmode)
	{
		p->w.pixdraw.a = item->opacity;
		p->w.ntmp[0] = TRUE;
	}
	
	//

	switch(tool)
	{
		case TOOL_PEN:
		case TOOL_BRUSH:
		case TOOL_BRUSH_ERASE:
			//塗りつぶし、アンチエイリアス

			if(fillmode)
				p->w.ntmp[0] = item->flags & BRUSHITEM_F_ANTIALIAS;
		
			//ストローク重ね塗り

			if(!fillmode && item->pixmode == PIXELMODE_BLEND_STROKE)
			{
				info->funcDrawPixel = TileImage_setPixel_draw_brush_stroke;

				p->w.opdraw_flags |= DRAW_OPDRAW_F_BRUSH_STROKE;
			}

			if(tool == TOOL_BRUSH_ERASE)
				info->funcColor = TileImage_colfunc_erase;
			else
				info->funcColor = g_tileimg_colfunc[item->pixmode];
			break;
		case TOOL_DODGE:
			info->funcColor = TileImage_colfunc_dodge;
			break;
		case TOOL_BURN:
			info->funcColor = TileImage_colfunc_burn;
			break;
		case TOOL_BLUR:
			info->funcColor = TileImage_colfunc_blur;
			break;
	}
}

/** 描画前の各情報セット
 *
 * @param toolno  ツール番号。-1 でマスクなどのみセット */

void drawOpSub_setDrawInfo(DrawData *p,int toolno,int param)
{
	TileImageDrawInfo *info = &g_tileimage_dinfo;
	uint32_t val;

	info->funcDrawPixel = TileImage_setPixel_draw_direct;
	info->funcColor = TileImage_colfunc_normal;

	//テクスチャ

	info->texture = (p->tex.on)? p->tex.curimg: NULL;
	
	//レイヤマスク

	if(LAYERITEM_IS_MASK_UNDER(p->curlayer))
		info->img_mask = LayerList_getMaskImage(p->layerlist, p->curlayer);
	else
		info->img_mask = NULL;

	//アルファマスク

	info->alphamask_type = p->curlayer->alphamask;

	//色マスク

	info->colmask_type = p->col.colmask_type;
	info->colmask_col = p->col.colmask_col;

	//描画先イメージ

	p->w.dstimg = p->curlayer->img;

	//描画色

	p->w.pixdraw.r = M_GET_R(p->col.drawcol);
	p->w.pixdraw.g = M_GET_G(p->col.drawcol);
	p->w.pixdraw.b = M_GET_B(p->col.drawcol);
	p->w.pixdraw.a = 255;

	p->w.opdraw_flags = 0;

	//---------

	if(toolno < 0) return;

	switch(toolno)
	{
		//ドットペン/ドットペン消しゴム
		/* param: 0=通常 1=1px消しゴム -1=塗りつぶし */
		case TOOL_DOTPEN:
		case TOOL_DOTPEN_ERASE:
			if(param == 1)
				_setdrawinfo_1px_erase(p, info);
			else
				_setdrawinfo_dotpen(p, info, (toolno == TOOL_DOTPEN_ERASE), (param == -1));
			break;

		//ブラシ系
		/* param: 0=通常 -1=塗りつぶし*/
		case TOOL_PEN:
		case TOOL_BRUSH:
		case TOOL_WATER:
		case TOOL_BRUSH_ERASE:
		case TOOL_DODGE:
		case TOOL_BURN:
		case TOOL_BLUR:
			_setdrawinfo_brush(p, info, toolno, (param == -1));
			break;

		//指先
		case TOOL_FINGER:
			val = p->tool.opt_finger;

			//サイズ、形状
			DotPenStyle_create(0, FINGER_GET_SIZE(val));
			info->dotpen_size = DotPenStyle_get(&info->dotpen_buf);

			p->w.pixdraw.a = FINGER_GET_STRENGTH(val);
			info->funcDrawPixel = TileImage_setPixel_draw_dotpen_direct;
			info->funcColor = TileImage_colfunc_finger;
			break;

		//図形塗りつぶし
		/* ntmp[0] : アンチエイリアス */
		case TOOL_FILL_POLYGON:
		case TOOL_FILL_POLYGON_ERASE:
			val = (toolno == TOOL_FILL_POLYGON)? p->tool.opt_fillpoly: p->tool.opt_fillpoly_erase;

			p->w.pixdraw.a = FILLPOLY_GET_OPACITY(val);
			p->w.ntmp[0] = FILLPOLY_IS_ANTIALIAS(val);

			if(toolno == TOOL_FILL_POLYGON_ERASE)
				info->funcColor = TileImage_colfunc_erase;
			else
				info->funcColor = g_tileimg_colfunc[FILLPOLY_GET_PIXMODE(val)];
			break;

		//塗りつぶし
		case TOOL_FILL:
			val = p->tool.opt_fill;
			
			p->w.pixdraw.a = FILL_GET_OPACITY(val);
			info->funcColor = g_tileimg_colfunc[FILL_GET_PIXMODE(val)];
			break;
	
		//不透明範囲消去 (テクスチャ無効)
		case TOOL_FILL_ERASE:
			info->funcColor = TileImage_colfunc_erase;
			info->texture = NULL;
			break;

		//グラデーション
		case TOOL_GRADATION:
			info->funcColor = g_tileimg_colfunc[GRAD_GET_PIXMODE(p->tool.opt_grad)];
			break;

		//選択範囲
		case TOOL_SELECT:
			info->funcColor = TileImage_colfunc_overwrite;
			break;

		//範囲イメージ移動
		case TOOL_SEL_MOVE:
			if(SELMOVECOPY_IS_MOVE_OVERWRITE(p->tool.opt_selmovecopy))
				info->funcColor = TileImage_colfunc_overwrite;

			drawOpSub_clearDrawMasks();
			break;

		//範囲イメージコピー
		case TOOL_SEL_COPY:
			if(SELMOVECOPY_IS_COPY_OVERWRITE(p->tool.opt_selmovecopy))
				info->funcColor = TileImage_colfunc_overwrite;

			drawOpSub_clearDrawMasks();
			break;

		//スタンプ
		case TOOL_STAMP:
			info->texture = NULL;

			//上書き
			if(STAMP_IS_OVERWRITE(p->tool.opt_stamp))
				info->funcColor = TileImage_colfunc_overwrite;
			break;
	}
}

/** 上書き描画用に情報セット
 *
 * 範囲内イメージのコピー/切り取り、イメージ入れ替え、選択範囲貼り付け
 * 範囲編集、フィルタ */

void drawOpSub_setDrawInfo_forOverwrite()
{
	drawOpSub_setDrawInfo(APP_DRAW, -1, 0);
	drawOpSub_clearDrawMasks();

	g_tileimage_dinfo.funcColor = TileImage_colfunc_overwrite;
}

/** 描画時のマスク類 (レイヤマスク/テクスチャ/色マスク/Aマスク) をクリア */

void drawOpSub_clearDrawMasks()
{
	TileImageDrawInfo *info = &g_tileimage_dinfo;

	info->img_mask = NULL;
	info->texture = NULL;
	info->colmask_type = 0;
	info->alphamask_type = 0;
}

/** グラデーション描画時の情報をセット */

void drawOpSub_setDrawGradationInfo(TileImageDrawGradInfo *info)
{
	DrawData *p = APP_DRAW;
	uint32_t val;
	uint8_t flags = 0;
	const uint8_t *buf;

	val = p->tool.opt_grad;

	//フラグ (ツール設定から)

	if(GRAD_IS_REVERSE(val)) flags |= TILEIMAGE_DRAWGRAD_F_REVERSE;
	if(GRAD_IS_LOOP(val)) flags |= TILEIMAGE_DRAWGRAD_F_LOOP;

	//グラデーションデータ
	/* buf == NULL で [描画色->背景色] データ */

	if(GRAD_IS_NOT_CUSTOM(val))
		buf = NULL;
	else
		buf = GradationList_getBuf_atIndex(GRAD_GET_SELNO(val));

	if(!buf)
		buf = GradationList_getDefaultData();
	else
	{
		if(*buf & GRADDAT_F_LOOP) flags |= TILEIMAGE_DRAWGRAD_F_LOOP;
		if(*buf & GRADDAT_F_SINGLE_COL) flags |= TILEIMAGE_DRAWGRAD_F_SINGLE_COL;

		buf++;
	}

	//TileImageDrawGradInfo

	info->buf = buf;
	info->flags = flags;
	info->opacity = GRAD_GET_OPACITY(val);
	
	info->pixdraw.r = M_GET_R(p->col.drawcol);
	info->pixdraw.g = M_GET_G(p->col.drawcol);
	info->pixdraw.b = M_GET_B(p->col.drawcol);

	info->pixbkgnd.r = M_GET_R(p->col.bkgndcol);
	info->pixbkgnd.g = M_GET_G(p->col.bkgndcol);
	info->pixbkgnd.b = M_GET_B(p->col.bkgndcol);
}


//=================================
// キャンバス領域への直接描画
//=================================


/** キャンバス領域への直接描画開始 */

mPixbuf *drawOpSub_beginAreaDraw()
{
	return mWidgetBeginDirectDraw(M_WIDGET(APP_WIDGETS->canvas_area));
}

/** キャンバス領域への直接描画終了 */

void drawOpSub_endAreaDraw(mPixbuf *pixbuf,mBox *box)
{
	//[debug]
	//mPixbufBox(pixbuf, box->x, box->y, box->w, box->h, mRGBtoPix(0xff0000));

	mWidgetEndDirectDraw(M_WIDGET(APP_WIDGETS->canvas_area), pixbuf);

	mWidgetUpdateBox_box(M_WIDGET(APP_WIDGETS->canvas_area), box);
}


//============================
// 位置取得
//============================


/** イメージ位置をグリッド吸着処理して返す
 *
 * @return TRUE で吸着処理された */

mBool drawOpSub_pointAttachGrid(DrawData *p,mDoublePoint *dpt)
{
	GridListData *pi;
	int x,y,offx,offy,xx,yy,ax,ay;

	x = floor(dpt->x);
	y = floor(dpt->y);

	//イメージ範囲外は範囲内に調整

	if(x < 0) x = 0;
	else if(x > p->imgw) x = p->imgw;

	if(y < 0) y = 0;
	else if(y > p->imgh) y = p->imgh;

	//

	ax = ay = -1;

	for(pi = (GridListData *)APP_CONF->grid_list.top; pi; pi = (GridListData *)pi->i.next)
	{
		if(!(pi->flags & GRIDDATA_F_ON)) continue;

		//1x1グリッドは除く

		if(pi->w == 1 && pi->h == 1) continue;

		//倍率

		if((pi->flags & GRIDDATA_F_HAVE_ZOOM)
			&& (p->canvas_zoom < pi->zoom_min || p->canvas_zoom > pi->zoom_max))
			continue;

		//範囲

		if(pi->flags & GRIDDATA_F_HAVE_AREA)
		{
			if(x < pi->area[0] || x > pi->area[2] || y < pi->area[1] || y > pi->area[3])
				continue;

			offx = pi->area[0];
			offy = pi->area[1];
		}
		else
			offx = offy = 0;

		//

		xx = (x - offx) / pi->w * pi->w + offx;
		yy = (y - offy) / pi->h * pi->h + offy;

		if(ax == -1)
			ax = xx, ay = yy;
		else
		{
			//カーソル位置と距離が近い方
			
			if(abs(xx - x) < abs(ax - x))
				ax = xx;

			if(abs(yy - y) < abs(ay - y))
				ay = yy;
		}
	}

	if(ax == -1)
		return FALSE;
	else
	{
		dpt->x = ax;
		dpt->y = ay;
		return TRUE;
	}
}

/** 領域座標からイメージ座標の位置を取得 */

static mBool _get_point_image(DrawData *p,mDoublePoint *dpt)
{
	//領域 -> イメージ
	
	drawCalc_areaToimage_double(p, &dpt->x, &dpt->y, dpt->x, dpt->y);

	//グリッド吸着

	if(drawOpSub_isAttachGrid())
		return drawOpSub_pointAttachGrid(p, dpt);

	return FALSE;
}

/** グリッド吸着処理 (領域座標 -> 領域座標) */

void drawOpSub_pointAttachGrid_area(DrawData *p,mPoint *pt)
{
	mDoublePoint dpt;

	dpt.x = pt->x, dpt.y = pt->y;

	if(_get_point_image(p, &dpt))
		drawCalc_imageToarea_pt_double(p, pt, dpt.x, dpt.y);
}


/** 現在の領域位置を int で取得 */

void drawOpSub_getAreaPoint_int(DrawData *p,mPoint *pt)
{
	mDoublePoint dpt;

	//グリッド吸着時は、一度イメージ位置で取得して戻す

	if(drawOpSub_isAttachGrid())
	{
		dpt.x = p->w.dptAreaCur.x;
		dpt.y = p->w.dptAreaCur.y;
		
		if(_get_point_image(p, &dpt))
		{
			drawCalc_imageToarea_pt_double(p, pt, dpt.x, dpt.y);
			return;
		}
	}

	//通常時
	
	pt->x = floor(p->w.dptAreaCur.x);
	pt->y = floor(p->w.dptAreaCur.y);
}

/** 現在の領域位置を int で取得 (生データ) */

void drawOpSub_getAreaPoint_int_raw(DrawData *p,mPoint *pt)
{
	pt->x = floor(p->w.dptAreaCur.x);
	pt->y = floor(p->w.dptAreaCur.y);
}

/** DrawPoint の領域位置から mDoublePoint のイメージ位置を取得 */

void drawOpSub_getImagePoint_fromDrawPoint(DrawData *p,mDoublePoint *pt,DrawPoint *dpt)
{
	pt->x = dpt->x, pt->y = dpt->y;

	_get_point_image(p, pt);
}

/** 現在のイメージ位置を int で取得 */

void drawOpSub_getImagePoint_int(DrawData *p,mPoint *pt)
{
	mDoublePoint dpt;

	dpt.x = p->w.dptAreaCur.x;
	dpt.y = p->w.dptAreaCur.y;

	_get_point_image(p, &dpt);

	pt->x = floor(dpt.x);
	pt->y = floor(dpt.y);
}

/** 現在のイメージ位置を int で取得 (生データ) */

void drawOpSub_getImagePoint_int_raw(DrawData *p,mPoint *pt)
{
	double x,y;

	drawCalc_areaToimage_double(p, &x, &y, p->w.dptAreaCur.x, p->w.dptAreaCur.y);

	pt->x = floor(x);
	pt->y = floor(y);
}

/** 現在のイメージ位置を取得 (double) */

void drawOpSub_getImagePoint_double(DrawData *p,mDoublePoint *pt)
{
	pt->x = p->w.dptAreaCur.x;
	pt->y = p->w.dptAreaCur.y;

	_get_point_image(p, pt);
}

/** 描画位置取得 (DrawPoint) */

void drawOpSub_getDrawPoint(DrawData *p,DrawPoint *dst)
{
	mDoublePoint dpt;

	dpt.x = p->w.dptAreaCur.x;
	dpt.y = p->w.dptAreaCur.y;

	_get_point_image(p, &dpt);

	dst->x = dpt.x;
	dst->y = dpt.y;
	dst->pressure = p->w.dptAreaCur.pressure;
}

/** 描画位置取得 (ドットペン自由線時) */

void drawOpSub_getDrawPoint_dotpen(DrawData *p,mPoint *pt)
{
	mDoublePoint dpt;

	dpt.x = p->w.dptAreaCur.x;
	dpt.y = p->w.dptAreaCur.y;

	_get_point_image(p, &dpt);

	if(p->rule.type)
	{
		dpt.x = floor(dpt.x) + 0.5;
		dpt.y = floor(dpt.y) + 0.5;

		(p->rule.funcGetPoint)(p, &dpt.x, &dpt.y);
	}

	pt->x = floor(dpt.x);
	pt->y = floor(dpt.y);
}


//============================
// 図形描画用、位置取得
//============================


/** 直線描画時の位置を取得 */

void drawOpSub_getDrawLinePoints(DrawData *p,
	mDoublePoint *pt1,mDoublePoint *pt2,mBool coordinate_image)
{
	if(coordinate_image)
	{
		//イメージ座標

		pt1->x = p->w.pttmp[0].x, pt1->y = p->w.pttmp[0].y;
		pt2->x = p->w.pttmp[1].x, pt2->y = p->w.pttmp[1].y;
	}
	else
	{
		//領域座標
		/* dpt_tmp = グリッド吸着なしの領域位置。
		 * グリッド吸着ありの場合は、元の位置から再計算。 */

		pt1->x = p->w.dpt_tmp[0].x, pt1->y = p->w.dpt_tmp[0].y;
		pt2->x = p->w.dpt_tmp[1].x, pt2->y = p->w.dpt_tmp[1].y;

		_get_point_image(p, pt1);
		_get_point_image(p, pt2);
	}
}

/** 四角形描画時の4点を取得
 *
 * @param coordinate_image TRUE:イメージ座標 FALSE:領域座標 */

void drawOpSub_getDrawBoxPoints(DrawData *p,mDoublePoint *pt,mBool coordinate_image)
{
	mDoublePoint pt1,pt2;

	pt1.x = p->w.pttmp[0].x, pt1.y = p->w.pttmp[0].y;
	pt2.x = p->w.pttmp[1].x, pt2.y = p->w.pttmp[1].y;

	if(coordinate_image)
	{
		//イメージ座標

		pt[0] = pt1;
		pt[1].x = pt2.x, pt[1].y = pt1.y;
		pt[2] = pt2;
		pt[3].x = pt1.x, pt[3].y = pt2.y;
	}
	else
	{
		//領域座標
		
		drawCalc_areaToimage_double(p, &pt[0].x, &pt[0].y, pt1.x, pt1.y);
		drawCalc_areaToimage_double(p, &pt[1].x, &pt[1].y, pt2.x, pt1.y);
		drawCalc_areaToimage_double(p, &pt[2].x, &pt[2].y, pt2.x, pt2.y);
		drawCalc_areaToimage_double(p, &pt[3].x, &pt[3].y, pt1.x, pt2.y);
	}
}

/** 回転なし時の四角形描画の範囲取得
 *
 * 四角形塗りつぶし時。 */

void drawOpSub_getDrawBox_noangle(DrawData *p,mBox *box)
{
	mPoint pt[4];

	drawCalc_areaToimage_pt(p, pt    , p->w.pttmp[0].x, p->w.pttmp[0].y);
	drawCalc_areaToimage_pt(p, pt + 1, p->w.pttmp[1].x, p->w.pttmp[0].y);
	drawCalc_areaToimage_pt(p, pt + 2, p->w.pttmp[1].x, p->w.pttmp[1].y);
	drawCalc_areaToimage_pt(p, pt + 3, p->w.pttmp[0].x, p->w.pttmp[1].y);

	mBoxSetByPoints(box, pt, 4);
}

/** 楕円描画時の位置と半径を取得 */

void drawOpSub_getDrawEllipseParam(DrawData *p,
	mDoublePoint *pt_ct,mDoublePoint *pt_radius,mBool coordinate_image)
{
	double xr,yr;

	if(coordinate_image)
	{
		//イメージ座標

		pt_ct->x = p->w.pttmp[0].x;
		pt_ct->y = p->w.pttmp[0].y;
		pt_radius->x = p->w.pttmp[1].x;
		pt_radius->y = p->w.pttmp[1].y;
	}
	else
	{
		//領域座標

		drawCalc_areaToimage_double_pt(p, pt_ct, p->w.pttmp);

		xr = p->w.pttmp[1].x;
		yr = p->w.pttmp[1].y;

		pt_radius->x = xr * p->viewparam.scalediv;
		pt_radius->y = yr * p->viewparam.scalediv;
	}
}

/** pttmp[0] を 1 .. num までにコピー */

void drawOpSub_copyTmpPoint(DrawData *p,int num)
{
	int i;

	for(i = 1; i <= num; i++)
		p->w.pttmp[i] = p->w.pttmp[0];
}


//==========================
// ほか
//==========================


/** 選択範囲の移動時の計算
 *
 * ptd_tmp[0] : 総移動数 (領域座標)
 * pttmp[2]   : 現在の総相対移動数 (イメージ座標)
 *
 * @param ptret    前回からの相対移動数
 * @param in_image 範囲がイメージの範囲内に収まるようにする
 * @return 前回から移動するか */

mBool drawOpSub_calc_moveForSelect(DrawData *p,uint32_t state,mPoint *ptret,mBool in_image)
{
	mPoint pt,pt2;
	mDoublePoint dpt;
	double x,y;

	//カーソル移動距離

	x = p->w.dptAreaCur.x - p->w.dptAreaLast.x;
	y = p->w.dptAreaCur.y - p->w.dptAreaLast.y;

	if(state & M_MODS_CTRL)  x = 0;
	if(state & M_MODS_SHIFT) y = 0;

	//総移動数

	p->w.ptd_tmp[0].x += x;
	p->w.ptd_tmp[0].y += y;

	//総移動数を キャンバス -> イメージ 座標変換

	pt2.x = (int)p->w.ptd_tmp[0].x;
	pt2.y = (int)p->w.ptd_tmp[0].y;

	drawCalc_areaToimage_relative(p, &pt, &pt2);

	//グリッド吸着の場合、左上座標を吸着させて差を取得

	if(drawOpSub_isAttachGrid())
	{
		dpt.x = p->w.boxtmp[0].x + pt.x;
		dpt.y = p->w.boxtmp[0].y + pt.y;

		if(drawOpSub_pointAttachGrid(p, &dpt))
		{
			pt.x = (int)dpt.x - p->w.boxtmp[0].x;
			pt.y = (int)dpt.y - p->w.boxtmp[0].y;
		}
	}

	//イメージの範囲内に調整

	if(in_image)
	{
		pt2.x = p->w.boxtmp[0].x + pt.x;
		pt2.y = p->w.boxtmp[0].y + pt.y;

		if(pt2.x < 0)
			pt2.x = 0;
		else if(pt2.x + p->w.boxtmp[0].w > p->imgw)
			pt2.x = p->imgw - p->w.boxtmp[0].w;

		if(pt2.y < 0)
			pt2.y = 0;
		else if(pt2.y + p->w.boxtmp[0].h > p->imgh)
			pt2.y = p->imgh - p->w.boxtmp[0].h;

		pt.x = pt2.x - p->w.boxtmp[0].x;
		pt.y = pt2.y - p->w.boxtmp[0].y;
	}

	//前回と比較して、移動したか

	pt2.x = pt.x - p->w.pttmp[2].x;
	pt2.y = pt.y - p->w.pttmp[2].y;

	if(pt2.x == 0 && pt2.y == 0)
		return FALSE;
	else
	{
		p->w.pttmp[2] = pt;

		*ptret = pt2;
		
		return TRUE;
	}
}

/** 選択範囲の領域内にポインタがあるか */

mBool drawOpSub_isPoint_inSelect(DrawData *p)
{
	mPoint pt;

	if(p->sel.boxsel.w == 0)
		return FALSE;
	else
	{
		drawOpSub_getImagePoint_int_raw(p, &pt);

		return (p->sel.boxsel.x <= pt.x && pt.x < p->sel.boxsel.x + p->sel.boxsel.w
			&& p->sel.boxsel.y <= pt.y && pt.y < p->sel.boxsel.y + p->sel.boxsel.h);
	}
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DrawData
 *
 * 操作 - XOR 処理
 *****************************************/

#include <stdlib.h>

#include "mDef.h"
#include "mRectBox.h"
#include "mPixbuf.h"

#include "defDraw.h"
#include "TileImage.h"
#include "TileImageDrawInfo.h"
#include "FillPolygon.h"

#include "StatusBar.h"

#include "draw_main.h"
#include "draw_calc.h"
#include "draw_op_def.h"
#include "draw_op_func.h"
#include "draw_op_sub.h"
#include "draw_rule.h"
#include "draw_select.h"



//=============================
// sub
//=============================


/** 四角形、矩形範囲取得
 *
 * 現在位置 (x,y) と開始点 (ptstart) から矩形範囲を取得。 */

static void _common_rect_get(DrawData *p,mRect *rc,mPoint *ptstart,int x,int y,uint32_t state)
{
	int x1,y1,x2,y2,w,h;

	//左上、左下

	if(x < ptstart->x)
		x1 = x, x2 = ptstart->x;
	else
		x1 = ptstart->x, x2 = x;

	if(y < ptstart->y)
		y1 = y, y2 = ptstart->y;
	else
		y1 = ptstart->y, y2 = y;

	//正方形調整

	if(state & M_MODS_SHIFT)
	{
		w = x2 - x1;
		h = y2 - y1;

		if(w < h)
		{
			if(x < ptstart->x)
				x1 = x2 - h;
			else
				x2 = x1 + h;
		}
		else
		{
			if(y < ptstart->y)
				y1 = y2 - w;
			else
				y2 = y1 + w;
		}
	}

	rc->x1 = x1, rc->y1 = y1;
	rc->x2 = x2, rc->y2 = y2;
}

/** 四角形時、pttmp[0],[1] に矩形セット */

static void _common_rect_set(DrawData *p,mPoint *pt,uint32_t state)
{
	mRect rc;

	_common_rect_get(p, &rc, p->w.pttmp + 2, pt->x, pt->y, state);

	p->w.pttmp[0].x = rc.x1;
	p->w.pttmp[0].y = rc.y1;
	p->w.pttmp[1].x = rc.x2;
	p->w.pttmp[1].y = rc.y2;
}


//=============================
// XOR直線 (領域/イメージ座標)
//=============================
/*
 * ntmp[1] : イメージ座標で扱うか
 * pttmp[0] : 始点
 * pttmp[1] : 終点 (現在の位置)
 * dpt_tmp[0,1] : 始点、終点 (グリッド吸着前の領域座標)
 *
 * - 実際の直線描画時は、dpt_tmp (グリッド吸着なしの座標) から描画位置が計算される。
 * 	 そのため、dpt_tmp の座標はグリッド吸着を適用してはいけない。
 *
 * - ドット描画の場合、最初からイメージ座標で処理しないと、
 *   45度単位が実際に描画した時、座標変換の誤差でずれる場合がある。
 */


/** 移動 (連続直線/集中線/多角形と共用) */

static void _xorline_motion(DrawData *p,uint32_t state)
{
	mPoint pt;

	//現在の位置

	if(p->w.ntmp[1])
		drawOpSub_getImagePoint_int(p, &pt);
	else
		drawOpSub_getAreaPoint_int(p, &pt);

	if(pt.x == p->w.pttmp[1].x && pt.y == p->w.pttmp[1].y)
		return;

	//

	drawOpXor_drawline(p);

	if(p->w.ntmp[1])
	{
		//---- イメージ座標

		if(state & M_MODS_SHIFT)
			drawCalc_fitLine45(&pt, p->w.pttmp);
	}
	else
	{
		//---- 領域座標

		if(!(state & M_MODS_SHIFT))
			//通常
			p->w.dpt_tmp[1] = p->w.dptAreaCur;
		else
		{
			//45度単位

			if(drawOpSub_isAttachGrid())
			{
				/* [グリッド吸着時]
				 * - dpt_tmp はグリッド吸着前の座標にしなければならない。
				 * - 45度単位よりグリッド吸着の座標を優先。
				 *   (45度補正の後、グリッド吸着処理) */
				
				drawOpSub_getAreaPoint_int_raw(p, &pt);
				drawCalc_fitLine45(&pt, p->w.pttmp);

				p->w.dpt_tmp[1].x = pt.x;
				p->w.dpt_tmp[1].y = pt.y;

				drawOpSub_pointAttachGrid_area(p, &pt);
			}
			else
			{
				//グリッド吸着なし
				
				drawCalc_fitLine45(&pt, p->w.pttmp);

				p->w.dpt_tmp[1].x = pt.x;
				p->w.dpt_tmp[1].y = pt.y;
			}
		}
	}

	p->w.pttmp[1] = pt;

	drawOpXor_drawline(p);
}

/** 離し */

static mBool _xorline_release(DrawData *p)
{
	drawOpXor_drawline(p);

	switch(p->w.opsubtype)
	{
		//直線描画
		case DRAW_OPSUB_DRAW_LINE:
			drawOpDraw_line(p, p->w.ntmp[1]);
			break;
		//ベジェ曲線
		case DRAW_OPSUB_TO_BEZIER:
			return drawOp_xorline_to_bezier(p);
		//定規設定
		case DRAW_OPSUB_RULE_SETTING:
			drawRule_setting_line(p);
			break;
		//グラデーション描画
		case DRAW_OPSUB_DRAW_GRADATION:
			drawOpDraw_gradation(p);
			break;
	}
	
	return TRUE;
}

/** 押し */

mBool drawOpXor_line_press(DrawData *p,int opsubtype,mBool coordinate_image)
{
	drawOpSub_setOpInfo(p, DRAW_OPTYPE_XOR_LINE,
		_xorline_motion, _xorline_release, opsubtype);

	p->w.opflags |= DRAW_OPFLAGS_MOTION_POS_INT;

	p->w.ntmp[1] = coordinate_image;

	if(coordinate_image)
		drawOpSub_getImagePoint_int(p, p->w.pttmp);
	else
		drawOpSub_getAreaPoint_int(p, p->w.pttmp);

	p->w.pttmp[1] = p->w.pttmp[0];
	p->w.dpt_tmp[0] = p->w.dpt_tmp[1] = p->w.dptAreaCur;

	drawOpXor_drawline(p);

	return TRUE;
}


//=============================
// XOR 四角形 (領域座標)
//=============================
/*
	pttmp[0] : 左上
	pttmp[1] : 右下
	pttmp[2] : 開始点
*/


/** 移動 */

static void _xorbox_area_motion(DrawData *p,uint32_t state)
{
	mPoint pt;

	drawOpXor_drawBox_area(p);

	drawOpSub_getAreaPoint_int(p, &pt);

	_common_rect_set(p, &pt, state);

	drawOpXor_drawBox_area(p);
}

/** 離し */

static mBool _xorbox_area_release(DrawData *p)
{
	drawOpXor_drawBox_area(p);

	switch(p->w.opsubtype)
	{
		//四角形枠描画
		case DRAW_OPSUB_DRAW_FRAME:
			drawOpDraw_box(p, FALSE);
			break;
		//四角形塗りつぶし
		case DRAW_OPSUB_DRAW_FILL:
			drawOpDraw_fillBox_forArea(p);
			break;
	}
	
	return TRUE;
}

/** 押し */

mBool drawOpXor_boxarea_press(DrawData *p,int opsubtype)
{
	drawOpSub_setOpInfo(p, DRAW_OPTYPE_XOR_BOXAREA,
		_xorbox_area_motion, _xorbox_area_release, opsubtype);

	p->w.opflags |= DRAW_OPFLAGS_MOTION_POS_INT;

	drawOpSub_getAreaPoint_int(p, p->w.pttmp);

	p->w.pttmp[1] = p->w.pttmp[2] = p->w.pttmp[0];

	drawOpXor_drawBox_area(p);

	return TRUE;
}


//=================================
// XOR 四角形 (イメージ座標)
//=================================
/*
	ntmp[0]  : グリッド吸着か
	pttmp[0] : 左上位置 (イメージ座標)
	pttmp[1] : 右下位置 (イメージ座標)
	pttmp[2] : 開始点 (イメージ座標)
	pttmp[3] : 前回の位置 (イメージ座標)

	boxtmp[0] : 結果の mBox
	rctmp[0]  : 結果の mRect

	グリッド吸着時は、右下を -1 する。
	ただし、幅または高さが 1px の場合は除く。
*/


/** 移動 */

static void _xorbox_image_motion(DrawData *p,uint32_t state)
{
	mPoint pt;

	drawOpSub_getImagePoint_int(p, &pt);

	if(pt.x != p->w.pttmp[3].x || pt.y != p->w.pttmp[3].y)
	{
		drawOpXor_drawBox_image(p);

		_common_rect_set(p, &pt, state);

		//グリッド吸着補正

		if(p->w.ntmp[0])
		{
			if(p->w.pttmp[0].x != p->w.pttmp[1].x)
				p->w.pttmp[1].x--;

			if(p->w.pttmp[0].y != p->w.pttmp[1].y)
				p->w.pttmp[1].y--;
		}

		drawOpXor_drawBox_image(p);

		p->w.pttmp[3] = pt;

		//statusbar

		StatusBar_setHelp_imgbox();
	}
}

/** 離し */

static mBool _xorbox_image_release(DrawData *p)
{
	mBool not_1x1;

	drawOpXor_drawBox_image(p);

	//結果範囲 (mBox, mRect)

	mBoxSetByPoint(p->w.boxtmp, p->w.pttmp, p->w.pttmp + 1);

	p->w.rctmp[0].x1 = p->w.pttmp[0].x, p->w.rctmp[0].y1 = p->w.pttmp[0].y;
	p->w.rctmp[0].x2 = p->w.pttmp[1].x, p->w.rctmp[0].y2 = p->w.pttmp[1].y;

	//1x1 でないか

	not_1x1 = (p->w.boxtmp[0].w != 1 || p->w.boxtmp[0].h != 1);

	//statusbar

	StatusBar_setHelp_tool();

	//

	switch(p->w.opsubtype)
	{
		//四角形枠
		case DRAW_OPSUB_DRAW_FRAME:
			drawOpDraw_box(p, TRUE);
			break;
		//四角形塗りつぶし
		case DRAW_OPSUB_DRAW_FILL:
			drawOpDraw_fillBox_forImage(p);
			break;
		//選択範囲、範囲セット
		case DRAW_OPSUB_SET_SELECT:
			if(drawCalc_getImageBox_rect(p, &p->w.boxtmp[0], &p->w.rctmp[0]))
				drawSelect_setSelectBox(p, &p->w.boxtmp[0]);
			break;
		//スタンプ画像セット
		case DRAW_OPSUB_SET_STAMP:
			if(not_1x1)
				drawOp_setStampImage(p);
			break;
		//範囲イメージ移動/コピー
		case DRAW_OPSUB_SEL_MOVECOPY:
			if(not_1x1)
				drawOp_selMoveCopy_select(p);
			break;
		//範囲イメージ置換
		case DRAW_OPSUB_SEL_REPLACE:
			if(not_1x1)
				drawOp_selReplace_select(p);
			break;
	}
	
	return TRUE;
}

/** 押し */

mBool drawOpXor_boximage_press(DrawData *p,int opsubtype)
{
	drawOpSub_setOpInfo(p, DRAW_OPTYPE_XOR_BOXIMAGE,
		_xorbox_image_motion, _xorbox_image_release, opsubtype);

	p->w.opflags |= DRAW_OPFLAGS_MOTION_POS_INT;

	p->w.ntmp[0] = drawOpSub_isAttachGrid();

	drawOpSub_getImagePoint_int(p, p->w.pttmp);

	drawOpSub_copyTmpPoint(p, 3);

	drawOpXor_drawBox_image(p);

	//statusbar

	StatusBar_setHelp_imgbox();

	return TRUE;
}


//=============================
// XOR 楕円 (領域/イメージ座標)
//=============================
/*
 * ntmp[0]  : イメージ座標か
 * pttmp[0] : 位置
 * pttmp[1] : 半径
 * pttmp[2] : 押し位置
 * rctmp[0] : 矩形での範囲
 */


/** 移動 */

static void _xorellipse_motion(DrawData *p,uint32_t state)
{
	mPoint pt;
	int x,y,xr,yr;

	drawOpXor_drawEllipse(p);

	if(p->w.ntmp[0])
		drawOpSub_getImagePoint_int(p, &pt);
	else
		drawOpSub_getAreaPoint_int(p, &pt);

	//

	if((state & M_MODS_CTRL) && p->w.opsubtype != DRAW_OPSUB_RULE_SETTING)
	{
		//---- +Ctrl : 矩形
		/* 定規の楕円設定の場合は、自由線+Ctrl での動作のため、
		 * 矩形指定は無効にする。 */

		_common_rect_get(p, p->w.rctmp, p->w.pttmp + 2, pt.x, pt.y, state);

		p->w.pttmp[1].x = (p->w.rctmp[0].x2 - p->w.rctmp[0].x1 + 1) / 2;
		p->w.pttmp[1].y = (p->w.rctmp[0].y2 - p->w.rctmp[0].y1 + 1) / 2;
		p->w.pttmp[0].x = p->w.rctmp[0].x1 + p->w.pttmp[1].x;
		p->w.pttmp[0].y = p->w.rctmp[0].y1 + p->w.pttmp[1].y;
	}
	else
	{
		//---- 通常 : 中心と半径

		x = p->w.pttmp[0].x;
		y = p->w.pttmp[0].y;
		
		xr = abs(pt.x - x);
		yr = abs(pt.y - y);

		if(state & M_MODS_SHIFT)
		{
			if(xr > yr) yr = xr;
			else xr = yr;
		}

		p->w.pttmp[1].x = xr;
		p->w.pttmp[1].y = yr;

		p->w.rctmp[0].x1 = x - xr;
		p->w.rctmp[0].y1 = y - yr;
		p->w.rctmp[0].x2 = x + xr;
		p->w.rctmp[0].y2 = y + yr; 
	}

	drawOpXor_drawEllipse(p);
}

/** 離し */

static mBool _xorellipse_release(DrawData *p)
{
	drawOpXor_drawEllipse(p);

	switch(p->w.opsubtype)
	{
		//円枠描画
		case DRAW_OPSUB_DRAW_FRAME:
			drawOpDraw_ellipse(p, p->w.ntmp[0]);
			break;
		//円塗りつぶし描画
		case DRAW_OPSUB_DRAW_FILL:
			drawOpDraw_fillEllipse(p, p->w.ntmp[0]);
			break;
		//定規設定
		case DRAW_OPSUB_RULE_SETTING:
			drawRule_setting_ellipse(p);
			break;
	}
	
	return TRUE;
}

/** 押し */

mBool drawOpXor_ellipse_press(DrawData *p,int opsubtype,mBool coordinate_image)
{
	drawOpSub_setOpInfo(p, DRAW_OPTYPE_XOR_ELLIPSE,
		_xorellipse_motion, _xorellipse_release, opsubtype);

	p->w.opflags |= DRAW_OPFLAGS_MOTION_POS_INT;

	p->w.ntmp[0] = coordinate_image;

	if(coordinate_image)
		drawOpSub_getImagePoint_int(p, p->w.pttmp);
	else
		drawOpSub_getAreaPoint_int(p, p->w.pttmp);

	p->w.pttmp[1].x = p->w.pttmp[1].y = 0;
	p->w.pttmp[2] = p->w.pttmp[0];

	mRectSetByPoint(&p->w.rctmp[0], p->w.pttmp);

	drawOpXor_drawEllipse(p);

	return TRUE;
}


//========================================
// XOR 複数直線 (連続直線/集中線)
//========================================
/*
 * 直線と同じ。
 * 
 * pttmp[2] : 一番最初の点 (保存用)
 * dpt_tmp[2] : 上と同じ (グリッド吸着なしの領域座標) */


/** ２回目以降の押し時 */

static void _xorsumline_pressInGrab(DrawData *p,uint32_t state)
{
	drawOpXor_drawline(p);

	drawOpDraw_lineSuccConc(p);

	if(p->w.opsubtype == DRAW_OPSUB_DRAW_SUCCLINE)
	{
		p->w.pttmp[0] = p->w.pttmp[1];
		p->w.dpt_tmp[0] = p->w.dpt_tmp[1];
	}

	drawOpXor_drawline(p);
}

/** 各アクション (操作終了) */

static mBool _xorsumline_action(DrawData *p,int action)
{
	//BackSpace は連続直線のみ

	if(action == DRAW_FUNCACTION_KEY_BACKSPACE
		&& p->w.opsubtype != DRAW_OPSUB_DRAW_SUCCLINE)
		return FALSE;

	//

	drawOpXor_drawline(p);

	//BackSpace (連続直線) => 始点と結ぶ

	if(action == DRAW_FUNCACTION_KEY_BACKSPACE)
	{
		p->w.pttmp[1] = p->w.pttmp[2];
		p->w.dpt_tmp[1] = p->w.dpt_tmp[2];

		drawOpDraw_lineSuccConc(p);
	}

	drawOpSub_finishDraw_workrect(p);

	return TRUE;
}

/** 複数直線の初期化 (多角形と共用) */

static void _xorsumline_init(DrawData *p,int optype,int opsubtype,mBool coordinate_image)
{
	drawOpSub_setOpInfo(p, optype,
		_xorline_motion, drawOp_common_norelease, opsubtype);

	p->w.opflags |= DRAW_OPFLAGS_MOTION_POS_INT;

	p->w.ntmp[1] = coordinate_image;

	if(coordinate_image)
		drawOpSub_getImagePoint_int(p, p->w.pttmp);
	else
		drawOpSub_getAreaPoint_int(p, p->w.pttmp);

	drawOpSub_copyTmpPoint(p, 2);

	p->w.dpt_tmp[0] = p->w.dpt_tmp[1] = p->w.dpt_tmp[2] = p->w.dptAreaCur;

	drawOpXor_drawline(p);
}

/** 押し */

mBool drawOpXor_sumline_press(DrawData *p,int opsubtype,mBool coordinate_image)
{
	_xorsumline_init(p, DRAW_OPTYPE_XOR_SUMLINE, opsubtype, coordinate_image);

	p->w.funcPressInGrab = _xorsumline_pressInGrab;
	p->w.funcAction = _xorsumline_action;

	//描画開始
	drawOpSub_setDrawInfo(p, p->w.optoolno, 0);
	drawOpSub_beginDraw(p);

	return TRUE;
}


//=================================
// 多角形
//=================================
/*
 * 連続直線/集中線と同じ。
 * 
 * boxtmp[0] : XOR 描画用範囲
 * 
 * - 確定した線は XOR 用イメージに描画。
 * - 現在の線はキャンバスに直接 XOR 描画。
 */ 


/** ２回目以降の押し時 */

static void _xorpolygon_pressInGrab(DrawData *p,uint32_t state)
{
	mDoublePoint pt;

	//ポイント追加

	drawOpSub_getImagePoint_fromDrawPoint(p, &pt, p->w.dpt_tmp + 1);

	FillPolygon_addPoint(p->w.fillpolygon, pt.x, pt.y);

	//XOR

	drawOpXor_drawline(p);
	drawOpXor_drawPolygon(p);

	TileImage_drawLineB(p->tileimgTmp, p->w.pttmp[0].x, p->w.pttmp[0].y,
		p->w.pttmp[1].x, p->w.pttmp[1].y, &p->w.pixdraw, FALSE);

	drawOpXor_drawPolygon(p);

	p->w.pttmp[0] = p->w.pttmp[1];
	
	drawOpXor_drawline(p);
}

/** 各アクション (操作終了) */

static mBool _xorpolygon_action(DrawData *p,int action)
{
	//BackSpace 無効

	if(action == DRAW_FUNCACTION_KEY_BACKSPACE) return FALSE;

	//XOR 消去

	drawOpXor_drawline(p);
	drawOpXor_drawPolygon(p);

	drawOpSub_freeTmpImage(p);

	//描画

	if(action != DRAW_FUNCACTION_KEY_ESC
		&& action != DRAW_FUNCACTION_UNGRAB)
	{
		if(FillPolygon_closePoint(p->w.fillpolygon))
		{
			switch(p->w.opsubtype)
			{
				//多角形塗りつぶし
				case DRAW_OPSUB_DRAW_FILL:
					drawOpDraw_fillPolygon(p);
					break;
				//スタンプイメージセット
				case DRAW_OPSUB_SET_STAMP:
					drawOp_setStampImage(p);
					break;
				//範囲イメージ移動/コピー
				case DRAW_OPSUB_SEL_MOVECOPY:
					drawOp_selMoveCopy_select(p);
					break;
			}
		}
	}

	drawOpSub_freeFillPolygon(p);

	return TRUE;
}

/** 多角形初期化 (投げ縄と共用) */

static mBool _fillpolygon_init(DrawData *p)
{
	mDoublePoint pt;

	if(!drawOpSub_createTmpImage_area(p))
		return FALSE;

	//FillPolygon 作成

	if(!(p->w.fillpolygon = FillPolygon_new()))
	{
		drawOpSub_freeTmpImage(p);
		return FALSE;
	}

	//現在位置追加

	drawOpSub_getImagePoint_double(p, &pt);

	FillPolygon_addPoint(p->w.fillpolygon, pt.x, pt.y);

	//

	g_tileimage_dinfo.funcDrawPixel = TileImage_setPixel_new_notp;

	p->w.pixdraw.a = 255;

	return TRUE;
}

/** 最初の押し時 */

mBool drawOpXor_polygon_press(DrawData *p,int opsubtype)
{
	if(!_fillpolygon_init(p)) return FALSE;

	//XOR 更新範囲

	p->w.boxtmp[0].x = p->w.boxtmp[0].y = 0;
	p->w.boxtmp[0].w = p->szCanvas.w;
	p->w.boxtmp[0].h = p->szCanvas.h;

	//

	_xorsumline_init(p, DRAW_OPTYPE_XOR_POLYGON, opsubtype, FALSE);

	p->w.funcPressInGrab = _xorpolygon_pressInGrab;
	p->w.funcAction = _xorpolygon_action;

	return TRUE;
}


//=============================
// XOR 投げ縄
//=============================
/*
	pttmp[0] : 線描画用、現在の位置
	pttmp[1] : 前回の位置
*/


/** 移動 */

static void _xorlasso_motion(DrawData *p,uint32_t state)
{
	mDoublePoint pt;

	p->w.pttmp[1] = p->w.pttmp[0];

	drawOpSub_getAreaPoint_int(p, p->w.pttmp);

	drawOpXor_drawLasso(p, FALSE);

	//ポイント追加

	drawOpSub_getImagePoint_double(p, &pt);
	FillPolygon_addPoint(p->w.fillpolygon, pt.x, pt.y);
}

/** 離し */

static mBool _xorlasso_release(DrawData *p)
{
	drawOpXor_drawLasso(p, TRUE);
	drawOpSub_freeTmpImage(p);

	if(FillPolygon_closePoint(p->w.fillpolygon))
	{
		switch(p->w.opsubtype)
		{
			//フリーハンド塗りつぶし
			case DRAW_OPSUB_DRAW_FILL:
				drawOpDraw_fillPolygon(p);
				break;
			//スタンプイメージセット
			case DRAW_OPSUB_SET_STAMP:
				drawOp_setStampImage(p);
				break;
			//範囲イメージ移動/コピー
			case DRAW_OPSUB_SEL_MOVECOPY:
				drawOp_selMoveCopy_select(p);
				break;
		}
	}

	drawOpSub_freeFillPolygon(p);

	return TRUE;
}

/** 各アクション (キーなどの操作は無効) */

static mBool _xorlasso_action(DrawData *p,int action)
{
	if(action == DRAW_FUNCACTION_UNGRAB)
		return _xorlasso_release(p);
	else
		return FALSE;
}

/** 押し時 */

mBool drawOpXor_lasso_press(DrawData *p,int opsubtype)
{
	if(!_fillpolygon_init(p)) return FALSE;

	drawOpSub_setOpInfo(p, DRAW_OPTYPE_XOR_LASSO,
		_xorlasso_motion, _xorlasso_release, opsubtype);

	p->w.funcAction = _xorlasso_action;

	drawOpSub_getAreaPoint_int(p, p->w.pttmp);

	drawOpSub_copyTmpPoint(p, 2);

	drawOpXor_drawLasso(p, FALSE);

	return TRUE;
}


//=========================================
// 定規の位置設定 (集中線、同心円[正円])
//=========================================
/*
	pttmp[0] : 押し位置
*/


/** 離し */

static mBool _rulepoint_release(DrawData *p)
{
	drawOpXor_drawCrossPoint(p);

	return TRUE;
}

/** 押し */

mBool drawOpXor_rulepoint_press(DrawData *p)
{
	drawOpSub_setOpInfo(p, DRAW_OPTYPE_TMP,
		NULL, _rulepoint_release, 0);

	drawOpSub_getAreaPoint_int(p, p->w.pttmp);

	drawOpXor_drawCrossPoint(p);

	return TRUE;
}


/********************************
 * XOR 描画
 ********************************/


/** XOR 直線描画 (領域/イメージ座標)
 *
 * pttmp[0] が始点、pttmp[1] が終点 */

void drawOpXor_drawline(DrawData *p)
{
	mPixbuf *pixbuf;
	mBox box;
	mPoint pt1,pt2;
	double d;

	if((pixbuf = drawOpSub_beginAreaDraw()))
	{
		if(p->w.ntmp[1])
		{
			//イメージ座標 -> 領域座標
			//(表示倍率300%以上なら、ドットの中心位置に)

			d = (p->canvas_zoom < 3000)? 0: 0.5;
		
			drawCalc_imageToarea_pt_double(p, &pt1, p->w.pttmp[0].x + d, p->w.pttmp[0].y + d);
			drawCalc_imageToarea_pt_double(p, &pt2, p->w.pttmp[1].x + d, p->w.pttmp[1].y + d);
		}
		else
		{
			pt1 = p->w.pttmp[0];
			pt2 = p->w.pttmp[1];
		}

		mPixbufLine(pixbuf, pt1.x, pt1.y, pt2.x, pt2.y, MPIXBUF_COL_XOR);

		mBoxSetByPoint(&box, &pt1, &pt2);

		drawOpSub_endAreaDraw(pixbuf, &box);
	}
}

/** XOR 四角形描画 (領域) */

void drawOpXor_drawBox_area(DrawData *p)
{
	mPixbuf *pixbuf;
	mBox box;

	if((pixbuf = drawOpSub_beginAreaDraw()))
	{
		box.x = p->w.pttmp[0].x;
		box.y = p->w.pttmp[0].y;
		box.w = p->w.pttmp[1].x - p->w.pttmp[0].x + 1;
		box.h = p->w.pttmp[1].y - p->w.pttmp[0].y + 1;

		mPixbufBoxSlow(pixbuf, box.x, box.y, box.w, box.h, MPIXBUF_COL_XOR);

		drawOpSub_endAreaDraw(pixbuf, &box);
	}
}

/** XOR 四角形描画 (イメージに対する) */

void drawOpXor_drawBox_image(DrawData *p)
{
	int x1,y1,x2,y2,i;
	mPoint pt[5];
	mPixbuf *pixbuf;
	mBox box;

	if((pixbuf = drawOpSub_beginAreaDraw()))
	{
		x1 = p->w.pttmp[0].x, y1 = p->w.pttmp[0].y;
		x2 = p->w.pttmp[1].x, y2 = p->w.pttmp[1].y;

		if(p->canvas_zoom != 1000)
			x2++, y2++;

		//イメージ -> 領域座標

		drawCalc_imageToarea_pt(p, pt, x1, y1);
		drawCalc_imageToarea_pt(p, pt + 1, x2, y1);
		drawCalc_imageToarea_pt(p, pt + 2, x2, y2);
		drawCalc_imageToarea_pt(p, pt + 3, x1, y2);
		pt[4] = pt[0];

		//更新範囲

		mBoxSetByPoints(&box, pt, 4);

		//描画

		if(x1 == x2 && y1 == y2)
			mPixbufSetPixel(pixbuf, pt[0].x, pt[0].y, MPIXBUF_COL_XOR);
		else if(x1 == x2 || y1 == y2)
			mPixbufLine(pixbuf, pt[0].x, pt[0].y, pt[2].x, pt[2].y, MPIXBUF_COL_XOR);
		else
		{
			for(i = 0; i < 4; i++)
				mPixbufLine_noend(pixbuf, pt[i].x, pt[i].y, pt[i + 1].x, pt[i + 1].y, MPIXBUF_COL_XOR);
		}

		drawOpSub_endAreaDraw(pixbuf, &box);
	}
}

/** XOR 円描画 (領域/イメージ座標) */

void drawOpXor_drawEllipse(DrawData *p)
{
	mPixbuf *pixbuf;
	mRect rc;
	mBox box;
	double d;

	if((pixbuf = drawOpSub_beginAreaDraw()))
	{
		rc = p->w.rctmp[0];

		//イメージ座標
		
		if(p->w.ntmp[0])
		{
			d = (p->canvas_zoom < 3000)? 0: 0.5;
		
			drawCalc_imageToarea_pt_double(p, (mPoint *)&rc.x1, rc.x1 + d, rc.y1 + d);
			drawCalc_imageToarea_pt_double(p, (mPoint *)&rc.x2, rc.x2 + d, rc.y2 + d);
		}
		
		mPixbufEllipse(pixbuf, rc.x1, rc.y1, rc.x2, rc.y2, MPIXBUF_COL_XOR);

		mBoxSetByRect(&box, &rc);

		drawOpSub_endAreaDraw(pixbuf, &box);
	}
}

/** XOR ベジェ曲線描画 */

void drawOpXor_drawBezier(DrawData *p,mBool erase)
{
	mPixbuf *pixbuf;

	//描画

	if(!erase)
	{
		TileImage_freeAllTiles(p->tileimgTmp);

		TileImageDrawInfo_clearDrawRect();
		TileImage_drawBezier_forXor(p->tileimgTmp, p->w.pttmp, p->w.opsubtype);

		drawCalc_clipArea_toBox(p, p->w.boxtmp, &g_tileimage_dinfo.rcdraw);
	}

	//XOR

	if((pixbuf = drawOpSub_beginAreaDraw()))
	{
		TileImage_blendXorToPixbuf(p->tileimgTmp, pixbuf, p->w.boxtmp);

		drawOpSub_endAreaDraw(pixbuf, p->w.boxtmp);
	}
}

/** XOR 多角形描画 */

void drawOpXor_drawPolygon(DrawData *p)
{
	mPixbuf *pixbuf;

	if((pixbuf = drawOpSub_beginAreaDraw()))
	{
		TileImage_blendXorToPixbuf(p->tileimgTmp, pixbuf, p->w.boxtmp);

		drawOpSub_endAreaDraw(pixbuf, p->w.boxtmp);
	}
}

/** XOR 投げ縄描画 */

void drawOpXor_drawLasso(DrawData *p,mBool erase)
{
	mPixbuf *pixbuf;
	mBox box;
	mRect rc;

	if((pixbuf = drawOpSub_beginAreaDraw()))
	{
		if(erase)
		{
			//消去

			box.x = box.y = 0;
			box.w = p->szCanvas.w, box.h = p->szCanvas.h;

			TileImage_blendXorToPixbuf(p->tileimgTmp, pixbuf, &box);
		}
		else
		{
			//新しい線描画

			mRectSetByPoint_minmax(&rc, p->w.pttmp, p->w.pttmp + 1);

			if(drawCalc_clipArea_toBox(p, &box, &rc))
			{
				TileImage_blendXorToPixbuf(p->tileimgTmp, pixbuf, &box);

				TileImage_drawLineB(p->tileimgTmp, p->w.pttmp[1].x, p->w.pttmp[1].y,
					p->w.pttmp[0].x, p->w.pttmp[0].y, &p->w.pixdraw, FALSE);

				TileImage_blendXorToPixbuf(p->tileimgTmp, pixbuf, &box);
			}
		}

		drawOpSub_endAreaDraw(pixbuf, &box);
	}
}

/** XOR 十字線描画 */

void drawOpXor_drawCrossPoint(DrawData *p)
{
	mPixbuf *pixbuf;
	int x,y;
	mBox box;

	if((pixbuf = drawOpSub_beginAreaDraw()))
	{
		x = p->w.pttmp[0].x;
		y = p->w.pttmp[0].y;
	
		mPixbufLineH(pixbuf, x - 9, y, 19, MPIXBUF_COL_XOR);
		mPixbufLineV(pixbuf, x, y - 9, 19, MPIXBUF_COL_XOR);

		box.x = x - 9, box.y = y - 9;
		box.w = box.h = 19;

		drawOpSub_endAreaDraw(pixbuf, &box);
	}
}

/** XOR ブラシサイズ円描画 */

/* pttmp[0] : 中心位置
 * ntmp[0]  : 半径 px サイズ */

void drawOpXor_drawBrushSizeCircle(DrawData *p,mBool erase)
{
	mPixbuf *pixbuf;
	mBox box;
	int r;

	if(erase)
		box = p->w.boxtmp[0];
	else
	{
		r = p->w.ntmp[0];
		
		box.x = p->w.pttmp[0].x - r;
		box.y = p->w.pttmp[0].y - r;
		box.w = box.h = (r << 1) + 1;

		p->w.boxtmp[0] = box;
	}

	//描画

	if((pixbuf = drawOpSub_beginAreaDraw()))
	{
		mPixbufEllipse(pixbuf,
			box.x, box.y, box.x + box.w - 1, box.y + box.h - 1, MPIXBUF_COL_XOR);

		drawOpSub_endAreaDraw(pixbuf, &box);
	}
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DrawData
 *
 * レイヤ関連
 *****************************************/

#include <string.h>

#include "mDef.h"
#include "mStr.h"
#include "mRectBox.h"

#include "defDraw.h"
#include "defFileFormat.h"
#include "AppErr.h"

#include "PopupThread.h"
#include "Docks_external.h"

#include "LayerList.h"
#include "LayerItem.h"
#include "TileImage.h"
#include "Undo.h"

#include "draw_main.h"
#include "draw_op_sub.h"



/** 新規レイヤ */

void drawLayer_newLayer(DrawData *p,mBool folder)
{
	LayerItem *pi;

	//追加

	if(folder)
		pi = LayerList_addLayer(p->layerlist, p->curlayer);
	else
		pi = LayerList_addLayer_image(p->layerlist, p->curlayer);

	if(!pi) return;

	//名前、フラグ

	if(folder)
	{
		mStrdup_ptr(&pi->name, "folder");

		pi->flags |= LAYERITEM_F_FOLDER_EXPAND;
	}
	else
		LayerList_setItemName_byNum(p->layerlist, pi);

	//

	p->curlayer = pi;

	//

	Undo_addLayerNew();

	DockLayer_update_all();
}


//===============================
// ファイルから読み込み
//===============================


typedef struct
{
	const char *filename;
	TileImage *img;
	LayerItem *item;
	int format;
	mBool ignore_alpha,
		leave_trans;
}_thdata_loadfile;


/** [スレッド] ファイルから読み込み */

static int _thread_loadfile(mPopupProgress *prog,void *data)
{
	_thdata_loadfile *p = (_thdata_loadfile *)data;

	if(p->format == FILEFORMAT_APD_v3)
	{
		//APD v3

		p->item = LayerList_addLayer_apdv3(APP_DRAW->layerlist, APP_DRAW->curlayer,
				p->filename, prog);

		return (p->item)? APPERR_OK: APPERR_LOAD;
	}
	else
	{
		//画像ファイル

		p->img = TileImage_loadFile(p->filename, -1,
			p->ignore_alpha, p->leave_trans, 20000, NULL, prog, NULL);

		return (p->img)? APPERR_OK: APPERR_LOAD;
	}
}

/** ファイルから読み込んで新規レイヤ
 *
 * @param rcupdate 更新範囲が入る。複数ファイルの同時読み込み用 (NULL で単体更新)
 * @return APPERR */

int drawLayer_newLayer_file(DrawData *p,const char *filename,
	mBool ignore_alpha,mBool leave_trans,mRect *rcupdate)
{
	_thdata_loadfile dat;
	LayerItem *item;
	int err,format;
	mRect rc;
	mStr str = MSTR_INIT;

	//ファイルフォーマット (通常画像 or APD v3)

	format = FileFormat_getbyFileHeader(filename);

	if(!FileFormat_isNormalImage(format) && format != FILEFORMAT_APD_v3)
	{
		return (format == FILEFORMAT_APD_v1v2)? APPERR_LOAD_ONLY_APDv3: APPERR_UNSUPPORTED_FORMAT;
	}

	//スレッド用データ

	mMemzero(&dat, sizeof(_thdata_loadfile));

	dat.filename = filename;
	dat.format = format;
	dat.ignore_alpha = ignore_alpha;
	dat.leave_trans = leave_trans;

	//読み込み

	err = PopupThread_run(&dat, _thread_loadfile);
	if(err == -1) err = APPERR_ALLOC;

	if(err != APPERR_OK) return err;

	//レイヤ追加

	if(format == FILEFORMAT_APD_v3)
		item = dat.item;
	else
	{
		//------ 画像ファイルから
	
		item = LayerList_addLayer(p->layerlist, p->curlayer);
		if(!item)
		{
			TileImage_free(dat.img);
			return APPERR_ALLOC;
		}

		item->img = dat.img;

		//名前 (ファイル名から)

		mStrPathGetFileNameNoExt(&str, filename);

		mStrdup_ptr(&item->name, str.buf);

		mStrFree(&str);
	}

	//

	p->curlayer = item;

	/* 読み込まれた結果フォルダの場合があるので注意 */

	//undo

	Undo_addLayerNew();

	//更新など

	TileImage_getHaveImageRect_pixel(item->img, &rc, NULL);

	if(rcupdate)
		mRectUnion(rcupdate, &rc);
	else
	{
		drawUpdate_rect_imgcanvas_canvasview_fromRect(p, &rc);
		DockLayer_update_all();
	}

	return APPERR_OK;
}


//===============================
// コマンド
//===============================


/** カレントレイヤを複製 */

void drawLayer_copy(DrawData *p)
{
	LayerItem *pi;

	//フォルダは不可

	if(LAYERITEM_IS_FOLDER(p->curlayer)) return;

	//複製

	pi = LayerList_dupLayer(p->layerlist, p->curlayer);
	if(!pi) return;

	p->curlayer = pi;

	//undo

	Undo_addLayerCopy();

	//更新

	drawUpdate_rect_imgcanvas_canvasview_inLayerHave(p, pi);
	DockLayer_update_all();
}

/** カレントレイヤを削除 */

void drawLayer_delete(DrawData *p,mBool update)
{
	mRect rc;
	LayerItem *cur;

	cur = p->curlayer;

	/* 親がルートで、前後にレイヤがない場合 (そのレイヤがフォルダで、子がある場合も含む)、
	 * 削除するとレイヤが一つもなくなるので、削除しない */

	if(!(cur->i.parent) && !(cur->i.prev) && !(cur->i.next))
		return;

	//undo、更新範囲

	if(update)
	{
		Undo_addLayerDelete();

		LayerItem_getVisibleImageRect(cur, &rc);
	}

	//削除

	p->curlayer = LayerList_deleteLayer(p->layerlist, cur);

	//更新

	if(update)
	{
		drawUpdate_rect_imgcanvas_canvasview_fromRect(p, &rc);
		DockLayer_update_all();
	}
}

/** レイヤ削除 (UNDO 用) */

void drawLayer_deleteForUndo(DrawData *p,LayerItem *item)
{
	LayerItem *cur;

	if(!item) return;

	//削除後のカレントレイヤ
	/* item が現在のカレントレイヤの場合、
	 * または現在のカレントレイヤが item の子の場合、
	 * カレントレイヤが削除されることになるので、item の前後をカレントにする */

	cur = p->curlayer;

	if(item == cur || LayerItem_isChildItem(cur, item))
	{
		cur = LayerItem_getNextPass(item);
		if(!cur) cur = LayerItem_getPrevExpand(item);
	}

	//レイヤ削除

	LayerList_deleteLayer(APP_DRAW->layerlist, item);

	//カレントレイヤ

	p->curlayer = cur;
}

/** カレントレイヤを消去 */

void drawLayer_erase(DrawData *p)
{
	mRect rc;

	if(drawOpSub_canDrawLayer(p)) return;

	Undo_addLayerClearImage();

	LayerItem_getVisibleImageRect(p->curlayer, &rc);

	//

	TileImage_clear(p->curlayer->img);

	//更新

	drawUpdate_rect_imgcanvas_canvasview_fromRect(p, &rc);
	DockLayer_update_curlayer(FALSE);
}

/** イメージ全体の編集 (左右、上下反転)
 *
 * フォルダの場合は下位レイヤもすべて処理される。 */

void drawLayer_editFullImage(DrawData *p,int type)
{
	mRect rc;

	if(LayerItem_editFullImage(p->curlayer, type, &rc))
	{
		//undo

		Undo_addLayerReverseHorzVert(type);

		//更新

		drawUpdate_rect_imgcanvas_canvasview_fromRect(p, &rc);

		if(LAYERITEM_IS_IMAGE(p->curlayer))
			DockLayer_update_curlayer(FALSE);
		else
			DockLayer_update_all();
	}
}


//===============================
// 下レイヤ結合/移す
//===============================


typedef struct
{
	LayerItem *item_src,*item_dst;
	int opacity_dst;
	mRect rcarea;
}_thdata_combine;


/** [スレッド] 下レイヤに結合/移す */

static int _thread_combine(mPopupProgress *prog,void *data)
{
	_thdata_combine *p = (_thdata_combine *)data;

	TileImage_combine(p->item_dst->img, p->item_src->img, &p->rcarea,
		p->item_src->opacity, p->opacity_dst, p->item_src->blendmode, prog);

	return 1;
}

/** 下レイヤに結合 or 移す */

void drawLayer_combine(DrawData *p,mBool drop)
{
	LayerItem *item_src,*item_dst;
	mRect rcsrc,rcdst;
	_thdata_combine dat;

	//結合元と結合先レイヤ (フォルダは除く)

	item_src = p->curlayer;
	if(LAYERITEM_IS_FOLDER(item_src)) return;

	item_dst = (LayerItem *)item_src->i.next;
	if(!item_dst || LAYERITEM_IS_FOLDER(item_dst)) return;

	//イメージ範囲

	TileImage_getHaveImageRect_pixel(item_src->img, &rcsrc, NULL);
	TileImage_getHaveImageRect_pixel(item_dst->img, &rcdst, NULL);

	//"移す"で、元イージが空ならそのまま

	if(drop && mRectIsEmpty(&rcsrc)) return;

	//処理する px 範囲
	/* "結合"の場合、結合先のレイヤ不透明度を結合後イメージに適用させた後、
	 * レイヤ不透明度を 100% に変更するので、結合先全体も処理範囲に含める。 */

	if(!drop)
		mRectUnion(&rcsrc, &rcdst);
	
	//undo

	Undo_addLayerDropOrCombine(drop);

	//結合処理

	if(!mRectIsEmpty(&rcsrc))
	{
		dat.item_dst = item_dst;
		dat.item_src = item_src;
		dat.opacity_dst = (drop)? LAYERITEM_OPACITY_MAX: item_dst->opacity;
		dat.rcarea = rcsrc;

		if(PopupThread_run(&dat, _thread_combine) == -1)
			return;
	}

	//

	if(drop)
	{
		//"移す" => カレントレイヤを消去

		TileImage_clear(item_src->img);

		DockLayer_update_curlayer(FALSE);
		DockLayer_update_layer(item_dst);
	}
	else
	{
		//"結合"

		item_dst->opacity = LAYERITEM_OPACITY_MAX;

		//カレントレイヤを削除
		drawLayer_delete(p, FALSE);

		DockLayer_update_all();
	}

	drawUpdate_rect_imgcanvas_canvasview_fromRect(p, &rcsrc);
}


//===============================
// 複数レイヤ結合
//===============================


typedef struct
{
	TileImage *imgdst;
	LayerItem *topitem;
	mRect rcupdate;
}_thdata_combinemulti;


/** [スレッド] 複数レイヤ結合 */

static int _thread_combine_multi(mPopupProgress *prog,void *data)
{
	_thdata_combinemulti *p = (_thdata_combinemulti *)data;
	LayerItem *pi;
	int num;
	mRect rc;

	mRectEmpty(&p->rcupdate);

	//結合数

	for(pi = p->topitem, num = 0; pi; pi = pi->link, num++);

	//結合

	mPopupProgressThreadSetMax(prog, num);

	for(pi = p->topitem; pi; pi = pi->link)
	{
		if(TileImage_getHaveImageRect_pixel(pi->img, &rc, NULL))
		{
			TileImage_combine(p->imgdst, pi->img, &rc,
				LayerItem_getOpacity_real(pi), LAYERITEM_OPACITY_MAX,
				pi->blendmode, NULL);

			mRectUnion(&p->rcupdate, &rc);
		}

		mPopupProgressThreadIncPos(prog);
	}

	return 1;
}

/** 複数レイヤ結合 */

void drawLayer_combineMulti(DrawData *p,int target,mBool newlayer)
{
	LayerItem *top,*newitem,*pi;
	TileImage *img;
	_thdata_combinemulti dat;

	//リンクをセット (top が NULL の場合あり)

	top = LayerList_setLink_combineMulti(p->layerlist, target, p->curlayer);

	//結合用イメージ作成

	img = TileImage_new(TILEIMAGE_COLTYPE_RGBA, 1, 1);
	if(!img) return;

	//img にイメージ結合

	if(!top)
		mRectEmpty(&dat.rcupdate);
	else
	{
		dat.imgdst = img;
		dat.topitem = top;

		if(PopupThread_run(&dat, _thread_combine_multi) == -1)
		{
			TileImage_free(img);
			return;
		}
	}

	//undo (新規レイヤでない場合)

	if(!newlayer)
	{
		if(target == 0)
			Undo_addLayerCombineAll();
		else
			Undo_addLayerCombineFolder();
	}

	//各レイヤ処理

	switch(target)
	{
		//すべての表示レイヤ
		case 0:
			if(!newlayer)
				LayerList_clear(p->layerlist);

			newitem = LayerList_addLayer(p->layerlist, NULL);

			if(newlayer)
				LayerList_moveitem(p->layerlist, newitem, LayerList_getItem_top(p->layerlist), FALSE);

			LayerList_setItemName_byNum(p->layerlist, newitem);
			break;
		//フォルダ内
		case 1:
			newitem = LayerList_addLayer(p->layerlist, NULL);

			LayerList_moveitem(p->layerlist, newitem, p->curlayer, FALSE);

			mStrdup_ptr(&newitem->name, p->curlayer->name);

			if(!newlayer)
				drawLayer_delete(p, FALSE);
			break;
		//チェック (チェックの一番上のレイヤの上へ)
		default:
			for(pi = top; pi && pi->link; pi = pi->link);

			newitem = LayerList_addLayer(p->layerlist, pi);

			LayerList_setItemName_byNum(p->layerlist, newitem);
			break;
	}

	newitem->img = img;

	p->curlayer = newitem;

	//undo (新規レイヤ時)

	if(newlayer)
		Undo_addLayerNew();

	//更新

	drawUpdate_rect_imgcanvas_canvasview_fromRect(p, &dat.rcupdate);
	DockLayer_update_all();
}


//===============================
// 画像の統合
//===============================


/** [スレッド] 画像の統合 */

static int _thread_blendall(mPopupProgress *prog,void *data)
{
	TileImage *img = (TileImage *)data;

	//ImageBuf24 に合成

	drawImage_blendImage_real(APP_DRAW);

	//ImageBuf24 からイメージ作成

	TileImage_setImage_fromImageBuf24(img, APP_DRAW->blendimg, prog, FALSE);
	
	return 1;
}

/** 画像の統合 (すべて合成) */

void drawLayer_blendAll(DrawData *p)
{
	TileImage *img;
	LayerItem *item;
	
	//統合後のイメージを作成

	img = TileImage_new(TILEIMAGE_COLTYPE_RGBA, p->imgw, p->imgh);
	if(!img) return;

	//実行

	if(PopupThread_run(img, _thread_blendall) == -1)
	{
		TileImage_free(img);
		return;
	}

	//undo

	Undo_addLayerCombineAll();

	//レイヤクリア & 追加

	LayerList_clear(p->layerlist);

	item = LayerList_addLayer(p->layerlist, NULL);

	LayerList_setItemName_byNum(p->layerlist, item);
	item->img = img;

	p->curlayer = item;

	//更新

	drawUpdate_all_layer();
}


//===============================
// 状態など変更
//===============================


/** カレントレイヤ変更
 *
 * レイヤ一覧上で、変更前のレイヤと変更後のレイヤが更新される。
 * 変更前のレイヤを更新させない場合は DrawData::curlayer を NULL にしておく。
 *
 * @return 変更されたか */

mBool drawLayer_setCurrent(DrawData *p,LayerItem *item)
{
	if(item == p->curlayer)
		return FALSE;
	else
	{
		LayerItem *last = p->curlayer;

		p->curlayer = item;

		DockLayer_update_layer(last);
		DockLayer_update_curlayer(TRUE);

		return TRUE;
	}
}

/** カレントレイヤ変更 (一覧上でカレントが見える状態にする)
 *
 * - item = カレントの場合でも、スクロールは行う。
 * - item がフォルダに隠れて見えない場合は親を展開。
 * - 一覧上で範囲外の位置にあるならスクロール。 */

void drawLayer_setCurrent_visibleOnList(DrawData *p,LayerItem *item)
{
	LayerItem *last;
	mBool update_all = FALSE;

	//以前のカレント (変わらないなら NULL)

	last = p->curlayer;
	if(last == item) last = NULL;

	//カレント変更

	p->curlayer = item;

	//閉じたフォルダに隠れている場合は親を展開

	if(!LayerItem_isVisibleOnList(item))
	{
		LayerItem_setExpandParent(item);
		update_all = TRUE;
	}

	//更新

	DockLayer_update_changecurrent_visible(last, update_all);
}

/** フォルダの展開状態を反転 */

void drawLayer_revFolderExpand(DrawData *p,LayerItem *item)
{
	item->flags ^= LAYERITEM_F_FOLDER_EXPAND;

	DockLayer_update_all();
}

/** 表示/非表示を反転 */

void drawLayer_revVisible(DrawData *p,LayerItem *item)
{
	mRect rc;

	if(LAYERITEM_IS_VISIBLE(item))
	{
		//表示 => 非表示

		LayerItem_getVisibleImageRect(item, &rc);

		item->flags ^= LAYERITEM_F_VISIBLE;

		drawUpdate_rect_imgcanvas_canvasview_fromRect(p, &rc);
	}
	else
	{
		//非表示 => 表示

		item->flags ^= LAYERITEM_F_VISIBLE;

		drawUpdate_rect_imgcanvas_canvasview_inLayerHave(p, item);
	}
}

/** ロック状態を反転 */

void drawLayer_revLock(DrawData *p,LayerItem *item)
{
	item->flags ^= LAYERITEM_F_LOCK;

	//undo

	Undo_addLayerFlags(item, LAYERITEM_F_LOCK);

	//更新

	DockLayer_update_layer(item);
}

/** 塗りつぶし判定元フラグ反転 */

void drawLayer_revFillRef(DrawData *p,LayerItem *item)
{
	item->flags ^= LAYERITEM_F_FILLREF;

	DockLayer_update_layer(item);
}

/** チェックフラグ反転 */

void drawLayer_revChecked(DrawData *p,LayerItem *item)
{
	item->flags ^= LAYERITEM_F_CHECKED;

	DockLayer_update_layer(item);
}

/** レイヤマスク反転 */

void drawLayer_revLayerMask(DrawData *p,LayerItem *item)
{
	item->flags ^= LAYERITEM_F_MASK_UNDER;

	DockLayer_update_layer(item);
}


//=========================
// 表示
//=========================


/** すべて表示/非表示/カレントのみ
 *
 * @param type [0]非表示 [1]表示 [2]カレントのみ */

void drawLayer_showAll(DrawData *p,int type)
{
	LayerList_setVisible_all(p->layerlist, (type == 1));

	if(type == 2)
		LayerItem_setVisible(p->curlayer);

	drawUpdate_all_layer(p);
}

/** チェックレイヤの表示反転 */

void drawLayer_showRevChecked(DrawData *p)
{
	mRect rc;

	LayerList_showRevChecked(p->layerlist, &rc);

	drawUpdate_rect_imgcanvas_canvasview_fromRect(p, &rc);
	DockLayer_update_all();
}

/** フォルダを除くレイヤの表示反転 */

void drawLayer_showRevImage(DrawData *p)
{
	mRect rc;

	LayerList_showRevImage(p->layerlist, &rc);

	drawUpdate_rect_imgcanvas_canvasview_fromRect(p, &rc);
	DockLayer_update_all();
}

/** すべてのレイヤの指定フラグを OFF */

void drawLayer_allFlagsOff(DrawData *p,uint32_t flags)
{
	LayerList_allFlagsOff(p->layerlist, flags);

	DockLayer_update_all();
}


//==============================
// 移動
//==============================


/** カレントレイヤを上下に移動 (同じフォルダ内で) */

void drawLayer_moveUpDown(DrawData *p,mBool bUp)
{
	int parent,no;

	LayerList_getItemPos_forParent(p->layerlist, p->curlayer, &parent, &no);

	if(LayerList_moveitem_updown(p->layerlist, p->curlayer, bUp))
	{
		//undo

		Undo_addLayerMoveList(p->curlayer, parent, no);

		//更新

		drawUpdate_rect_imgcanvas_canvasview_inLayerHave(p, p->curlayer);
		DockLayer_update_all();
	}
}

/** D&D によるレイヤ移動
 *
 * @param type [0]挿入 [1]フォルダ */

void drawLayer_moveDND(DrawData *p,LayerItem *dst,int type)
{
	LayerItem *cur;
	mTreeItem *parent,*prev;
	int pno,no;

	cur = p->curlayer;
	parent = cur->i.parent;
	prev = cur->i.prev;

	LayerList_getItemPos_forParent(p->layerlist, cur, &pno, &no);

	//移動

	LayerList_moveitem(p->layerlist, cur, dst, (type == 1));

	//位置が変わっていない

	if(cur->i.parent == parent && cur->i.prev == prev)
		return;

	//undo

	Undo_addLayerMoveList(cur, pno, no);

	//更新

	DockLayer_update_all();
	drawUpdate_rect_imgcanvas_canvasview_inLayerHave(p, cur);
}

/** アンドゥ実行時のレイヤ順番移動 */

mBool drawLayer_moveForUndo(DrawData *p,int *val,mRect *update)
{
	//移動

	if(!LayerList_moveitem_forUndo(p->layerlist, val, update))
		return FALSE;

	/* 現在のカレントがリスト上で見えない場合、
	 * (移動後に非展開のフォルダの子になった場合)
	 * カレントの親をすべて展開して見えるようにする。 */

	if(!LayerItem_isVisibleOnList(p->curlayer))
		LayerItem_setExpandParent(p->curlayer);

	return TRUE;
}


/** レイヤの選択を一つ上下に移動 */

void drawLayer_currentSelUpDown(DrawData *p,mBool up)
{
	LayerItem *cur = p->curlayer;

	if(up)
		cur = LayerItem_getPrevExpand(cur);
	else
		cur = LayerItem_getNextExpand(cur);

	if(cur)
		drawLayer_setCurrent(p, cur);
}

/** 押し位置に点がある最上位のレイヤを選択 */

void drawLayer_selectPixelTopLayer(DrawData *p)
{
	LayerItem *item;
	mPoint pt;

	drawOpSub_getImagePoint_int(p, &pt);

	item = LayerList_getItem_topPixelLayer(p->layerlist, pt.x, pt.y);

	if(item)
		drawLayer_setCurrent_visibleOnList(p, item);
}

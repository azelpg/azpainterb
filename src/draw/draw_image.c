/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DrawData
 *
 * イメージ関連
 *****************************************/

#include "mDef.h"
#include "mPopupProgress.h"

#include "defMacros.h"
#include "defDraw.h"
#include "defScalingType.h"
#include "AppErr.h"

#include "ImageBuf24.h"
#include "ImageBuf8.h"

#include "TileImage.h"
#include "TileImageDrawInfo.h"
#include "LayerList.h"
#include "LayerItem.h"
#include "Undo.h"

#include "PopupThread.h"
#include "Docks_external.h"

#include "draw_main.h"
#include "draw_select.h"



//===================
// sub
//===================


/** イメージサイズ変更時 */

static void _change_imagesize(DrawData *p)
{
	g_tileimage_dinfo.imgw = p->imgw;
	g_tileimage_dinfo.imgh = p->imgh;
}


//=========================
// サイズ変更
//=========================


/** イメージ変更時やイメージサイズ変更時
 *
 * @param change_file 編集ファイルが変わった時 TRUE。イメージサイズのみの変更時は FALSE */

void drawImage_afterChange(DrawData *p,mBool change_file)
{
	//選択範囲系をキャンセル
	drawSelect_cancel(FALSE);

	_change_imagesize(p);

	if(change_file)
		drawCanvas_fitWindow(p);
	else
		//リサイズ時はスクロールをリセットするだけ
		drawCanvas_setScrollDefault(p);

	drawUpdate_all_layer(p);

	DockCanvasView_changeImageSize();
}

/** 途中でキャンバスイメージを変更する時 */

mBool drawImage_changeImageSize(DrawData *p,int w,int h)
{
	//作業用イメージ再作成

	ImageBuf24_free(p->blendimg);

	if(!(p->blendimg = ImageBuf24_new(w, h)))
	{
		//失敗した場合、元のサイズで
		p->blendimg = ImageBuf24_new(p->imgw, p->imgh);
		return FALSE;
	}

	//

	p->imgw = w;
	p->imgh = h;

	return TRUE;
}


//=========================
//
//=========================


/** 新規イメージ作成
 *
 * @param dpi      0 以下でデフォルト値
 * @param addlayer レイヤを追加するか */

mBool drawImage_new(DrawData *p,int w,int h,int dpi,mBool addlayer)
{
	if(w < 1) w = 1;
	if(h < 1) h = 1;

	//undo & レイヤをクリア

	Undo_deleteAll();
	Undo_clearUpdateFlag();

	LayerList_clear(p->layerlist);

	//情報

	p->imgw = w;
	p->imgh = h;
	p->imgdpi = (dpi <= 0)? 96: dpi;
	p->load_tpcol = -1;
	p->curlayer = NULL;

	_change_imagesize(p);

	//作業用イメージ

	ImageBuf24_free(p->blendimg);

	if(!(p->blendimg = ImageBuf24_new(w, h)))
		return FALSE;

	//レイヤ追加

	if(addlayer)
	{
		if(!(p->curlayer = LayerList_addLayer_newimage(p->layerlist)))
			return FALSE;
	}

	return TRUE;
}

/** 画像読み込みエラー時の処理
 *
 * @return FALSE で現状のまま変更なし */

mBool drawImage_onLoadError(DrawData *p)
{
	LayerItem *top;

	top = LayerList_getItem_top(p->layerlist);

	if(!top || !p->curlayer)
	{
		//新規イメージ
	
		if(!top)
			drawImage_new(p, 400, 400, -1, TRUE);

		//カレントレイヤ

		if(!p->curlayer)
			p->curlayer = top;

		return TRUE;
	}

	return FALSE;
}

/** 画像ファイルを読み込んで作成
 *
 * @return エラーコード */

int drawImage_loadFile(DrawData *p,const char *filename,
	int format,mBool ignore_alpha,mBool leave_trans,
	mPopupProgress *prog,char **errmes)
{
	TileImage *img;
	LayerItem *li;
	TileImageLoadFileInfo info;

	*errmes = NULL;

	//TileImage に読み込み

	img = TileImage_loadFile(filename, format,
		ignore_alpha, leave_trans, IMAGE_SIZE_MAX, &info, prog, errmes);

	if(!img) return APPERR_LOAD;

	//新規キャンバス

	if(!drawImage_new(p, info.width, info.height, info.dpi, FALSE))
		goto ERR;

	//レイヤ作成

	li = LayerList_addLayer(p->layerlist, NULL);
	if(!li) goto ERR;

	li->img = img;
	mStrdup_ptr(&li->name, "layer0");

	//カレントレイヤ

	p->curlayer = li;

	//透過色

	p->load_tpcol = info.transparent;

	return APPERR_OK;

ERR:
	TileImage_free(img);
	return APPERR_ALLOC;
}


//=============================
// キャンバスサイズ変更
//=============================


typedef struct
{
	int w,h,offx,offy;
}_thdata_resizecanvas;


static int _thread_resize_canvas(mPopupProgress *prog,void *data)
{
	_thdata_resizecanvas *p = (_thdata_resizecanvas *)data;
	LayerItem *pi;
	TileImage *img;
	mRect rc;

	rc.x1 = rc.y1 = 0;
	rc.x2 = p->w - 1;
	rc.y2 = p->h - 1;

	mPopupProgressThreadSetMax(prog, LayerList_getNum(APP_DRAW->layerlist));

	for(pi = LayerList_getItem_top(APP_DRAW->layerlist); pi; pi = LayerItem_getNext(pi))
	{
		if(pi->img)
		{
			//新しいイメージ作成

			img = TileImage_new(TILEIMAGE_COLTYPE_RGBA, p->w, p->h);
			if(!img) return FALSE;

			//範囲内のイメージのみをコピー

			TileImage_copyImage_inRect(img, pi->img, &rc, p->offx, p->offy);

			//イメージ入れ替え

			TileImage_free(pi->img);
			pi->img = img;
		}

		mPopupProgressThreadIncPos(prog);
	}

	return TRUE;
}

/** キャンバスサイズ変更 */

mBool drawImage_resizeCanvas(DrawData *p,int w,int h,int movx,int movy,mBool crop)
{
	_thdata_resizecanvas dat;
	int ret;
	int bw,bh;

	bw = p->imgw, bh = p->imgh;

	//作業用、サイズ変更

	if(!drawImage_changeImageSize(p, w, h))
		return FALSE;

	//

	if(!crop)
	{
		//----- 切り取らない
		
		//undo

		Undo_addResizeCanvas_moveOffset(movx, movy, bw, bh);

		//各レイヤのオフセット位置を相対移動

		LayerList_moveOffset_rel_all(p->layerlist, movx, movy);

		ret = TRUE;
	}
	else
	{
		//----- 範囲外を切り取り

		//undo

		Undo_addResizeCanvas_crop(bw, bh);

		//

		dat.w = w, dat.h = h;
		dat.offx = movx;
		dat.offy = movy;

		ret = PopupThread_run(&dat, _thread_resize_canvas);
		if(ret == -1) ret = FALSE;
	}

	return ret;
}


//============================
// キャンバス拡大縮小
//============================


typedef struct
{
	int w,h,type;
}_thdata_scalecanvas;


static int _thread_scale_canvas(mPopupProgress *prog,void *data)
{
	_thdata_scalecanvas *p = (_thdata_scalecanvas *)data;
	LayerItem *pi;
	TileImage *img;
	mSize sizeSrc,sizeDst;

	sizeSrc.w = APP_DRAW->imgw;
	sizeSrc.h = APP_DRAW->imgh;
	sizeDst.w = p->w;
	sizeDst.h = p->h;

	mPopupProgressThreadSetMax(prog,
		LayerList_getNormalLayerNum(APP_DRAW->layerlist) * 6);

	for(pi = LayerList_getItem_top(APP_DRAW->layerlist); pi; pi = LayerItem_getNext(pi))
	{
		if(pi->img)
		{
			img = TileImage_new(TILEIMAGE_COLTYPE_RGBA, sizeDst.w, sizeDst.h);
			if(!img) return FALSE;

			if(!TileImage_scaling(img, pi->img, p->type, &sizeSrc, &sizeDst, prog))
			{
				TileImage_free(img);
				return FALSE;
			}

			TileImage_free(pi->img);
			pi->img = img;
		}
	}

	return TRUE;
}

/** キャンバス拡大縮小 */

mBool drawImage_scaleCanvas(DrawData *p,int w,int h,int dpi,int type)
{
	_thdata_scalecanvas dat;
	int ret;

	dat.w = w;
	dat.h = h;
	dat.type = type;

	//undo

	Undo_addScaleCanvas();

	//スレッド

	ret = PopupThread_run(&dat, _thread_scale_canvas);
	if(ret != 1) return FALSE;

	//作業用変更

	if(!drawImage_changeImageSize(p, w, h))
		return FALSE;

	//DPI 変更

	if(dpi != -1)
		p->imgdpi = dpi;

	return TRUE;
}


//========================


/** 表示用ではなく実際のイメージ用にレイヤ合成 */

void drawImage_blendImage_real(DrawData *p)
{
	mBox box;

	ImageBuf24_fillWhite(p->blendimg);

	box.x = box.y = 0;
	box.w = p->imgw, box.h = p->imgh;

	drawUpdate_blendImage_layer(p, &box);
}

/** 指定位置の全レイヤ合成後の色を取得 */

uint32_t drawImage_getBlendColor_atPoint(DrawData *p,int x,int y,PixelRGB *pixdst)
{
	LayerItem *pi;
	PixelRGBA pix;

	pix.r = pix.g = pix.b = 255;

	pi = LayerList_getItem_bottomVisibleImage(p->layerlist);

	for( ; pi; pi = LayerItem_getPrevVisibleImage(pi))
		TileImage_getBlendPixel(pi->img, x, y, &pix, LayerItem_getOpacity_real(pi), pi->blendmode);

	if(pixdst)
	{
		pixdst->r = pix.r;
		pixdst->g = pix.g;
		pixdst->b = pix.b;
	}

	return M_RGB(pix.r, pix.g, pix.b);
}

/** 全レイヤを通常結合したY1行の RGBA バッファ取得
 *
 * PNG + アルファチャンネル 保存用 */

void drawImage_getBlendRow_RGBA(DrawData *p,uint8_t *dst,int y)
{
	LayerItem *pi;
	PixelRGBA pixres,pixsrc,*pd;
	int x,a;

	pd = (PixelRGBA *)dst;

	for(x = 0; x < p->imgw; x++)
	{
		pixres.c = 0;

		pi = LayerList_getItem_bottomVisibleImage(p->layerlist);

		for( ; pi; pi = LayerItem_getPrevVisibleImage(pi))
		{
			a = LayerItem_getOpacity_real(pi);
			
			TileImage_getPixel(pi->img, x, y, &pixsrc);

			pixsrc.a = pixsrc.a * a >> 7;

			if(pixsrc.a)
				TileImage_colfunc_normal(pi->img, &pixres, &pixsrc, NULL);
		}

		*(pd++) = pixres;
	}
}

/** テクスチャ画像読み込み */

ImageBuf8 *drawTexture_loadImage(const char *filename,int *err)
{
	ImageBuf24 *img24;
	ImageBuf8 *img8;

	//24bit 読み込み

	img24 = ImageBuf24_loadFile(filename, 0xffffff);
	if(!img24)
	{
		*err = APPERR_LOAD;
		return NULL;
	}

	//サイズ制限

	if(img24->w > 2048 || img24->h > 2048)
	{
		ImageBuf24_free(img24);

		*err = APPERR_LARGE_SIZE;
		return NULL;
	}

	//8bit に変換

	img8 = ImageBuf8_createFromImageBuf24(img24);

	ImageBuf24_free(img24);

	if(!img8)
	{
		*err = APPERR_ALLOC;
		return NULL;
	}

	return img8;
}

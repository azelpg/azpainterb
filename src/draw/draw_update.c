/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DrawData
 *
 * 更新関連
 *****************************************/

#include "mDef.h"
#include "mWindowDef.h"
#include "mWidget.h"
#include "mPixbuf.h"
#include "mRectBox.h"

#include "defConfig.h"
#include "defDraw.h"
#include "defWidgets.h"
#include "defGridData.h"

#include "draw_op_def.h"
#include "draw_main.h"
#include "draw_calc.h"

#include "LayerList.h"
#include "LayerItem.h"
#include "TileImage.h"
#include "ImageBuf24.h"
#include "PixbufDraw.h"

#include "Docks_external.h"



/** キャンバスエリアを更新 */

void drawUpdate_canvasArea()
{
	mWidgetUpdate(M_WIDGET(APP_WIDGETS->canvas_area));
}

/** すべて更新
 *
 * [ 合成イメージ、キャンバスエリア、(Dock)キャンバスビュー ] */

void drawUpdate_all()
{
	drawUpdate_blendImage_full(APP_DRAW);
	drawUpdate_canvasArea();

	DockCanvasView_update();
}

/** 全体 + レイヤ一覧を更新 */

void drawUpdate_all_layer()
{
	drawUpdate_all();

	DockLayer_update_all();
}

/** 合成イメージを全体更新
 *
 * 背景とレイヤイメージ */

void drawUpdate_blendImage_full(DrawData *p)
{
	mBox box;

	box.x = box.y = 0;
	box.w = p->imgw, box.h = p->imgh;

	//背景

	if(APP_CONF->fView & CONFIG_VIEW_F_BKGND_PLAID)
	{
		ImageBuf24_fillPlaid_box(p->blendimg, &box,
			APP_CONF->colBkgndPlaid[0], APP_CONF->colBkgndPlaid[1]);
	}
	else
		ImageBuf24_fillWhite(p->blendimg);

	//合成

	drawUpdate_blendImage_layer(p, &box);
}

/** 合成イメージ:レイヤイメージを範囲更新
 *
 * [!] 背景は描画しない */

void drawUpdate_blendImage_layer(DrawData *p,mBox *box)
{
	LayerItem *pi;
	int opacity,mode;

	mode = p->sel.mode;

	pi = LayerList_getItem_bottomVisibleImage(p->layerlist);

	if(mode == DRAW_SELMODE_MOVECOPY_PASTE
		|| mode == DRAW_SELMODE_SELECT_PASTE)
	{
		//----- 貼付けモード時

		for(; pi; pi = LayerItem_getPrevVisibleImage(pi))
		{
			opacity = LayerItem_getOpacity_real(pi);
		
			TileImage_blendToImage24_rgba(pi->img, p->blendimg,
				box, opacity, pi->blendmode);

			//貼付けイメージをカレントの上に挿入

			if(pi == p->curlayer)
			{
				TileImage_blendToImage24_rgba(
					(mode == DRAW_SELMODE_SELECT_PASTE)? p->sel.img_selcopy: p->sel.img_movecopy,
					p->blendimg, box, opacity, pi->blendmode);
			}
		}

		//カレントがフォルダの場合は表示されないので、最後に

		if(!p->curlayer->img)
		{
			TileImage_blendToImage24_rgba(
				(mode == DRAW_SELMODE_SELECT_PASTE)? p->sel.img_selcopy: p->sel.img_movecopy,
				p->blendimg, box, 128, 0);
		}
	}
	else if(p->drawtext.in_dialog
		&& (p->drawtext.flags & DRAW_DRAWTEXT_F_PREVIEW))
	{
		//----- テキスト描画、ダイアログ中のプレビュー

		for(; pi; pi = LayerItem_getPrevVisibleImage(pi))
		{
			opacity = LayerItem_getOpacity_real(pi);
		
			TileImage_blendToImage24_rgba(pi->img, p->blendimg,
				box, opacity, pi->blendmode);

			//プレビューイメージを挿入

			if(pi == p->curlayer)
			{
				TileImage_blendToImage24_rgba(p->tileimgTmp,
					p->blendimg, box, opacity, pi->blendmode);
			}
		}
	}
	else
	{
		//----- 通常時
		
		for(; pi; pi = LayerItem_getPrevVisibleImage(pi))
		{
			TileImage_blendToImage24_rgba(pi->img, p->blendimg,
				box, LayerItem_getOpacity_real(pi), pi->blendmode);
		}
	}
}

/** キャンバスを描画
 *
 * 合成イメージをソースとして mPixbuf に描画。
 *
 * @param pixbuf 描画対象のキャンバスエリアの mPixbuf
 * @param box    NULL で全体 */

void drawUpdate_drawCanvas(DrawData *p,mPixbuf *pixbuf,mBox *box)
{
	CanvasDrawInfo di;
	mBox boximg;
	mRect rc;
	int n;

	//描画情報

	if(box)
		di.boxdst = *box;
	else
	{
		di.boxdst.x = di.boxdst.y = 0;
		di.boxdst.w = p->szCanvas.w;
		di.boxdst.h = p->szCanvas.h;
	}

	di.originx = p->imgoriginX;
	di.originy = p->imgoriginY;
	di.scrollx = p->ptScroll.x - (p->szCanvas.w >> 1);
	di.scrolly = p->ptScroll.y - (p->szCanvas.h >> 1);
	di.mirror  = p->canvas_mirror;
	di.imgw = p->imgw;
	di.imgh = p->imgh;
	di.bkgndcol = APP_CONF->colCanvasBkgnd;
	di.param = &p->viewparam;

	//------ 描画

	if(p->canvas_angle)
	{
		//回転あり

		n = p->canvas_angle;
		
		if(p->bCanvasLowQuality
			|| (p->canvas_zoom == 1000 && (n == 9000 || n == 18000 || n == 27000)))
			//キャンバス移動中、または90 度単位 (倍率100%) の場合は低品質
			ImageBuf24_drawMainCanvas_rotate_normal(p->blendimg, pixbuf, &di);
		else
			ImageBuf24_drawMainCanvas_rotate_oversamp(p->blendimg, pixbuf, &di);
	}
	else
	{
		//回転なし (200%以下 [!] 100%は除く 時は高品質で)

		if(!p->bCanvasLowQuality && p->canvas_zoom != 1000 && p->canvas_zoom < 2000)
			ImageBuf24_drawMainCanvas_oversamp(p->blendimg, pixbuf, &di);
		else 
			ImageBuf24_drawMainCanvas_nearest(p->blendimg, pixbuf, &di);
	}

	//------ グリッド

	if((APP_CONF->fView & CONFIG_VIEW_F_GRID)
		&& drawCalc_areaToimage_box(p, &boximg, &di.boxdst))
	{
		GridListData *pi;

		//リストで上にあるものほど前面にしたいので、後ろから描画

		for(pi = (GridListData *)APP_CONF->grid_list.bottom; pi; pi = (GridListData *)pi->i.prev)
		{
			n = pi->flags;
			
			if(n & GRIDDATA_F_ON)
			{
				//表示倍率条件

				if((n & GRIDDATA_F_HAVE_ZOOM)
					&& (p->canvas_zoom < pi->zoom_min || p->canvas_zoom > pi->zoom_max))
					continue;
			
				//範囲

				if(n & GRIDDATA_F_HAVE_AREA)
				{
					rc.x1 = pi->area[0], rc.y1 = pi->area[1];
					rc.x2 = pi->area[2], rc.y2 = pi->area[3];

					//始点がイメージの範囲外なら表示しない

					if(rc.x1 >= p->imgw || rc.y1 >= p->imgh)
						continue;

					//イメージの範囲内に収める

					if(rc.x2 > p->imgw) rc.x2 = p->imgw;
					if(rc.y2 > p->imgh) rc.y2 = p->imgh;
				}

				//描画
				
				pixbufDrawGrid(pixbuf, &di.boxdst, &boximg,
					pi->w, pi->h, pi->col,
					(n & GRIDDATA_F_HAVE_AREA)? &rc: NULL, &di);
			}
		}
	}

	//------ 選択範囲

	if(p->sel.boxsel.w)
		pixbufDrawSelectBox(pixbuf, &p->sel.boxsel, &di);
}

/** 合成イメージの範囲更新
 *
 * @param boximg  イメージ範囲内であること */

void drawUpdate_rect_blendimg(DrawData *p,mBox *boximg)
{
	//背景

	if(APP_CONF->fView & CONFIG_VIEW_F_BKGND_PLAID)
	{
		ImageBuf24_fillPlaid_box(p->blendimg, boximg,
			APP_CONF->colBkgndPlaid[0], APP_CONF->colBkgndPlaid[1]);
	}
	else
		ImageBuf24_fillBox(p->blendimg, boximg, 0xffffff);

	//レイヤ

	drawUpdate_blendImage_layer(p, boximg);
}

/** キャンバスエリアの範囲更新 */

void drawUpdate_rect_canvas(DrawData *p,mBox *boximg)
{
	mBox boxc;
	mWidget *wgarea;
	mPixbuf *pixbuf;

	wgarea = M_WIDGET(APP_WIDGETS->canvas_area);

	if(drawCalc_imageToarea_box(p, &boxc, boximg))
	{
		//キャンバス描画

		pixbuf = mWidgetBeginDirectDraw(wgarea);

		if(pixbuf)
		{
			drawUpdate_drawCanvas(p, pixbuf, &boxc);

			//[debug]
			//mPixbufBox(pixbuf, boxc.x, boxc.y, boxc.w, boxc.h, 0xff0000);

			mWidgetEndDirectDraw(wgarea, pixbuf);

			//ウィンドウ更新

			mWidgetUpdateBox_box(wgarea, &boxc);
		}
	}
}


//===========================
// 範囲更新
//===========================


/** 範囲更新 (合成イメージ + キャンバスエリア)
 *
 * @param boximg イメージ範囲内であること */

void drawUpdate_rect_imgcanvas(DrawData *p,mBox *boximg)
{
	//合成イメージ

	drawUpdate_rect_blendimg(p, boximg);

	//キャンバス

	drawUpdate_rect_canvas(p, boximg);
}

/** 範囲更新、選択範囲用
 *
 * 選択範囲がイメージの範囲外でも表示されるように。
 *
 * @param rc  NULL で現在の選択範囲 */

void drawUpdate_rect_imgcanvas_forSelect(DrawData *p,mRect *rc)
{
	mBox box;
	mRect rc2;

	if(rc)
		rc2 = *rc;
	else
		mRectSetByBox(&rc2, &p->sel.boxsel);

	//合成イメージ

	if(drawCalc_getImageBox_rect(p, &box, &rc2))
		drawUpdate_rect_blendimg(p, &box);

	//キャンバス

	mBoxSetByRect(&box, &rc2);

	box.x -= 2;
	box.y -= 2;
	box.w += 4;
	box.h += 4;

	drawUpdate_rect_canvas(p, &box);
}

/** 選択範囲のキャンバス領域を更新
 *
 * @param box NULL で現在の選択範囲 */

void drawUpdate_rect_canvas_select(DrawData *p,mBox *box)
{
	mBox boxu;

	if(box)
		boxu = *box;
	else
		boxu = p->sel.boxsel;

	//1px 拡張して表示しているので、更新範囲を拡張

	boxu.x -= 2;
	boxu.y -= 2;
	boxu.w += 4;
	boxu.h += 4;

	drawUpdate_rect_canvas(p, &boxu);
}

/** 範囲更新 (mRect)
 *
 * @param rc  イメージ範囲 */

void drawUpdate_rect_imgcanvas_fromRect(DrawData *p,mRect *rc)
{
	mBox box;

	if(drawCalc_getImageBox_rect(p, &box, rc))
		drawUpdate_rect_imgcanvas(p, &box);
}

/** 範囲更新 + [dock]キャンバスビュー */

void drawUpdate_rect_imgcanvas_canvasview_fromRect(DrawData *p,mRect *rc)
{
	mBox box;

	if(drawCalc_getImageBox_rect(p, &box, rc))
	{
		drawUpdate_rect_imgcanvas(p, &box);

		DockCanvasView_updateRect(&box);
	}
}

/** 範囲更新 + [dock]キャンバスビュー (レイヤの表示イメージ部分)
 *
 * フォルダの場合、フォルダ下の全ての表示レイヤの範囲。
 *
 * @param item  NULL でカレントレイヤ */

void drawUpdate_rect_imgcanvas_canvasview_inLayerHave(DrawData *p,LayerItem *item)
{
	mRect rc;

	if(!item) item = p->curlayer;

	if(LayerItem_getVisibleImageRect(item, &rc))
		drawUpdate_rect_imgcanvas_canvasview_fromRect(p, &rc);
}

/** [dock]キャンバスビューのみ更新 (mRect) */

void drawUpdate_canvasview(DrawData *p,mRect *rc)
{
	mBox box;

	if(drawCalc_getImageBox_rect(p, &box, rc))
		DockCanvasView_updateRect(&box);
}

/** [dock]キャンバスビューがルーペ時、更新
 *
 * ドットペンの押し時など、カーソルが移動されていない状態でイメージが更新されたときに使う。 */

void drawUpdate_canvasview_inLoupe()
{
	if(APP_CONF->canvasview_flags & CONFIG_CANVASVIEW_F_LOUPE_MODE)
		DockCanvasView_update();
}

/** 描画終了時の範囲更新
 *
 * @param boximg イメージ範囲。x < 0 で範囲なし */

void drawUpdate_endDraw_box(DrawData *p,mBox *boximg)
{
	//[Dock]キャンバスビュー

	if(boximg->x >= 0)
		DockCanvasView_updateRect(boximg);
}

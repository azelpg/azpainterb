/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DrawData
 *
 * 操作 - 選択範囲系
 *****************************************/

#include <stdlib.h>

#include "mDef.h"
#include "mRectBox.h"

#include "defDraw.h"

#include "draw_main.h"
#include "draw_calc.h"
#include "draw_select.h"
#include "draw_op_def.h"
#include "draw_op_sub.h"
#include "draw_op_func.h"

#include "macroToolOpt.h"
#include "AppCursor.h"
#include "AppErr.h"

#include "TileImage.h"
#include "TileImageDrawInfo.h"
#include "LayerItem.h"

#include "MainWindow.h"
#include "MainWinCanvas.h"
#include "Docks_external.h"
#include "StatusBar.h"




/** 矩形、多角形、フリーハンドの範囲を TileImage [A1] に描画 */

TileImage *drawOp_createSelectImage(DrawData *p,mRect *rcarea)
{
	TileImage *img = NULL;
	mBox box;
	PixelRGBA pix;

	pix.c = 0xffffffff;

	switch(p->w.optype)
	{
		//矩形 (イメージ座標)
		case DRAW_OPTYPE_XOR_BOXIMAGE:
			if(!drawCalc_getImageBox_rect(p, &box, &p->w.rctmp[0]))
				return NULL;

			img = TileImage_new(TILEIMAGE_COLTYPE_1BIT, box.w, box.h);
			if(!img) return NULL;

			g_tileimage_dinfo.funcDrawPixel = TileImage_setPixel_new;

			TileImage_setOffset(img, box.x, box.y);
			TileImage_drawFillBox(img, box.x, box.y, box.w, box.h, &pix);

			mRectSetByBox(rcarea, &box);
			break;

		//多角形、フリーハンド
		case DRAW_OPTYPE_XOR_POLYGON:
		case DRAW_OPTYPE_XOR_LASSO:
			img = TileImage_new(TILEIMAGE_COLTYPE_1BIT, p->imgw, p->imgh);
			if(!img) return NULL;

			g_tileimage_dinfo.funcDrawPixel = TileImage_setPixel_new_drawrect;

			TileImageDrawInfo_clearDrawRect();
		
			TileImage_drawFillPolygon(img, p->w.fillpolygon, &pix, FALSE);

			drawOpSub_freeFillPolygon(p);

			//範囲 (イメージの範囲内に)

			*rcarea = g_tileimage_dinfo.rcdraw;

			if(!drawCalc_clipImageRect(p, rcarea))
			{
				TileImage_free(img);
				img = NULL;
			}
			break;
	}

	return img;
}


//=============================
// スタンプ
//=============================


/** 範囲内イメージをスタンプ画像にセット */

void drawOp_setStampImage(DrawData *p)
{
	TileImage *img;
	mRect rc;

	//選択範囲イメージ作成

	img = drawOp_createSelectImage(p, &rc);
	if(!img)
	{
		drawSelect_clearStamp(p);
		return;
	}

	//範囲内のイメージを img_stamp にコピー

	TileImage_free(p->sel.img_stamp);

	p->sel.img_stamp = TileImage_createCopyImage_forStamp(p->curlayer->img, img, &rc);

	p->sel.size_stamp.w = rc.x2 - rc.x1 + 1;
	p->sel.size_stamp.h = rc.y2 - rc.y1 + 1;

	TileImage_free(img);

	//カーソル変更

	MainWinCanvasArea_setCursor(APP_CURSOR_STAMP);

	//プレビュー更新

	DockOption_changeStampImage();
}

/** スタンプイメージ貼り付け */

static mBool _stamp_cmd_paste(DrawData *p)
{
	mPoint pt;

	if(drawOpSub_canDrawLayer(p)) return FALSE;

	//貼り付け位置

	drawOpSub_getImagePoint_int(p, &pt);

	if(!STAMP_IS_LEFTTOP(p->tool.opt_stamp))
	{
		pt.x -= p->sel.size_stamp.w / 2;
		pt.y -= p->sel.size_stamp.h / 2;
	}

	//描画

	drawOpSub_setDrawInfo(p, TOOL_STAMP, 0);
	drawOpSub_beginDraw(p);

	TileImage_pasteImage_forStamp(p->w.dstimg, pt.x, pt.y, p->sel.img_stamp,
		p->sel.size_stamp.w, p->sel.size_stamp.h);

	drawOpSub_finishDraw_single(p);

	//

	drawOpSub_setOpInfo(p, DRAW_OPTYPE_TMP, NULL, NULL, 0);

	return TRUE;
}

/** 押し
 *
 * @return -1 で範囲選択 */

int drawOp_stamp_press(DrawData *p)
{
	//+Ctrl : イメージをクリアして範囲選択へ

	if(p->w.press_state & M_MODS_CTRL)
		drawSelect_clearStamp(p);

	//イメージがあれば貼付け、なければ範囲選択
	
	if(p->sel.img_stamp)
		return _stamp_cmd_paste(p);
	else
	{
		//カレントレイヤがフォルダの場合は無効

		if(drawOpSub_canDrawLayer(p) == CANDRAWLAYER_FOLDER)
			return FALSE;

		return -1;
	}
}


//=============================
// 選択範囲 +Ctrl 移動
//=============================
/*
 * pttmp[0]   : 左上位置
 * pttmp[1]   : 右下位置
 * pttmp[2]   : 現在の総相対移動数 (イメージ座標)
 * 
 * ptd_tmp[0] : 総移動数 (領域座標)
 * boxtmp[0]  : 元の範囲
 */

/** 移動 */

static void _selectmove_motion(DrawData *p,uint32_t state)
{
	mPoint pt;

	if(drawOpSub_calc_moveForSelect(p, 0, &pt, TRUE))
	{
		drawOpXor_drawBox_image(p);

		p->w.pttmp[0].x += pt.x;
		p->w.pttmp[0].y += pt.y;
		p->w.pttmp[1].x += pt.x;
		p->w.pttmp[1].y += pt.y;

		drawOpXor_drawBox_image(p);
	}
}

/** 離し */

static mBool _selectmove_release(DrawData *p)
{
	drawOpXor_drawBox_image(p);

	p->sel.boxsel = p->w.boxtmp[0];
	p->sel.boxsel.x = p->w.pttmp[0].x;
	p->sel.boxsel.y = p->w.pttmp[0].y;

	drawUpdate_rect_canvas_select(p, NULL);

	return TRUE;
}

/** 押し */

static mBool _selectmove_press(DrawData *p)
{
	drawOpSub_setOpInfo(p, DRAW_OPTYPE_TMP,
		_selectmove_motion, _selectmove_release, 0);

	p->w.drag_cursor_type = APP_CURSOR_SEL_MOVE;

	//

	p->w.boxtmp[0] = p->sel.boxsel;

	p->w.ptd_tmp[0].x = p->w.ptd_tmp[0].y = 0;

	p->w.pttmp[0].x = p->sel.boxsel.x;
	p->w.pttmp[0].y = p->sel.boxsel.y;
	p->w.pttmp[1].x = p->sel.boxsel.x + p->sel.boxsel.w - 1;
	p->w.pttmp[1].y = p->sel.boxsel.y + p->sel.boxsel.h - 1;

	p->w.pttmp[2].x = p->w.pttmp[2].y = 0;

	//選択範囲を消して、XOR 描画

	p->sel.boxsel.w = 0;

	drawUpdate_rect_canvas_select(p, &p->w.boxtmp[0]);
	
	drawOpXor_drawBox_image(p);

	return TRUE;
}


//=============================
// 選択範囲
//=============================


/** 貼付け確定 */

static mBool _select_cmd_paste(DrawData *p)
{
	//描画

	if(drawOpSub_canDrawLayer(p) == CANDRAWLAYER_OK)
	{
		drawOpSub_setDrawInfo_forOverwrite();
		drawOpSub_beginDraw(p);

		if(SELECT_IS_OVERWRITE(p->tool.opt_select))
			g_tileimage_dinfo.funcColor = TileImage_colfunc_overwrite;
		else
			g_tileimage_dinfo.funcColor = TileImage_colfunc_normal;

		TileImage_pasteImage_forSelImage(p->w.dstimg, p->sel.img_selcopy, &p->sel.boxsel);

		drawOpSub_finishDraw_single(p);
	}

	//終了

	drawSelect_cancel(TRUE);

	drawOpSub_setOpInfo(p, DRAW_OPTYPE_TMP, NULL, NULL, 0);

	return TRUE;
}

/** 押し時 */

mBool drawOp_select_press(DrawData *p)
{
	if(p->sel.mode == DRAW_SELMODE_SELECT_PASTE)
	{
		//====== 貼付モード時

		if(drawOpSub_isPoint_inSelect(p))
			return drawOp_pasteMove_press(p, p->sel.img_selcopy);
		else
			return _select_cmd_paste(p);
	}
	else
	{
		//====== 範囲選択時

		if(p->sel.cur_resize_cursor != 0)
		{
			//選択範囲リサイズ

			return drawOp_selectResize_press(p);
		}
		else if(p->w.press_state & M_MODS_CTRL)
		{
			//+Ctrl : 範囲内をドラッグで、範囲移動

			if(drawOpSub_isPoint_inSelect(p))
				return _selectmove_press(p);
			else
				return FALSE;
		}
		else
		{
			//範囲消去
			
			drawSelect_setSelectBox(p, NULL);

			//新規選択
			
			return drawOpXor_boximage_press(p, DRAW_OPSUB_SET_SELECT);
		}
	}
}


//===========================================
// 貼付イメージの移動
//===========================================
/*
 * ptmp       : 貼付イメージ (TileImage *)
 * pttmp[2]   : 現在の総相対移動数 (イメージ座標)
 * ptd_tmp[0] : 総移動数 (領域座標)
 * boxtmp[0]  : 元の範囲
 * rcdraw     : 更新用 (タイマーで更新されたらクリアされる)
 */


/** 移動 */

static void _pastemove_motion(DrawData *p,uint32_t state)
{
	mPoint pt;
	mRect rc,rc2;

	if(drawOpSub_calc_moveForSelect(p, 0, &pt, FALSE))
	{
		//更新範囲
		
		mRectSetByBox(&rc2, &p->sel.boxsel);
		drawCalc_unionRect_relmove(&rc, &rc2, pt.x, pt.y);

		//相対移動
	
		p->sel.boxsel.x += pt.x;
		p->sel.boxsel.y += pt.y;

		TileImage_moveOffset_rel((TileImage *)p->w.ptmp, pt.x, pt.y);

		//更新

		mRectUnion(&p->w.rcdraw, &rc);

		MainWinCanvasArea_setTimer_updateSelectMove();
	}
}

/** 離し */

static mBool _pastemove_release(DrawData *p)
{
	MainWinCanvasArea_clearTimer_updateSelectMove();

	return TRUE;
}

/** 押し */

mBool drawOp_pasteMove_press(DrawData *p,TileImage *img)
{
	drawOpSub_setOpInfo(p, DRAW_OPTYPE_TMP,
		_pastemove_motion, _pastemove_release, 0);

	p->w.ptmp = img;

	p->w.boxtmp[0] = p->sel.boxsel;
	p->w.ptd_tmp[0].x = p->w.ptd_tmp[0].y = 0;
	p->w.pttmp[2].x = p->w.pttmp[2].y = 0;

	return TRUE;
}


//=============================
// 範囲イメージ移動/コピー
//=============================


/** 範囲決定時 */

void drawOp_selMoveCopy_select(DrawData *p)
{
	mRect rc;

	//範囲イメージ [A1] を作成

	p->sel.img_movecopy = drawOp_createSelectImage(p, &rc);
	if(!p->sel.img_movecopy) return;

	//ドラッグ待機モードへ

	p->sel.mode = DRAW_SELMODE_MOVECOPY_WAIT;

	MainWinCanvasArea_setCursor(APP_CURSOR_SEL_MOVE);

	//矩形範囲表示

	p->sel.boxsel.x = rc.x1;
	p->sel.boxsel.y = rc.y1;
	p->sel.boxsel.w = rc.x2 - rc.x1 + 1;
	p->sel.boxsel.h = rc.y2 - rc.y1 + 1;

	drawUpdate_rect_canvas_select(p, NULL);
}

/** 待機モードから貼付けモードへ */

static mBool _selmovecopy_cmd_to_pastemode(DrawData *p)
{
	TileImage *img;
	mRect rc;

	//描画不可

	if(drawOpSub_canDrawLayer(p)) return FALSE;

	//範囲 => mRect

	mRectSetByBox(&rc, &p->sel.boxsel);

	//範囲内イメージをコピー or 切り取り

	if(p->w.optoolno == TOOL_SEL_COPY)
	{
		//コピー

		img = TileImage_createCopyImage_forSelImage(
			p->curlayer->img, p->sel.img_movecopy, &rc, FALSE);
	}
	else
	{
		//移動 (切り取り)

		drawOpSub_setDrawInfo_forOverwrite();
		drawOpSub_beginDraw(p);

		img = TileImage_createCopyImage_forSelImage(
			p->curlayer->img, p->sel.img_movecopy, &rc, TRUE);

		drawOpSub_finishDraw_single(p);
	}

	//範囲イメージを解放して、コピーイメージをセット

	TileImage_free(p->sel.img_movecopy);
	p->sel.img_movecopy = img;
	
	if(!img) return FALSE;

	//モード変更

	p->sel.mode = DRAW_SELMODE_MOVECOPY_PASTE;

	//更新

	drawUpdate_rect_imgcanvas_forSelect(p, &rc);

	return TRUE;
}

/** 待機モードをキャンセル */

static void _selmovecopy_cmd_wait_cancel(DrawData *p)
{
	mBox box;

	drawOpSub_freeSelMoveCopyImage(p);

	box = p->sel.boxsel;

	p->sel.mode = DRAW_SELMODE_NONE;
	p->sel.boxsel.w = 0;

	MainWinCanvasArea_setCursor_forTool();

	drawUpdate_rect_canvas_select(p, &box);
}

/** 貼付け確定 */

static mBool _selmovecopy_cmd_paste(DrawData *p,mBool mode_end)
{
	//描画

	if(drawOpSub_canDrawLayer(p) == CANDRAWLAYER_OK)
	{
		drawOpSub_setDrawInfo(p, p->w.optoolno, 0);
		drawOpSub_beginDraw(p);

		TileImage_pasteImage_forSelImage(p->w.dstimg, p->sel.img_movecopy, &p->sel.boxsel);

		drawOpSub_finishDraw_single(p);
	}

	//貼付けモードを終了

	if(mode_end)
	{
		drawSelect_cancel(TRUE);

		drawOpSub_setOpInfo(p, DRAW_OPTYPE_TMP, NULL, NULL, 0);
	}
		
	return TRUE;
}

/** 押し
 *
 * @return -1 で範囲選択へ */

int drawOp_selectMoveCopy_press(DrawData *p)
{
	switch(p->sel.mode)
	{
		//貼付け移動モード
		case DRAW_SELMODE_MOVECOPY_PASTE:
			if(drawOpSub_isPoint_inSelect(p))
			{
				//範囲内が押されたら、再移動
				/* +Ctrl : 貼付け後、終了せずに再び貼付けモードへ */

				if(p->w.press_state & M_MODS_CTRL)
					_selmovecopy_cmd_paste(p, FALSE);
				
				return drawOp_pasteMove_press(p, p->sel.img_movecopy);
			}
			else
				//範囲外が押されたら、貼付け確定
				return _selmovecopy_cmd_paste(p, TRUE);
			break;

		//ドラッグ待機モード
		case DRAW_SELMODE_MOVECOPY_WAIT:
			if(drawOpSub_isPoint_inSelect(p))
			{
				//範囲内が押されたら、貼付けモードへ

				if(!_selmovecopy_cmd_to_pastemode(p))
					return FALSE;

				return drawOp_pasteMove_press(p, p->sel.img_movecopy);
			}
			else
			{
				//範囲外が押されたら、範囲をクリアして新規選択へ

				_selmovecopy_cmd_wait_cancel(p);

				return -1;
			}
			break;
	}
	
	return -1;
}


//=============================
// 矩形範囲イメージ置換え
//=============================


/** 範囲決定時 */

void drawOp_selReplace_select(DrawData *p)
{
	mRect rc;
	mBox box;

	mRectSetByBox(&rc, &p->w.boxtmp[0]);

	//イメージの範囲外なら何もしない

	if(!drawCalc_getImageBox_rect(p, &box, &rc))
		return;

	//
	
	p->sel.boxsel = box;
	p->sel.mode = DRAW_SELMODE_REPLACE_DEST;

	//範囲表示

	drawUpdate_rect_canvas_select(p, NULL);

	//カーソル変更

	MainWinCanvasArea_setCursor(APP_CURSOR_SEL_REPLACE);
}

/** モードを初期状態へ */

static void _selreplace_reset(DrawData *p)
{
	mBox box;

	box = p->sel.boxsel;

	p->sel.mode = DRAW_SELMODE_NONE;
	p->sel.boxsel.w = 0;

	drawUpdate_rect_canvas_select(p, &box);

	MainWinCanvasArea_setCursor_forTool();
}

/** 入れ替え実行 */

static mBool _selreplace_replace(DrawData *p)
{
	mPoint pt;
	mBox box,box2;

	if(drawOpSub_canDrawLayer(p)) return FALSE;

	drawOpSub_getImagePoint_int(p, &pt);

	box = p->sel.boxsel;

	//範囲が重なっていたらエラー

	box2 = box;
	box2.x = pt.x;
	box2.y = pt.y;

	if(mBoxIsCross(&box, &box2))
	{
		MainWindow_apperr(APPERR_SELREPLACE_CROSS, NULL);
		return FALSE;
	}

	//描画

	drawOpSub_setDrawInfo_forOverwrite();
	drawOpSub_beginDraw_single(p);

	TileImage_replaceImage_box(p->w.dstimg, &box, &pt);

	drawOpSub_endDraw_single(p);

	//

	_selreplace_reset(p);

	drawOpSub_setOpInfo(p, DRAW_OPTYPE_TMP, NULL, NULL, 0);

	return TRUE;
}

/** 押し */

mBool drawOp_selectReplace_press(DrawData *p)
{
	//+Ctrl : 入れ替え元範囲をクリアして再選択

	if((p->w.press_state & M_MODS_CTRL) && p->sel.mode == DRAW_SELMODE_REPLACE_DEST)
		_selreplace_reset(p);

	//

	if(p->sel.mode == DRAW_SELMODE_NONE)
		//矩形選択
		return drawOpXor_boximage_press(p, DRAW_OPSUB_SEL_REPLACE);
	else
		//入れ替え位置決定
		return _selreplace_replace(p);
}


//=============================
// 選択範囲のリサイズ
//=============================
/*
 * pttmp[0] : 左上位置
 * pttmp[1] : 右下位置
 * pttmp[2] : 前回のイメージ位置
 */


enum
{
	_SELEDGE_LEFT   = 1<<0,
	_SELEDGE_TOP    = 1<<1,
	_SELEDGE_RIGHT  = 1<<2,
	_SELEDGE_BOTTOM = 1<<3
};


/** 非操作時の移動時、カーソル変更 */

void drawOp_selectSetCursor_motion(DrawData *p)
{
	mPoint pt;
	mRect rc,rcsel;
	int w,pos,no;

	drawOpSub_getImagePoint_int_raw(p, &pt);

	//選択範囲

	mRectSetByBox(&rcsel, &p->sel.boxsel);

	//カーソルと選択範囲の各辺との距離

	rc.x1 = abs(rcsel.x1 - pt.x);
	rc.y1 = abs(rcsel.y1 - pt.y);
	rc.x2 = abs(rcsel.x2 - pt.x);
	rc.y2 = abs(rcsel.y2 - pt.y);

	//判定の半径幅 (イメージの px 単位)

	if(p->canvas_zoom >= 2000)
		w = 2;
	else
		w = (int)(p->viewparam.scalediv * 3 + 0.5);

	//カーソルに近い辺と四隅を判定

	if(rc.x1 < w && rc.y1 < w)
		pos = _SELEDGE_LEFT | _SELEDGE_TOP; //左上
	else if(rc.x1 < w && rc.y2 < w)
		pos = _SELEDGE_LEFT | _SELEDGE_BOTTOM; //左下
	else if(rc.x2 < w && rc.y1 < w)
		pos = _SELEDGE_RIGHT | _SELEDGE_TOP; //右上
	else if(rc.x2 < w && rc.y2 < w)
		pos = _SELEDGE_RIGHT | _SELEDGE_BOTTOM; //右下
	else if(rc.x1 < w && rcsel.y1 <= pt.y && pt.y <= rcsel.y2)
		pos = _SELEDGE_LEFT;	//左
	else if(rc.x2 < w && rcsel.y1 <= pt.y && pt.y <= rcsel.y2)
		pos = _SELEDGE_RIGHT;	//右
	else if(rc.y1 < w && rcsel.x1 <= pt.x && pt.x <= rcsel.x2)
		pos = _SELEDGE_TOP;		//上
	else if(rc.y2 < w && rcsel.x1 <= pt.x && pt.x <= rcsel.x2)
		pos = _SELEDGE_BOTTOM;	//下
	else
		pos = 0;

	//カーソル変更

	if(pos != p->sel.cur_resize_cursor)
	{
		p->sel.cur_resize_cursor = pos;

		if(pos == 0)
			no = APP_CURSOR_SELECT;
		else if(pos == (_SELEDGE_LEFT | _SELEDGE_TOP)
			|| pos == (_SELEDGE_RIGHT | _SELEDGE_BOTTOM))
			no = APP_CURSOR_LEFT_TOP;
		else if(pos == (_SELEDGE_LEFT | _SELEDGE_BOTTOM)
			|| pos == (_SELEDGE_RIGHT | _SELEDGE_TOP))
			no = APP_CURSOR_RIGHT_TOP;
		else if(pos == _SELEDGE_LEFT || pos == _SELEDGE_RIGHT)
			no = APP_CURSOR_RESIZE_HORZ;
		else
			no = APP_CURSOR_RESIZE_VERT;

		MainWinCanvasArea_setCursor(no);
	}
}

/** 移動 */

static void _selectResize_motion(DrawData *p,uint32_t state)
{
	mPoint pt;
	int n,mx,my;

	//移動数

	drawOpSub_getImagePoint_int(p, &pt);

	mx = pt.x - p->w.pttmp[2].x;
	my = pt.y - p->w.pttmp[2].y;

	if(mx == 0 && my == 0) return;

	//移動

	drawOpXor_drawBox_image(p);

	if(p->sel.cur_resize_cursor & _SELEDGE_LEFT)
	{
		//左

		n = p->w.pttmp[0].x + mx;

		if(n < 0) n = 0;
		else if(n > p->w.pttmp[1].x) n = p->w.pttmp[1].x;

		p->w.pttmp[0].x = n;
	}
	else if(p->sel.cur_resize_cursor & _SELEDGE_RIGHT)
	{
		//右

		n = p->w.pttmp[1].x + mx;

		if(n < p->w.pttmp[0].x) n = p->w.pttmp[0].x;
		else if(n >= p->imgw) n = p->imgw - 1;

		p->w.pttmp[1].x = n;
	}

	if(p->sel.cur_resize_cursor & _SELEDGE_TOP)
	{
		//上

		n = p->w.pttmp[0].y + my;

		if(n < 0) n = 0;
		else if(n > p->w.pttmp[1].y) n = p->w.pttmp[1].y;

		p->w.pttmp[0].y = n;
	}
	else if(p->sel.cur_resize_cursor & _SELEDGE_BOTTOM)
	{
		//下

		n = p->w.pttmp[1].y + my;

		if(n < p->w.pttmp[0].y) n = p->w.pttmp[0].y;
		else if(n >= p->imgh) n = p->imgh - 1;

		p->w.pttmp[1].y = n;
	}

	drawOpXor_drawBox_image(p);

	//

	p->w.pttmp[2] = pt;

	//status bar

	StatusBar_setHelp_imgbox();
}

/** 離し */

static mBool _selectResize_release(DrawData *p)
{
	drawOpXor_drawBox_image(p);

	p->sel.boxsel.x = p->w.pttmp[0].x;
	p->sel.boxsel.y = p->w.pttmp[0].y;
	p->sel.boxsel.w = p->w.pttmp[1].x - p->w.pttmp[0].x + 1;
	p->sel.boxsel.h = p->w.pttmp[1].y - p->w.pttmp[0].y + 1;

	//範囲が 1x1 の場合は 2x1 とする

	if(p->sel.boxsel.w == 1 && p->sel.boxsel.h == 1)
		p->sel.boxsel.w = 2;

	drawUpdate_rect_canvas_select(p, NULL);

	//カーソル再判定

	drawOp_selectSetCursor_motion(p);

	//statusbar

	StatusBar_setHelp_tool();

	return TRUE;
}

/** 選択範囲リサイズ、押し時 */

mBool drawOp_selectResize_press(DrawData *p)
{
	mBox box;

	drawOpSub_setOpInfo(p, DRAW_OPTYPE_TMP,
		_selectResize_motion, _selectResize_release, 0);

	//

	box = p->sel.boxsel;

	p->w.pttmp[0].x = box.x;
	p->w.pttmp[0].y = box.y;
	p->w.pttmp[1].x = box.x + box.w - 1;
	p->w.pttmp[1].y = box.y + box.h - 1;

	drawOpSub_getImagePoint_int(p, &p->w.pttmp[2]);

	//選択範囲を消して、XOR 描画

	p->sel.boxsel.w = 0;

	drawUpdate_rect_canvas_select(p, &box);
	
	drawOpXor_drawBox_image(p);

	//status bar

	StatusBar_setHelp_imgbox();

	return TRUE;
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DrawData
 *
 * 初期化など
 *****************************************/

#include <time.h>

#include "mDef.h"
#include "mStr.h"
#include "mRandXorShift.h"

#include "defMacros.h"
#include "defDraw.h"
#include "defConfig.h"

#include "draw_main.h"
#include "draw_calc.h"
#include "draw_select.h"

#include "ImageBuf24.h"
#include "ImageBuf8.h"
#include "TileImage.h"
#include "LayerList.h"
#include "DrawFont.h"
#include "AppCursor.h"

#include "MainWinCanvas.h"
#include "StatusBar.h"
#include "Docks_external.h"



//=============================
// 初期化
//=============================


/** 解放 */

void DrawData_free()
{
	DrawData *p = APP_DRAW;

	if(p)
	{
		DrawFont_free(p->drawtext.font);

		mStrFree(&p->drawtext.strText);
		mStrFree(&p->drawtext.strName);
		mStrFree(&p->drawtext.strStyle);
		
		ImageBuf24_free(p->blendimg);
		ImageBuf8_free(p->tex.curimg);
		
		TileImage_free(p->tileimgDraw);
		TileImage_free(p->tileimgDraw2);
		TileImage_free(p->tileimgTmp);
		
		TileImage_free(p->sel.img_stamp);
		TileImage_free(p->sel.img_movecopy);
		TileImage_free(p->sel.img_selcopy);

		LayerList_free(p->layerlist);

		mFree(p);
	}

	TileImage_finish();
}

/** DrawData 作成
 *
 * [!] 設定ファイルの読み込み前に行う */

mBool DrawData_new()
{
	DrawData *p;

	p = (DrawData *)mMalloc(sizeof(DrawData), TRUE);
	if(!p) return FALSE;

	APP_DRAW = p;

	//値初期化

	p->canvas_zoom = 1000;
	p->szCanvas.w = p->szCanvas.h = 100;

	drawCalc_setCanvasViewParam(p);

	p->rule.ellipse_yx = 1;

	//TileImage 初期化

	if(!TileImage_init()) return FALSE;

	//作成

	p->layerlist = LayerList_new();

	//乱数初期化

	mRandXorShift_init(time(NULL));

	return TRUE;
}

/** ウィンドウ表示前の初期化 */

void drawInit_beforeShow()
{
	DrawData *p = APP_DRAW;
	
	//新規イメージ

	drawImage_new(p, APP_CONF->init_imgw, APP_CONF->init_imgh, 300, TRUE);

	//合成

	drawUpdate_blendImage_full(p);

	//キャンバス状態

	drawCanvas_setScrollDefault(p);
	drawCanvas_setZoomAndAngle(p, 0, 0, 0, FALSE);
}


//=============================
// カラー関連
//=============================


/** 描画色の RGB 値を取得 */

void drawColor_getDrawColor_rgb(int *dst)
{
	uint32_t col = APP_DRAW->col.drawcol;
	
	dst[0] = M_GET_R(col);
	dst[1] = M_GET_G(col);
	dst[2] = M_GET_B(col);
}

/** 描画色を PixelRGBA で取得 */

void drawColor_getDrawColor_pixRGBA(PixelRGBA *pix)
{
	uint32_t col = APP_DRAW->col.drawcol;
	
	pix->r = M_GET_R(col);
	pix->g = M_GET_G(col);
	pix->b = M_GET_B(col);
	pix->a = 255;
}

/** 描画色セット */

void drawColor_setDrawColor(uint32_t col)
{
	APP_DRAW->col.drawcol = col & 0xffffff;
}

/** 背景色セット */

void drawColor_setBkgndColor(uint32_t col)
{
	APP_DRAW->col.bkgndcol = col & 0xffffff;
}

/** 描画色/背景色入れ替え*/

void drawColor_toggleDrawCol(DrawData *p)
{
	uint32_t tmp;

	tmp = p->col.drawcol;
	p->col.drawcol = p->col.bkgndcol;
	p->col.bkgndcol = tmp;
}


//=============================
// 色マスク関連
//=============================


/** 色マスクのタイプを変更
 *
 * @param type -1:マスク反転  -2:逆マスク反転 */

void drawColorMask_setType(DrawData *p,int type)
{
	if(type >= 0)
		p->col.colmask_type = type;
	else
	{
		type = -type;
	
		if(p->col.colmask_type == type)
			p->col.colmask_type = 0;
		else
			p->col.colmask_type = type;
	}
}

/** 色マスクに色をセット
 *
 * @param col  -1 で描画色 */

void drawColorMask_setColor(DrawData *p,int col)
{
	p->col.colmask_num = 1;
	p->col.colmask_col[0] = (col == -1)? p->col.drawcol: col;
	p->col.colmask_col[1] = -1;
}

/** 色マスクに色を追加
 *
 * @param col  -1 で描画色 */

mBool drawColorMask_addColor(DrawData *p,int col)
{
	int i;

	if(p->col.colmask_num < COLORMASK_MAXNUM)
	{
		if(col == -1) col = p->col.drawcol;
	
		//同じ色が存在するか

		for(i = 0; i < p->col.colmask_num; i++)
		{
			if(p->col.colmask_col[i] == col)
				return FALSE;
		}
		
		//追加
	
		p->col.colmask_col[p->col.colmask_num] = col;
		p->col.colmask_col[p->col.colmask_num + 1] = -1;

		p->col.colmask_num++;
	}

	return TRUE;
}

/** 色マスクの色を削除
 *
 * @param no  負の値で最後の色 */

mBool drawColorMask_delColor(DrawData *p,int no)
{
	int i;
	int *pcol = p->col.colmask_col;

	//1色しかないならそのまま

	if(p->col.colmask_num == 1) return FALSE;

	if(no < 0) no = p->col.colmask_num - 1;

	//終端の -1 も含め詰める
	
	for(i = no; i < p->col.colmask_num; i++)
		pcol[i] = pcol[i + 1];

	p->col.colmask_num--;

	return TRUE;
}

/** 色マスクの色を最初の１色のみにする */

void drawColorMask_clear(DrawData *p)
{
	p->col.colmask_col[1] = -1;
	p->col.colmask_num = 1;
}



//=============================
// ツール
//=============================


/** ツール変更
 *
 * @param no -1 で直前のツール切り替え */

void drawTool_setTool(DrawData *p,int no)
{
	//直前のツール切り替え

	if(no == -1)
		no = p->tool.last_no;

	//変更

	if(p->tool.no != no)
	{
		//選択範囲をキャンセル

		drawSelect_cancel(TRUE);

		//

		p->tool.last_no = p->tool.no;

		p->tool.no = no;

		//各ウィジェット

		DockTool_changeTool();
		DockOption_changeTool();

		StatusBar_setHelp_tool();

		//カーソル

		MainWinCanvasArea_setCursor_forTool();
	}
}

/** ツールサブ変更 */

void drawTool_setToolSubtype(DrawData *p,int subno)
{
	if(subno != p->tool.subno[p->tool.no])
	{
		p->tool.subno[p->tool.no] = subno;

		DockTool_changeToolSub();

		StatusBar_setHelp_tool();
	}
}

/** 現在のツールがブラシ系ツールかどうか */

mBool drawTool_isBrushTool()
{
	int no = APP_DRAW->tool.no;

	return (no >= TOOL_PEN && no <= TOOL_BLUR);
}

/** 現在のツールが描画タイプを持つか */

mBool drawTool_isHaveDrawType()
{
	int no = APP_DRAW->tool.no;

	return (no <= TOOL_BLUR && no != TOOL_WATER);
}

/** ツールのカーソル番号取得 */

int drawCursor_getToolCursor(int toolno)
{
	int no;

	switch(toolno)
	{
		case TOOL_TEXT:
			no = APP_CURSOR_TEXT;
			break;
		case TOOL_MOVE:
			no = APP_CURSOR_MOVE;
			break;
		case TOOL_SELECT:
		case TOOL_SEL_MOVE:
		case TOOL_SEL_COPY:
		case TOOL_SEL_REPLACE:
			no = APP_CURSOR_SELECT;
			break;
		case TOOL_STAMP:
			if(APP_DRAW->sel.img_stamp)
				no = APP_CURSOR_STAMP;
			else
				no = APP_CURSOR_SELECT;
			break;
		case TOOL_CANVAS_ROTATE:
			no = APP_CURSOR_ROTATE;
			break;
		case TOOL_CANVAS_MOVE:
			no = APP_CURSOR_HAND;
			break;
		case TOOL_SPOIT:
			no = APP_CURSOR_SPOIT;
			break;
		default:
			no = APP_CURSOR_DRAW;
			break;
	}

	return no;
}

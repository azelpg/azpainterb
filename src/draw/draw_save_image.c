/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * DrawData
 *
 * PNG/JPEG/GIF/BMP 保存
 ************************************/

#include <string.h>

#include "mDef.h"
#include "mSaveImage.h"
#include "mPopupProgress.h"

#include "defDraw.h"
#include "defConfig.h"
#include "defFileFormat.h"

#include "ImageBuf24.h"

#include "draw_main.h"


//--------------------

typedef struct
{
	mSaveImage base;

	mPopupProgress *prog;
	uint8_t *curbuf;
	int format,
		pitch_src,
		cury,
		tpindex;
}_saveimg_norm;

//--------------------


/** 保存設定 */

static int _norm_set_option(mSaveImage *p)
{
	_saveimg_norm *pp = (_saveimg_norm *)p;
	ConfigSaveOption *opt = &APP_CONF->save;

	switch(pp->format)
	{
		//PNG
		case FILEFORMAT_PNG:
		case FILEFORMAT_PNG_ALPHA:
			mSaveImagePNG_setCompressionLevel(p, opt->png_complevel);

			if(opt->tpcol >= 0)
				mSaveImagePNG_setTransparent_8bit(p, opt->tpcol);
			break;
	
		//JPEG
		case FILEFORMAT_JPEG:
			mSaveImageJPEG_setQuality(p, opt->jpeg_quality);
			mSaveImageJPEG_setSamplingFactor(p, opt->jpeg_sampling_factor);

			if(opt->flags & CONFIG_SAVEOPTION_F_JPEG_PROGRESSIVE)
				mSaveImageJPEG_setProgression(p);
			break;

		//GIF
		case FILEFORMAT_GIF:
			if(APP_CONF->save.tpcol >= 0)
				mSaveImageGIF_setTransparent(p, pp->tpindex);
			break;
	}

	return MSAVEIMAGE_ERR_OK;
}

/** Y1行を送る */

static int _norm_send_row(mSaveImage *info,uint8_t *buf,int pitch)
{
	_saveimg_norm *p = (_saveimg_norm *)info;

	switch(p->base.coltype)
	{
		//RGB
		case MSAVEIMAGE_COLTYPE_RGB:
			memcpy(buf, p->curbuf, pitch);
			break;
		//パレット
		case MSAVEIMAGE_COLTYPE_PALETTE:
			ImageBuf24_setRow_forPalette(APP_DRAW->blendimg, buf, p->curbuf,
				p->base.palette_buf, p->base.palette_num);
			break;
		//RGBA (PNG)
		case MSAVEIMAGE_COLTYPE_RGBA:
			drawImage_getBlendRow_RGBA(APP_DRAW, buf, p->cury);
			break;
	}

	p->curbuf += p->pitch_src;
	p->cury++;

	mPopupProgressThreadIncSubStep(p->prog);

	return MSAVEIMAGE_ERR_OK;
}

/** blendimg から通常画像 (RGB) に保存
 *
 * @return 0:失敗 1:成功 2:GIF保存不可 */

int drawFile_save_image(DrawData *p,const char *filename,int format,mPopupProgress *prog)
{
	_saveimg_norm *info;
	mSaveImageFunc func = NULL;
	mBool ret = FALSE;
	uint8_t *palbuf = NULL;
	int palnum,tpindex;

	//BMP/PNG/GIF の場合、パレット取得 (256 色より多ければ NULL)

	if(format == FILEFORMAT_PNG || format == FILEFORMAT_GIF || format == FILEFORMAT_BMP)
	{
		tpindex = APP_CONF->save.tpcol;
		
		palbuf = ImageBuf24_getPalette(p->blendimg, &palnum, &tpindex);
	}

	//GIF で 256 色より多い場合はエラー

	if(format == FILEFORMAT_GIF && !palbuf)
		return 2;

	//情報

	info = (_saveimg_norm *)mSaveImage_create(sizeof(_saveimg_norm));
	if(!info)
	{
		mFree(palbuf);
		return FALSE;
	}

	info->prog = prog;
	info->curbuf = p->blendimg->buf;
	info->format = format;
	info->pitch_src = p->blendimg->pitch;
	info->tpindex = tpindex;

	info->base.output.type = MSAVEIMAGE_OUTPUT_TYPE_PATH;
	info->base.output.filename = filename;
	info->base.width = p->imgw;
	info->base.height = p->imgh;
	info->base.sample_bits = 8;
	info->base.resolution_unit = MSAVEIMAGE_RESOLITION_UNIT_DPI;
	info->base.resolution_horz = p->imgdpi;
	info->base.resolution_vert = p->imgdpi;
	info->base.send_row = _norm_send_row;
	info->base.set_option = _norm_set_option;

	if(format == FILEFORMAT_PNG_ALPHA)
		info->base.coltype = MSAVEIMAGE_COLTYPE_RGBA;
	else if(palbuf)
	{
		//256色以下ならパレットイメージ
		
		info->base.coltype = MSAVEIMAGE_COLTYPE_PALETTE;
		info->base.bits = 8;
		info->base.palette_buf = palbuf;
		info->base.palette_num = palnum;
	}
	else
		info->base.coltype = MSAVEIMAGE_COLTYPE_RGB;

	//保存関数

	switch(format)
	{
		case FILEFORMAT_PNG:
		case FILEFORMAT_PNG_ALPHA:
			func = mSaveImagePNG;
			break;
		case FILEFORMAT_JPEG:
			func = mSaveImageJPEG;
			break;
		case FILEFORMAT_GIF:
			func = mSaveImageGIF;
			break;
		case FILEFORMAT_BMP:
			func = mSaveImageBMP;
			break;
	}

	//保存

	mPopupProgressThreadBeginSubStep_onestep(prog, 20, p->imgh);

	if(func)
		ret = (func)(M_SAVEIMAGE(info));

	mSaveImage_free(M_SAVEIMAGE(info));

	mFree(palbuf);

	return ret;
}


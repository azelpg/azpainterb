/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DrawData 選択範囲系処理
 *****************************************/
/*
 * __MainWindow_selectcmd() 時、
 * 貼付けモード (DrawData::sel.mode) 中は実行されないようにしてあるので、
 * メニューなどからの実行時は、各コマンドで判定しなくてもよい。
 */

#include "mDef.h"
#include "mRectBox.h"
#include "mPopupProgress.h"

#include "defWidgets.h"
#include "defDraw.h"
#include "defConfig.h"
#include "AppErr.h"

#include "TileImage.h"
#include "TileImageDrawInfo.h"
#include "LayerItem.h"

#include "draw_main.h"
#include "draw_calc.h"
#include "draw_op_def.h"
#include "draw_op_sub.h"

#include "MainWindow.h"
#include "MainWinCanvas.h"
#include "PopupThread.h"
#include "Docks_external.h"


/** キャンバスビューが全体更新された時 */

void drawSelect_canvasviewUpdate_inPasteMode(DrawData *p)
{
	/* 貼付モード中は、blendimg に貼付イメージを合成するので、
	 * モード中にキャンバスビューが更新されると、
	 * 貼付イメージも表示されてしまう。
	 * そのため、モード中に全体更新が行われたら、
	 * モード終了後にキャンバスビューを更新して元に戻す。 */

	if(APP_DRAW->sel.mode == DRAW_SELMODE_MOVECOPY_PASTE
		|| APP_DRAW->sel.mode == DRAW_SELMODE_SELECT_PASTE)
	{
		APP_DRAW->sel.is_canvasview_update = TRUE;
	}
}

/** 選択範囲リサイズカーソル状態になっている場合、クリア */

void drawSelect_clearResizeCursor()
{
	if(APP_DRAW->sel.cur_resize_cursor)
	{
		MainWinCanvasArea_setCursor_forTool();

		APP_DRAW->sel.cur_resize_cursor = 0;
	}
}

/** 選択範囲や貼付け状態をキャンセルする
 *
 * [ 貼り付け確定後、イメージサイズ変更時、ツール変更時 ]
 *
 * @param update 範囲消去の更新を行うか */

void drawSelect_cancel(mBool update)
{
	DrawData *p = APP_DRAW;
	mRect rc;

	//モード解除

	if(p->sel.mode)
	{
		switch(p->sel.mode)
		{
			//範囲イメージ移動/コピー [待機]
			case DRAW_SELMODE_MOVECOPY_WAIT:
				drawOpSub_freeSelMoveCopyImage(p);
				break;
			//範囲イメージ移動/コピー [貼付]
			case DRAW_SELMODE_MOVECOPY_PASTE:
				drawOpSub_freeSelMoveCopyImage(p);

				if(p->sel.is_canvasview_update)
				{
					DockCanvasView_update();
					p->sel.is_canvasview_update = 0;
				}
				break;
			//選択範囲、貼付け
			case DRAW_SELMODE_SELECT_PASTE:
				if(p->sel.is_canvasview_update)
				{
					DockCanvasView_update();
					p->sel.is_canvasview_update = 0;
				}
				break;
		}

		//カーソルを元に戻す

		MainWinCanvasArea_setCursor_forTool();

		p->sel.mode = DRAW_SELMODE_NONE;
	}

	//選択範囲消す

	if(p->sel.boxsel.w)
	{
		mRectSetByBox(&rc, &p->sel.boxsel);

		p->sel.boxsel.w = 0;

		if(update)
			drawUpdate_rect_imgcanvas_forSelect(p, &rc);
	}

	//リサイズカーソルクリア

	drawSelect_clearResizeCursor();
}


//===========================


/** 処理対象の選択範囲を取得
 *
 * 範囲がない場合はイメージ全体 */

void drawSelect_getSelectBox(DrawData *p,mBox *box)
{
	if(p->sel.boxsel.w)
		*box = p->sel.boxsel;
	else
	{
		box->x = box->y = 0;
		box->w = p->imgw;
		box->h = p->imgh;
	}
}

/** フィルタの処理範囲を取得
 *
 * 選択ツールで範囲が選択されている場合はその範囲。
 * そうでなければカレントレイヤの全体。
 *
 * @param proc_color カラー処理用の範囲
 * @return FALSE でカラー処理時、カレントレイヤが空イメージ */

mBool drawSelect_getSelectBox_forFilter(DrawData *p,mRect *rc,mBox *box,mBool proc_color)
{
	if(p->tool.no == TOOL_SELECT && p->sel.boxsel.w)
	{
		*box = p->sel.boxsel;

		mRectSetByBox(rc, box);
	}
	else
	{
		if(proc_color)
		{
			/* カラー処理時は、確保されているタイルの範囲。
			 * 透明部分は処理されないので、省く。  */
			
			if(!TileImage_getHaveImageRect_pixel(p->curlayer->img, rc, NULL))
				return FALSE;
		}
		else
		{
			//カレントの処理可能範囲

			TileImage_getCanDrawRect_pixel(p->curlayer->img, rc);
		}

		mBoxSetByRect(box, rc);
	}

	return TRUE;
}

/** (フィルタ用) 選択範囲があればその範囲。なければイメージ全体。 */

void drawSelect_getSelectBox_forFilter_inImage(mRect *rc,mBox *box)
{
	DrawData *p = APP_DRAW;
	mBox box1;

	if(p->tool.no == TOOL_SELECT && p->sel.boxsel.w)
		box1 = p->sel.boxsel;
	else
	{
		box1.x = box1.y = 0;
		box1.w = p->imgw;
		box1.h = p->imgh;
	}

	if(box) *box = box1;
	if(rc) mRectSetByBox(rc, &box1);
}

/** 選択範囲の範囲をセット
 *
 * @param boxsel  NULL でクリア */

void drawSelect_setSelectBox(DrawData *p,mBox *boxsel)
{
	mBox box,boxu;
	mRect rc;

	//カーソルクリア

	drawSelect_clearResizeCursor();

	//boxsel をイメージ範囲内に => box
	/* 1x1 の場合はクリアする */
	
	if(boxsel)
	{
		mRectSetByBox(&rc, boxsel);

		if(!drawCalc_getImageBox_rect(p, &box, &rc)
			|| (box.w == 1 && box.h == 1))
			boxsel = NULL;
	}

	//

	if(!boxsel)
	{
		//クリア
		
		if(p->sel.boxsel.w)
		{
			boxu = p->sel.boxsel;
			
			p->sel.boxsel.w = 0;
			
			drawUpdate_rect_canvas_select(p, &boxu);
		}
	}
	else if(p->sel.boxsel.w == 0)
	{
		//範囲がない状態でセット

		p->sel.boxsel = box;
			
		drawUpdate_rect_canvas_select(p, &box);
	}
	else
	{
		//範囲を変更

		boxu = p->sel.boxsel;
		mBoxUnion(&boxu, &box);

		p->sel.boxsel = box;

		drawUpdate_rect_canvas_select(p, &boxu);
	}
}

/** 全て選択 */

void drawSelect_allSelect(DrawData *p)
{
	p->sel.boxsel.x = p->sel.boxsel.y = 0;
	p->sel.boxsel.w = p->imgw;
	p->sel.boxsel.h = p->imgh;

	drawUpdate_canvasArea();
}


//========================
// コピー/ペースト
//========================


/** コピー/切り取り */

void drawSelect_copy_and_cut(DrawData *p,mBool cut)
{
	mBox box;

	if(p->sel.boxsel.w == 0) return;

	//解放

	TileImage_free(p->sel.img_selcopy);
	p->sel.img_selcopy = NULL;

	//

	box = p->sel.boxsel;

	if(!cut)
		//コピー
		p->sel.img_selcopy = TileImage_createCopyImage_forBoxSel(p->curlayer->img, &box, FALSE);
	else
	{
		//切り取り

		drawOpSub_setDrawInfo_forOverwrite();
		drawOpSub_beginDraw(p);

		p->sel.img_selcopy = TileImage_createCopyImage_forBoxSel(p->curlayer->img, &box, TRUE);

		drawOpSub_finishDraw_single(p);
	}

	//サイズ

	p->sel.size_selcopy.w = box.w;
	p->sel.size_selcopy.h = box.h;
}

/** 貼付け */

void drawSelect_paste(DrawData *p)
{
	mBox box;

	//コピーイメージがない
	
	if(!p->sel.img_selcopy) return;

	//ツール変更

	drawTool_setTool(p, TOOL_SELECT);

	//現在の範囲を消去

	if(p->sel.boxsel.w)
	{
		box = p->sel.boxsel;

		p->sel.boxsel.w = 0;
		
		drawUpdate_rect_canvas_select(p, &box);
	}

	//貼付けモードへ

	drawCalc_setSelectPasteBox(p);

	p->sel.mode = DRAW_SELMODE_SELECT_PASTE;

	TileImage_setOffset(p->sel.img_selcopy, p->sel.boxsel.x, p->sel.boxsel.y);

	drawUpdate_rect_imgcanvas_forSelect(p, NULL);
}


//========================
// 編集系
//========================


/** 塗りつぶし */

void drawSelect_fill(DrawData *p)
{
	mBox box;

	if(drawOpSub_canDrawLayer_andSelMode(p)) return;

	drawSelect_getSelectBox(p, &box);

	drawOpSub_setDrawInfo(p, TOOL_SELECT, 0);
	drawOpSub_beginDraw_single(p);

	TileImage_drawFillBox(p->w.dstimg,
		box.x, box.y, box.w, box.h, &p->w.pixdraw);

	drawOpSub_endDraw_single(p);
}

/** 消去 */

void drawSelect_erase(DrawData *p)
{
	mBox box;

	if(drawOpSub_canDrawLayer_andSelMode(p)) return;

	drawSelect_getSelectBox(p, &box);

	drawOpSub_setDrawInfo(p, TOOL_SELECT, 0);
	drawOpSub_beginDraw_single(p);

	g_tileimage_dinfo.funcColor = TileImage_colfunc_erase;

	TileImage_drawFillBox(p->w.dstimg,
		box.x, box.y, box.w, box.h, &p->w.pixdraw);

	drawOpSub_endDraw_single(p);
}

/** トリミング */

void drawSelect_trim(DrawData *p)
{
	if(p->sel.boxsel.w == 0
		|| drawOpSub_canDrawLayer_andSelMode(p) == CANDRAWLAYER_SELMODE)
		return;

	if(!drawImage_resizeCanvas(p, p->sel.boxsel.w, p->sel.boxsel.h,
		-p->sel.boxsel.x, -p->sel.boxsel.y, TRUE))
		MainWindow_apperr(APPERR_ALLOC, NULL);

	MainWindow_updateNewCanvas(APP_WIDGETS->mainwin, NULL);
}

/** タイル状に並べる */

void drawSelect_arrange_tile(DrawData *p,int type)
{
	//範囲がない場合はなし

	if(p->sel.boxsel.w == 0
		|| drawOpSub_canDrawLayer_andSelMode(p))
		return;

	//

	drawOpSub_setDrawInfo_forOverwrite();
	drawOpSub_beginDraw_single(p);

	TileImage_arrangeTile(p->w.dstimg, type, &p->sel.boxsel);

	drawOpSub_endDraw_single(p);
}

/** 左右/上下反転、90度回転*/

void drawSelect_reverse_and_rotate90(DrawData *p,int type)
{
	TileImage *img = NULL;
	mBox box;

	if(drawOpSub_canDrawLayer_andSelMode(p)) return;

	drawSelect_getSelectBox(p, &box);

	//----------

	drawOpSub_setDrawInfo_forOverwrite();
	drawOpSub_beginDraw_single(p);

	//回転時は、範囲内イメージをコピー & クリア

	if(type >= 2)
	{
		img = drawOpSub_createTileImage_box(&box);
		if(img)
			TileImage_copyImage_and_clear(p->w.dstimg, img, &box);
	}

	//描画

	switch(type)
	{
		case 0:
			TileImage_rectReverse_horz(p->w.dstimg, &box);
			break;
		case 1:
			TileImage_rectReverse_vert(p->w.dstimg, &box);
			break;
		case 2:
			TileImage_rectRotate_left(p->w.dstimg, img, &box);
			break;
		case 3:
			TileImage_rectRotate_right(p->w.dstimg, img, &box);
			break;
	}

	TileImage_free(img);

	drawOpSub_endDraw_single(p);
}


//==============================
// 拡大縮小
//==============================


typedef struct
{
	TileImage *img;	//拡大縮小後のイメージ
	mBox *box;
	int type,w,h;
}_thdata_scaling;


/** [スレッド] 拡大縮小 */

static int _thread_scaling(mPopupProgress *prog,void *data)
{
	_thdata_scaling *p = (_thdata_scaling *)data;
	TileImage *img_src,*img;
	mSize sizeSrc,sizeDst;
	int ret;

	//範囲イメージをコピー

	img_src = TileImage_createCopyImage_box(APP_DRAW->curlayer->img, p->box);
	if(!img_src) return FALSE;

	//出力先イメージ作成

	img = TileImage_new(TILEIMAGE_COLTYPE_RGBA, p->w, p->h);
	if(!img)
	{
		TileImage_free(img_src);
		return FALSE;
	}

	//拡大縮小

	sizeSrc.w = p->box->w;
	sizeSrc.h = p->box->h;
	sizeDst.w = p->w;
	sizeDst.h = p->h;

	mPopupProgressThreadSetMax(prog, 6);

	ret = TileImage_scaling(img, img_src, p->type, &sizeSrc, &sizeDst, prog);

	TileImage_free(img_src);

	if(!ret)
	{
		TileImage_free(img);
		return FALSE;
	}
	else
	{
		p->img = img;
		return TRUE;
	}
}

/** 拡大縮小 */

int drawSelect_scaling(DrawData *p,mBox *box,int type,int w,int h)
{
	_thdata_scaling dat;
	int ret;
	mBox boxdst;

	if(drawOpSub_canDrawLayer(p)) return APPERR_OK;

	dat.box = box;
	dat.type = type;
	dat.w = w;
	dat.h = h;

	//実行 (拡大縮小後のイメージを作成)

	ret = PopupThread_run(&dat, _thread_scaling);
	if(ret == -1 || !ret)
		return APPERR_ALLOC;

	//イメージを貼り付け
	/* 拡大時はカレントレイヤの描画可能範囲の外には描画されない */

	drawOpSub_setDrawInfo_forOverwrite();
	drawOpSub_beginDraw_single(p);

	boxdst = *box;
	boxdst.w = w;
	boxdst.h = h;

	TileImage_pasteEditImage(p->w.dstimg, dat.img, box, &boxdst);

	drawOpSub_endDraw_single(p);

	//

	TileImage_free(dat.img);

	//選択範囲を拡大縮小後の範囲に変更

	drawSelect_setSelectBox(p, &boxdst);

	return APPERR_OK;
}


//==============================
// 回転
//==============================


typedef struct
{
	TileImage *dstimg,*srcimg;
	int type,angle;
	mBox box;
}_thdata_rotate;


/** [スレッド] 拡大縮小 */

static int _thread_rotate(mPopupProgress *prog,void *data)
{
	_thdata_rotate *p = (_thdata_rotate *)data;

	TileImage_rotate(p->dstimg, p->srcimg, &p->box, p->type, p->angle, prog);

	return TRUE;
}

/** 回転 */

void drawSelect_rotate(DrawData *p,int type,int angle)
{
	_thdata_rotate dat;

	if(drawOpSub_canDrawLayer(p)) return;

	dat.dstimg = p->curlayer->img;
	dat.type = type;
	dat.angle = angle;

	drawSelect_getSelectBox(p, &dat.box);

	//範囲イメージをコピー

	dat.srcimg = TileImage_createCopyImage_box(dat.dstimg, &dat.box);
	if(!dat.srcimg) return;

	//実行

	drawOpSub_setDrawInfo_forOverwrite();
	drawOpSub_beginDraw_single(p);

	PopupThread_run(&dat, _thread_rotate);

	drawOpSub_endDraw_single(p);

	TileImage_free(dat.srcimg);
}


//===============================
// ファイルから貼り付け
//===============================


typedef struct
{
	const char *filename;
	TileImage *img;
	TileImageLoadFileInfo info;
	mBool ignore_alpha,
		leave_trans;
}_thdata_loadfile;


/** [スレッド] ファイルから読み込み */

static int _thread_loadfile(mPopupProgress *prog,void *data)
{
	_thdata_loadfile *p = (_thdata_loadfile *)data;

	p->img = TileImage_loadFile(p->filename, -1,
		p->ignore_alpha, p->leave_trans, 20000, &p->info, prog, NULL);

	return (p->img)? APPERR_OK: APPERR_LOAD;
}

/** ファイルから貼付け */

int drawSelect_pasteFromFile(DrawData *p,const char *filename,
	mBool ignore_alpha,mBool leave_trans)
{
	_thdata_loadfile dat;
	int err;

	dat.filename = filename;
	dat.ignore_alpha = ignore_alpha;
	dat.leave_trans = leave_trans;

	//読み込み

	err = PopupThread_run(&dat, _thread_loadfile);
	if(err == -1) err = APPERR_ALLOC;

	if(err != APPERR_OK) return err;

	//貼り付けイメージにセット

	TileImage_free(p->sel.img_selcopy);

	p->sel.img_selcopy = dat.img;

	p->sel.size_selcopy.w = dat.info.width;
	p->sel.size_selcopy.h = dat.info.height;

	//貼り付けモードへ

	drawSelect_paste(p);

	return APPERR_OK;
}


//==============================
// スタンプ
//==============================


/** スタンプイメージをクリア */

void drawSelect_clearStamp(DrawData *p)
{
	if(p->sel.img_stamp)
	{
		TileImage_free(p->sel.img_stamp);
		p->sel.img_stamp = NULL;

		DockOption_changeStampImage();

		MainWinCanvasArea_setCursor_forTool();
	}
}

/** 画像ファイルからスタンプイメージ読み込み */

void drawSelect_loadStampImage(DrawData *p,const char *filename)
{
	TileImageLoadFileInfo info;

	TileImage_free(p->sel.img_stamp);

	p->sel.img_stamp = TileImage_loadFile(filename, -1,
		APP_CONF->openimage_flags & CONFIG_OPENIMAGE_F_IGNORE_ALPHA,
		APP_CONF->openimage_flags & CONFIG_OPENIMAGE_F_LEAVE_TRANS,
		10000, &info, NULL, NULL);

	if(p->sel.img_stamp)
	{
		p->sel.size_stamp.w = info.width;
		p->sel.size_stamp.h = info.height;

		//カーソル変更
		MainWinCanvasArea_setCursor_forTool();
	}
}

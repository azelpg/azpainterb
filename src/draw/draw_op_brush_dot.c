/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * DrawData
 *
 * 操作 - ドットペン/ブラシ
 *****************************************/
/*
 * [!] DrawData::w.ntmp[0] はドットペン時に drawOpSub_setDrawInfo() で使われるので注意。
 */


#include <stdlib.h>
#include <math.h>

#include "mDef.h"

#include "defDraw.h"
#include "defConfig.h"

#include "TileImage.h"
#include "TileImageDrawInfo.h"
#include "BrushList.h"
#include "BrushItem.h"
#include "BrushDrawParam.h"
#include "DrawPointBuf.h"

#include "draw_main.h"
#include "draw_calc.h"
#include "draw_op_def.h"
#include "draw_op_sub.h"
#include "draw_op_func.h"

#include "Docks_external.h"



//=============================
// ドットペン [自由線]
//=============================
/*
 * +Shift : 1px消しゴム
 * 
 * ntmp[0] : 細線か (drawOpSub_setDrawInfo() でセットされる)
 * pttmp   : [0] 前回の位置 [1] 2つ前の位置
 */


/** 移動 */

static void _dotpen_free_motion(DrawData *p,uint32_t state)
{
	mPoint pt,ptlast;
	mBool draw = TRUE;

	//位置

	drawOpSub_getDrawPoint_dotpen(p, &pt);

	ptlast = p->w.pttmp[0];

	//前回と同じ位置なら描画しない

	if(pt.x == ptlast.x && pt.y == ptlast.y)
		return;

	//細線時、2つ前の位置が斜めの場合は描画しない

	if(p->w.ntmp[0])
	{
		if(abs(pt.x - p->w.pttmp[1].x) == 1 && abs(pt.y - p->w.pttmp[1].y) == 1
			&& (pt.x == ptlast.x || pt.y == ptlast.y))
			draw = FALSE;
	}

	//描画

	if(draw)
	{
		TileImageDrawInfo_clearDrawRect();

		if(p->w.ntmp[0])
		{
			//細線
			TileImage_drawLineF(p->w.dstimg, ptlast.x, ptlast.y,
				pt.x, pt.y, &p->w.pixdraw);
		}
		else
		{
			//通常
			TileImage_drawLineB(p->w.dstimg, ptlast.x, ptlast.y,
				pt.x, pt.y, &p->w.pixdraw, TRUE);
		}

		drawOpSub_addrect_and_update(p, FALSE);
	}

	//位置記録

	if(draw)
		p->w.pttmp[1] = ptlast;

	p->w.pttmp[0] = pt;
}

/** 離し */

static mBool _dotpen_free_release(DrawData *p)
{
	drawOpSub_finishDraw_workrect(p);
	
	return TRUE;
}

/** 押し */

mBool drawOp_dotpen_free_press(DrawData *p)
{
	mPoint pt;

	drawOpSub_setOpInfo(p, DRAW_OPTYPE_DRAW_FREE,
		_dotpen_free_motion, _dotpen_free_release, 0);

	//描画情報

	drawOpSub_setDrawInfo(p, p->w.optoolno, ((p->w.press_state & M_MODS_SHIFT) != 0));

	//描画位置

	drawOpSub_getDrawPoint_dotpen(p, &pt);

	p->w.pttmp[0] = p->w.pttmp[1] = pt;

	//描画開始

	drawOpSub_beginDraw(p);

	//最初の点を打つ

	(g_tileimage_dinfo.funcDrawPixel)(p->w.dstimg, pt.x, pt.y, &p->w.pixdraw);

	//更新

	drawOpSub_addrect_and_update(p, FALSE);

	//[dock]キャンバスビュー、ルーペ時は描画後に更新が行われないので、ここで行う
	drawUpdate_canvasview_inLoupe();

	return TRUE;
}


//===========================
// ブラシ描画 [自由線]
//===========================


/** ブラシ線描画 */

static void _brush_free_draw(DrawData *p,DrawPointBuf_pt *pt)
{
	TileImageDrawInfo_clearDrawRect();

	TileImage_drawBrushFree(p->w.dstimg, pt->x, pt->y, pt->pressure);

	drawOpSub_addrect_and_update(p, TRUE);
}

/** 離し */

static mBool _brush_free_release(DrawData *p)
{
	DrawPointBuf_pt pt;

	//残りの線を描画

	while(DrawPointBuf_getFinishPoint(&pt))
		_brush_free_draw(p, &pt);

	//終了

	drawOpSub_finishDraw_workrect(p);

	return TRUE;
}

/** 移動 */

static void _brush_free_motion(DrawData *p,uint32_t state)
{
	DrawPoint dpt;
	DrawPointBuf_pt pt;

	//位置

	drawOpSub_getDrawPoint(p, &dpt);

	if(p->rule.type)
		(p->rule.funcGetPoint)(p, &dpt.x, &dpt.y);

	if(p->w.opflags & DRAW_OPFLAGS_BRUSH_PRESSURE_MAX)
		dpt.pressure = 1;

	DrawPointBuf_addPoint(dpt.x, dpt.y, dpt.pressure);

	//描画
	/* pt = 手ぶれ補正後の位置 */

	if(DrawPointBuf_getPoint(&pt))
		_brush_free_draw(p, &pt);
}

/** 押し */

mBool drawOp_brush_free_press(DrawData *p,mBool pressure_max)
{
	DrawPoint dpt;

	drawOpSub_setOpInfo(p, DRAW_OPTYPE_DRAW_FREE,
		_brush_free_motion, _brush_free_release, 0);

	//描画情報

	drawOpSub_setDrawInfo(p, p->w.optoolno, 0);

	//筆圧最大フラグ

	if(pressure_max)
		p->w.opflags |= DRAW_OPFLAGS_BRUSH_PRESSURE_MAX;

	//位置

	drawOpSub_getDrawPoint(p, &dpt);
	if(pressure_max) dpt.pressure = 1;

	//ポイントバッファ
	
	DrawPointBuf_setSmoothing(
		g_tileimage_dinfo.brushparam->smoothing_type,
		g_tileimage_dinfo.brushparam->smoothing_str);

	DrawPointBuf_setPoint(dpt.x, dpt.y, dpt.pressure);

	//開始

	TileImage_beginDrawBrush_free(p->w.dstimg, dpt.x, dpt.y, dpt.pressure);

	drawOpSub_beginDraw(p);

	return TRUE;
}


//=============================
// 指先
//=============================
/*
 * pttmp[0] : 前回の位置
 */


/** 移動 */

static void _finger_motion(DrawData *p,uint32_t state)
{
	mPoint pt,ptlast;

	//位置

	drawOpSub_getDrawPoint_dotpen(p, &pt);

	ptlast = p->w.pttmp[0];

	//前回と同じ位置なら描画しない

	if(pt.x == ptlast.x && pt.y == ptlast.y)
		return;

	//描画

	TileImageDrawInfo_clearDrawRect();

	TileImage_drawLineB(p->w.dstimg, ptlast.x, ptlast.y,
		pt.x, pt.y, &p->w.pixdraw, TRUE);

	drawOpSub_addrect_and_update(p, FALSE);

	//位置記録

	p->w.pttmp[0] = pt;
}

/** 押し */

mBool drawOp_finger_press(DrawData *p)
{
	mPoint pt;

	drawOpSub_setOpInfo(p, DRAW_OPTYPE_DRAW_FREE,
		_finger_motion, _dotpen_free_release, 0);

	//描画情報

	drawOpSub_setDrawInfo(p, p->w.optoolno, 0);

	//描画位置

	drawOpSub_getDrawPoint_dotpen(p, &pt);

	p->w.pttmp[0] = pt;

	//現在の色を指先バッファにセット

	if(!TileImage_setFingerBuf(p->w.dstimg, pt.x, pt.y))
		return FALSE;

	//描画開始

	drawOpSub_beginDraw(p);

	return TRUE;
}


//=============================
// 描画処理
//=============================


/** 直線描画 */

void drawOpDraw_line(DrawData *p,mBool coordinate_image)
{
	mDoublePoint pt1,pt2;

	drawOpSub_getDrawLinePoints(p, &pt1, &pt2, coordinate_image);

	drawOpSub_setDrawInfo(p, p->w.optoolno, 0);
	drawOpSub_beginDraw_single(p);

	if(drawOpSub_isDotPenDraw())
		TileImage_drawLineB(p->w.dstimg, pt1.x, pt1.y, pt2.x, pt2.y, &p->w.pixdraw, FALSE);
	else
	{
		TileImage_drawBrushLine_headtail(p->w.dstimg,
			pt1.x, pt1.y, pt2.x, pt2.y, p->headtail.curval[0]);
	}

	drawOpSub_endDraw_single(p);
}

/** 四角形枠描画 */

void drawOpDraw_box(DrawData *p,mBool coordinate_image)
{
	mDoublePoint pt[4];
	int i,n;

	drawOpSub_getDrawBoxPoints(p, pt, coordinate_image);

	drawOpSub_setDrawInfo(p, p->w.optoolno, 0);
	drawOpSub_beginDraw_single(p);

	if(drawOpSub_isDotPenDraw())
	{
		//ドットペン
		
		for(i = 0; i < 4; i++)
		{
			n = (i + 1) & 3;
		
			TileImage_drawLineB(p->w.dstimg,
				pt[i].x, pt[i].y, pt[n].x, pt[n].y, &p->w.pixdraw, TRUE);
		}
	}
	else
		TileImage_drawBrushBox(p->w.dstimg, pt);

	drawOpSub_endDraw_single(p);
}

/** 楕円枠描画 */

void drawOpDraw_ellipse(DrawData *p,mBool coordinate_image)
{
	mDoublePoint pt,pt_r;

	drawOpSub_setDrawInfo(p, p->w.optoolno, 0);
	drawOpSub_beginDraw_single(p);

	if(coordinate_image)
	{
		/* ドットペンでキャンバス回転なしの場合、
		 * ドット単位で描画 */

		TileImage_drawEllipse_dot(p->w.dstimg,
			p->w.rctmp[0].x1, p->w.rctmp[0].y1,
			p->w.rctmp[0].x2, p->w.rctmp[0].y2, &p->w.pixdraw);
	}
	else
	{
		drawOpSub_getDrawEllipseParam(p, &pt, &pt_r, coordinate_image);

		TileImage_drawEllipse(p->w.dstimg,
			pt.x, pt.y, pt_r.x, pt_r.y, &p->w.pixdraw,
			drawOpSub_isDotPenDraw(), &p->viewparam, p->canvas_mirror);
	}

	drawOpSub_endDraw_single(p);
}

/** 連続直線/集中線 描画 */

void drawOpDraw_lineSuccConc(DrawData *p)
{
	mDoublePoint pt1,pt2;

	drawOpSub_getDrawLinePoints(p, &pt1, &pt2, p->w.ntmp[1]);

	TileImageDrawInfo_clearDrawRect();

	if(drawOpSub_isDotPenDraw())
		TileImage_drawLineB(p->w.dstimg, pt1.x, pt1.y, pt2.x, pt2.y, &p->w.pixdraw, FALSE);
	else
	{
		TileImage_drawBrushLine_headtail(p->w.dstimg,
			pt1.x, pt1.y, pt2.x, pt2.y, p->headtail.curval[0]);
	}

	drawOpSub_addrect_and_update(p, FALSE);
}


//===========================
// ベジェ曲線
//===========================
/*
	pttmp[0-3] : 始点、制御点1、制御点2、終点
	boxtmp[0]  : XOR 描画された範囲
	opsubtype  : 0=1つ目の制御点  1=2つ目の制御点
*/


/** 移動 */

static void _bezier_motion(DrawData *p,uint32_t state)
{
	mPoint pt;

	drawOpXor_drawBezier(p, TRUE);

	drawOpSub_getAreaPoint_int(p, &pt);

	if(state & M_MODS_SHIFT)
		drawCalc_fitLine45(&pt, p->w.pttmp + (p->w.opsubtype? 3: 0));

	p->w.pttmp[1 + p->w.opsubtype] = pt;

	if(p->w.opsubtype == 0)
		p->w.pttmp[2] = pt;

	drawOpXor_drawBezier(p, FALSE);
}

/** 離し */

static mBool _bezier_release(DrawData *p)
{
	drawOpXor_drawBezier(p, TRUE);

	if(p->w.opsubtype == 0)
	{
		//----- 2つ目の制御点へ

		p->w.pttmp[2] = p->w.pttmp[1];
		p->w.opsubtype = 1;

		drawOpXor_drawBezier(p, FALSE);

		return FALSE;
	}
	else
	{
		//----- 終了

		mDoublePoint pt[4];
		int i;

		//イメージ位置

		for(i = 0; i < 4; i++)
			drawCalc_areaToimage_double_pt(p, pt + i, p->w.pttmp + i);

		//描画

		drawOpSub_setDrawInfo(p, p->w.optoolno, 0);
		drawOpSub_beginDraw_single(p);

		TileImage_drawBezier(p->w.dstimg, pt, &p->w.pixdraw,
			drawOpSub_isDotPenDraw(), p->headtail.curval[1]);

		drawOpSub_endDraw_single(p);

		//

		drawOpSub_freeTmpImage(p);

		return TRUE;
	}
}

/** 各アクション (操作終了) */

static mBool _bezier_action(DrawData *p,int action)
{
	switch(action)
	{
		//右ボタン/ESC/グラブ解放 : キャンセル
		case DRAW_FUNCACTION_RBTT_UP:
		case DRAW_FUNCACTION_KEY_ESC:
		case DRAW_FUNCACTION_UNGRAB:
			drawOpXor_drawBezier(p, TRUE);
			drawOpSub_freeTmpImage(p);
			return TRUE;
		//BackSpace : 一つ目の制御点に戻る
		case DRAW_FUNCACTION_KEY_BACKSPACE:
			if(p->w.opsubtype == 1)
			{
				p->w.opsubtype = 0;
				_bezier_motion(p, 0);
			}
			return FALSE;
		//ほか : 何もしない
		default:
			return FALSE;
	}
}

/** xorline から制御点指定へ移行
 *
 * @return グラブ解除するか */

mBool drawOp_xorline_to_bezier(DrawData *p)
{
	if(!drawOpSub_createTmpImage_area(p))
		return TRUE;
		
	drawOpSub_setOpInfo(p, DRAW_OPTYPE_XOR_BEZIER,
		_bezier_motion, _bezier_release, 0);

	p->w.funcAction = _bezier_action;

	p->w.pttmp[3] = p->w.pttmp[1];
	p->w.pttmp[1] = p->w.pttmp[2] = p->w.pttmp[3];

	g_tileimage_dinfo.funcDrawPixel = TileImage_setPixel_new_drawrect;

	drawOpXor_drawBezier(p, FALSE);

	return FALSE;
}


//=====================================================
// [ブラシ系] +Shift 左右ドラッグでのブラシサイズ変更
//=====================================================
/*
	pttmp[0]   : 押し時の位置
	ntmp[0]    : 半径サイズ(px)
	ntmp[1]    : 押し時のブラシサイズ
	boxtmp[0]  : XOR 描画範囲
	ptmp       : 対象ブラシアイテムポインタ (BrushItem *)
*/


/** 移動 */

static void _dragBrushSize_motion(DrawData *p,uint32_t state)
{
	BrushItem *item = (BrushItem *)p->w.ptmp;
	int n;

	//半径

	n = p->w.ntmp[1] + (int)((p->w.dptAreaCur.x - p->w.dptAreaPress.x) * APP_CONF->dragBrushSize_step);

	n = BrushItem_adjustRadius(item, n);
	
	//変更

	if(n != item->radius)
	{
		drawOpXor_drawBrushSizeCircle(p, TRUE);

		p->w.ntmp[0] = (int)(n * 0.1 * p->viewparam.scale + 0.5);

		drawOpXor_drawBrushSizeCircle(p, FALSE);

		//

		item->radius = n;

		//[dock]オプションの半径バー値変更

		DockOption_changeBrushRadius(n);
	}
}

/** 離し */

static mBool _dragBrushSize_release(DrawData *p)
{
	drawOpXor_drawBrushSizeCircle(p, TRUE);
	
	return TRUE;
}

/** 押し */

mBool drawOp_dragBrushSize_press(DrawData *p)
{
	BrushItem *item;

	drawOpSub_setOpInfo(p, DRAW_OPTYPE_DRAG_BRUSHSIZE,
		_dragBrushSize_motion, _dragBrushSize_release, 0);

	//押し時の位置

	drawOpSub_getAreaPoint_int_raw(p, &p->w.pttmp[0]);

	//対象ブラシ

	item = BrushList_getEditItem(BrushList_getListNo_fromTool(p->w.optoolno));

	p->w.ptmp = item;
	p->w.ntmp[0] = (int)(item->radius * 0.1 * p->viewparam.scale + 0.5);
	p->w.ntmp[1] = item->radius;

	//円描画

	drawOpXor_drawBrushSizeCircle(p, FALSE);

	return TRUE;
}

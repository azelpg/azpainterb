/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * ConfigData 関数
 * 
 * (設定データ処理)
 *****************************************/

#include <stdio.h>

#include "mDef.h"
#include "mAppDef.h"
#include "mStr.h"
#include "mList.h"
#include "mIniWrite.h"
#include "mIniRead.h"
#include "mPath.h"
#include "mUtilFile.h"
#include "mDirEntry.h"

#include "defConfig.h"
#include "defGridData.h"


//--------------------

#define _CONFIG_GRID_FILE  "grid.conf"

//--------------------



/** 解放 */

void ConfigData_free()
{
	ConfigData *p = APP_CONF;

	if(p)
	{
		mListDeleteAll(&p->grid_list);
	
		mStrFree(&p->strFontStyle_gui);
		mStrFree(&p->strFontStyle_dock);
		mStrFree(&p->strTempDir);
		mStrFree(&p->strTempDirProc);

		mStrFree(&p->strImageViewerDir);
		mStrFree(&p->strGridFileDir);
		mStrFree(&p->strTextureFileDir);
		mStrFree(&p->strLayerFileDir);
		mStrFree(&p->strStampFileDir);
		mStrFree(&p->strSelectFileDir);
		mStrFree(&p->strThemeFile);

		mStrArrayFree(p->strRecentFile, CONFIG_RECENTFILE_NUM);
		mStrArrayFree(p->strRecentOpenDir, CONFIG_RECENTDIR_NUM);
		mStrArrayFree(p->strRecentSaveDir, CONFIG_RECENTDIR_NUM);

		mFree(p->cursor_buf);
		mFree(p->toolbar_btts);

		mFree(p);
	}
}

/** ConfigData 作成 */

mBool ConfigData_new()
{
	ConfigData *p;

	p = (ConfigData *)mMalloc(sizeof(ConfigData), TRUE);
	if(!p)
		return FALSE;
	else
	{
		APP_CONF = p;
		return TRUE;
	}
}

/** 作業用ディレクトリの初期値セット */

void ConfigData_setTempDir_init()
{
	char *path;

	if(mStrIsEmpty(&APP_CONF->strTempDir))
	{
		path = mGetTempPath();
	
		mStrSetText(&APP_CONF->strTempDir, path);

		mFree(path);
	}
}

/** 作業用ディレクトリ削除 */

void ConfigData_deleteTempDir()
{
	mDirEntry *dir;
	mStr str = MSTR_INIT;

	if(mStrIsEmpty(&APP_CONF->strTempDirProc)) return;

	//残っているファイルを削除

	dir = mDirEntryOpen(APP_CONF->strTempDirProc.buf);
	if(!dir) return;

	while(mDirEntryRead(dir))
	{
		if(!mDirEntryIsDirectory(dir))
		{
			mDirEntryGetFileName_str(dir, &str, TRUE);
			mDeleteFile(str.buf);
		}
	}

	mDirEntryClose(dir);

	mStrFree(&str);

	//ディレクトリを削除

	mDeleteDir(APP_CONF->strTempDirProc.buf);
}

/** 作業用ディレクトリ作成 */

void ConfigData_createTempDir()
{
	char *name;
	mStr *pstr;

	pstr = &APP_CONF->strTempDirProc;

	//<tmp>/~azpainterb

	mStrCopy(pstr,  &APP_CONF->strTempDir);
	mStrPathAdd(pstr, "~azpainterb");

	if(!mIsFileExist(pstr->buf, TRUE))
	{
		if(!mCreateDir(pstr->buf))
		{
			mStrFree(pstr);
			return;
		}
	}

	//プロセスごとのディレクトリ

	name = mGetProcessTempName();

	mStrPathAdd(pstr, name);

	mFree(name);

	//存在していたら削除

	if(mIsFileExist(pstr->buf, TRUE))
		ConfigData_deleteTempDir();

	//作成

	if(!mCreateDir(pstr->buf))
		mStrFree(pstr);
}


//==============================
// グリッドデータ
//==============================


/** ファイルからグリッドデータ読み込み */

mBool ConfigData_loadGrid(mList *list,const char *filename)
{
	mIniRead *ini;
	int i,num;
	GridListData *pi;

	if(!filename)
		ini = mIniReadLoadFile2(MAPP->pathConfig, _CONFIG_GRID_FILE);
	else
		ini = mIniReadLoadFile(filename);

	if(!ini) return FALSE;

	//バージョン

	mIniReadSetGroup(ini, "grid");

	if(mIniReadInt(ini, "ver", 0) != 1)
	{
		mIniReadEnd(ini);
		return FALSE;
	}

	num = mIniReadInt(ini, "num", 0);

	//データ

	for(i = 0; i < num; i++)
	{
		if(!mIniReadSetGroup_int(ini, i)) break;

		pi = (GridListData *)mListAppendNew(list, sizeof(GridListData), NULL);
		if(!pi) break;

		pi->w = mIniReadInt(ini, "w", 16);
		pi->h = mIniReadInt(ini, "h", 16);
		pi->col = mIniReadHex(ini, "col", 0x320000ff);
		pi->flags = mIniReadInt(ini, "flags", GRIDDATA_F_ON);
		pi->zoom_min = mIniReadInt(ini, "zoom_min", 0);
		pi->zoom_max = mIniReadInt(ini, "zoom_max", 0);

		mIniReadNums(ini, "area", pi->area, 4, 2, FALSE);
	}

	mIniReadEnd(ini);

	return TRUE;
}

/** グリッドデータをファイルに保存
 *
 * @param filename NULL で設定データ */

mBool ConfigData_saveGrid(mList *list,const char *filename)
{
	FILE *fp;
	GridListData *pi;
	int no;

	if(!filename)
		fp = mIniWriteOpenFile2(MAPP->pathConfig, _CONFIG_GRID_FILE);
	else
		fp = mIniWriteOpenFile(filename);

	if(!fp) return FALSE;

	//

	mIniWriteGroup(fp, "grid");
	mIniWriteInt(fp, "ver", 1);
	mIniWriteInt(fp, "num", list->num);

	for(pi = (GridListData *)list->top, no = 0; pi; pi = (GridListData *)pi->i.next, no++)
	{
		mIniWriteGroup_int(fp, no);

		mIniWriteInt(fp, "w", pi->w);
		mIniWriteInt(fp, "h", pi->h);
		mIniWriteHex(fp, "col", pi->col);
		mIniWriteInt(fp, "flags", pi->flags);
		mIniWriteInt(fp, "zoom_min", pi->zoom_min);
		mIniWriteInt(fp, "zoom_max", pi->zoom_max);
		mIniWriteNums(fp, "area", pi->area, 4, 2, FALSE);
	}

	fclose(fp);

	return TRUE;
}

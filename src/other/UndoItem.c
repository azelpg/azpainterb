/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * UndoItem
 *
 * 各タイプごとの処理
 *****************************************/
/*
 * 実行処理時に失敗しても更新は行われるので、更新情報は常にセットすること。
 */

#include "mDef.h"
#include "mRectBox.h"
#include "mUndo.h"

#include "defDraw.h"

#include "LayerList.h"
#include "LayerItem.h"
#include "TileImage.h"

#include "Undo.h"
#include "UndoItem.h"

#include "draw_calc.h"
#include "draw_layer.h"



//==============================
// 共通処理
//==============================


/** レイヤ削除処理 */

mBool UndoItem_common_runLayerDelete(LayerItem *item,UndoUpdateInfo *info)
{
	if(!item) return FALSE;

	//更新範囲
	/* 削除前に行う。
	 * 空レイヤの場合、更新範囲は空となるが、レイヤ一覧は常に更新しなければらなない。 */

	info->type = UNDO_UPDATE_RECT_AND_LAYERLIST;

	LayerItem_getVisibleImageRect(item, &info->rc);

	//削除

	drawLayer_deleteForUndo(APP_DRAW, item);

	return TRUE;
}


//==============================
// 単体レイヤ
//==============================


/** 単体レイヤ書き込み */

mBool UndoItem_writeSingleLayer(UndoItem *p,LayerItem *li)
{
	if(!UndoItem_alloc(p, -1)) return FALSE;

	if(!UndoItem_openWrite(p)) return FALSE;

	UndoItem_writeLayerInfo(p, li);
	UndoItem_writeTileImage(p, li->img);

	UndoItem_closeWrite(p);

	return TRUE;
}

/** 単体レイヤ復元 */

mBool UndoItem_restoreSingleLayer(UndoItem *p,int parent,int pos,mRect *rc)
{
	mBool ret;

	if(!UndoItem_openRead(p)) return FALSE;

	ret = UndoItem_readLayer(p, parent, pos, rc);

	UndoItem_closeRead(p);

	return ret;
}

/** 単体レイヤのイメージを書き込み */

mBool UndoItem_writeSingleLayerImage(UndoItem *p,LayerItem *item)
{
	if(!UndoItem_alloc(p, -1)
		|| !UndoItem_openWrite(p))
		return FALSE;

	UndoItem_writeTileImage(p, item->img);

	UndoItem_closeWrite(p);

	return TRUE;
}

/** 単体レイヤのイメージを復元 */

mBool UndoItem_restoreSingleLayerImage(UndoItem *p,LayerItem *item)
{
	mBool ret;

	if(!UndoItem_openRead(p)) return FALSE;

	ret = UndoItem_readLayerImage(p, item);

	UndoItem_closeRead(p);

	return ret;
}


//==============================
// ２つのレイヤ
//==============================


/** item とその下のレイヤイメージを書き込み */

mBool UndoItem_writeTwoLayersImage(UndoItem *p,LayerItem *item)
{
	if(!item
		|| !UndoItem_alloc(p, -1)
		|| !UndoItem_openWrite(p))
		return FALSE;

	UndoItem_writeTileImage(p, item->img);
	UndoItem_writeTileImage(p, LAYERITEM(item->i.next)->img);

	UndoItem_closeWrite(p);

	return TRUE;
}

/** item とその下のレイヤイメージを復元 */

mBool UndoItem_restoreTwoLayersImage(UndoItem *p,LayerItem *item)
{
	mBool ret = FALSE;

	if(!UndoItem_openRead(p)) return FALSE;

	if(UndoItem_readLayerImage(p, item))
		ret = UndoItem_readLayerImage(p, LayerItem_getNext(item));

	UndoItem_closeRead(p);

	return ret;
}

/** 下レイヤへ結合時の書き込み */

mBool UndoItem_writeLayerCombine(UndoItem *p,int settype)
{
	LayerItem *item;

	item = UndoItem_getLayerFromNo_forParent(p->val[1], p->val[2]);
	if(!item) return FALSE;

	if(!UndoItem_alloc(p, -1)
		|| !UndoItem_openWrite(p))
		return FALSE;

	UndoItem_writeLayerInfo(p, item);
	UndoItem_writeTileImage(p, item->img);

	if(settype == MUNDO_TYPE_UNDO)
	{
		item = LayerItem_getNext(item);
	
		UndoItem_writeLayerInfo(p, item);
		UndoItem_writeTileImage(p, item->img);
	}

	UndoItem_closeWrite(p);

	return TRUE;
}


//==============================
// 複数レイヤ
//==============================


/** 単体レイヤ書き込み (フォルダの場合は下位全て) */

mBool UndoItem_writeSingleLayer_forFolder(UndoItem *p,LayerItem *item)
{
	LayerItem *li;
	int16_t lno[2];
	int no1,no2;

	if(!item) return FALSE;

	if(!UndoItem_alloc(p, -1) ||
		!UndoItem_openWrite(p))
		return FALSE;

	for(li = item; li; li = LayerItem_getNextRoot(li, item))
	{
		//レイヤ番号

		LayerList_getItemPos_forParent(APP_DRAW->layerlist, li, &no1, &no2);

		lno[0] = no1;
		lno[1] = no2;

		UndoItem_write(p, lno, 4);

		//

		UndoItem_writeLayerInfo(p, li);
		if(li->img) UndoItem_writeTileImage(p, li->img);
	}

	//終了

	lno[0] = lno[1] = -1;
	UndoItem_write(p, lno, 4);

	UndoItem_closeWrite(p);

	return TRUE;
}

/** すべてのレイヤを書き込み */

mBool UndoItem_writeAllLayers(UndoItem *p)
{
	LayerItem *li;
	int16_t lno[2];
	int no1,no2;

	if(!UndoItem_alloc(p, -1) ||
		!UndoItem_openWrite(p))
		return FALSE;

	for(li = LayerList_getItem_top(APP_DRAW->layerlist); li; li = LayerItem_getNext(li))
	{
		//レイヤ番号

		LayerList_getItemPos_forParent(APP_DRAW->layerlist, li, &no1, &no2);

		lno[0] = no1;
		lno[1] = no2;

		UndoItem_write(p, lno, 4);

		//

		UndoItem_writeLayerInfo(p, li);
		if(li->img) UndoItem_writeTileImage(p, li->img);
	}

	//終了

	lno[0] = lno[1] = -1;
	UndoItem_write(p, lno, 4);

	UndoItem_closeWrite(p);

	return TRUE;
}

/** すべてのレイヤのイメージを書き込み (フォルダは除く) */

mBool UndoItem_writeAllLayersImage(UndoItem *p)
{
	LayerItem *li;
	int16_t lno;

	if(!UndoItem_alloc(p, -1) ||
		!UndoItem_openWrite(p))
		return FALSE;

	for(li = LayerList_getItem_top(APP_DRAW->layerlist); li; li = LayerItem_getNext(li))
	{
		if(!li->img) continue;
		
		//レイヤ番号

		lno = LayerList_getItemPos(APP_DRAW->layerlist, li);

		UndoItem_write(p, &lno, 2);

		//イメージ

		UndoItem_writeTileImage(p, li->img);
	}

	//終了

	lno = -1;
	UndoItem_write(p, &lno, 2);

	UndoItem_closeWrite(p);

	return TRUE;
}

/** 複数レイヤ復元 */

mBool UndoItem_restoreLayers(UndoItem *p,mRect *rcupdate)
{
	int16_t lno[2];
	mBool ret = FALSE;

	mRectEmpty(rcupdate);

	if(!UndoItem_openRead(p)) return FALSE;

	while(1)
	{
		//レイヤ番号
		
		if(!UndoItem_read(p, lno, 4)) break;

		if(lno[1] == -1)
		{
			ret = TRUE;
			break;
		}

		//レイヤ復元

		if(!UndoItem_readLayer(p, lno[0], lno[1], rcupdate))
			break;
	}

	UndoItem_closeRead(p);

	return ret;
}

/** 全レイヤのイメージを復元 */

mBool UndoItem_restoreAllLayersImage(UndoItem *p)
{
	int16_t lno;
	mBool ret = FALSE;

	if(!UndoItem_openRead(p)) return FALSE;

	while(1)
	{
		//レイヤ番号
		
		if(!UndoItem_read(p, &lno, 2)) break;

		if(lno == -1)
		{
			ret = TRUE;
			break;
		}

		//イメージ復元

		if(!UndoItem_readLayerImage(p, UndoItem_getLayerFromNo(lno)))
			break;
	}

	UndoItem_closeRead(p);

	return ret;
}


//==============================
// レイヤ全体の左右/上下反転
//==============================


/** レイヤ全体の左右/上下反転
 *
 * [!] レイヤのロックが実行時と同じ状態でなければならない。 */

mBool UndoItem_runLayerReverseHorzVert(UndoItem *p,UndoUpdateInfo *info)
{
	LayerItem *li;

	li = UndoItem_getLayerFromNo(p->val[0]);
	if(!li) return FALSE;

	if(LayerItem_editFullImage(li, p->val[1], &info->rc))
		info->type = UNDO_UPDATE_RECT;

	return TRUE;
}


//==========================
// レイヤオフセット移動
//==========================
/*
 * val[0] : 相対移動値X
 * val[1] : 相対移動値Y
 * val[2] : レイヤ数
 * val[3] : レイヤ数が1の場合、対象のレイヤ番号
 */


/** リンクされたレイヤの各番号を書き込み */

mBool UndoItem_writeLinkLayersNo(UndoItem *p,LayerItem *top,int num)
{
	LayerItem *pi;
	uint16_t no;

	if(!UndoItem_alloc(p, num * 2)) return FALSE;

	if(!UndoItem_openWrite(p)) return FALSE;

	//レイヤ番号

	for(pi = top; pi; pi = pi->link)
	{
		no = LayerList_getItemPos(APP_DRAW->layerlist, pi);

		UndoItem_write(p, &no, 2);
	}

	UndoItem_closeWrite(p);

	return TRUE;
}

/** レイヤオフセット位置移動 */

mBool UndoItem_runLayerMoveOffset(UndoItem *p,UndoUpdateInfo *info)
{
	LayerItem *li;
	mRect rc,rc1,rc2;
	int i;
	uint16_t no;

	if(p->val[2] == 1)
	{
		//移動したレイヤは1つ

		li = UndoItem_getLayerFromNo(p->val[3]);
		if(!li) return FALSE;

		TileImage_getHaveImageRect_pixel(li->img, &rc1, NULL);

		TileImage_moveOffset_rel(li->img, p->val[0], p->val[1]);

		drawCalc_unionRect_relmove(&rc, &rc1, p->val[0], p->val[1]);
	}
	else
	{
		//複数レイヤ
		/* カレントがフォルダだった場合。
		 * ロック状態のレイヤやフォルダレイヤは除外されている。 */

		mRectEmpty(&rc);

		if(!UndoItem_openRead(p)) return FALSE;

		for(i = p->val[2]; i > 0; i--)
		{
			UndoItem_read(p, &no, 2);

			li = UndoItem_getLayerFromNo(no);
			if(!li) return FALSE;

			TileImage_getHaveImageRect_pixel(li->img, &rc1, NULL);

			TileImage_moveOffset_rel(li->img, p->val[0], p->val[1]);

			drawCalc_unionRect_relmove(&rc2, &rc1, p->val[0], p->val[1]);
			mRectUnion(&rc, &rc2);
		}

		UndoItem_closeRead(p);
	}

	info->type = UNDO_UPDATE_RECT;
	info->rc = rc;

	return TRUE;
}


//====================================
// レイヤ関連
//====================================


/** レイヤ新規追加 */

mBool UndoItem_runLayerNew(UndoItem *p,UndoUpdateInfo *info,int runtype)
{
	if(runtype == MUNDO_TYPE_UNDO)
	{
		//レイヤ削除

		return UndoItem_common_runLayerDelete(
			UndoItem_getLayerFromNo_forParent(p->val[0], p->val[1]), info);
	}
	else
	{
		//レイヤ復元

		info->type = UNDO_UPDATE_RECT_AND_LAYERLIST;

		return UndoItem_restoreSingleLayer(p, p->val[0], p->val[1], &info->rc);
	}
}

/** レイヤ複製 */

mBool UndoItem_runLayerCopy(UndoItem *p,UndoUpdateInfo *info,int runtype)
{
	LayerItem *src,*item;

	src = UndoItem_getLayerFromNo(p->val[0]);
	if(!src) return FALSE;

	if(runtype == MUNDO_TYPE_UNDO)
	{
		//レイヤ削除

		return UndoItem_common_runLayerDelete(src, info);
	}
	else
	{
		//レイヤ複製

		item = LayerList_dupLayer(APP_DRAW->layerlist, src);
		if(!item) return FALSE;

		info->type = UNDO_UPDATE_RECT_AND_LAYERLIST;

		LayerItem_getVisibleImageRect(item, &info->rc);

		return TRUE;
	}
}

/** レイヤ削除 */

mBool UndoItem_runLayerDelete(UndoItem *p,UndoUpdateInfo *info,int runtype)
{
	if(runtype == MUNDO_TYPE_REDO)
	{
		//レイヤ削除

		return UndoItem_common_runLayerDelete(UndoItem_getLayerFromNo(p->val[0]), info);
	}
	else
	{
		//レイヤ復元

		info->type = UNDO_UPDATE_RECT_AND_LAYERLIST;

		return UndoItem_restoreLayers(p, &info->rc);
	}

	return TRUE;
}

/** レイヤイメージクリア */

mBool UndoItem_runLayerClearImage(UndoItem *p,UndoUpdateInfo *info,int runtype)
{
	LayerItem *item;
	mBool ret;

	item = UndoItem_getLayerFromNo(p->val[0]);
	if(!item) return FALSE;

	info->type = UNDO_UPDATE_RECT_AND_LAYERLIST_ONE;
	info->layer = item;

	if(runtype == MUNDO_TYPE_UNDO)
	{
		//イメージ復元

		ret = UndoItem_restoreSingleLayerImage(p, item);

		if(ret)
			LayerItem_getVisibleImageRect(item, &info->rc);

		return ret;
	}
	else
	{
		//クリア

		LayerItem_getVisibleImageRect(item, &info->rc);

		TileImage_clear(item->img);
	}

	return TRUE;
}

/** 下レイヤに移す/結合 */

mBool UndoItem_runLayerDropOrCombine(UndoItem *p,UndoUpdateInfo *info,int runtype)
{
	LayerItem *item;
	mRect rc;
	mBool ret = FALSE;

	item = UndoItem_getLayerFromNo_forParent(p->val[1], p->val[2]);
	if(!item) return FALSE;

	//

	if(p->val[0])
	{
		//------ 下レイヤに移す (item = 上のレイヤ)

		if(runtype == MUNDO_TYPE_REDO)
			TileImage_getHaveImageRect_pixel(item->img, &rc, NULL);

		if(!UndoItem_restoreTwoLayersImage(p, item))
			return FALSE;

		if(runtype == MUNDO_TYPE_UNDO)
			TileImage_getHaveImageRect_pixel(item->img, &rc, NULL);

		info->type = UNDO_UPDATE_RECT_AND_LAYERLIST_TWO;
		info->layer = item;
		info->rc = rc;

		return TRUE;
	}
	else
	{
		//------- 下レイヤに結合

		mRectEmpty(&rc);

		if(UndoItem_openRead(p))
		{
			if(runtype == MUNDO_TYPE_UNDO)
			{
				//UNDO : 上のレイヤを復元 + 下レイヤのイメージを復元

				if(UndoItem_readLayer(p, p->val[1], p->val[2], &rc))
					ret = UndoItem_readLayerOverwrite(p, item, &rc);
			}
			else
			{
				//REDO : 結合後のイメージを復元 + 上のレイヤを削除
				//item = 上のレイヤ

				TileImage_getHaveImageRect_pixel(item->img, &rc, NULL);

				if(UndoItem_readLayerOverwrite(p, LayerItem_getNext(item), &rc))
				{
					drawLayer_deleteForUndo(APP_DRAW, item);

					ret = TRUE;
				}
			}

			UndoItem_closeRead(p);
		}

		info->type = UNDO_UPDATE_RECT_AND_LAYERLIST;
		info->rc = rc;

		return ret;
	}
}

/** 全レイヤの結合 */

mBool UndoItem_runLayerCombineAll(UndoItem *p,UndoUpdateInfo *info,int runtype)
{
	LayerItem *item;
	mBool ret = FALSE;

	if(runtype == MUNDO_TYPE_UNDO)
	{
		//UNDO : 全レイヤ復元 => 結合後レイヤを削除

		item = LayerList_getItem_top(APP_DRAW->layerlist);

		if(UndoItem_restoreLayers(p, &info->rc))
		{
			drawLayer_deleteForUndo(APP_DRAW, item);
			
			ret = TRUE;
		}
	}
	else
	{
		//REDO : レイヤすべて削除 => 結合後レイヤを復元

		LayerList_clear(APP_DRAW->layerlist);

		ret = UndoItem_restoreSingleLayer(p, -1, 0, &info->rc);

		APP_DRAW->curlayer = LayerList_getItem_top(APP_DRAW->layerlist);
	}

	info->type = UNDO_UPDATE_ALL_AND_LAYERLIST;

	return ret;
}

/** フォルダレイヤ結合 */

mBool UndoItem_runLayerCombineFolder(UndoItem *p,UndoUpdateInfo *info,int runtype)
{
	LayerItem *item;
	mRect rc,rc2;
	mBool ret = FALSE;

	item = UndoItem_getLayerFromNo_forParent(p->val[0], p->val[1]);
	if(!item) return FALSE;

	mRectEmpty(&rc);

	if(runtype == MUNDO_TYPE_UNDO)
	{
		//UNDO : フォルダ復元 => 結合後レイヤ削除

		if(UndoItem_restoreLayers(p, &rc))
		{
			drawLayer_deleteForUndo(APP_DRAW, item);
			ret = TRUE;
		}
	}
	else
	{
		//REDO : 結合後レイヤ復元 => フォルダ削除

		if(UndoItem_restoreSingleLayer(p, p->val[0], p->val[1], &rc))
		{
			//削除フォルダのイメージ範囲
		
			if(LayerItem_getVisibleImageRect(item, &rc2))
				mRectUnion(&rc, &rc2);

			//削除

			drawLayer_deleteForUndo(APP_DRAW, item);

			ret = TRUE;
		}
	}

	info->type = UNDO_UPDATE_RECT_AND_LAYERLIST;
	info->rc = rc;

	return ret;
}

/** レイヤ順番移動 */

mBool UndoItem_runLayerMoveList(UndoItem *p,UndoUpdateInfo *info)
{
	info->type = UNDO_UPDATE_RECT_AND_LAYERLIST;

	return drawLayer_moveForUndo(APP_DRAW, p->val, &info->rc);
}


/** キャンバスイメージ変更(切り取り)/拡大縮小 */

mBool UndoItem_runCanvasResizeOrScale(UndoItem *p,UndoUpdateInfo *info)
{
	mBool ret;

	ret = UndoItem_restoreAllLayersImage(p);

	//dpi

	if(p->type == UNDO_TYPE_SCALECANVAS)
		APP_DRAW->imgdpi = p->val[2];

	//更新

	info->type = UNDO_UPDATE_CANVAS_RESIZE;
	info->rc.x1 = p->val[0];
	info->rc.y1 = p->val[1];

	return ret;
}


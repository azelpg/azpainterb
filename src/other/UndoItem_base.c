/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * UndoItem
 *
 * 基本処理
 *****************************************/

#include <stdio.h>
#include <string.h>

#include "mDef.h"
#include "mStr.h"
#include "mUndo.h"
#include "mUtilFile.h"
#include "mUtilStdio.h"

#include "Undo.h"
#include "UndoItem.h"
#include "UndoMaster.h"

#include "defConfig.h"


//------------------

#define _UNDO g_app_undo

//------------------



//=========================
// sub
//=========================


/** バッファ確保 */

static mBool _alloc_buf(UndoItem *p,int size)
{
	p->buf = (uint8_t *)mMalloc(size, FALSE);
	if(!p->buf) return FALSE;

	p->size = size;
	p->fileno = -1;

	_UNDO->used_bufsize += size;

	return TRUE;
}

/** ファイル確保 */

static mBool _alloc_file(UndoItem *p,int size)
{
	if(mStrIsEmpty(&APP_CONF->strTempDirProc)) return FALSE;

	p->fileno = _UNDO->cur_fileno;

	_UNDO->cur_fileno = (_UNDO->cur_fileno + 1) & 0xffff;

	p->size = (size <= 0)? 0: size;

	return TRUE;
}

/** ファイル名取得 */

static void _get_filename(mStr *str,int no)
{
	char m[16];

	snprintf(m, 16, "undo%04x", no);

	mStrCopy(str, &APP_CONF->strTempDirProc);
	mStrPathAdd(str, m);
}


//=========================
// main
//=========================


/** 解放 */

void UndoItem_free(UndoItem *p)
{
	/* データが必要ない (バッファもファイルも確保しない) 場合があるので、
	 * p->buf の値で判定せずに fileno で判定する。 */

	if(p->fileno == -1)
	{
		//バッファ
		
		if(p->buf)
		{
			mFree(p->buf);

			_UNDO->used_bufsize -= p->size;
		}
	}
	else
	{
		//ファイル削除

		mStr str = MSTR_INIT;

		_get_filename(&str, p->fileno);

		mDeleteFile(str.buf);

		mStrFree(&str);
	}
}

/** 確保
 *
 * @param size 0 以下でファイルへ */

mBool UndoItem_alloc(UndoItem *p,int size)
{
	mBool ret;

	if(size <= 0 || _UNDO->used_bufsize + size > APP_CONF->undo_maxbufsize)
	{
		//ファイル優先

		ret = _alloc_file(p, size);
	}
	else
	{
		//バッファ優先 (固定サイズ)

		ret = _alloc_buf(p, size);

		if(!ret)
			ret = _alloc_file(p, size);
	}

	return ret;
}

/** データをコピー */

mBool UndoItem_copy(UndoItem *dst,UndoItem *src)
{
	uint8_t *buf;
	mBool ret = FALSE;

	if(!UndoItem_alloc(dst, src->size))
		return FALSE;

	//コピー

	if(dst->buf && src->buf)
	{
		memcpy(dst->buf, src->buf, src->size);
		return TRUE;
	}
	else
	{
		//一方または両方がファイルの場合

		buf = (uint8_t *)mMalloc(src->size, FALSE);
		if(!buf) return FALSE;

		if(UndoItem_openRead(src) && UndoItem_openWrite(dst))
		{
			if(UndoItem_read(src, buf, src->size))
			{
				UndoItem_write(dst, buf, src->size);
				ret = TRUE;
			}
		}

		UndoItem_closeRead(src);
		UndoItem_closeWrite(dst);

		mFree(buf);

		return ret;
	}
}


//=========================
// 書き込み
//=========================


/** 書き込み開く */

mBool UndoItem_openWrite(UndoItem *p)
{
	mBool ret = TRUE;

	if(p->fileno == -1)
	{
		//バッファ

		_UNDO->writebuf = p->buf;
		_UNDO->writesize = 0;
	}
	else
	{
		//ファイル

		mStr str = MSTR_INIT;

		_get_filename(&str, p->fileno);

		_UNDO->writefp = mFILEopenUTF8(str.buf, "wb");

		ret = (_UNDO->writefp != NULL);

		mStrFree(&str);
	}

	return ret;
}

/** 書き込み */

void UndoItem_write(UndoItem *p,void *buf,int size)
{
	if(!buf || size <= 0) return;

	if(p->fileno == -1)
	{
		memcpy(_UNDO->writebuf, buf, size);

		_UNDO->writebuf += size;
		_UNDO->writesize += size;
	}
	else
		fwrite(buf, 1, size, _UNDO->writefp);
}

/** 書き込み閉じる */

void UndoItem_closeWrite(UndoItem *p)
{
	if(_UNDO->writefp)
	{
		fclose(_UNDO->writefp);
		_UNDO->writefp = NULL;
	}
}


//=========================
// 読み込み
//=========================


/** 読み込み開く */

mBool UndoItem_openRead(UndoItem *p)
{
	mBool ret = TRUE;

	if(p->fileno == -1)
	{
		//バッファ

		_UNDO->readbuf = p->buf;
	}
	else
	{
		//ファイル

		mStr str = MSTR_INIT;

		_get_filename(&str, p->fileno);

		_UNDO->readfp = mFILEopenUTF8(str.buf, "rb");

		ret = (_UNDO->readfp != NULL);

		mStrFree(&str);
	}

	return ret;
}

/** 読み込み */

mBool UndoItem_read(UndoItem *p,void *buf,int size)
{
	if(p->fileno == -1)
	{
		memcpy(buf, _UNDO->readbuf, size);

		_UNDO->readbuf += size;

		return TRUE;
	}
	else
	{
		return (fread(buf, 1, size, _UNDO->readfp) == size);
	}
}

/** 読み込みシーク */

void UndoItem_readSeek(UndoItem *p,int seek)
{
	if(p->fileno == -1)
		_UNDO->readbuf += seek;
	else
		fseek(_UNDO->readfp, seek, SEEK_CUR);
}

/** 読み込み閉じる */

void UndoItem_closeRead(UndoItem *p)
{
	if(_UNDO->readfp)
	{
		fclose(_UNDO->readfp);
		_UNDO->readfp = NULL;
	}
}


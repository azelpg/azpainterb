/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * ブラシリストデータ
 *****************************************/

#include <stdio.h>
#include <string.h>

#include "mDef.h"
#include "mList.h"
#include "mUtilStdio.h"
#include "mStr.h"
#include "mGui.h"

#include "defMacros.h"
#include "defPixelMode.h"
#include "defTool.h"

#include "BrushList.h"
#include "BrushItem.h"
#include "BrushDrawParam.h"


//-------------------

#define BRUSHITEM_MAX_NUM  400

//-------------------

typedef struct
{
	mList list[BRUSHLIST_NUM];

	BrushItem item_edit[BRUSHLIST_NUM];	//編集用アイテム
	BrushItem *item_sel[BRUSHLIST_NUM];	//選択アイテム

	BrushDrawParam drawparam;	//描画用情報
}BrushList;

static BrushList *g_dat = NULL;

//-------------------


//*******************************************
// BrushItem : アイテム
//*******************************************


/** 破棄ハンドラ */

static void _item_destroy_handle(mListItem *item)
{
	mFree(BRUSHITEM(item)->name);
}

/** デフォルト値セット */

static void _item_set_default(BrushItem *p,int no)
{
	mBool pen,dodge_burn;

	pen = (no == BRUSHLIST_PEN);
	dodge_burn = (no == BRUSHLIST_DODGE || no == BRUSHLIST_BURN);

	p->radius = (pen)? 40: 300;
	p->radius_min = 0;
	p->radius_max = (pen)? 500: 4000;

	p->min_size = (dodge_burn || no == BRUSHLIST_BLUR)? 300: 0;
	p->min_opacity = (dodge_burn)? 200: 1000;
	p->pressure_gamma = 100;
	p->interval = (dodge_burn)? 30: 20;

	if(no == BRUSHLIST_BLUR)
		p->opacity = 64;
	else
		p->opacity = (dodge_burn)? 15: 255;

	p->pixmode = (pen || no == BRUSHLIST_ERASE)? PIXELMODE_BLEND_STROKE: PIXELMODE_BLEND_PIXEL;
	p->smoothing_type = 0;
	p->smoothing_str = 1;
	p->water_type = 1;
	p->flags = BRUSHITEM_F_ANTIALIAS;

	if(pen || no == BRUSHLIST_WATER)
		p->shape = 1;
	else
		p->shape = (dodge_burn)? 2: 0;
}

/** アイテムの値をコピー */

static void _item_copy(BrushItem *dst,BrushItem *src)
{
	BrushItem tmp;

	//文字列解放

	mFree(dst->name);

	//tmp にコピー&修正して、dst へセット

	tmp = *src;

	tmp.i = dst->i;  //mListItem は維持

	*dst = tmp;

	//文字列コピー

	dst->name = NULL;

	mStrdup_ptr(&dst->name, src->name);
}

/** アイテムの名前以外をコピー */

static void _item_copy_value(BrushItem *dst,BrushItem *src)
{
	memcpy(&dst->radius, &src->radius, BRUSHITEM_VAL16_NUM * 2);
	memcpy(&dst->opacity, &src->opacity, BRUSHITEM_VAL8_NUM);
}

/** 半径値を最小値〜最大値の範囲内に収める */

int BrushItem_adjustRadius(BrushItem *p,int val)
{
	int min,max;

	min = p->radius_min;
	max = p->radius_max;

	if(min == 0) min = BRUSHITEM_RADIUS_MIN;
	if(max == 0) max = BRUSHITEM_RADIUS_MAX;

	if(val < min)
		val = min;
	else if(val > max)
		val = max;

	return val;
}


//*******************************************
// BrushList : ブラシリスト
//*******************************************


/** 解放 */

void BrushList_free()
{
	int i;

	if(g_dat)
	{
		for(i = 0; i < BRUSHLIST_NUM; i++)
		{
			mListDeleteAll(&g_dat->list[i]);

			_item_destroy_handle(M_LISTITEM(&g_dat->item_edit[i]));
		}

		mFree(g_dat);
	}
}

/** 確保 & 初期化 */

mBool BrushList_new()
{
	int i;

	g_dat = (BrushList *)mMalloc(sizeof(BrushList), TRUE);
	if(!g_dat) return FALSE;

	//編集用ブラシのデフォルトをセット

	for(i = 0; i < BRUSHLIST_NUM; i++)
		_item_set_default(g_dat->item_edit + i, i);

	return TRUE;
}

/** (リスト編集用) 新規ブラシ作成/複製
 *
 * @param src  NULL で新規。そうでなければ複製元。 */

BrushItem *BrushList_edit_newItem(mList *list,int listno,BrushItem *src)
{
	BrushItem *item;
	mListItem *ins;

	if(list->num >= BRUSHITEM_MAX_NUM)
		return NULL;

	if(src)
		ins = src->i.next;

	item = (BrushItem *)mListAppendNew(list, sizeof(BrushItem), _item_destroy_handle);
	if(!item) return NULL;
	
	if(!src)
	{
		//新規時 : デフォルトセット
		
		mStrdup_ptr(&item->name, "new");

		_item_set_default(item, listno);
	}
	else
	{
		//複製時
		
		_item_copy(item, src);

		//src の次へ移動

		mListMove(list, M_LISTITEM(item), ins);
	}

	return item;
}

/** リストを複製 */

mBool BrushList_dup(int listno,mList *dst)
{
	BrushItem *ps,*pd;
	mListItem tmp;

	for(ps = BrushList_getTopItem(listno); ps; ps = BRUSHITEM(ps->i.next))
	{
		pd = (BrushItem *)mListAppendNew(dst, sizeof(BrushItem), _item_destroy_handle);
		if(!pd) return FALSE;

		tmp = pd->i;
		*pd = *ps;
		pd->i = tmp;

		//名前

		pd->name = NULL;

		mStrdup_ptr(&pd->name, ps->name);
	}

	return TRUE;
}

/** リストデータを置き換え & 選択セット */

void BrushList_replace(int listno,mList *src,BrushItem *selitem)
{
	mListDeleteAll(g_dat->list + listno);

	g_dat->list[listno] = *src;
	g_dat->item_sel[listno] = selitem;

	/* リスト編集ダイアログ OK 後に実行され、
	 * その後 BrushList_setSelectItem() が実行されるので、
	 * 編集用データはここでセットする必要はない。 */
}


//===========================
// 取得
//===========================


/** ツール番号からリスト番号を取得 */

int BrushList_getListNo_fromTool(int tool)
{
	switch(tool)
	{
		case TOOL_PEN: return BRUSHLIST_PEN;
		case TOOL_BRUSH: return BRUSHLIST_BRUSH;
		case TOOL_WATER: return BRUSHLIST_WATER;
		case TOOL_BRUSH_ERASE: return BRUSHLIST_ERASE;
		case TOOL_DODGE: return BRUSHLIST_DODGE;
		case TOOL_BURN: return BRUSHLIST_BURN;
		case TOOL_BLUR: return BRUSHLIST_BLUR;
		default: return -1;
	}
}

/** 先頭アイテム取得 */

BrushItem *BrushList_getTopItem(int listno)
{
	return (BrushItem *)g_dat->list[listno].top;
}

/** 編集用アイテム取得 */

BrushItem *BrushList_getEditItem(int listno)
{
	return g_dat->item_edit + listno;
}

/** 選択アイテム取得 */

BrushItem *BrushList_getSelectItem(int listno)
{
	return g_dat->item_sel[listno];
}


//===========================
// セット
//===========================


/** 選択アイテム変更
 *
 * @param item  NULL で選択なし */

void BrushList_setSelectItem(int listno,BrushItem *item)
{
	g_dat->item_sel[listno] = item;

	if(item)
	{
		//編集用アイテムに値をコピー

		_item_copy_value(g_dat->item_edit + listno, item);
	}
}

/** 編集用アイテムの値を選択アイテムに保存 */

void BrushList_saveItemValue(int listno)
{
	BrushItem *dst;

	dst = g_dat->item_sel[listno];

	if(dst)
		_item_copy_value(dst, g_dat->item_edit + listno);
}

/** 描画時の情報取得 */

BrushDrawParam *BrushList_getDrawParam(int toolno,BrushItem **ppitem)
{
	BrushDrawParam *dst = &g_dat->drawparam;
	BrushItem *pi;
	uint16_t water_val[5][3] = {
		{800,1000,500}, {500,900,400}, {300,600,200},
		{100,1000,50}, {100,100,200}
	};
	int i;

	//編集用アイテムから

	pi = g_dat->item_edit + BrushList_getListNo_fromTool(toolno);

	//

	dst->radius = pi->radius * 0.1;
	dst->opacity = pi->opacity;
	dst->min_size = pi->min_size * 0.001;
	dst->min_opacity = pi->min_opacity * 0.001;

	dst->smoothing_type = pi->smoothing_type;
	dst->smoothing_str = pi->smoothing_str;
	dst->shape = pi->shape;

	//間隔

	if(toolno == TOOL_BLUR)
		//ぼかしは濃度を強さとして間隔を調整
		dst->interval = (255 - pi->opacity) * 0.005 + 0.04;
	else
		dst->interval = pi->interval * 0.01;

	//フラグ

	dst->flags = 0;

	if(pi->flags & BRUSHITEM_F_ANTIALIAS)
		dst->flags |= BRUSHDP_F_ANTIALIAS;

	if(pi->flags & BRUSHITEM_F_CURVE)
		dst->flags |= BRUSHDP_F_CURVE;

	//筆圧ガンマ (100 = 変化なしの場合は、計算を行わないように)

	if(pi->pressure_gamma != 100)
	{
		dst->flags |= BRUSHDP_F_PRESSURE_GAMMA;
		dst->pressure_gamma = pi->pressure_gamma * 0.01;
	}

	//水彩

	if(toolno == TOOL_WATER)
	{
		dst->flags |= BRUSHDP_F_WATER;

		for(i = 0; i < 3; i++)
			dst->water[i] = water_val[pi->water_type][i] * 0.001;
	}

	//

	*ppitem = pi;

	return dst;
}


//===========================
// 値の更新
//===========================


/** 半径 */

void BrushList_update_radius(int listno,int val)
{
	g_dat->item_edit[listno].radius = val;
}

/** 濃度 */

void BrushList_update_opacity(int listno,int val)
{
	g_dat->item_edit[listno].opacity = val;
}

/** 補正タイプ */

void BrushList_update_smoothing_type(int listno,int val)
{
	g_dat->item_edit[listno].smoothing_type = val;
}

/** 補正強さ */

void BrushList_update_smoothing_str(int listno,int val)
{
	g_dat->item_edit[listno].smoothing_str = val;
}

/** 水彩タイプ */

void BrushList_update_water_type(int listno,int val)
{
	g_dat->item_edit[listno].water_type = val;
}

/** 間隔 */

void BrushList_update_interval(int listno,int val)
{
	g_dat->item_edit[listno].interval = val;
}



//===========================
// 設定ファイル
//===========================


/** ファイル開く */

static FILE *_open_configfile(const char *mode)
{
	mStr str = MSTR_INIT;
	FILE *fp;

	mAppGetConfigPath(&str, CONFIG_FILENAME_BRUSHLIST);

	fp = mFILEopenUTF8(str.buf, mode);

	mStrFree(&str);

	return fp;
}

/** ブラシデータ読み込み */

static mBool _read_brushitem(FILE *fp,BrushItem *pi,mBool read_name)
{
	uint16_t len,*p16;
	int i;

	//名前

	if(read_name)
	{
		if(!mFILEread16BE(fp, &len)) return FALSE;

		if(len)
		{
			pi->name = (char *)mMalloc(len + 1, TRUE);
			if(pi->name)
				fread(pi->name, 1, len, fp);
		}
	}

	//16bit 値

	p16 = &pi->radius;

	for(i = 0; i < BRUSHITEM_VAL16_NUM; i++, p16++)
	{
		if(!mFILEread16BE(fp, p16)) return FALSE;
	}

	//8bit 値

	if(fread(&pi->opacity, 1, BRUSHITEM_VAL8_NUM, fp) != BRUSHITEM_VAL8_NUM)
		return FALSE;

	return TRUE;
}

/** 読み込み */

void BrushList_loadConfig()
{
	FILE *fp;
	uint8_t ver,toolid;
	uint16_t num,sel;
	BrushItem *pi;

	fp = _open_configfile("rb");
	if(!fp) return;

	//ヘッダ

	if(!mFILEreadCompareStr(fp, "AZPBBRUSH")
		|| !mFILEreadByte(fp, &ver)
		|| ver != 0)
		goto END;

	//

	while(mFILEreadByte(fp, &toolid) && toolid != 255 && toolid < BRUSHLIST_NUM)
	{
		//個数

		if(!mFILEread16BE(fp, &num)) break;

		//選択番号

		if(!mFILEread16BE(fp, &sel)) break;

		//編集用データ

		if(!_read_brushitem(fp, g_dat->item_edit + toolid, FALSE))
			break;

		//リストデータ

		for(; num; num--)
		{
			//アイテム作成

			pi = (BrushItem *)mListAppendNew(g_dat->list + toolid,
				sizeof(BrushItem), _item_destroy_handle);
			if(!pi) break;

			//読み込み

			if(!_read_brushitem(fp, pi, TRUE))
				break;
		}

		//選択アイテム

		g_dat->item_sel[toolid] = (BrushItem *)mListGetItemByIndex(g_dat->list + toolid, sel);
	}
	
END:
	fclose(fp);
}

/** ブラシデータ書き込み */

static void _write_brushitem(FILE *fp,BrushItem *pi,mBool write_name)
{
	int i,len;
	uint16_t *p16;

	//名前

	if(write_name)
	{
		len = (pi->name)? strlen(pi->name): 0;

		mFILEwrite16BE(fp, len);

		if(pi->name) fwrite(pi->name, 1, len, fp);
	}

	//16bit 値

	p16 = &pi->radius;

	for(i = 0; i < BRUSHITEM_VAL16_NUM; i++)
		mFILEwrite16BE(fp, *(p16++));

	//8bit 値

	fwrite(&pi->opacity, 1, BRUSHITEM_VAL8_NUM, fp);
}

/** 書き込み */

void BrushList_saveConfig()
{
	FILE *fp;
	int i,n;
	BrushItem *pi;

	fp = _open_configfile("wb");
	if(!fp) return;

	//ヘッダ

	fputs("AZPBBRUSH", fp);
	mFILEwriteByte(fp, 0);

	//

	for(i = 0; i < BRUSHLIST_NUM; i++)
	{
		//ツールID

		mFILEwriteByte(fp, i);

		//個数 (編集用データは含まない)

		mFILEwrite16BE(fp, g_dat->list[i].num);

		//選択番号

		n = mListGetItemIndex(g_dat->list + i, M_LISTITEM(g_dat->item_sel[i]));
		if(n == -1) n = 0;

		mFILEwrite16BE(fp, n); 

		//編集用データ (名前は含まない)

		_write_brushitem(fp, g_dat->item_edit + i, FALSE);

		//リストデータ

		for(pi = BRUSHITEM(g_dat->list[i].top); pi; pi = BRUSHITEM(pi->i.next))
			_write_brushitem(fp, pi, TRUE);
	}

	mFILEwriteByte(fp, 255);

	fclose(fp);
}


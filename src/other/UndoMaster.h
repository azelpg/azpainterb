/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct
{
	mUndo undo;

	UndoUpdateInfo update;	//更新情報
	uint8_t change;			//データが変更されたかのフラグに使う

	int used_bufsize,		//現在使われているデータバッファサイズ
		cur_fileno,			//ファイル番号の現在値
		writesize;
	uint8_t *tilebuf_src,	//タイル圧縮用バッファ
		*tilebuf_enc,
		*writebuf,
		*readbuf;
	FILE *writefp,
		*readfp;
}UndoMaster;

extern UndoMaster *g_app_undo;

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * UndoItem
 *
 * sub
 *****************************************/

#include <stdio.h>
#include <string.h>

#include "mDef.h"
#include "mRectBox.h"
#include "mUndo.h"

#include "defDraw.h"

#include "LayerList.h"
#include "LayerItem.h"
#include "defTileImage.h"
#include "TileImage.h"

#include "Undo.h"
#include "UndoMaster.h"
#include "UndoItem.h"

#include "draw_layer.h"



//==================================
// レイヤ番号取得/セット
//==================================


/** レイヤ番号からレイヤ取得 */

LayerItem *UndoItem_getLayerFromNo(int no)
{
	return LayerList_getItem_fromPos(APP_DRAW->layerlist, no);
}

/** 親と相対位置のレイヤ番号からレイヤ取得 */

LayerItem *UndoItem_getLayerFromNo_forParent(int parent,int pos)
{
	LayerItem *item[2];

	LayerList_getItems_fromPos(APP_DRAW->layerlist, item, parent, pos);

	return item[1];
}

/** val にカレントレイヤの番号をセット */

void UndoItem_setval_curlayer(UndoItem *p,int valno)
{
	p->val[valno] = LayerList_getItemPos(APP_DRAW->layerlist, APP_DRAW->curlayer);
}

/** val に指定レイヤの番号をセット */

void UndoItem_setval_layerno(UndoItem *p,int valno,LayerItem *item)
{
	p->val[valno] = LayerList_getItemPos(APP_DRAW->layerlist, item);
}

/** val に指定レイヤの親番号とレイヤ番号をセット */

void UndoItem_setval_layerno_forParent(UndoItem *p,int valno,LayerItem *item)
{
	LayerList_getItemPos_forParent(APP_DRAW->layerlist, item,
		p->val + valno, p->val + valno + 1);
}


//==================================
// レイヤ書き込み
//==================================


/** レイヤ情報書き込み */

void UndoItem_writeLayerInfo(UndoItem *p,LayerItem *li)
{
	UndoLayerInfo info;
	uint16_t len;

	//レイヤ名

	if(li->name)
		len = strlen(li->name);
	else
		len = 0;

	UndoItem_write(p, &len, 2);
	UndoItem_write(p, li->name, len);

	//情報

	info.opacity = li->opacity;
	info.blendmode = li->blendmode;
	info.alphamask = li->alphamask;
	info.flags = li->flags;

	if(!li->img) info.flags |= UNDOLAYERINFO_F_FOLDER;

	UndoItem_write(p, &info, sizeof(UndoLayerInfo));
}

/** タイルイメージ書き込み */
/* [TileImageInfo]
 * [タイルイメージ]
 *   2byte: タイルX位置 (0xffff で終了)
 *   2byte: タイルY位置
 *   2byte: サイズ
 *   2byte: 圧縮サイズ (0xffff で無圧縮)
 */

void UndoItem_writeTileImage(UndoItem *p,TileImage *img)
{
	TileImageInfo info;
	uint8_t **pptile,*srcbuf,*encbuf;
	int ix,iy,tw,th,srcsize,encsize;
	uint16_t wd;

	if(!img) return;

	//タイルイメージ情報

	TileImage_getInfo(img, &info);

	UndoItem_write(p, &info, sizeof(TileImageInfo));

	//各タイル

	srcbuf = g_app_undo->tilebuf_src;
	encbuf = g_app_undo->tilebuf_enc;

	pptile = img->ppbuf;
	tw = img->tilew;
	th = img->tileh;

	for(iy = 0; iy < th; iy++)
	{
		for(ix = 0; ix < tw; ix++, pptile++)
		{
			if(*pptile)
			{
				srcsize = TileImage_getTile_forUndo(srcbuf, *pptile);

				encsize = CompressUndoTile(srcsize, 64 * 64);

				wd = ix;
				UndoItem_write(p, &wd, 2);

				wd = iy;
				UndoItem_write(p, &wd, 2);

				wd = srcsize;
				UndoItem_write(p, &wd, 2);

				if(encsize == -1)
				{
					//無圧縮
					wd = 0xffff;
					UndoItem_write(p, &wd, 2);
					UndoItem_write(p, srcbuf, srcsize);
				}
				else
				{
					//圧縮
					wd = encsize;
					UndoItem_write(p, &wd, 2);
					UndoItem_write(p, encbuf, encsize);
				}
			}
		}
	}

	//終了
	wd = 0xffff;
	UndoItem_write(p, &wd, 2);
}


//==================================
// レイヤ復元
//==================================


/** レイヤ情報を読み込み
 *
 * @param ppname 空文字列の場合はセットされないので、NULL をセットしておく。 */

static mBool _read_layerinfo(UndoItem *p,UndoLayerInfo *info,char **ppname)
{
	char *name;
	uint16_t len;

	//名前

	if(!UndoItem_read(p, &len, 2)) return FALSE;

	if(len)
	{
		name = (char *)mMalloc(len + 1, FALSE);
		if(!name) return FALSE;

		if(!UndoItem_read(p, name, len)) return FALSE;

		name[len] = 0;

		*ppname = name;
	}

	//情報

	return UndoItem_read(p, info, sizeof(UndoLayerInfo));
}

/** レイヤ情報をセット */

static void _set_layerinfo(LayerItem *item,UndoLayerInfo *info,char *name)
{
	mFree(item->name);
	item->name = name;
	
	item->blendmode = info->blendmode;
	item->opacity = info->opacity;
	item->alphamask = info->alphamask;
	item->flags = info->flags & (~UNDOLAYERINFO_F_FOLDER);
}

/** レイヤを一つ復元
 *
 * @param rcupdate  更新範囲が追加される */

mBool UndoItem_readLayer(UndoItem *p,int parent_pos,int rel_pos,mRect *rcupdate)
{
	UndoLayerInfo info;
	LayerItem *item;
	mRect rc;
	char *name = NULL;

	//レイヤ情報

	if(!_read_layerinfo(p, &info, &name)) goto ERR;

	//レイヤ作成

	item = LayerList_addLayer_pos(APP_DRAW->layerlist, parent_pos, rel_pos);
	if(!item) goto ERR;

	//情報をセット

	_set_layerinfo(item, &info, name);

	//イメージ

	if(!(info.flags & UNDOLAYERINFO_F_FOLDER))
	{
		//読み込み & 作成
	
		if(!UndoItem_readLayerImage(p, item)) return FALSE;

		//更新範囲
		/* 複数レイヤの復元時は、
		 * レイヤが非表示状態でも範囲として必要な場合があるので (結合時など)
		 * LayerItem_getVisibleImageRect() で取得しない。 */

		if(TileImage_getHaveImageRect_pixel(item->img, &rc, NULL))
			mRectUnion(rcupdate, &rc);
	}

	return TRUE;

	//レイヤ作成前にエラー時は名前を解放
ERR:
	mFree(name);
	return FALSE;
}

/** 既存のレイヤに情報とイメージを上書き復元 */

mBool UndoItem_readLayerOverwrite(UndoItem *p,LayerItem *item,mRect *rcupdate)
{
	UndoLayerInfo info;
	char *name = NULL;
	mRect rc;

	//レイヤ情報

	if(!_read_layerinfo(p, &info, &name))
	{
		mFree(name);
		return FALSE;
	}

	_set_layerinfo(item, &info, name);

	//イメージ

	if(!UndoItem_readLayerImage(p, item))
		return FALSE;

	//範囲追加

	if(TileImage_getHaveImageRect_pixel(item->img, &rc, NULL))
		mRectUnion(rcupdate, &rc);

	return TRUE;
}

/** レイヤイメージの復元 */

mBool UndoItem_readLayerImage(UndoItem *p,LayerItem *item)
{
	TileImageInfo info;
	TileImage *img;
	uint8_t *srcbuf,*encbuf,*tile;
	uint16_t tx,ty,srcsize,encsize;

	if(!item) return FALSE;

	//イメージ情報

	if(!UndoItem_read(p, &info, sizeof(TileImageInfo)))
		return FALSE;

	//既存イメージは削除

	TileImage_free(item->img);
	item->img = NULL;

	//イメージ作成

	img = TileImage_newFromInfo(TILEIMAGE_COLTYPE_RGBA, &info);
	if(!img) return FALSE;

	item->img = img;

	//------ タイル読み込み

	srcbuf = g_app_undo->tilebuf_src;
	encbuf = g_app_undo->tilebuf_enc;

	while(1)
	{
		//情報
	
		if(!UndoItem_read(p, &tx, 2)) return FALSE;

		if(tx == 0xffff) break;

		if(!UndoItem_read(p, &ty, 2)
			|| !UndoItem_read(p, &srcsize, 2)
			|| !UndoItem_read(p, &encsize, 2))
			return FALSE;

		//タイル作成

		tile = TileImage_getTileAlloc_atpos(img, tx, ty);
		if(!tile) return FALSE;

		//イメージ

		if(encsize == 0xffff)
		{
			//無圧縮
			
			if(!UndoItem_read(p, srcbuf, srcsize))
				return FALSE;
		}
		else
		{
			//圧縮

			if(!UndoItem_read(p, encbuf, encsize))
				return FALSE;

			if(!UncompressUndoTile(srcsize, encsize, 64 * 64))
				return FALSE;
		}

		TileImage_setTile_forUndo(tile, srcbuf);
	}

	return TRUE;
}


//==================================
// タイル圧縮
//==================================
/*
 * 透明部分を圧縮するため、A=0 の部分は RGB データを省いている。
 *
 * [圧縮用タイルデータ]
 * 
 *  64 * 64 : Alpha
 *  3 * 不透明の点の数 : R-G-B 順で各点が並ぶ
 *
 * A 部分は 1byte 単位、RGB 部分は 3byte 単位でランレングス圧縮。
 */


/** 展開 */

mBool UncompressUndoTile(int outsize,int insize,int alphasize)
{
	uint8_t *ps,*pd,r,g,b;
	int mode,len,size,bytes;

	if(outsize > 64 * 64 * 4 || insize >= outsize) return FALSE;

	ps = g_app_undo->tilebuf_enc;
	pd = g_app_undo->tilebuf_src;

	//--------- A

	size = alphasize;
	mode = 0;

	while(size)
	{
		if(mode == 0)
		{
			//----- 非連続モード

			do
			{
				//長さ

				insize--;
				if(insize < 0) return FALSE;

				len = *(ps++);
				if(len == 0) break;

				//データ

				size -= len;
				if(size < 0) return FALSE;

				memcpy(pd, ps, len);
				pd += len;

				r = ps[len - 1];

				ps += len;
				insize -= len;
				
			} while(len == 255);

			mode = 1;
		}
		else
		{
			//------ 連続モード

			do
			{
				//長さ

				insize--;
				if(insize < 0) return FALSE;

				len = *(ps++);
				if(len == 0) break;

				//データ

				size -= len;
				if(size < 0) return FALSE;

				memset(pd, r, len);
				pd += len;
				
			} while(len == 255);

			mode = 0;
		}
	}

	//--------- RGB

	size = outsize - alphasize;
	mode = 0;

	while(size)
	{
		if(mode == 0)
		{
			//----- 非連続モード

			do
			{
				//長さ
				
				insize--;
				if(insize < 0) return FALSE;

				len = *(ps++);
				if(len == 0) break;

				bytes = len * 3;

				//データ

				size -= bytes;
				if(size < 0) return FALSE;

				memcpy(pd, ps, bytes);
				pd += bytes;

				ps += bytes - 3;
				r = ps[0];
				g = ps[1];
				b = ps[2];
				ps += 3;

				insize -= bytes;
				
			} while(len == 255);

			mode = 1;
		}
		else
		{
			//------ 連続モード

			do
			{
				//長さ
				
				insize--;
				if(insize < 0) return FALSE;

				len = *(ps++);
				if(len == 0) break;

				//データ

				size -= len * 3;
				if(size < 0) return FALSE;

				for(bytes = len; bytes; bytes--, pd += 3)
				{
					pd[0] = r;
					pd[1] = g;
					pd[2] = b;
				}
				
			} while(len == 255);

			mode = 0;
		}
	}

	return TRUE;
}

/** 圧縮
 *
 * @param alphasize 先頭のアルファ値部分のサイズ
 * @return 圧縮サイズ。-1 で無圧縮。 */

int CompressUndoTile(int size,int alphasize)
{
	uint8_t *ps,*pstop,*psend,*pd,r,g,b;
	int mode,encsize = 0,len,n,bytes;

	pd = g_app_undo->tilebuf_enc;
	ps = g_app_undo->tilebuf_src;

	//------ A

	psend = ps + alphasize;
	mode = 0;

	while(ps < psend)
	{
		if(mode == 0)
		{
			//----- 非連続モード (長さ + 非連続データ)

			/* [a b c d   d     ]
			 *        ^ps ^ps+1
			 *
			 * この場合、abcd までがデータ。
			 * データの終端まで来た場合は ps == psend - 1 */

			for(pstop = ps; ps < psend - 1 && *ps != ps[1]; ps++);

			len = ps - pstop + 1;
			r = *ps;
			ps++;
			
			//出力

			while(len)
			{
				n = (len < 255)? len: 255;
				
				encsize += 1 + n;
				if(encsize >= size) return -1;

				*pd = n;
				memcpy(pd + 1, pstop, n);

				pstop += n;
				pd += 1 + n;

				//最後が 255 の場合は 0 を追加
				
				if(len == 255)
				{
					encsize++;
					if(encsize >= size) return -1;
					
					*(pd++) = 0;
				}

				len -= n;
			}

			mode = 1;
		}
		else
		{
			//---- 連続モード (前回の非連続の最後の値が連続している数)

			for(pstop = ps; ps < psend && *ps == r; ps++);

			len = ps - pstop;

			//出力個数

			n = (len + 255) / 255;

			encsize += n;
			if(encsize >= size) return -1;

			//出力

			for(; n; n--, len -= 255)
				*(pd++) = (len < 255)? len: 255;

			mode = 0;
		}
	}

	//------ RGB (3byte 単位)

	psend = g_app_undo->tilebuf_src + size;
	mode = 0;

	while(ps < psend)
	{
		if(mode == 0)
		{
			//----- 非連続モード

			for(pstop = ps; ps < psend - 3; ps += 3)
			{
				if(ps[0] == ps[3] && ps[1] == ps[4] && ps[2] == ps[5])
					break;
			}

			len = (ps - pstop) / 3 + 1;

			r = ps[0];
			g = ps[1];
			b = ps[2];
			ps += 3;
			
			//出力

			while(len)
			{
				n = (len < 255)? len: 255;
				bytes = n * 3;
				
				encsize += 1 + bytes;
				if(encsize >= size) return -1;

				*pd = n;
				memcpy(pd + 1, pstop, bytes);

				pd += 1 + bytes;
				pstop += bytes;

				//最後が 255 の場合は 0 を追加
				
				if(len == 255)
				{
					encsize++;
					if(encsize >= size) return -1;
					
					*(pd++) = 0;
				}

				len -= n;
			}

			mode = 1;
		}
		else
		{
			//---- 連続モード

			for(pstop = ps; ps < psend; ps += 3)
			{
				if(ps[0] != r || ps[1] != g || ps[2] != b)
					break;
			}

			len = (ps - pstop) / 3;

			//出力個数

			n = (len + 255) / 255;

			encsize += n;
			if(encsize >= size) return -1;

			//出力

			for(; n; n--, len -= 255)
				*(pd++) = (len < 255)? len: 255;

			mode = 0;
		}
	}
	
	return encsize;
}


/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * テクスチャライブラリデータ
 ************************************/
/*
 * - データが変更された時のみ、終了時に保存する。
 * 
 * [イメージデータ]
 *
 * 2byte: width
 * 2byte: height
 * 1byte: bits (1,2,8)
 *
 * 2bit の場合はこの後4色分のパレットデータ。
 * data... (X-Y は連続したビット)
 *
 * [!] 1bit は白黒の場合のみ。
 */

#include <string.h>
#include <stdio.h>

#include "mDef.h"
#include "mMemAuto.h"
#include "mPixbuf.h"
#include "mUtil.h"
#include "mUtilStdio.h"
#include "mGui.h"
#include "mStr.h"
#include "mZlib.h"

#include "defMacros.h"
#include "TextureLibrary.h"
#include "ImageBuf8.h"


//-------------------

typedef struct
{
	mMemAuto memList,
		memDat;
	int num;
}TextureLibrary;

static TextureLibrary g_texlib[TEXTURE_LIBRARY_NUM];
static mBool g_exitsave = FALSE;	//終了時にライブラリを保存するか

//-------------------

#define _CONFIG_FILE_CURRENT  "texcur.dat"

#define _APPENDSIZE_LIST  (16 * 4)
#define _APPENDSIZE_DAT   1024

//-------------------



//==========================
// sub
//==========================


/** データサイズ取得 */

static int _get_datasize(int w,int h,int bits)
{
	if(bits == 1)
		return 5 + ((w * h + 7) >> 3);
	else if(bits == 2)
		return 5 + 4 + ((w * h + 3) >> 2);
	else
		return 5 + w * h;
}

/** リストデータ追加 */

static mBool _append_list(TextureLibrary *p)
{
	uint32_t offset;

	//新規確保

	if(!p->memList.buf)
	{
		if(!mMemAutoAlloc(&p->memList, 16 * 4, _APPENDSIZE_LIST))
			return FALSE;
	}

	//追加

	offset = p->memDat.curpos;

	return mMemAutoAppend(&p->memList, &offset, 4);
}

/** 指定番号のテクスチャデータ取得 */

static uint8_t *_get_texture(int libno,int texno,int *pw,int *ph,int *pbits)
{
	uint32_t offset;
	uint8_t *buf;

	//オフセット

	offset = *((uint32_t *)g_texlib[libno].memList.buf + texno);

	//データ

	buf = g_texlib[libno].memDat.buf + offset;

	*pw = mGetBuf16BE(buf);
	*ph = mGetBuf16BE(buf + 2);
	*pbits = buf[4];

	return buf + 5;
}

/** ImageBuf8 からテクスチャデータに変換 */

static void _convert_image(uint8_t *dst,ImageBuf8 *img,int bits,uint8_t *palbuf)
{
	uint8_t *ps = img->buf,f,val;
	int i,j,shift;

	//ヘッダ
	
	mSetBuf16BE(dst, img->w);
	mSetBuf16BE(dst + 2, img->h);
	dst[4] = bits;

	dst += 5;

	//イメージ

	i = img->w * img->h;

	switch(bits)
	{
		//1bit
		case 1:
			for(f = 0x80, val = 0; i; i--, ps++)
			{
				if(*ps == 255) val |= f;

				if(f == 1) *(dst++) = val;

				f >>= 1;
				if(f == 0) f = 0x80, val = 0;
			}

			if(f != 0x80) *dst = val;
			break;
		//2bit
		case 2:
			memcpy(dst, palbuf, 4);
			dst += 4;

			for(val = 0, shift = 6; i; i--, ps++)
			{
				//パレット番号

				for(j = 0; j < 4; j++)
				{
					if(palbuf[j] == *ps) break;
				}

				val |= j << shift;

				if(shift == 0) *(dst++) = val;

				shift -= 2;
				if(shift < 0) shift = 6, val = 0;
			}

			if(shift != 6) *dst = val;
			break;
		//8bit
		default:
			memcpy(dst, ps, i);
			break;
	}
}


//==========================
// main
//==========================


/** 初期化 */

void TextureLibrary_init()
{
	memset(g_texlib, 0, sizeof(TextureLibrary) * TEXTURE_LIBRARY_NUM);
}

/** 解放 */

void TextureLibrary_free()
{
	int i;

	for(i = 0; i < TEXTURE_LIBRARY_NUM; i++)
	{
		mMemAutoFree(&g_texlib[i].memList);
		mMemAutoFree(&g_texlib[i].memDat);
	}
}

/** 個数を取得 */

int TextureLibrary_getNum(int libno)
{
	return g_texlib[libno].num;
}

/** 画像追加 */

mBool TextureLibrary_append(int libno,ImageBuf8 *img)
{
	TextureLibrary *p = g_texlib + libno;
	int bits,datsize;
	uint8_t pal[4],*buf;
	mBool addlist = FALSE;

	if(p->num >= 10000) return FALSE;

	//ビット数とパレット

	bits = ImageBuf8_getColorBits(img, pal);

	//データサイズ計算

	datsize = _get_datasize(img->w, img->h, bits);

	//データ確保＆作成

	buf = (uint8_t *)mMalloc(datsize, FALSE);
	if(!buf) return FALSE;

	_convert_image(buf, img, bits, pal);

	//リストデータ追加
	
	if(!_append_list(p)) goto ERR;

	addlist = TRUE;

	//イメージデータ追加

	if(!p->memDat.buf)
	{
		if(!mMemAutoAlloc(&p->memDat, 1024, _APPENDSIZE_DAT)) goto ERR;
	}

	if(!mMemAutoAppend(&p->memDat, buf, datsize))
		goto ERR;

	//

	mFree(buf);

	p->num++;
	g_exitsave = TRUE;

	return TRUE;

	//エラー
ERR:
	if(addlist) mMemAutoBack(&p->memList, 4);
	mFree(buf);
	return FALSE;
}

/** テクスチャを削除 */

void TextureLibrary_delete(int libno,int tno)
{
	TextureLibrary *p = g_texlib + libno;
	uint32_t *plist;
	int i,size;

	plist = (uint32_t *)p->memList.buf + tno;

	if(tno == p->num - 1)
		//終端の場合
		size = p->memDat.curpos - *plist;
	else
	{
		size = plist[1] - plist[0];

		//データをずらす
		
		memmove(p->memDat.buf + plist[0], p->memDat.buf + plist[1],
			p->memDat.curpos - plist[1]);
	}

	//オフセット位置をずらす

	for(i = p->num - 1 - tno; i > 0; i--, plist++)
		*plist = plist[1] - size;

	//

	mMemAutoBack(&p->memList, 4);
	mMemAutoBack(&p->memDat, size);
	
	p->num--;
	g_exitsave = TRUE;
}

/** ライブラリを空にする */

void TextureLibrary_empty(int libno)
{
	TextureLibrary *p = g_texlib + libno;

	if(p->memList.buf)
	{
		mMemAutoFree(&p->memList);
		mMemAutoFree(&p->memDat);

		p->num = 0;
		g_exitsave = TRUE;
	}
}


//=======================


/** テクスチャ画像を ImageBuf8 で作成 */

ImageBuf8 *TextureLibrary_createImage(int libno,int tno)
{
	ImageBuf8 *img;
	uint8_t *texbuf,*pd,*ps,f;
	int tw,th,bits,i,shift;

	//取得

	texbuf = _get_texture(libno, tno, &tw, &th, &bits);

	//作成

	img = ImageBuf8_new(tw, th);
	if(!img) return NULL;

	//変換

	pd = img->buf;
	ps = texbuf;

	switch(bits)
	{
		//1bit
		case 1:
			for(i = tw * th, f = 0x80; i; i--)
			{
				*(pd++) = (*ps & f)? 255: 0;

				f >>= 1;
				if(f == 0) f = 0x80, ps++;
			}
			break;
		//2bit
		case 2:
			ps += 4;
			
			for(i = tw * th, shift = 6; i; i--)
			{
				*(pd++) = texbuf[(*ps >> shift) & 3];

				shift -= 2;
				if(shift < 0) shift = 6, ps++;
			}
			break;
		//8bit
		case 8:
			memcpy(pd, texbuf, tw * th);
			break;
	}

	return img;
}

/** 指定テクスチャを mPixbuf に描画 */

void TextureLibrary_drawImage(int libno,int no,mPixbuf *pixbuf,int x,int y,int w,int h)
{
	uint8_t *pd,*ps,*psY,*imgbuf,f,fleft;
	int bpp,pitchd,tw,th,tbits,ix,iy,tx,ty,pos,shift,shift_left;
	uint32_t col,palcol[4];
	mBox box;

	//pixbuf

	if(!mPixbufGetClipBox_d(pixbuf, &box, x, y, w, h))
		return;

	pd = mPixbufGetBufPtFast(pixbuf, box.x, box.y);
	bpp = pixbuf->bpp;
	pitchd = pixbuf->pitch_dir - box.w * bpp;

	//データ

	imgbuf = _get_texture(libno, no, &tw, &th, &tbits);

	//描画

	psY = imgbuf;

	switch(tbits)
	{
		//1bit
		case 1:
			col = mGraytoPix(255);
			fleft = 0x80;
			pos = 0;
			
			for(iy = ty = 0; iy < box.h; iy++, pd += pitchd)
			{
				ps = psY;
				f = fleft;
				
				for(ix = tx = 0; ix < box.w; ix++, pd += bpp)
				{
					(pixbuf->setbuf)(pd, (*ps & f)? 0: col);

					//

					tx++;
					if(tx == tw)
						tx = 0, f = fleft, ps = psY;
					else
					{
						f >>= 1;
						if(f == 0) f = 0x80, ps++;
					}
				}

				ty++;
				if(ty == th)
					//(0,0) へ戻る
					ty = 0, fleft = 0x80, pos = 0, psY = imgbuf;
				else
				{
					pos += tw;
					psY = imgbuf + (pos >> 3);
					fleft = 1 << (7- (pos & 7));
				}
			}
			break;
		//2bit
		case 2:
			//パレット色

			for(ix = 0; ix < 4; ix++)
				palcol[ix] = mGraytoPix(255 - imgbuf[ix]);

			imgbuf += 4;
			
			//

			psY = imgbuf;
			pos = 0;
			shift_left = 6;
			
			for(iy = ty = 0; iy < box.h; iy++, pd += pitchd)
			{
				ps = psY;
				shift = shift_left;
				
				for(ix = tx = 0; ix < box.w; ix++, pd += bpp)
				{
					(pixbuf->setbuf)(pd, palcol[(*ps >> shift) & 3]);

					//

					tx++;
					if(tx == tw)
						tx = 0, shift = shift_left, ps = psY;
					else
					{
						shift -= 2;
						if(shift < 0) shift = 6, ps++;
					}
				}

				ty++;
				if(ty == th)
					//(0,0) へ戻る
					ty = 0, shift_left = 6, pos = 0, psY = imgbuf;
				else
				{
					pos += tw;
					psY = imgbuf + (pos >> 2);
					shift_left = 6 - ((pos & 3) << 1);
				}
			}
			break;
		//8bit
		case 8:
			for(iy = ty = 0; iy < box.h; iy++, pd += pitchd)
			{
				ps = psY;
				
				for(ix = tx = 0; ix < box.w; ix++, pd += bpp)
				{
					(pixbuf->setbuf)(pd, mGraytoPix(255 - *ps));

					//

					tx++;
					if(tx == tw)
						tx = 0, ps = psY;
					else
						ps++;
				}

				ty++;
				if(ty == th)
					ty = 0, psY = imgbuf;
				else
					psY += tw;
			}
			break;
	}
}


//================================
// 読み書き sub
//================================


/** ファイルを開く */

static FILE *_open_file(const char *filename,const char *mode)
{
	mStr str = MSTR_INIT;
	FILE *fp;

	mAppGetConfigPath(&str, filename);

	fp = mFILEopenUTF8(str.buf, mode);

	mStrFree(&str);

	return fp;
}


//================================
// 読み込み
//================================


/** カレントテクスチャ読み込み */

static ImageBuf8 *_read_config_curtex()
{
	FILE *fp;
	ImageBuf8 *img = NULL;
	uint8_t ver;
	uint16_t w,h;

	fp = _open_file(_CONFIG_FILE_CURRENT, "rb");
	if(!fp) return NULL;

	//ヘッダ

	if(!mFILEreadCompareStr(fp, "AZPBTEXCUR")
		|| !mFILEreadByte(fp, &ver)
		|| ver != 0)
		goto END;

	//幅、高さ (0 で空)

	if(!mFILEread16BE(fp, &w) || !mFILEread16BE(fp, &h)
		|| w == 0 || h == 0)
		goto END;

	//イメージ

	img = ImageBuf8_new(w, h);
	if(img)
		fread(img->buf, 1, w * h, fp);

END:
	fclose(fp);

	return img;
}

/** ライブラリデータ読み込み */

static mBool _read_library(TextureLibrary *p,FILE *fp)
{
	uint16_t num;
	uint32_t datsize,compsize;
	mZlibDecode *dec;

	//個数 (0xffff で終了)

	if(!mFILEread16BE(fp, &num) || num == 0xffff)
		return FALSE;

	//空にする

	mMemAutoFree(&p->memList);
	mMemAutoFree(&p->memDat);
	p->num = 0;

	//個数が 0 で空

	if(num == 0) return TRUE;

	//リスト

	if(!mMemAutoAlloc(&p->memList, num << 2, _APPENDSIZE_LIST))
		return FALSE;

	if(mFILEreadArray32BE(fp, p->memList.buf, num) != num)
		return FALSE;

	//データ

	if(!mFILEread32BE(fp, &datsize)
		|| !mFILEread32BE(fp, &compsize))
		return FALSE;

	if(!mMemAutoAlloc(&p->memDat, datsize, _APPENDSIZE_DAT))
		return FALSE;

	//展開

	dec = mZlibDecodeNew(4096, 15);
	if(!dec) return FALSE;

	mZlibDecodeSetIO_stdio(dec, fp);
	mZlibDecodeReadOnce(dec, p->memDat.buf, datsize, compsize);

	mZlibDecodeFree(dec);

	//

	p->memList.curpos = num << 2;
	p->memDat.curpos = datsize;
	p->num = num;

	return TRUE;
}

/** ライブラリの設定ファイルを読み込み */

static void _read_config_library()
{
	FILE *fp;
	uint8_t ver;
	int i;

	fp = _open_file(CONFIG_FILENAME_TEXTURELIB, "rb");
	if(!fp) return;

	//ヘッダ

	if(!mFILEreadCompareStr(fp, "AZPBTEXLIB")
		|| !mFILEreadByte(fp, &ver)
		|| ver != 0)
		goto END;

	//ライブラリ

	for(i = 0; i < TEXTURE_LIBRARY_NUM; i++)
	{
		if(!_read_library(g_texlib + i, fp))
			break;
	}

END:
	fclose(fp);
}

/** 設定ファイルから読み込み
 *
 * @return カレントテクスチャ */

ImageBuf8 *TextureLibrary_loadConfig()
{
	//ライブラリ

	_read_config_library();

	//カレントテクスチャ

	return _read_config_curtex();
}

/** ライブラリファイルから読み込み */

mBool TextureLibrary_loadFile(const char *filename,int libno)
{
	FILE *fp;
	uint8_t ver;

	fp = mFILEopenUTF8(filename, "rb");
	if(!fp) return FALSE;

	//ヘッダ

	if(!mFILEreadCompareStr(fp, "AZPBTEXLIB")
		|| !mFILEreadByte(fp, &ver)
		|| ver != 0)
	{
		fclose(fp);
		return FALSE;
	}

	if(!_read_library(g_texlib + libno, fp))
	{
		fclose(fp);
		return FALSE;
	}

	fclose(fp);

	g_exitsave = TRUE;

	return TRUE;
}


//================================
// 書き込み
//================================


/** カレントテクスチャ書き込み */

static void _write_config_curtex(ImageBuf8 *curimg)
{
	FILE *fp;

	fp = _open_file(_CONFIG_FILE_CURRENT, "wb");
	if(!fp) return;

	fputs("AZPBTEXCUR", fp);
	mFILEwriteByte(fp, 0);

	if(!curimg)
		mFILEwriteZero(fp, 4);
	else
	{
		mFILEwrite16BE(fp, curimg->w);
		mFILEwrite16BE(fp, curimg->h);

		fwrite(curimg->buf, 1, curimg->w * curimg->h, fp);
	}

	fclose(fp);
}

/** 各ライブラリ書き込み */

static void _write_library(TextureLibrary *p,FILE *fp)
{
	mZlibEncode *enc;
	uint32_t size;
	fpos_t pos;

	//個数

	mFILEwrite16BE(fp, p->num);

	if(p->num == 0) return;

	//リストデータ

	mFILEwriteArray32BE(fp, p->memList.buf, p->num);

	//データサイズ

	mFILEwrite32BE(fp, p->memDat.curpos);

	//圧縮サイズ (仮)

	fgetpos(fp, &pos);
	mFILEwrite32BE(fp, 0);

	//圧縮データ

	enc = mZlibEncodeNew_simple(4096, 6);
	if(!enc) return;

	mZlibEncodeSetIO_stdio(enc, fp);

	mZlibEncodeSend(enc, p->memDat.buf, p->memDat.curpos);

	mZlibEncodeFlushEnd(enc);

	size = mZlibEncodeGetWriteSize(enc);

	mZlibEncodeFree(enc);

	//圧縮サイズ

	fsetpos(fp, &pos);
	mFILEwrite32BE(fp, size);
	fseek(fp, 0, SEEK_END);
}

/** ライブラリを設定ファイルに保存 */

static void _write_config_library()
{
	FILE *fp;
	int i;

	fp = _open_file(CONFIG_FILENAME_TEXTURELIB, "wb");
	if(!fp) return;

	//ヘッダ

	fputs("AZPBTEXLIB", fp);
	mFILEwriteByte(fp, 0);

	//ライブラリ

	for(i = 0; i < TEXTURE_LIBRARY_NUM; i++)
		_write_library(g_texlib + i, fp);

	mFILEwrite16BE(fp, 0xffff);

	fclose(fp);
}

/** 設定ファイルに保存 */

void TextureLibrary_saveConfig(ImageBuf8 *curimg,mBool save_curimg)
{
	//カレントテクスチャ

	if(save_curimg)
		_write_config_curtex(curimg);

	//ライブラリ

	if(g_exitsave)
		_write_config_library();
}

/** ライブラリファイルに保存 */

mBool TextureLibrary_saveFile(const char *filename,int libno)
{
	FILE *fp;

	fp = mFILEopenUTF8(filename, "wb");
	if(!fp) return FALSE;

	fputs("AZPBTEXLIB", fp);
	mFILEwriteByte(fp, 0);

	_write_library(g_texlib + libno, fp);

	mFILEwrite16BE(fp, 0xffff);

	fclose(fp);

	return TRUE;
}

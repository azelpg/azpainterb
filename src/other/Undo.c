/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * Undo
 *****************************************/

#include <stdio.h>
#include <string.h>

#include "mDef.h"
#include "mRectBox.h"
#include "mUndo.h"

#include "defDraw.h"

#include "LayerList.h"
#include "LayerItem.h"
#include "defTileImage.h"
#include "TileImage.h"

#include "Undo.h"
#include "UndoItem.h"
#include "UndoMaster.h"


//--------------------

UndoMaster *g_app_undo = NULL;

#define _UNDO  g_app_undo

//--------------------


//========================
// sub
//========================


/** アイテム破棄ハンドラ */

static void _destroy_item_handle(mListItem *item)
{
	UndoItem_free((UndoItem *)item);
}

/** ハンドラ:アイテム作成 */

static mListItem *_create_handle(mUndo *p)
{
	UndoItem *pi;

	//作成

	pi = (UndoItem *)mMalloc(sizeof(UndoItem), TRUE);

	if(pi)
	{
		pi->i.destroy = _destroy_item_handle;
		pi->fileno = -1;

		/* fileno を -1 にしておかないと、アイテム破棄時に
		 * 番号 0 のアンドゥファイルが削除される。 */
	}

	//変更フラグ
	/* アンドゥ追加、またはアンドゥ/リドゥ実行時 */

	_UNDO->change = TRUE;

	return (mListItem *)pi;
}

/** ハンドラ:逆のデータセット */

static mBool _setreverse_handle(mUndo *undo,mListItem *dstitem,mListItem *srcitem,int settype)
{
	UndoItem *dst,*src;

	dst = (UndoItem *)dstitem;
	src = (UndoItem *)srcitem;

	//ソースの情報コピー

	dst->type = src->type;
	memcpy(dst->val, src->val, sizeof(int) * UNDO_VAL_NUM);

	//

	switch(src->type)
	{
		//タイルイメージ
		case UNDO_TYPE_TILES_IMAGE:
			return UndoItem_writeUndoTilesReverseImage(dst, src, settype);

		//イメージ、小範囲
		case UNDO_TYPE_SMALL_IMAGE:
			return UndoItem_writeSmallImage(dst, NULL);
	
		//レイヤ追加
		case UNDO_TYPE_LAYER_NEW:
			if(settype == MUNDO_TYPE_REDO)
			{
				return UndoItem_writeSingleLayer(dst,
					UndoItem_getLayerFromNo_forParent(src->val[0], src->val[1]));
			}
			break;

		//レイヤ削除
		case UNDO_TYPE_LAYER_DELETE:
			if(settype == MUNDO_TYPE_UNDO)
			{
				return UndoItem_writeSingleLayer_forFolder(dst,
						UndoItem_getLayerFromNo(src->val[0]));
			}
			break;

		//レイヤイメージクリア
		case UNDO_TYPE_LAYER_CLEARIMG:
			if(settype == MUNDO_TYPE_UNDO)
				return UndoItem_writeSingleLayerImage(dst, UndoItem_getLayerFromNo(src->val[0]));
			break;

		//下レイヤに移す/結合
		case UNDO_TYPE_LAYER_DROP_OR_COMBINE:
			if(src->val[0])
			{
				return UndoItem_writeTwoLayersImage(dst,
					UndoItem_getLayerFromNo_forParent(src->val[1], src->val[2]));
			}
			else
				return UndoItem_writeLayerCombine(dst, settype);
			break;

		//すべてのレイヤ結合
		case UNDO_TYPE_LAYER_COMBINE_ALL:
			if(settype == MUNDO_TYPE_REDO)
				return UndoItem_writeSingleLayer(dst, LayerList_getItem_top(APP_DRAW->layerlist));
			else
				return UndoItem_writeAllLayers(dst);
			break;

		//フォルダレイヤ結合
		case UNDO_TYPE_LAYER_COMBINE_FOLDER:
			if(settype == MUNDO_TYPE_REDO)
			{
				return UndoItem_writeSingleLayer(dst,
					UndoItem_getLayerFromNo_forParent(src->val[0], src->val[1]));
			}
			else
			{
				return UndoItem_writeSingleLayer_forFolder(dst,
					UndoItem_getLayerFromNo_forParent(src->val[0], src->val[1]));
			}
			break;
	
		//レイヤオフセット移動
		case UNDO_TYPE_LAYER_MOVE_OFFSET:
			dst->val[0] = -dst->val[0];
			dst->val[1] = -dst->val[1];

			//レイヤ数が1より大きければデータコピー
			if(dst->val[2] > 1)
				return UndoItem_copy(dst, src);
			break;

		//レイヤ一覧位置移動
		case UNDO_TYPE_LAYER_MOVE_LIST:
			dst->val[0] = src->val[2];
			dst->val[1] = src->val[3];
			dst->val[2] = src->val[0];
			dst->val[3] = src->val[1];
			break;

		//キャンバスサイズ変更 (オフセット移動)
		case UNDO_TYPE_RESIZECANVAS_MOVEOFFSET:
			dst->val[0] = -src->val[0];
			dst->val[1] = -src->val[1];
			dst->val[2] = APP_DRAW->imgw;
			dst->val[3] = APP_DRAW->imgh;
			break;

		//キャンバスサイズ変更 (切り取り)
		//キャンバス拡大縮小
		case UNDO_TYPE_RESIZECANVAS_CROP:
		case UNDO_TYPE_SCALECANVAS:
			dst->val[0] = APP_DRAW->imgw;
			dst->val[1] = APP_DRAW->imgh;

			if(src->type == UNDO_TYPE_SCALECANVAS)
				dst->val[2] = APP_DRAW->imgdpi;

			return UndoItem_writeAllLayersImage(dst);
	}

	return TRUE;
}

/** ハンドラ:実行 */

static mBool _run_handle(mUndo *pundo,mListItem *runitem,int runtype)
{
	UndoMaster *undo = (UndoMaster *)pundo;
	UndoItem *item = (UndoItem *)runitem;
	LayerItem *layer;
	UndoUpdateInfo *update;

	update = &undo->update;

	//デフォルトで更新なし

	update->type = UNDO_UPDATE_NONE;
	mRectEmpty(&update->rc);

	//

	switch(item->type)
	{
		//タイルイメージ
		//小範囲
		case UNDO_TYPE_TILES_IMAGE:
		case UNDO_TYPE_SMALL_IMAGE:
			update->type = UNDO_UPDATE_RECT;
			update->rc.x1 = item->val[1];
			update->rc.y1 = item->val[2];
			update->rc.x2 = item->val[3];
			update->rc.y2 = item->val[4];
			update->layer = UndoItem_getLayerFromNo(item->val[0]);

			if(item->type == UNDO_TYPE_TILES_IMAGE)
				return UndoItem_restoreUndoTilesImage(item, runtype);
			else
				return UndoItem_restoreSmallImage(item);

		//レイヤ追加
		case UNDO_TYPE_LAYER_NEW:
			return UndoItem_runLayerNew(item, update, runtype);

		//レイヤ複製
		case UNDO_TYPE_LAYER_COPY:
			return UndoItem_runLayerCopy(item, update, runtype);

		//レイヤ削除
		case UNDO_TYPE_LAYER_DELETE:
			return UndoItem_runLayerDelete(item, update, runtype);

		//レイヤイメージクリア
		case UNDO_TYPE_LAYER_CLEARIMG:
			return UndoItem_runLayerClearImage(item, update, runtype);

		//下レイヤに移す/結合
		case UNDO_TYPE_LAYER_DROP_OR_COMBINE:
			return UndoItem_runLayerDropOrCombine(item, update, runtype);

		//すべてのレイヤ結合
		case UNDO_TYPE_LAYER_COMBINE_ALL:
			return UndoItem_runLayerCombineAll(item, update, runtype);

		//フォルダレイヤ結合
		case UNDO_TYPE_LAYER_COMBINE_FOLDER:
			return UndoItem_runLayerCombineFolder(item, update, runtype);
	
		//レイヤオフセット位置移動
		case UNDO_TYPE_LAYER_MOVE_OFFSET:
			return UndoItem_runLayerMoveOffset(item, update);

		//レイヤフラグ反転
		case UNDO_TYPE_LAYER_FLAGS:
			layer = UndoItem_getLayerFromNo(item->val[0]);
			if(!layer) return FALSE;

			layer->flags ^= item->val[1];

			update->type = UNDO_UPDATE_LAYERLIST_ONE;
			update->layer = layer;
			break;

		//レイヤ全体の左右/上下反転
		case UNDO_TYPE_LAYER_REVERSE_HORZVERT:
			return UndoItem_runLayerReverseHorzVert(item, update);

		//レイヤ順番移動
		case UNDO_TYPE_LAYER_MOVE_LIST:
			return UndoItem_runLayerMoveList(item, update);

		//キャンバスサイズ変更 (オフセット移動)
		case UNDO_TYPE_RESIZECANVAS_MOVEOFFSET:
			LayerList_moveOffset_rel_all(APP_DRAW->layerlist, item->val[0], item->val[1]);

			update->type = UNDO_UPDATE_CANVAS_RESIZE;
			update->rc.x1 = item->val[2];
			update->rc.y1 = item->val[3];
			break;

		//キャンバスサイズ変更 (切り取り)
		//キャンバス拡大縮小
		case UNDO_TYPE_RESIZECANVAS_CROP:
		case UNDO_TYPE_SCALECANVAS:
			return UndoItem_runCanvasResizeOrScale(item, update);
	}

	return TRUE;
}

/** アンドゥデータ追加 */

static UndoItem *_add_item(int type)
{
	UndoItem *pi;

	pi = (UndoItem *)_create_handle(&_UNDO->undo);
	if(!pi) return NULL;

	mUndoAdd(&_UNDO->undo, M_LISTITEM(pi));

	pi->type = type;

	return pi;
}


//========================
// main
//========================


/** 作成 */

mBool Undo_new()
{
	UndoMaster *p;

	p = (UndoMaster *)mMalloc(sizeof(UndoMaster), TRUE);
	if(!p) return FALSE;

	//タイル圧縮用バッファ

	p->tilebuf_src = (uint8_t *)mMalloc(64 * 64 * 4, FALSE);
	p->tilebuf_enc = (uint8_t *)mMalloc(64 * 64 * 4, FALSE);
	
	if(!p->tilebuf_src || !p->tilebuf_enc)
	{
		mFree(p->tilebuf_src);
		mFree(p->tilebuf_enc);
		mFree(p);
		return FALSE;
	}

	//
	
	g_app_undo = p;

	p->undo.maxnum = 20;
	p->undo.create = _create_handle;
	p->undo.setreverse = _setreverse_handle;
	p->undo.run = _run_handle;

	return TRUE;
}

/** 解放 */

void Undo_free()
{
	if(g_app_undo)
	{
		mUndoDeleteAll(&_UNDO->undo);

		mFree(_UNDO->tilebuf_src);
		mFree(_UNDO->tilebuf_enc);
		mFree(g_app_undo);
	}
}

/** データ更新用フラグを OFF */

void Undo_clearUpdateFlag()
{
	_UNDO->change = FALSE;
}

/** アンドゥ最大回数をセット */

void Undo_setMaxNum(int num)
{
	_UNDO->undo.maxnum = num;
}

/** アンドゥ、リドゥデータがあるか */

mBool Undo_isHave(mBool redo)
{
	return mUndoIsHave(&_UNDO->undo, redo);
}

/** データが変更されているか */

mBool Undo_isChange()
{
	/* フラグは、新規作成時やファイル保存時はクリアされる。
	 * その時点からアンドゥが追加されたか、
	 * またはアンドゥ/リドゥが実行されたかどうかによって、
	 * データが変更されたか (保存が必要か) どうかを判定する。 */

	return _UNDO->change;
}

/** すべて削除 */

void Undo_deleteAll()
{
	mUndoDeleteAll(&_UNDO->undo);
}

/** アンドゥ/リドゥ実行
 *
 * @return 更新を行うか
 * (実行処理で失敗した場合は中途半端な所で止まっている場合が
 *  あるので、実行に失敗しても更新は行う) */

mBool Undo_runUndoRedo(mBool redo,UndoUpdateInfo *info)
{
	int ret;

	if(redo)
		ret = mUndoRunRedo(&_UNDO->undo);
	else
		ret = mUndoRunUndo(&_UNDO->undo);

	if(ret == MUNDO_RUNERR_OK || ret == MUNDO_RUNERR_RUN)
	{
		*info = _UNDO->update;
		return TRUE;
	}
	else
		return FALSE;
}


//====================


/** 追加:タイルイメージ
 *
 * @param rc 更新されたイメージの範囲 */

void Undo_addTilesImage(TileImageInfo *info,mRect *rc)
{
	UndoItem *pi = _add_item(UNDO_TYPE_TILES_IMAGE);
	TileImage *img;

	if(pi)
	{
		//セット
		
		UndoItem_setval_curlayer(pi, 0);

		pi->val[1] = rc->x1;
		pi->val[2] = rc->y1;
		pi->val[3] = rc->x2;
		pi->val[4] = rc->y2;

		//小範囲か
		/* タイル配列に変化がない & 幅x高さ が 48x48 以内の場合 */

		img = APP_DRAW->curlayer->img;

		if(img->tilew == info->tilew && img->tileh == info->tileh
			&& (rc->x2 - rc->x1 + 1) * (rc->y2 - rc->y1 + 1) <= 48 * 48)
		{
			pi->type = UNDO_TYPE_SMALL_IMAGE;

			UndoItem_writeSmallImage(pi, APP_DRAW->tileimgDraw);
		}
		else
			UndoItem_writeUndoTilesImage(pi, info);
	}
}

/** 追加:新規レイヤ追加
 *
 * レイヤ追加 & カレントセット後に実行 */

void Undo_addLayerNew()
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_NEW);

	if(pi)
		UndoItem_setval_layerno_forParent(pi, 0, APP_DRAW->curlayer);
}

/** 追加:レイヤ複製 */

void Undo_addLayerCopy()
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_COPY);

	if(pi)
		UndoItem_setval_curlayer(pi, 0);
}

/** 追加:レイヤ削除 */

void Undo_addLayerDelete()
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_DELETE);

	if(pi)
	{
		UndoItem_setval_curlayer(pi, 0);

		UndoItem_writeSingleLayer_forFolder(pi, APP_DRAW->curlayer);
	}
}

/** 追加:レイヤイメージクリア */

void Undo_addLayerClearImage()
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_CLEARIMG);

	if(pi)
	{
		UndoItem_setval_curlayer(pi, 0);

		UndoItem_writeSingleLayerImage(pi, APP_DRAW->curlayer);
	}
}

/** 追加:下レイヤに移す/結合 */

void Undo_addLayerDropOrCombine(mBool drop)
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_DROP_OR_COMBINE);

	if(pi)
	{
		pi->val[0] = drop;
		UndoItem_setval_layerno_forParent(pi, 1, APP_DRAW->curlayer);

		if(drop)
			UndoItem_writeTwoLayersImage(pi, APP_DRAW->curlayer);
		else
			UndoItem_writeLayerCombine(pi, MUNDO_TYPE_UNDO);
	}
}

/** 追加:すべてのレイヤの結合 */

void Undo_addLayerCombineAll()
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_COMBINE_ALL);

	if(pi)
		UndoItem_writeAllLayers(pi);
}

/** 追加:フォルダレイヤの結合 */

void Undo_addLayerCombineFolder()
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_COMBINE_FOLDER);

	if(pi)
	{
		UndoItem_setval_layerno_forParent(pi, 0, APP_DRAW->curlayer);

		UndoItem_writeSingleLayer_forFolder(pi, APP_DRAW->curlayer);
	}
}

/** 追加:レイヤのフラグ変更 */

void Undo_addLayerFlags(LayerItem *item,uint32_t flags)
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_FLAGS);

	if(pi)
	{
		UndoItem_setval_layerno(pi, 0, item);
		pi->val[1] = flags;
	}
}

/** 追加:レイヤ全体の左右/上下反転 */

void Undo_addLayerReverseHorzVert(int type)
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_REVERSE_HORZVERT);

	if(pi)
	{
		UndoItem_setval_curlayer(pi, 0);
		pi->val[1] = type;
	}
}

/** 追加:レイヤオフセット移動 */

void Undo_addLayerMoveOffset(int mx,int my,LayerItem *top,int num)
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_MOVE_OFFSET);

	if(pi)
	{
		pi->val[0] = -mx;
		pi->val[1] = -my;
		pi->val[2] = num;

		if(num == 1)
			//レイヤ数が1の場合は val[3] に番号をセット
			UndoItem_setval_layerno(pi, 3, top);
		else
			UndoItem_writeLinkLayersNo(pi, top, num);
	}
}

/** 追加:レイヤ一覧上の位置移動 (移動後に呼び出す) */

void Undo_addLayerMoveList(LayerItem *item,int parent,int pos)
{
	UndoItem *pi = _add_item(UNDO_TYPE_LAYER_MOVE_LIST);

	/* [0] 移動後の親位置
	 * [1] 移動後の相対位置
	 * [2] 移動前の親位置
	 * [3] 移動前の相対位置 */

	if(pi)
	{
		UndoItem_setval_layerno_forParent(pi, 0, item);
		pi->val[2] = parent;
		pi->val[3] = pos;
	}
}

/** キャンバスサイズ変更
 *
 * 範囲外を切り取らず、レイヤ全体をオフセット移動した時。 */

void Undo_addResizeCanvas_moveOffset(int mx,int my,int w,int h)
{
	UndoItem *pi = _add_item(UNDO_TYPE_RESIZECANVAS_MOVEOFFSET);

	if(pi)
	{
		pi->val[0] = -mx;
		pi->val[1] = -my;
		pi->val[2] = w;
		pi->val[3] = h;
	}
}

/** キャンバスサイズ変更 (切り取り時) */

void Undo_addResizeCanvas_crop(int w,int h)
{
	UndoItem *pi = _add_item(UNDO_TYPE_RESIZECANVAS_CROP);

	if(pi)
	{
		pi->val[0] = w;
		pi->val[1] = h;

		UndoItem_writeAllLayersImage(pi);
	}
}

/** キャンバス拡大縮小 */

void Undo_addScaleCanvas()
{
	UndoItem *pi = _add_item(UNDO_TYPE_SCALECANVAS);

	if(pi)
	{
		pi->val[0] = APP_DRAW->imgw;
		pi->val[1] = APP_DRAW->imgh;
		pi->val[2] = APP_DRAW->imgdpi;

		UndoItem_writeAllLayersImage(pi);
	}
}


/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * UndoItem
 *
 * タイルイメージ
 *****************************************/

#include <stdio.h>

#include "mDef.h"
#include "mUndo.h"

#include "defConfig.h"
#include "defDraw.h"

#include "defTileImage.h"
#include "TileImage.h"
#include "LayerItem.h"

#include "Undo.h"
#include "UndoMaster.h"
#include "UndoItem.h"


//--------------------

#define _FULLSIZE_MAX  (128 * 1024)

//--------------------



//==================================
// sub
//==================================


/** 対象のレイヤイメージ取得 */

static TileImage *_get_layerimg(UndoItem *p)
{
	LayerItem *item;

	item = UndoItem_getLayerFromNo(p->val[0]);
	if(!item) return NULL;

	return item->img;
}

/** バッファ書き込みの最大サイズ取得 */

static int _get_bufmaxsize()
{
	int size;
	
	size = APP_CONF->undo_maxbufsize;
	if(size > _FULLSIZE_MAX) size = _FULLSIZE_MAX;

	return size;
}

/** タイルの圧縮サイズ取得 */

static int _get_enctilesize(uint8_t *tile)
{
	int srcsize,encsize;

	srcsize = TileImage_getTile_forUndo(g_app_undo->tilebuf_src, tile);

	encsize = CompressUndoTile(srcsize, 64 * 64);

	return (encsize == -1)? srcsize: encsize;
}


//==================================
// 小範囲イメージ
//==================================
/*
 * [2byte : 圧縮前サイズ]
 * [2byte : 圧縮サイズ (圧縮前サイズと同じで無圧縮)]
 * [データ]
 *   # 2bitフラグ (w x h)
 *     0 - 点をセット
 *     1 - 空タイル
 *     2 - 何もしない
 *   # A,RGB データ
 *
 * ! フラグデータは 64x64x4 のバッファ内に含めるので、
 *   イメージ範囲はフラグデータサイズ分を考慮しなければならない。
 *   なので、最大範囲は 64x64 ではなく少なめに 48x48 にしてある。
 */


/** 小範囲用イメージ書き込み
 *
 * @param img  NULL で UndoItem の情報から。それ以外はアンドゥ用の元イメージから  */

mBool UndoItem_writeSmallImage(UndoItem *p,TileImage *img)
{
	uint8_t *srcbuf,*encbuf,*pd_flag,*pd_alpha,*pd_rgb,first_write;
	int w,h,ix,iy,pixnum,topx,topy,flagsize,srcsize,encsize,shift,flag;
	uint16_t wsize[2];
	PixelRGBA pix;

	//APP_DRAW->tileimgDraw がソースか

	first_write = (img != NULL);

	//イメージ

	if(!img)
	{
		img = _get_layerimg(p);
		if(!img) return FALSE;
	}

	//

	topx = p->val[1];
	topy = p->val[2];
	w = p->val[3] - p->val[1] + 1;
	h = p->val[4] - p->val[2] + 1;

	pixnum = w * h;
	flagsize = (pixnum * 2 + 3) >> 2;

	srcbuf = g_app_undo->tilebuf_src;
	encbuf = g_app_undo->tilebuf_enc;

	//srcbuf の先頭にフラグ、後にイメージをセット

	pd_flag = srcbuf;
	pd_alpha = srcbuf + flagsize;
	pd_rgb = pd_alpha + pixnum;

	srcsize = flagsize + pixnum;
	shift = 6;

	mMemzero(pd_flag, flagsize);

	for(iy = 0; iy < h; iy++)
	{
		for(ix = 0; ix < w; ix++)
		{
			flag = TileImage_getPixel_forUndo(img, topx + ix, topy + iy, &pix);

			/* 最初の書き込み時は、以下のように変更。
			 * 1 空タイル => 描画時に変更されなかったタイルなので、点をセットしない
			 * 2 元が空   => アンドゥ実行時にタイルを解放する */

			if(first_write)
			{
				if(flag == 1)
					flag = 2;
				else if(flag == 2)
					flag = 1;
			}

			//フラグ

			*pd_flag |= flag << shift;

			shift -= 2;
			if(shift < 0) pd_flag++, shift = 6;	

			//RGBA
			
			*(pd_alpha++) = pix.a;

			if(pix.a)
			{
				pd_rgb[0] = pix.r;
				pd_rgb[1] = pix.g;
				pd_rgb[2] = pix.b;

				pd_rgb += 3;
				srcsize += 3;
			}
		}
	}

	//圧縮 (フラグ+アルファ値までを 1byte 単位で圧縮)

	encsize = CompressUndoTile(srcsize, flagsize + pixnum);

	wsize[0] = srcsize;
	wsize[1] = (encsize == -1 || encsize == srcsize)? srcsize: encsize;
	
	//書き込み

	if(!UndoItem_alloc(p, 4 + wsize[1])
		|| !UndoItem_openWrite(p))
		return FALSE;

	UndoItem_write(p, wsize, 4);

	if(encsize == -1)
		UndoItem_write(p, srcbuf, srcsize);
	else
		UndoItem_write(p, encbuf, encsize);

	UndoItem_closeWrite(p);

	return TRUE;
}

/** 小範囲イメージ復元 */

mBool UndoItem_restoreSmallImage(UndoItem *p)
{
	TileImage *img;
	int topx,topy,w,h,pixnum,ix,iy,shift,flagsize;
	uint16_t wsize[2];
	uint8_t *srcbuf,*encbuf,*ps_flag,*ps_alpha,*ps_rgb,flag;
	PixelRGBA pix;
	mBool ret = FALSE;

	//イメージ

	img = _get_layerimg(p);
	if(!img) return FALSE;

	//

	topx = p->val[1];
	topy = p->val[2];
	w = p->val[3] - p->val[1] + 1;
	h = p->val[4] - p->val[2] + 1;

	pixnum = w * h;
	flagsize = (pixnum * 2 + 3) >> 2;

	srcbuf = g_app_undo->tilebuf_src;
	encbuf = g_app_undo->tilebuf_enc;

	//読み込み

	if(!UndoItem_openRead(p)) return FALSE;

	if(UndoItem_read(p, wsize, 4))
	{
		if(wsize[0] == wsize[1])
			//無圧縮
			ret = UndoItem_read(p, srcbuf, wsize[0]);
		else
		{
			//展開
			if(UndoItem_read(p, encbuf, wsize[1]))
				ret = UncompressUndoTile(wsize[0], wsize[1], flagsize + pixnum);
		}
	}

	UndoItem_closeRead(p);

	if(!ret) return FALSE;

	//点を復元

	ps_flag = srcbuf;
	ps_alpha = srcbuf + flagsize;
	ps_rgb = ps_alpha + pixnum;
	shift = 6;

	for(iy = 0; iy < h; iy++)
	{
		for(ix = 0; ix < w; ix++)
		{
			//RGBA
			
			pix.a = *(ps_alpha++);

			if(pix.a == 0)
				pix.c = 0;
			else
			{
				pix.r = ps_rgb[0];
				pix.g = ps_rgb[1];
				pix.b = ps_rgb[2];
				ps_rgb += 3;
			}

			//フラグ

			flag = (*ps_flag >> shift) & 3;
		
			shift -= 2;
			if(shift < 0) ps_flag++, shift = 6;

			//セット

			if(flag == 0)
				TileImage_setPixel_new(img, topx + ix, topy + iy, &pix);
			else if(flag == 1)
				//タイル解放
				TileImage_freeTile_atPixel(img, topx + ix, topy + iy);
		}
	}

	return TRUE;
}



//==================================
// タイルイメージ
//==================================
/*
 * TileImageInfo : 描画前のイメージ情報
 * 4byte : タイルの数
 * [タイルデータ]
 *   2byte x 2 : タイル位置
 *   2byte : 圧縮前サイズ (0 で空、最上位ビットON: UNDO 書き込み時に元が空)
 *   2byte : 圧縮サイズ (0 で空、データサイズと同じで無圧縮)
 *   圧縮データ
 */


/** アンドゥ書き込み時の情報取得
 *
 * @return 全データのサイズ (-1 でファイルに出力) */

static int _get_writeinfo_tiles(TileImage *img,uint32_t *ptilenum)
{
	uint8_t **pptile;
	int fullsize,maxsize;
	uint32_t i,num = 0;

	//データサイズが maxsize を超えた場合はファイルに出力する
	//(0 でファイルへ)

	maxsize = _get_bufmaxsize();

	//タイル数計算 + データサイズ計算

	fullsize = sizeof(TileImageInfo) + 4;

	pptile = img->ppbuf;

	for(i = img->tilew * img->tileh; i; i--, pptile++)
	{
		if(*pptile)
		{
			num++;
			fullsize += 8;

			//圧縮サイズ

			if(*pptile != TILEIMAGE_TILE_EMPTY)
			{
				if(fullsize < maxsize)
					fullsize += _get_enctilesize(*pptile);
			}
		}
	}

	//

	*ptilenum = num;

	return (fullsize < maxsize)? fullsize: -1;
}

/** 反転書き込み時のデータサイズ取得
 *
 * p は元データ
 *
 * @return -2:エラー -1:ファイルへ */

static int _get_rev_writesize(UndoItem *p,TileImage *img,int settype)
{
	TileImageInfo info;
	uint32_t tilenum,i;
	uint16_t tilepos[2],wsize[2];
	uint8_t *ptile;
	int ret = -2,fullsize,maxsize,toffx,toffy;

	maxsize = _get_bufmaxsize();
	if(maxsize == 0) return -1;

	//----------

	if(!UndoItem_openRead(p)) return -2;

	//イメージ情報とタイル数

	if(!UndoItem_read(p, &info, sizeof(TileImageInfo))
		|| !UndoItem_read(p, &tilenum, 4))
		goto ERR;

	//

	fullsize = sizeof(TileImageInfo) + 4;

	toffx = (info.offx - img->offx) >> 6;
	toffy = (info.offy - img->offy) >> 6;

	//タイル

	for(i = tilenum; i; i--)
	{
		//サイズ最大

		if(fullsize > maxsize) break;
	
		//位置とサイズ
		
		if(!UndoItem_read(p, tilepos, 4)
			|| !UndoItem_read(p, wsize, 4))
			goto ERR;

		UndoItem_readSeek(p, wsize[1]);

		fullsize += 8;

		//対象のタイル

		if(settype == MUNDO_TYPE_REDO)
		{
			//UNDO -> REDO

			ptile = TILEIMAGE_GETTILE_PT(img, tilepos[0], tilepos[1]);
		}
		else
		{
			//REDO -> UNDO

			if(wsize[0] & (1<<15))
				ptile = NULL;
			else
				ptile = TILEIMAGE_GETTILE_PT(img, tilepos[0] + toffx, tilepos[1] + toffy);
		}

		//タイル圧縮サイズ

		if(ptile)
			fullsize += _get_enctilesize(ptile);
	}

	ret = (fullsize < maxsize)? fullsize: -1;

ERR:
	UndoItem_closeRead(p);

	return ret;
}

/** アンドゥイメージ書き込み */

mBool UndoItem_writeUndoTilesImage(UndoItem *p,TileImageInfo *info)
{
	TileImage *img;
	uint32_t tilenum;
	uint16_t tilepos[2],wsize[2];
	int fullsize,tw,th,ix,iy,srcsize,encsize;
	uint8_t *srcbuf,*encbuf,**pptile;

	img = APP_DRAW->tileimgDraw;

	//情報

	fullsize = _get_writeinfo_tiles(img, &tilenum);

	//------ 書き込み

	if(!UndoItem_alloc(p, fullsize)
		|| !UndoItem_openWrite(p))
		return FALSE;

	//イメージ情報、タイル数

	UndoItem_write(p, info, sizeof(TileImageInfo));
	UndoItem_write(p, &tilenum, 4);

	//タイルデータ

	srcbuf = g_app_undo->tilebuf_src;
	encbuf = g_app_undo->tilebuf_enc;

	pptile = img->ppbuf;
	tw = img->tilew;
	th = img->tileh;

	for(iy = 0; iy < th; iy++)
	{
		for(ix = 0; ix < tw; ix++, pptile++)
		{
			if(!(*pptile)) continue;

			//タイル位置

			tilepos[0] = ix;
			tilepos[1] = iy;

			UndoItem_write(p, tilepos, 4);

			//タイルデータ

			if(*pptile == TILEIMAGE_TILE_EMPTY)
			{
				//元が空タイル

				wsize[0] = 1<<15;
				wsize[1] = 0;
				UndoItem_write(p, wsize, 4);
			}
			else
			{
				//圧縮

				srcsize = TileImage_getTile_forUndo(srcbuf, *pptile);

				encsize = CompressUndoTile(srcsize, 64 * 64);

				wsize[0] = srcsize;
				wsize[1] = (encsize == -1 || encsize == srcsize)? srcsize: encsize;

				UndoItem_write(p, wsize, 4);

				if(encsize == -1)
					UndoItem_write(p, srcbuf, srcsize);
				else
					UndoItem_write(p, encbuf, encsize);
			}
		}
	}

	UndoItem_closeWrite(p);
	
	return TRUE;
}

/** 反転イメージを書き込み */

mBool UndoItem_writeUndoTilesReverseImage(UndoItem *dst,UndoItem *src,int settype)
{
	TileImage *img;
	int fullsize,toffx,toffy,srcsize,encsize;
	TileImageInfo info;
	uint32_t tilenum,i;
	uint16_t tilepos[2],wsize[2];
	uint8_t *ptile,*srcbuf,*encbuf;
	mBool ret = FALSE;

	//イメージ

	img = _get_layerimg(dst);
	if(!img) return FALSE;

	//データサイズ取得

	fullsize = _get_rev_writesize(src, img, settype);
	if(fullsize == -2) return FALSE;

	//--------- 書き込み

	//元データ情報

	if(!UndoItem_openRead(src)) return FALSE;

	if(!UndoItem_read(src, &info, sizeof(TileImageInfo))
		|| !UndoItem_read(src, &tilenum, 4))
		goto ERR;

	//

	toffx = (info.offx - img->offx) >> 6;
	toffy = (info.offy - img->offy) >> 6;

	//書き込み

	if(!UndoItem_alloc(dst, fullsize)
		|| !UndoItem_openWrite(dst))
		goto ERR;

	TileImage_getInfo(img, &info);

	UndoItem_write(dst, &info, sizeof(TileImageInfo));
	UndoItem_write(dst, &tilenum, 4);

	//タイル

	srcbuf = g_app_undo->tilebuf_src;
	encbuf = g_app_undo->tilebuf_enc;

	for(i = tilenum; i; i--)
	{
		//src 読み込み

		if(!UndoItem_read(src, tilepos, 4)
			|| !UndoItem_read(src, wsize, 4))
			goto ERR;

		UndoItem_readSeek(src, wsize[1]);

		//位置書き込み

		UndoItem_write(dst, tilepos, 4);

		//対象タイル

		if(settype == MUNDO_TYPE_REDO)
		{
			//UNDO -> REDO

			ptile = TILEIMAGE_GETTILE_PT(img, tilepos[0], tilepos[1]);
		}
		else
		{
			//REDO -> UNDO

			if(wsize[0] & (1<<15))
				ptile = NULL;
			else
				ptile = TILEIMAGE_GETTILE_PT(img, tilepos[0] + toffx, tilepos[1] + toffy);
		}

		//サイズ & データ

		wsize[0] &= (1<<15);

		if(!ptile)
		{
			//空タイル

			wsize[1] = 0;
			UndoItem_write(dst, wsize, 4);
		}
		else
		{
			//圧縮

			srcsize = TileImage_getTile_forUndo(srcbuf, ptile);

			encsize = CompressUndoTile(srcsize, 64 * 64);

			wsize[0] |= srcsize;
			wsize[1] = (encsize == -1 || encsize == srcsize)? srcsize: encsize;

			UndoItem_write(dst, wsize, 4);

			if(encsize == -1)
				UndoItem_write(dst, srcbuf, srcsize);
			else
				UndoItem_write(dst, encbuf, encsize);
		}
	}

	ret = TRUE;

ERR:
	UndoItem_closeRead(src);
	UndoItem_closeWrite(dst);

	return ret;
}

/** タイルイメージ復元 */

mBool UndoItem_restoreUndoTilesImage(UndoItem *p,int runtype)
{
	TileImage *img;
	TileImageInfo info;
	uint32_t tilenum,i;
	uint16_t tilepos[2],wsize[2];
	uint8_t *ptile,*srcbuf,*encbuf;
	mBool ret = FALSE;

	//イメージ

	img = _get_layerimg(p);
	if(!img) return FALSE;

	//情報

	if(!UndoItem_openRead(p)) return FALSE;

	if(!UndoItem_read(p, &info, sizeof(TileImageInfo))
		|| !UndoItem_read(p, &tilenum, 4))
		goto ERR;

	//配列リサイズ (リドゥ時)

	if(runtype == MUNDO_TYPE_REDO
		&& !TileImage_resizeTileBuf_forUndo(img, &info))
		goto ERR;

	//タイル

	srcbuf = g_app_undo->tilebuf_src;
	encbuf = g_app_undo->tilebuf_enc;

	for(i = tilenum; i; i--)
	{
		//情報

		if(!UndoItem_read(p, tilepos, 4)
			|| !UndoItem_read(p, wsize, 4))
			goto ERR;

		//タイル復元

		wsize[0] &= ~(1<<15);

		if(wsize[0] == 0)
			//空タイル => 解放
			TileImage_freeTile_atPos(img, tilepos[0], tilepos[1]);
		else
		{
			//読み込み

			ptile = TileImage_getTileAlloc_atpos(img, tilepos[0], tilepos[1]);
			if(!ptile) goto ERR;

			if(wsize[0] == wsize[1])
			{
				//無圧縮

				if(!UndoItem_read(p, srcbuf, wsize[0]))
					goto ERR;
			}
			else
			{
				//展開

				if(!UndoItem_read(p, encbuf, wsize[1])
					|| !UncompressUndoTile(wsize[0], wsize[1], 64 * 64))
					goto ERR;
			}

			TileImage_setTile_forUndo(ptile, srcbuf);
		}
	}

	//配列リサイズ (アンドゥ時)

	if(runtype == MUNDO_TYPE_UNDO
		&& !TileImage_resizeTileBuf_forUndo(img, &info))
		goto ERR;

	//

	ret = TRUE;

ERR:
	UndoItem_closeRead(p);

	return ret;
}


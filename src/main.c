/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**********************************
 * main
 **********************************/

#include "mDef.h"
#include "mStr.h"
#include "mGui.h"
#include "mFont.h"
#include "mDockWidget.h"

#include "defMacros.h"
#include "defConfig.h"
#include "defDraw.h"
#include "defWidgets.h"

#include "ConfigData.h"
#include "AppCursor.h"
#include "ColorPalette.h"
#include "DotPenStyle.h"
#include "TextureLibrary.h"
#include "GradationList.h"
#include "BrushList.h"
#include "Undo.h"
#include "DrawFont.h"

#include "MainWindow.h"
#include "DockObject.h"
#include "Docks_external.h"

#include "draw_main.h"

#include "deftrans.h"

//イメージデータ、グローバル定義
#define IMAGEPATTERN_DEFINE
#include "dataImagePattern.h"


//-----------------------
/* グローバル変数定義 */

WidgetsData g_app_widgets_body,	//WidgetsData 本体
	*g_app_widgets = NULL;
ConfigData *g_app_config = NULL;
DrawData *g_app_draw = NULL;

//-----------------------

/* configfile.c */
void appLoadConfig(mDockWidgetState *dock_state);
void appSaveConfig();

/* 各パレットオブジェクト作成 */
void DockTool_new(mDockWidgetState *state);
void DockOption_new(mDockWidgetState *state);
void DockLayer_new(mDockWidgetState *state);
void DockColor_new(mDockWidgetState *state);
void DockColorPalette_new(mDockWidgetState *state);
void DockTexture_new(mDockWidgetState *state);
void DockCanvasCtrl_new(mDockWidgetState *state);
void DockCanvasView_new(mDockWidgetState *state);
void DockImageViewer_new(mDockWidgetState *state);
void DockFilterList_new(mDockWidgetState *state);
void DockColorWheel_new(mDockWidgetState *state);

//-----------------------


//===========================
// 初期化
//===========================


/** 設定ファイルのデフォルト構成
 *
 * 設定ファイルディレクトリにファイルがなければ、
 * データディレクトリからコピー */

static void _set_config_default()
{
	mAppCopyFile_dataToConfig(CONFIG_FILENAME_BRUSHLIST);
	mAppCopyFile_dataToConfig(CONFIG_FILENAME_TEXTURELIB);
	mAppCopyFile_dataToConfig(CONFIG_FILENAME_GRADATION);
	mAppCopyFile_dataToConfig(CONFIG_FILENAME_SHORTCUTKEY);
}

/** 初期化 */

static mBool _init(int argc,char **argv)
{
	mDockWidgetState dock_state[DOCKWIDGET_NUM];
	int i;

	//フォント初期化

	if(!DrawFont_init()) return FALSE;

	//設定ファイル用ディレクトリ作成
	/* 新しく作成された場合、デフォルトのファイル構成に */

	if(mAppCreateConfigDir(NULL) == 0)
		_set_config_default();

	//データ初期化

	mMemzero(&g_app_widgets_body, sizeof(WidgetsData));
	g_app_widgets = &g_app_widgets_body;

	ColorPalette_init();

	TextureLibrary_init();

	GradationList_init();

	if(!DotPenStyle_init()) return FALSE;

	//データ確保

	if(!ConfigData_new()
		|| !DrawData_new()
		|| !BrushList_new()
		|| !Undo_new())
		return FALSE;

	//設定ファイル読み込み

	appLoadConfig(dock_state);

	ConfigData_loadGrid(&APP_CONF->grid_list, NULL);
	ColorPalette_load();
	GradationList_load();
	BrushList_loadConfig();

	APP_DRAW->tex.curimg = TextureLibrary_loadConfig();

	//設定

	Undo_setMaxNum(APP_CONF->undo_maxnum);

	//作業用ディレクトリ作成

	ConfigData_createTempDir();

	//カーソル作成 (設定読込後に行う)

	AppCursor_init(APP_CONF->cursor_buf);

	//フォント作成

	mAppSetDefaultFont(APP_CONF->strFontStyle_gui.buf);

	APP_WIDGETS->font_dock = mFontCreateFromFormat(APP_CONF->strFontStyle_dock.buf);
	APP_WIDGETS->font_dock12px = mFontCreateFromFormat_size(APP_CONF->strFontStyle_dock.buf, -12);

	//テーマ読み込み

	mAppLoadThemeFile(APP_CONF->strThemeFile.buf);

	//メインウィンドウ作成

	MainWindow_new();

	//DockObject 作成

	DockTool_new(dock_state + DOCKWIDGET_TOOL);
	DockOption_new(dock_state + DOCKWIDGET_OPTION);
	DockLayer_new(dock_state + DOCKWIDGET_LAYER);
	DockColor_new(dock_state + DOCKWIDGET_COLOR);
	DockColorPalette_new(dock_state + DOCKWIDGET_COLOR_PALETTE);
	DockTexture_new(dock_state + DOCKWIDGET_TEXTURE);
	DockCanvasCtrl_new(dock_state + DOCKWIDGET_CANVAS_CTRL);
	DockCanvasView_new(dock_state + DOCKWIDGET_CANVAS_VIEW);
	DockImageViewer_new(dock_state + DOCKWIDGET_IMAGE_VIEWER);
	DockFilterList_new(dock_state + DOCKWIDGET_FILTER_LIST);
	DockColorWheel_new(dock_state + DOCKWIDGET_COLOR_WHEEL);

	//イメージなど初期化

	drawInit_beforeShow();

	//ウィンドウ表示

	MainWindow_showStart(APP_WIDGETS->mainwin);

	for(i = 0; i < DOCKWIDGET_NUM; i++)
		DockObject_showStart(APP_WIDGETS->dockobj[i]);

	//表示後更新

	DockCanvasView_changeImageSize();
	DockLayer_update_all();

	//ファイルを開く

	if(argc >= 2)
	{
		mStr str = MSTR_INIT;

		mStrSetTextLocal(&str, argv[1], -1);
		
		MainWindow_loadImage(APP_WIDGETS->mainwin, str.buf);

		mStrFree(&str);
	}

	return TRUE;
}


//===========================
// メイン
//===========================


/** 終了処理 */

static void _finish()
{
	int i;

	//設定ファイル保存

	appSaveConfig();

	ConfigData_saveGrid(&APP_CONF->grid_list, NULL);
	ColorPalette_savefile();
	TextureLibrary_saveConfig(APP_DRAW->tex.curimg, APP_DRAW->tex.save_curimg);
	GradationList_save();
	BrushList_saveConfig();

	//解放

	for(i = 0; i < DOCKWIDGET_NUM; i++)
	{
		DockObject_destroy(APP_WIDGETS->dockobj[i]);
		APP_WIDGETS->dockobj[i] = NULL;
	}

	mFontFree(APP_WIDGETS->font_dock);
	mFontFree(APP_WIDGETS->font_dock12px);

	AppCursor_free();

	DrawData_free();

	ColorPalette_free();
	DotPenStyle_free();
	TextureLibrary_free();
	GradationList_free();
	BrushList_free();
	Undo_free();

	DrawFont_finish();

	//作業用ディレクトリ削除

	ConfigData_deleteTempDir();

	ConfigData_free();
}

/** メイン */

int main(int argc,char **argv)
{
	if(mAppInit(&argc, argv)) return 1;

	mAppSetClassName("AzPainterB", "AzPainterB");

	mAppInitPenTablet();

	//パス

	mAppSetConfigPath(".azpainterb", TRUE);
	mAppSetDataPath(PACKAGE_DATA_DIR);

	//翻訳データ

	mAppLoadTranslation(g_deftransdat, NULL, "tr");

	//初期化

	if(!_init(argc, argv))
	{
		mDebug("! failed initialize\n");
		return 1;
	}

	//実行

	mAppRun();

	//終了

	_finish();

	mAppEnd();

	return 0;
}

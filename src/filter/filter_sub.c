/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**************************************
 * フィルタ処理
 *
 * サブ関数
 **************************************/

#include <math.h>

#include "mDef.h"
#include "mPopupProgress.h"
#include "mRandXorShift.h"
#include "mColorConv.h"

#include "defDraw.h"

#include "TileImage.h"
#include "TileImageDrawInfo.h"
#include "LayerItem.h"
#include "LayerList.h"

#include "FilterDrawInfo.h"
#include "filter_sub.h"


//========================
// 描画処理
//========================


/** 共通描画処理
 *
 * 関数で TRUE が返ったら色をセット */

mBool FilterSub_drawCommon(FilterDrawInfo *info,FilterDrawCommonFunc func)
{
	TileImage *imgsrc,*imgdst;
	TileImageSetPixelFunc setpix; 
	int ix,iy;
	PixelRGBA pix;

	imgsrc = info->imgsrc;
	imgdst = info->imgdst;

	setpix = g_tileimage_dinfo.funcDrawPixel;

	for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
	{
		for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
		{
			TileImage_getPixel(imgsrc, ix, iy, &pix);

			if((func)(info, ix, iy, &pix))
				(setpix)(imgdst, ix, iy, &pix);
		}

		if(info->prog)
			mPopupProgressThreadIncSubStep(info->prog);
	}

	return TRUE;
}

/** 3x3 フィルタ (RGB のみ) */

mBool FilterSub_draw3x3(FilterDrawInfo *info,Filter3x3Info *dat)
{
	TileImage *imgsrc,*imgdst;
	int ix,iy,ixx,iyy,n;
	double d[3],divmul,add,dd;
	PixelRGBA pix,pix2;
	TileImageSetPixelFunc setpix = g_tileimage_dinfo.funcDrawPixel; 

	imgsrc = info->imgsrc;
	imgdst = info->imgdst;

	divmul = dat->divmul;
	add = dat->add;

	for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
	{
		for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
		{
			TileImage_getPixel(imgsrc, ix, iy, &pix);
			if(pix.a == 0) continue;

			//3x3

			d[0] = d[1] = d[2] = 0;

			for(iyy = -1, n = 0; iyy <= 1; iyy++)
			{
				for(ixx = -1; ixx <= 1; ixx++, n++)
				{
					if(info->clipping)
						TileImage_getPixel_clip(imgsrc, ix + ixx, iy + iyy, &pix2);
					else
						TileImage_getPixel(imgsrc, ix + ixx, iy + iyy, &pix2);

					dd = dat->mul[n];

					d[0] += pix2.r * dd;
					d[1] += pix2.g * dd;
					d[2] += pix2.b * dd;
				}
			}

			//RGB

			for(ixx = 0; ixx < 3; ixx++)
			{
				n = (int)(d[ixx] * divmul + add);
				if(n < 0) n = 0;
				else if(n > 255) n = 255;

				pix.ar[ixx] = n;
			}

			//グレイスケール化

			if(dat->grayscale)
				pix.r = pix.g = pix.b = RGB_TO_GRAY(pix.r, pix.g, pix.b);

			//セット

			(setpix)(imgdst, ix, iy, &pix);
		}

		FilterSub_progIncSubStep(info);
	}

	return TRUE;
}

/** ソース画像を出力先にコピー (プレビュー用)
 *
 * imgdst は空状態である。 */

void FilterSub_copyImage_forPreview(FilterDrawInfo *info)
{
	if(info->in_dialog)
		TileImage_copyImage_inRect(info->imgdst, info->imgsrc, &info->rc, 0, 0);
}


//============================
// ガウスぼかし
//============================


/** ガウスぼかし処理
 *
 * @param setpix NULL で imgdst に通常描画 */

mBool FilterSub_gaussblur(FilterDrawInfo *info,
	TileImage *imgsrc,int radius,FilterDrawGaussBlurFunc setpix,FilterDrawGaussBlurSrcFunc setsrc)
{
	PixelRGBA *buf1,*buf2,pix,*ps,*psY,*pd;
	int ix,iy,jx,jy,i,range,pixlen,xnum,ynum,pos,blurlen,srcpixnum;
	double d[4],*gaussbuf,dtmp,weight;
	mRect rc;
	TileImageSetPixelFunc setpix_def = g_tileimage_dinfo.funcDrawPixel;

	range = radius * 3;
	pixlen = range * 2 + 64;
	blurlen = range * 2 + 1;
	srcpixnum = pixlen * pixlen;

	//バッファ

	buf1 = (PixelRGBA *)mMalloc(4 * pixlen * pixlen, FALSE);
	buf2 = (PixelRGBA *)mMalloc(4 * 64 * pixlen, FALSE);

	if(!buf1 || !buf2)
	{
		mFree(buf1);
		return FALSE;
	}

	//ガウステーブル

	gaussbuf = (double *)mMalloc(sizeof(double) * blurlen, FALSE);
	if(!gaussbuf)
	{
		mFree(buf1);
		mFree(buf2);
		return FALSE;
	}

	dtmp = 1.0 / (2 * radius * radius);
	weight = 0;

	for(i = 0, pos = -range; i < blurlen; i++, pos++)
	{
		gaussbuf[i] = exp(-(pos * pos) * dtmp);
		weight += gaussbuf[i];
	}

	weight = 1.0 / weight;

	//64x64 単位で処理

	FilterSub_progBeginOneStep(info, 50, ((info->box.w + 63) / 64) * ((info->box.h + 63) / 64));

	rc.y1 = info->rc.y1 - range;
	rc.y2 = rc.y1 + pixlen - 1;

	for(iy = info->rc.y1; iy <= info->rc.y2; iy += 64)
	{
		rc.x1 = info->rc.x1 - range;
		rc.x2 = rc.x1 + pixlen - 1;
	
		for(ix = info->rc.x1; ix <= info->rc.x2; ix += 64, rc.x1 += 64, rc.x2 += 64)
		{
			//buf1 にソースセット (すべて透明なら処理なし)
		
			if(FilterSub_getPixels(imgsrc, &rc, buf1, info->clipping))
			{
				FilterSub_progIncSubStep(info);
				continue;
			}

			//ソースの色を変更

			if(setsrc)
				(setsrc)(buf1, srcpixnum, info);

			//水平方向
			/* src: buf1 [x]pixlen x [y]pixlen
			 * dst: buf2 [y]pixlen x [x]64 */

			ps = buf1 + range;
			pd = buf2;

			for(jy = 0; jy < pixlen; jy++, pd++)
			{
				for(jx = 0, pos = 0; jx < 64; jx++, ps++, pos += pixlen)
				{
					d[0] = d[1] = d[2] = d[3] = 0;

					for(i = 0; i < blurlen; i++)
						FilterSub_addAdvColor_weight(d, gaussbuf[i], ps + i - range);

					FilterSub_getAdvColor(d, weight, pd + pos);
				}

				ps += pixlen - 64;
			}

			//垂直方向

			xnum = ynum = 64;
			if(ix + 63 > info->rc.x2) xnum = info->rc.x2 + 1 - ix;
			if(iy + 63 > info->rc.y2) ynum = info->rc.y2 + 1 - iy;

			psY = buf2 + range;

			for(jx = 0; jx < xnum; jx++)
			{
				for(jy = 0, ps = psY; jy < ynum; jy++, ps++)
				{
					d[0] = d[1] = d[2] = d[3] = 0;

					for(i = 0; i < blurlen; i++)
						FilterSub_addAdvColor_weight(d, gaussbuf[i], ps + i - range);

					FilterSub_getAdvColor(d, weight, &pix);

					if(setpix)
						(setpix)(ix + jx, iy + jy, &pix, info);
					else
						(setpix_def)(info->imgdst, ix + jx, iy + jy, &pix);
				}

				psY += pixlen;
			}

			FilterSub_progIncSubStep(info);
		}

		rc.y1 += 64;
		rc.y2 += 64;
	}

	mFree(buf1);
	mFree(buf2);
	mFree(gaussbuf);

	return TRUE;
}


//========================
// タイル
//========================


/** イメージの指定範囲の色をバッファに取得
 *
 * @return すべて透明か */

mBool FilterSub_getPixels(TileImage *img,mRect *rc,PixelRGBA *buf,mBool clipping)
{
	int ix,iy,f = 0;

	for(iy = rc->y1; iy <= rc->y2; iy++)
	{
		for(ix = rc->x1; ix <= rc->x2; ix++, buf++)
		{
			if(clipping)
				TileImage_getPixel_clip(img, ix, iy, buf);
			else
				TileImage_getPixel(img, ix, iy, buf);

			if(buf->a) f = 1;
		}
	}

	return (f == 0);
}


//========================
// 平均色
//========================


/** 平均色用に色を追加 */

void FilterSub_addAdvColor(double *d,PixelRGBA *pix)
{
	if(pix->a == 255)
	{
		d[0] += pix->r;
		d[1] += pix->g;
		d[2] += pix->b;
		d[3] += 1;
	}
	else if(pix->a)
	{
		double da = pix->a / 255.0;

		d[0] += pix->r * da;
		d[1] += pix->g * da;
		d[2] += pix->b * da;
		d[3] += da;
	}
}

/** 平均色用に色を追加 (重みあり) */

void FilterSub_addAdvColor_weight(double *d,double weight,PixelRGBA *pix)
{
	if(pix->a == 255)
	{
		d[0] += pix->r * weight;
		d[1] += pix->g * weight;
		d[2] += pix->b * weight;
		d[3] += weight;
	}
	else if(pix->a)
	{
		double a = pix->a / 255.0 * weight;

		d[0] += pix->r * a;
		d[1] += pix->g * a;
		d[2] += pix->b * a;
		d[3] += a;
	}
}

/** 平均色用に色を追加 (イメージの指定位置) */

void FilterSub_addAdvColor_at(TileImage *img,int x,int y,double *d,mBool clipping)
{
	PixelRGBA pix;

	if(clipping)
		TileImage_getPixel_clip(img, x, y, &pix);
	else
		TileImage_getPixel(img, x, y, &pix);

	FilterSub_addAdvColor(d, &pix);
}

/** 平均色用に色を追加 (指定位置 & 重みあり) */

void FilterSub_addAdvColor_at_weight(TileImage *img,int x,int y,double *d,double weight,mBool clipping)
{
	PixelRGBA pix;

	if(clipping)
		TileImage_getPixel_clip(img, x, y, &pix);
	else
		TileImage_getPixel(img, x, y, &pix);

	FilterSub_addAdvColor_weight(d, weight, &pix);
}

/** 平均色を取得 */

void FilterSub_getAdvColor(double *d,double weight_mul,PixelRGBA *pix)
{
	int a;
	double da;

	a = (int)(d[3] * weight_mul * 255 + 0.5);

	if(a == 0)
		pix->c = 0;
	else
	{
		da = 1.0 / d[3];

		pix->r = (int)(d[0] * da + 0.5);
		pix->g = (int)(d[1] * da + 0.5);
		pix->b = (int)(d[2] * da + 0.5);
		pix->a = a;
	}
}


//========================
// 線形補間
//========================


/** 色取得 */

static void _getlinercolor_getpixel(TileImage *img,int x,int y,PixelRGBA *pix,uint8_t flags)
{
	if(flags & 2)
	{
		//ループ

		int max;

		max = g_tileimage_dinfo.imgw;

		while(x < 0) x += max;
		while(x >= max) x -= max;

		max = g_tileimage_dinfo.imgh;

		while(y < 0) y += max;
		while(y >= max) y -= max;
	}

	if(flags & 1)
		TileImage_getPixel_clip(img, x, y, pix);
	else
		TileImage_getPixel(img, x, y, pix);
}

/** 線形補間で指定位置の色取得
 *
 * @param flags 0bit:クリッピング 1bit:ループ */

void FilterSub_getLinerColor(TileImage *img,double x,double y,PixelRGBA *pixdst,uint8_t flags)
{
	double d1,d2,fx1,fx2,fy1,fy2;
	int ix,iy,i,n;
	PixelRGBA pix[4];

	d2 = modf(x, &d1); if(d2 < 0) d2 += 1;
	ix = floor(d1);
	fx1 = d2;
	fx2 = 1 - d2;

	d2 = modf(y, &d1); if(d2 < 0) d2 += 1;
	iy = floor(d1);
	fy1 = d2;
	fy2 = 1 - d2;

	//周囲4点の色取得

	_getlinercolor_getpixel(img, ix, iy, pix, flags);
	_getlinercolor_getpixel(img, ix + 1, iy, pix + 1, flags);
	_getlinercolor_getpixel(img, ix, iy + 1, pix + 2, flags);
	_getlinercolor_getpixel(img, ix + 1, iy + 1, pix + 3, flags);

	//全て透明なら透明に

	if(pix[0].a + pix[1].a + pix[2].a + pix[3].a == 0)
	{
		pixdst->c = 0;
		return;
	}

	//アルファ値

	d1 = fy2 * (fx2 * pix[0].a + fx1 * pix[1].a) +
	     fy1 * (fx2 * pix[2].a + fx1 * pix[3].a);

	pixdst->a = (int)(d1 + 0.5);

	//RGB

	if(pixdst->a == 0)
		pixdst->c = 0;
	else
	{
		for(i = 0; i < 3; i++)
		{
			d2 = fy2 * (fx2 * pix[0].ar[i] * pix[0].a + fx1 * pix[1].ar[i] * pix[1].a) +
			     fy1 * (fx2 * pix[2].ar[i] * pix[2].a + fx1 * pix[3].ar[i] * pix[3].a);

			n = (int)(d2 / d1 + 0.5);
			if(n > 255) n = 255;

			pixdst->ar[i] = n;
		}
	}
}

/** イメージ範囲外時の背景指定の線形補間色取得
 *
 * @param type 0:透明 1:端の色 2:元のまま */

void FilterSub_getLinerColor_bkgnd(TileImage *img,double dx,double dy,
	int x,int y,PixelRGBA *pix,int type)
{
	if(type == 1
		|| (dx >= 0 && dx < g_tileimage_dinfo.imgw && dy >= 0 && dy < g_tileimage_dinfo.imgh))
		FilterSub_getLinerColor(img, dx, dy, pix, 1);
	else if(type == 0)
		pix->c = 0;
	else if(type == 2)
		TileImage_getPixel(img, x, y, pix);
}


//========================
// 進捗
//========================


void FilterSub_progSetMax(FilterDrawInfo *info,int max)
{
	if(info->prog)
		mPopupProgressThreadSetMax(info->prog, max);
}

void FilterSub_progInc(FilterDrawInfo *info)
{
	if(info->prog)
		mPopupProgressThreadIncPos(info->prog);
}

void FilterSub_progIncSubStep(FilterDrawInfo *info)
{
	if(info->prog)
		mPopupProgressThreadIncSubStep(info->prog);
}

void FilterSub_progBeginOneStep(FilterDrawInfo *info,int step,int max)
{
	if(info->prog)
		mPopupProgressThreadBeginSubStep_onestep(info->prog, step, max);
}

void FilterSub_progBeginSubStep(FilterDrawInfo *info,int step,int max)
{
	if(info->prog)
		mPopupProgressThreadBeginSubStep(info->prog, step, max);
}


//========================
// 取得
//========================


/** キャンバスイメージサイズを取得 */

void FilterSub_getImageSize(int *w,int *h)
{
	*w = g_tileimage_dinfo.imgw;
	*h = g_tileimage_dinfo.imgh;
}

/** TileImage 点描画関数を取得 */

void FilterSub_getPixelFunc(TileImageSetPixelFunc *func)
{
	*func = g_tileimage_dinfo.funcDrawPixel;
}

/** 描画する色を取得 */

void FilterSub_getDrawColor(FilterDrawInfo *info,int type,PixelRGBA *pix)
{
	switch(type)
	{
		//描画色
		case 0:
			*pix = info->pixdraw;
			break;
		//背景色
		case 1:
			*pix = info->pixbkgnd;
			break;
		//黒
		case 2:
			pix->r = pix->g = pix->b = 0;
			break;
		//白
		default:
			pix->r = pix->g = pix->b = 255;
			break;
	}

	pix->a = 255;
}

/** ソース画像から色を取得 (クリッピング設定有効) */

void FilterSub_getPixelSrc_clip(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	if(info->clipping)
		TileImage_getPixel_clip(info->imgsrc, x, y, pix);
	else
		TileImage_getPixel(info->imgsrc, x, y, pix);
}

/** チェックされている先頭のレイヤの画像を取得 */

TileImage *FilterSub_getCheckedLayerImage()
{
	LayerItem *item;

	item = LayerList_setLink_checked(APP_DRAW->layerlist);

	return (item)? item->img: NULL;
}

/** 円形の形状 (1bit) バッファを作成
 *
 * @param pixnum  点の数が返る */

uint8_t *FilterSub_createCircleShape(int radius,int *pixnum)
{
	uint8_t *buf,*pd,f;
	int ix,iy,xx,yy,size,num = 0;

	size = radius * 2 + 1;

	buf = (uint8_t *)mMalloc((size * size + 7) >> 3, TRUE);
	if(!buf) return NULL;

	pd = buf;
	f = 0x80;

	for(iy = 0; iy < size; iy++)
	{
		yy = iy - radius;
		yy *= yy;

		for(ix = 0, xx = -radius; ix < size; ix++, xx++)
		{
			if(xx * xx + yy <= radius)
			{
				*pd |= f;
				num++;
			}

			f >>= 1;
			if(f == 0) f = 0x80, pd++;
		}
	}

	*pixnum = num;

	return buf;
}


//========================
// 点描画
//========================


/** 描画色取得 */

static void _drawpoint_get_color(FilterDrawPointInfo *dat,PixelRGBA *pix)
{
	int i,c[3];

	switch(dat->coltype)
	{
		//描画色
		case 0:
			*pix = dat->info->pixdraw;
			break;
		//ランダム (グレイスケール)
		case 1:
			pix->r = pix->g = pix->b = mRandXorShift_getIntRange(0, 255);
			break;
		//ランダム (色相)
		case 2:
			mHSVtoRGB(mRandXorShift_getIntRange(0, 359) / 360.0, 1, 1, c);

			pix->r = c[0];
			pix->g = c[1];
			pix->b = c[2];
			break;
		//ランダム (RGB)
		default:
			for(i = 0; i < 3; i++)
				pix->ar[i] = mRandXorShift_getIntRange(0, 255);
			break;
	}

	pix->a = dat->opacity;
}

/** 1px 点を打つ */

static void _drawpoint_setpixel(int x,int y,PixelRGBA *pix,FilterDrawPointInfo *dat)
{
	mRect rc;

	//縁取り時のマスク

	if(dat->masktype == 1)
	{
		//外側:不透明部分には描画しない
		if(TileImage_isPixelOpaque(dat->imgsrc, x, y)) return;
	}
	else if(dat->masktype == 2)
	{
		//内側:透明部分には描画しない
		if(TileImage_isPixelTransparent(dat->imgsrc, x, y)) return;
	}

	//範囲外は描画しない

	rc = dat->info->rc;

	if(rc.x1 <= x && x <= rc.x2 && rc.y1 <= y && y <= rc.y2)
		(g_tileimage_dinfo.funcDrawPixel)(dat->imgdst, x, y, pix);
}

/** 点を打つ (ドット円) */

static void _drawpoint_setpixel_dot(int x,int y,FilterDrawPointInfo *dat)
{
	int ix,iy,yy,r,rr;
	PixelRGBA pix;

	_drawpoint_get_color(dat, &pix);

	r = dat->radius;
	rr = r * r;

	for(iy = -r; iy <= r; iy++)
	{
		yy = iy * iy;

		for(ix = -r; ix <= r; ix++)
		{
			if(ix * ix + yy <= rr)
				_drawpoint_setpixel(x + ix, y + iy, &pix, dat);
		}
	}
}

/** 点を打つ (アンチエイリアス円) */

static void _drawpoint_setpixel_aa(int x,int y,FilterDrawPointInfo *dat)
{
	int ix,iy,i,j,xtbl[4],ytbl[4],r,rr,srca,cnt;
	PixelRGBA pix;

	_drawpoint_get_color(dat, &pix);
	srca = pix.a;

	r = dat->radius;
	rr = r << 2;
	rr *= rr;

	for(iy = -r; iy <= r; iy++)
	{
		//Y テーブル

		for(i = 0, j = (iy << 2) - 2; i < 4; i++, j++)
			ytbl[i] = j * j;

		//
	
		for(ix = -r; ix <= r; ix++)
		{
			//X テーブル

			for(i = 0, j = (ix << 2) - 2; i < 4; i++, j++)
				xtbl[i] = j * j;

			//4x4

			cnt = 0;

			for(i = 0; i < 4; i++)
			{
				for(j = 0; j < 4; j++)
				{
					if(xtbl[j] + ytbl[i] < rr) cnt++;
				}
			}

			//セット

			if(cnt)
			{
				pix.a = srca * cnt >> 4;
				_drawpoint_setpixel(x + ix, y + iy, &pix, dat);
			}
		}
	}
}

/** 点を打つ (柔らかい円) */

static void _drawpoint_setpixel_soft(int x,int y,FilterDrawPointInfo *dat)
{
	int ix,iy,i,j,xtbl[4],ytbl[4],r,srca;
	double da,dr,rr;
	PixelRGBA pix;

	_drawpoint_get_color(dat, &pix);
	srca = pix.a;

	r = dat->radius;
	rr = 1.0 / ((r << 2) * (r << 2));

	for(iy = -r; iy <= r; iy++)
	{
		//Y テーブル

		for(i = 0, j = (iy << 2) - 2; i < 4; i++, j++)
			ytbl[i] = j * j;

		//
	
		for(ix = -r; ix <= r; ix++)
		{
			//X テーブル

			for(i = 0, j = (ix << 2) - 2; i < 4; i++, j++)
				xtbl[i] = j * j;

			//4x4

			da = 0;

			for(i = 0; i < 4; i++)
			{
				for(j = 0; j < 4; j++)
				{
					dr = (xtbl[j] + ytbl[i]) * rr;

					if(dr < 1.0) da += 1 - dr;
				}
			}

			//セット

			if(da != 0)
			{
				pix.a = (int)(srca * (da * (1.0 / 16)) + 0.5);
				_drawpoint_setpixel(x + ix, y + iy, &pix, dat);
			}
		}
	}
}

/** 点描画の関数を取得 */

void FilterSub_getDrawPointFunc(int type,FilterDrawPointFunc *dst)
{
	FilterDrawPointFunc func[3] = {
		_drawpoint_setpixel_dot, _drawpoint_setpixel_aa, _drawpoint_setpixel_soft
	};

	*dst = func[type];
}

/** 点描画の初期化 */

void FilterSub_initDrawPoint(FilterDrawInfo *info,mBool compalpha)
{
	//プレビュー時は色計算を有効に

	if(info->in_dialog)
		g_tileimage_dinfo.funcDrawPixel = TileImage_setPixel_new_colfunc;

	//色計算

	g_tileimage_dinfo.funcColor = (compalpha)? TileImage_colfunc_compareA: TileImage_colfunc_normal;
}


//========================
// 色変換
//========================


/** RGB -> YCrCb 変換 */

void FilterSub_RGBtoYCrCb(int *val)
{
	int c[3],i;

	c[0] = (val[0] * 77 + val[1] * 150 + val[2] * 29) >> 8;
	c[1] = ((val[0] * 128 - val[1] * 107 - val[2] * 21) >> 8) + 128;
	c[2] = ((val[0] * -43 - val[1] * 85 + val[2] * 128) >> 8) + 128;

	for(i = 0; i < 3; i++)
	{
		if(c[i] < 0) c[i] = 0;
		else if(c[i] > 255) c[i] = 255;

		val[i] = c[i];
	}
}

/** YCrCb -> RGB 変換 */

void FilterSub_YCrCbtoRGB(int *val)
{
	int c[3],i,cr,cb;

	cr = val[1] - 128;
	cb = val[2] - 128;

	c[0] = val[0] + (cr * 359 >> 8);
	c[1] = val[0] - ((cr * 183 + cb * 88) >> 8);
	c[2] = val[0] + (cb * 454 >> 8);

	for(i = 0; i < 3; i++)
	{
		if(c[i] < 0) c[i] = 0;
		else if(c[i] > 255) c[i] = 255;

		val[i] = c[i];
	}
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/***************************************
 * フィルタサブ関数
 ***************************************/

#ifndef FILTER_SUB_H
#define FILTER_SUB_H

#define RGB_TO_GRAY(r,g,b)  (((r) * 77 + (g) * 150 + (b) * 29) >> 8)

typedef struct
{
	double mul[9],divmul,add;
	mBool grayscale;
}Filter3x3Info;

typedef struct
{
	TileImage *imgdst,*imgsrc;
	int coltype,radius,opacity,masktype;
	FilterDrawInfo *info;
}FilterDrawPointInfo;


typedef mBool (*FilterDrawCommonFunc)(FilterDrawInfo *,int,int,PixelRGBA *);
typedef void (*FilterDrawPointFunc)(int,int,FilterDrawPointInfo *);
typedef void (*FilterDrawGaussBlurFunc)(int,int,PixelRGBA *,FilterDrawInfo *);
typedef void (*FilterDrawGaussBlurSrcFunc)(PixelRGBA *,int,FilterDrawInfo *);

/*---------*/

mBool FilterSub_drawCommon(FilterDrawInfo *info,FilterDrawCommonFunc func);
mBool FilterSub_draw3x3(FilterDrawInfo *info,Filter3x3Info *dat);

void FilterSub_copyImage_forPreview(FilterDrawInfo *info);

mBool FilterSub_gaussblur(FilterDrawInfo *info,
	TileImage *imgsrc,int radius,FilterDrawGaussBlurFunc setpix,FilterDrawGaussBlurSrcFunc setsrc);

mBool FilterSub_getPixels(TileImage *img,mRect *rc,PixelRGBA *buf,mBool clipping);

void FilterSub_addAdvColor(double *d,PixelRGBA *pix);
void FilterSub_addAdvColor_weight(double *d,double weight,PixelRGBA *pix);
void FilterSub_addAdvColor_at(TileImage *img,int x,int y,double *d,mBool clipping);
void FilterSub_addAdvColor_at_weight(TileImage *img,int x,int y,double *d,double weight,mBool clipping);
void FilterSub_getAdvColor(double *d,double weight_mul,PixelRGBA *pix);

void FilterSub_getLinerColor(TileImage *img,double x,double y,PixelRGBA *pix,uint8_t flags);
void FilterSub_getLinerColor_bkgnd(TileImage *img,double dx,double dy,
	int x,int y,PixelRGBA *pix,int type);

void FilterSub_progSetMax(FilterDrawInfo *info,int max);
void FilterSub_progInc(FilterDrawInfo *info);
void FilterSub_progIncSubStep(FilterDrawInfo *info);
void FilterSub_progBeginOneStep(FilterDrawInfo *info,int step,int max);
void FilterSub_progBeginSubStep(FilterDrawInfo *info,int step,int max);

void FilterSub_getImageSize(int *w,int *h);
void FilterSub_getPixelFunc(TileImageSetPixelFunc *func);
void FilterSub_getDrawColor(FilterDrawInfo *info,int type,PixelRGBA *pix);
void FilterSub_getPixelSrc_clip(FilterDrawInfo *info,int x,int y,PixelRGBA *pix);
TileImage *FilterSub_getCheckedLayerImage();
uint8_t *FilterSub_createCircleShape(int radius,int *pixnum);

void FilterSub_getDrawPointFunc(int type,FilterDrawPointFunc *dst);
void FilterSub_initDrawPoint(FilterDrawInfo *info,mBool compalpha);

void FilterSub_RGBtoYCrCb(int *val);
void FilterSub_YCrCbtoRGB(int *val);

#endif

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**************************************
 * フィルタ処理
 *
 * いろいろ1
 **************************************/

#include <math.h>

#include "mDef.h"
#include "mRandXorShift.h"

#include "TileImage.h"
#include "TileImageDrawInfo.h"

#include "FilterDrawInfo.h"
#include "filter_sub.h"



//=======================
// ピクセレート
//=======================


/** モザイク */

mBool FilterDraw_mozaic(FilterDrawInfo *info)
{
	int ix,iy,ixx,iyy,size;
	double weight,d[4];
	PixelRGBA pix;
	TileImageSetPixelFunc setpix;

	FilterSub_getPixelFunc(&setpix);

	size = info->val_bar[0];
	weight = 1.0 / (size * size);

	FilterSub_progBeginOneStep(info, 50, (info->box.h + size - 1) / size);

	for(iy = info->rc.y1; iy <= info->rc.y2; iy += size)
	{
		for(ix = info->rc.x1; ix <= info->rc.x2; ix += size)
		{
			//平均色

			d[0] = d[1] = d[2] = d[3] = 0;

			for(iyy = 0; iyy < size; iyy++)
			{
				for(ixx = 0; ixx < size; ixx++)
				{
					FilterSub_addAdvColor_at(info->imgsrc, ix + ixx, iy + iyy, d, info->clipping);
				}
			}

			FilterSub_getAdvColor(d, weight, &pix);

			//セット

			for(iyy = 0; iyy < size; iyy++)
			{
				if(iy + iyy > info->rc.y2) break;
			
				for(ixx = 0; ixx < size; ixx++)
				{
					if(ix + ixx > info->rc.x2) break;
				
					(setpix)(info->imgdst, ix + ixx, iy + iyy, &pix);
				}
			}
		}

		FilterSub_progIncSubStep(info);
	}

	return TRUE;
}


//=======================
// 水晶
//=======================


typedef struct
{
	int cx,cy,colnum;
	double dcol[4];
	PixelRGBA pix;
}_crystal_dat;


/** イメージ位置からブロックのポインタ取得
 *
 * (xpos,ypos) のブロックの周囲から、(x,y) と中心の距離が一番近いものを選択 */

static _crystal_dat *_crystal_get_block(_crystal_dat *buf,
	int x,int y,int xpos,int ypos,int xnum,int ynum)
{
	_crystal_dat *p;
	int ix,iy,minx,miny,minlen,len;

	minlen = -1;

	for(iy = ypos - 1; iy <= ypos + 1; iy++)
	{
		if(iy < 0 || iy >= ynum) continue;

		for(ix = xpos - 1; ix <= xpos + 1; ix++)
		{
			if(ix < 0 || ix >= xnum) continue;

			p = buf + iy * xnum + ix;

			len = (p->cx - x) * (p->cx - x) + (p->cy - y) * (p->cy - y);

			if(minlen < 0 || len < minlen)
			{
				minlen = len;
				minx = ix;
				miny = iy;
			}
		}
	}

	return buf + miny * xnum + minx;
}

mBool FilterDraw_crystal(FilterDrawInfo *info)
{
	_crystal_dat *workbuf,*p;
	int size,xnum,ynum,ix,iy,x,y,xx,yy;
	TileImageSetPixelFunc setpix;

	FilterSub_getPixelFunc(&setpix);

	size = info->val_bar[0];

	xnum = (info->box.w + size - 1) / size;
	ynum = (info->box.h + size - 1) / size;

	//作業用データ確保

	workbuf = (_crystal_dat *)mMalloc(sizeof(_crystal_dat) * xnum * ynum, TRUE);
	if(!workbuf) return FALSE;

	//各ブロックの中心位置をランダムで決定

	p = workbuf;

	for(iy = 0, yy = info->box.y; iy < ynum; iy++, yy += size)
	{
		for(ix = 0, xx = info->box.x; ix < xnum; ix++, xx += size, p++)
		{
			x = xx + mRandXorShift_getIntRange(0, size - 1);
			y = yy + mRandXorShift_getIntRange(0, size - 1);

			if(x > info->rc.x2) x = info->rc.x2;
			if(y > info->rc.y2) y = info->rc.y2;

			p->cx = x;
			p->cy = y;
		}
	}

	//平均色を使う場合、各ブロック内の色を加算

	if(info->val_ckbtt[0])
	{
		for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
		{
			yy = (iy - info->rc.y1) / size;
			
			for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
			{
				xx = (ix - info->rc.x1) / size;

				p = _crystal_get_block(workbuf, ix, iy, xx, yy, xnum, ynum);

				FilterSub_addAdvColor_at(info->imgsrc, ix, iy, p->dcol, info->clipping);
				p->colnum++;
			}
		}
	}

	//各ブロックの色を決定

	p = workbuf;

	for(iy = 0; iy < ynum; iy++)
	{
		for(ix = 0; ix < xnum; ix++, p++)
		{
			if(info->val_ckbtt[0])
				FilterSub_getAdvColor(p->dcol, 1.0 / p->colnum, &p->pix);
			else
				TileImage_getPixel(info->imgsrc, p->cx, p->cy, &p->pix);
		}
	}

	//色をセット

	for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
	{
		yy = (iy - info->rc.y1) / size;
		
		for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
		{
			xx = (ix - info->rc.x1) / size;

			p = _crystal_get_block(workbuf, ix, iy, xx, yy, xnum, ynum);

			(setpix)(info->imgdst, ix, iy, &p->pix);
		}

		FilterSub_progIncSubStep(info);
	}

	mFree(workbuf);

	return TRUE;
}

/** ハーフトーン */
/* val_bar : [0]サイズ [1..3]角度 */

mBool FilterDraw_halftone(FilterDrawInfo *info)
{
	int i,c,ix,iy,ixx,iyy;
	double len,len_div,len_half,rr,rrdiv,dcos[3],dsin[3],dx,dy,xx[3],yy[3],dx2,dy2,dyy;
	PixelRGBA pix;
	TileImageSetPixelFunc setpix;

	FilterSub_getPixelFunc(&setpix);

	len = info->val_bar[0] * 0.1;

	len_div = 1.0 / len;
	len_half = len * 0.5;
	rrdiv = len * len / M_MATH_PI / 255.0;

	//RGB 各角度の sin,cos 値

	for(i = 0; i < 3; i++)
	{
		c = info->val_bar[1 + i];
		//G,B は R と同じ角度に
		if(i && info->val_ckbtt[0]) c = info->val_bar[1];

		rr = -c / 180.0 * M_MATH_PI;
		dcos[i] = cos(rr);
		dsin[i] = sin(rr);
	}

	//

	for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
	{
		//x 先頭の各位置
	
		dx = info->rc.x1;
		dy = iy;

		for(i = 0; i < 3; i++)
		{
			xx[i] = dx * dcos[i] - dy * dsin[i];
			yy[i] = dx * dsin[i] + dy * dcos[i];
		}

		//
	
		for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
		{
			TileImage_getPixel(info->imgsrc, ix, iy, &pix);

			//透明部分は処理しない

			if(pix.a == 0)
			{
				for(i = 0; i < 3; i++)
				{
					xx[i] += dcos[i];
					yy[i] += dsin[i];
				}
				continue;
			}

			//グレイスケール化

			if(info->val_ckbtt[2])
				pix.r = pix.g = pix.b = RGB_TO_GRAY(pix.r, pix.g, pix.b);

			//RGB

			for(i = 0; i < 3; i++)
			{
				//ボックスの中心からの距離

				dx = xx[i] - (floor(xx[i] * len_div) * len + len_half);
				dy = yy[i] - (floor(yy[i] * len_div) * len + len_half);

				//RGB 値から半径取得

				rr = sqrt(pix.ar[i] * rrdiv);
				rr *= rr;

				//色取得

				if(pix.ar[i] == 255)
					c = 255;
				else if(info->val_ckbtt[1])
				{
					//アンチエイリアス
					/* サブピクセルごとに計算しないと、
					 * 次のボックスにまたがっている部分が問題になる */

					c = 0;

					for(iyy = 0, dy2 = dy; iyy < 5; iyy++, dy2 += 0.2)
					{
						if(dy2 >= len_half) dy2 -= len;
						dyy = dy2 * dy2;

						for(ixx = 0, dx2 = dx; ixx < 5; ixx++, dx2 += 0.2)
						{
							if(dx2 >= len_half) dx2 -= len;

							if(dx2 * dx2 + dyy < rr) c += 255;
						}
					}

					c /= 25;
				}
				else
				{
					//非アンチエイリアス

					if((int)(dx * dx + dy * dy + 0.5) < (int)(rr + 0.5))
						c = 255;
					else
						c = 0;
				}

				pix.ar[i] = c;

				//次の位置

				xx[i] += dcos[i];
				yy[i] += dsin[i];
			}

			//セット

			(setpix)(info->imgdst, ix, iy, &pix);
		}

		FilterSub_progIncSubStep(info);
	}

	return TRUE;
}


//=======================
// 輪郭
//=======================


/** シャープ */

mBool FilterDraw_sharp(FilterDrawInfo *info)
{
	Filter3x3Info dat;
	int i;
	double d;

	d = info->val_bar[0] * -0.01;

	for(i = 0; i < 9; i++)
		dat.mul[i] = d;

	dat.mul[4] = info->val_bar[0] * 0.08 + 1;
	dat.divmul = 1;
	dat.add = 0;
	dat.grayscale = FALSE;

	return FilterSub_draw3x3(info, &dat);
}

/** アンシャープマスク */

static void _unsharpmask_setpixel(int x,int y,PixelRGBA *pixg,FilterDrawInfo *info)
{
	PixelRGBA pix;
	int i,n,amount;

	TileImage_getPixel(info->imgsrc, x, y, &pix);

	if(pix.a == 0) return;

	amount = info->val_bar[1];

	for(i = 0; i < 3; i++)
	{
		n = pix.ar[i] + (pix.ar[i] - pixg->ar[i]) * amount / 100;
		if(n < 0) n = 0;
		else if(n > 255) n = 255;

		pix.ar[i] = n;
	}

	(g_tileimage_dinfo.funcDrawPixel)(info->imgdst, x, y, &pix);
}

mBool FilterDraw_unsharpmask(FilterDrawInfo *info)
{
	return FilterSub_gaussblur(info, info->imgsrc, info->val_bar[0],
		_unsharpmask_setpixel, NULL);
}

/** 輪郭抽出 (Sobel) */

mBool FilterDraw_sobel(FilterDrawInfo *info)
{
	TileImage *imgsrc;
	int ix,iy,ixx,iyy,n,i;
	double dx[3],dy[3],d;
	PixelRGBA pix,pix2;
	TileImageSetPixelFunc setpix;
	char hmask[9] = {-1,-2,-1, 0,0,0, 1,2,1},vmask[9] = {-1,0,1, -2,0,2, -1,0,1};

	FilterSub_getPixelFunc(&setpix);

	imgsrc = info->imgsrc;

	for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
	{
		for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
		{
			TileImage_getPixel(imgsrc, ix, iy, &pix);
			if(pix.a == 0) continue;

			//3x3

			dx[0] = dx[1] = dx[2] = 0;
			dy[0] = dy[1] = dy[2] = 0;

			for(iyy = -1, n = 0; iyy <= 1; iyy++)
			{
				for(ixx = -1; ixx <= 1; ixx++, n++)
				{
					TileImage_getPixel_clip(imgsrc, ix + ixx, iy + iyy, &pix2);

					for(i = 0; i < 3; i++)
					{
						d = pix2.ar[i] / 255.0;
					
						dx[i] += d * hmask[n];
						dy[i] += d * vmask[n];
					}
				}
			}

			//RGB

			for(i = 0; i < 3; i++)
			{
				n = (int)(sqrt(dx[i] * dx[i] + dy[i] * dy[i]) * 255 + 0.5);
				if(n > 255) n = 255;

				pix.ar[i] = n;
			}

			(setpix)(info->imgdst, ix, iy, &pix);
		}
	}
	
	return TRUE;
}

/** 輪郭抽出 (Laplacian) */

mBool FilterDraw_laplacian(FilterDrawInfo *info)
{
	Filter3x3Info dat;
	int i;

	for(i = 0; i < 9; i++)
		dat.mul[i] = -1;

	dat.mul[4] = 8;
	dat.divmul = 1;
	dat.add = 255;
	dat.grayscale = FALSE;

	return FilterSub_draw3x3(info, &dat);
}

/** ハイパス */

static void _highpass_setpixel(int x,int y,PixelRGBA *pixg,FilterDrawInfo *info)
{
	PixelRGBA pix;
	int i,n;

	TileImage_getPixel(info->imgsrc, x, y, &pix);
	if(pix.a == 0) return;

	for(i = 0; i < 3; i++)
	{
		n = pix.ar[i] - pixg->ar[i] + 128;
		if(n < 0) n = 0;
		else if(n > 255) n = 255;

		pix.ar[i] = n;
	}

	(g_tileimage_dinfo.funcDrawPixel)(info->imgdst, x, y, &pix);
}

mBool FilterDraw_highpass(FilterDrawInfo *info)
{
	return FilterSub_gaussblur(info, info->imgsrc, info->val_bar[0], _highpass_setpixel, NULL);
}

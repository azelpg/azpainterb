/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**************************************
 * フィルタ処理
 *
 * カラー/アルファ値
 **************************************/

#include <math.h>

#include "mDef.h"
#include "mPopupProgress.h"
#include "mColorConv.h"
#include "mRandXorShift.h"

#include "defTileImage.h"
#include "TileImage.h"
#include "LayerList.h"
#include "LayerItem.h"
#include "ImageBuf8.h"

#include "defDraw.h"
#include "draw_op_sub.h"

#include "FilterDrawInfo.h"
#include "filter_sub.h"


//--------------------

typedef void (*ColorCommonFunc)(FilterDrawInfo *,int,int,PixelRGBA *);

//--------------------


//========================
// カラー処理共通
//========================


/** キャンバスプレビュー用処理 */
/*
 * - ダイアログ中のキャンバスプレビュー時は、
 *   imgdst は imgsrc から複製されている。
 * - RGB のみの操作なので、A=0 の点は変更なしとなる。
 *   そのため、確保されているタイルだけ処理すれば良い。
 */

static mBool _color_common_forPreview(FilterDrawInfo *info,ColorCommonFunc func)
{
	TileImageTileRectInfo tinfo;
	uint8_t **ppsrc,**ppdst;
	PixelRGBA *ps,*pd,pix;
	int px,py,tx,ty,ix,iy,xx,yy;

	//処理範囲

	ppsrc = TileImage_getTileRectInfo(info->imgsrc, &tinfo, &info->box);
	if(!ppsrc) return TRUE;

	ppdst = TILEIMAGE_GETTILE_BUFPT(info->imgdst, tinfo.rctile.x1, tinfo.rctile.y1);

	//

	py = tinfo.pxtop.y;

	for(ty = tinfo.rctile.y1; ty <= tinfo.rctile.y2; ty++, py += 64)
	{
		px = tinfo.pxtop.x;

		for(tx = tinfo.rctile.x1; tx <= tinfo.rctile.x2; tx++, px += 64, ppsrc++, ppdst++)
		{
			if(!(*ppsrc)) continue;

			//タイル単位で処理

			ps = (PixelRGBA *)*ppsrc;
			pd = (PixelRGBA *)*ppdst;

			for(iy = 0, yy = py; iy < 64; iy++, yy++)
			{
				for(ix = 0, xx = px; ix < 64; ix++, xx++, ps++, pd++)
				{
					if(ps->a && tinfo.rcclip.x1 <= xx && xx < tinfo.rcclip.x2
						&& tinfo.rcclip.y1 <= yy && yy < tinfo.rcclip.y2)
					{
						pix = *ps;

						(func)(info, xx, yy, &pix);

						*pd = pix;
					}
				}
			}
		}

		ppsrc += tinfo.pitch;
		ppdst += tinfo.pitch;
	}

	return TRUE;
}

/** 実際の処理 */

static mBool _color_common_forReal(FilterDrawInfo *info,ColorCommonFunc func)
{
	int ix,iy;
	PixelRGBA pix;
	TileImage *img = info->imgdst;

	for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
	{
		for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
		{
			TileImage_getPixel(img, ix, iy, &pix);

			if(pix.a)
			{
				(func)(info, ix, iy, &pix);

				TileImage_setPixel_draw_direct(img, ix, iy, &pix);
			}
		}

		mPopupProgressThreadIncSubStep(info->prog);
	}

	return TRUE;
}

/** 色操作の共通処理 */

static mBool _color_common(FilterDrawInfo *info,ColorCommonFunc func)
{
	if(info->in_dialog)
		return _color_common_forPreview(info, func);
	else
		return _color_common_forReal(info, func);
}

/** ピクセル処理 (共通) : テーブルから取得 */

static void _colfunc_from_table(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	uint8_t *tbl = (uint8_t *)info->ptmp[0];
	int i;

	for(i = 0; i < 3; i++)
		pix->ar[i] = tbl[pix->ar[i]];
}


//========================
//
//========================


/** 明度・コントラスト調整
 *
 * 明度 : -255 .. 255
 * コントラスト : -100 .. 100 */

static void _colfunc_brightcont(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	uint8_t *tbl = (uint8_t *)info->ptmp[0];
	int i,val[3];

	//コントラスト

	for(i = 0; i < 3; i++)
		val[i] = tbl[pix->ar[i]];

	//明度

	if(info->val_bar[0])
	{
		FilterSub_RGBtoYCrCb(val);

		val[0] += info->val_bar[0];  //Y に加算

		FilterSub_YCrCbtoRGB(val);
	}

	for(i = 0; i < 3; i++)
		pix->ar[i] = val[i];
}

mBool FilterDraw_color_brightcont(FilterDrawInfo *info)
{
	uint8_t *buf;
	int i,n;
	double d;

	//コントラスト テーブル作成

	buf = (uint8_t *)mMalloc(256, FALSE);
	if(!buf) return FALSE;

	d = info->val_bar[1] / 100.0;

	for(i = 0; i < 256; i++)
	{
		n = round(i + d * (i - 128));

		if(n < 0) n = 0;
		else if(n > 255) n = 255;

		buf[i] = n;
	}
	
	//

	info->ptmp[0] = buf;

	_color_common(info, _colfunc_brightcont);

	mFree(buf);

	return TRUE;
}


/** ガンマ補正 */

mBool FilterDraw_color_gamma(FilterDrawInfo *info)
{
	uint8_t *buf;
	int i,n;
	double d;

	//テーブル作成

	buf = (uint8_t *)mMalloc(256, FALSE);
	if(!buf) return FALSE;

	d = 1.0 / (info->val_bar[0] * 0.01);

	for(i = 0; i < 256; i++)
	{
		n = round(pow(i / 255.0, d) * 255);

		if(n < 0) n = 0;
		else if(n > 255) n = 255;

		buf[i] = n;
	}
	
	//

	info->ptmp[0] = buf;

	_color_common(info, _colfunc_from_table);

	mFree(buf);

	return TRUE;
}


/** レベル補正 */

mBool FilterDraw_color_level(FilterDrawInfo *info)
{
	uint8_t *buf;
	int i,n,in_min,in_mid,in_max,out_min,out_max,out_mid;
	double d1,d2,d;

	in_min = info->val_bar[0];
	in_mid = info->val_bar[1];
	in_max = info->val_bar[2];
	out_min = info->val_bar[3];
	out_max = info->val_bar[4];

	out_mid = (out_max - out_min) >> 1;

	d1 = (double)out_mid / (in_mid - in_min);
	d2 = (double)(out_max - out_min - out_mid) / (in_max - in_mid);

	//テーブル作成

	buf = (uint8_t *)mMalloc(256, FALSE);
	if(!buf) return FALSE;

	for(i = 0; i < 256; i++)
	{
		//入力制限

		n = i;

		if(n < in_min) n = in_min;
		else if(n > in_max) n = in_max;

		//補正

		if(n < in_mid)
			d = (n - in_min) * d1;
		else
			d = (n - in_mid) * d2 + out_mid;
		
		buf[i] = (int)(d + out_min + 0.5);
	}
	
	//

	info->ptmp[0] = buf;

	_color_common(info, _colfunc_from_table);

	mFree(buf);

	return TRUE;
}


/** RGB 補正 */

static void _colfunc_rgb(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = pix->ar[i] + info->val_bar[i];

		if(n < 0) n = 0;
		else if(n > 255) n = 255;

		pix->ar[i] = n;
	}
}

mBool FilterDraw_color_rgb(FilterDrawInfo *info)
{
	return _color_common(info, _colfunc_rgb);
}


/** HSV 調整 */

static void _colfunc_hsv(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	double hsv[3];
	int c[3],i;

	mRGBtoHSV(pix->r, pix->g, pix->b, hsv);

	for(i = 0; i < 3; i++)
		hsv[i] += info->dtmp[i];

	if(hsv[0] < 0) hsv[0] += 1;
	else if(hsv[0] >= 1.0) hsv[0] -= 1;

	for(i = 1; i < 3; i++)
	{
		if(hsv[i] < 0) hsv[i] = 0;
		else if(hsv[i] > 1.0) hsv[i] = 1;
	}

	mHSVtoRGB(hsv[0], hsv[1], hsv[2], c);

	for(i = 0; i < 3; i++)
		pix->ar[i] = c[i];
}

mBool FilterDraw_color_hsv(FilterDrawInfo *info)
{
	info->dtmp[0] = info->val_bar[0] / 360.0;
	info->dtmp[1] = info->val_bar[1] / 100.0;
	info->dtmp[2] = info->val_bar[2] / 100.0;

	return _color_common(info, _colfunc_hsv);
}

/** HLS 調整 */

static void _colfunc_hls(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	double d[3];
	int c[3],i;

	mRGBtoHLS(pix->r, pix->g, pix->b, d);

	for(i = 0; i < 3; i++)
		d[i] += info->dtmp[i];

	if(d[0] < 0) d[0] += 1;
	else if(d[0] >= 1.0) d[0] -= 1;

	for(i = 1; i < 3; i++)
	{
		if(d[i] < 0) d[i] = 0;
		else if(d[i] > 1.0) d[i] = 1;
	}

	mHLStoRGB((int)(d[0] * 360 + 0.5), d[1], d[2], c);

	for(i = 0; i < 3; i++)
		pix->ar[i] = c[i];
}

mBool FilterDraw_color_hls(FilterDrawInfo *info)
{
	info->dtmp[0] = info->val_bar[0] / 360.0;
	info->dtmp[1] = info->val_bar[1] / 100.0;
	info->dtmp[2] = info->val_bar[2] / 100.0;

	return _color_common(info, _colfunc_hls);
}

/** ネガポジ反転 */

static void _colfunc_nega(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	int i;

	for(i = 0; i < 3; i++)
		pix->ar[i] = 255 - pix->ar[i];
}

mBool FilterDraw_color_nega(FilterDrawInfo *info)
{
	return _color_common(info, _colfunc_nega);
}


/** グレイスケール */

static void _colfunc_grayscale(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	pix->r = pix->g = pix->b = RGB_TO_GRAY(pix->r, pix->g, pix->b);
}

mBool FilterDraw_color_grayscale(FilterDrawInfo *info)
{
	return _color_common(info, _colfunc_grayscale);
}


/** セピアカラー */

static void _colfunc_sepia(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	int v;

	v = RGB_TO_GRAY(pix->r, pix->g, pix->b);

	pix->r = (v + 30 > 255)? 255: v + 30;
	pix->g = v;
	pix->b = (v - 15 < 0)? 0: v - 15;
}

mBool FilterDraw_color_sepia(FilterDrawInfo *info)
{
	return _color_common(info, _colfunc_sepia);
}


/** グラデーションマップ */

static void _colfunc_gradmap(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	int v;

	v = RGB_TO_GRAY(pix->r, pix->g, pix->b);

	TileImage_getGradationColor(pix, v / 255.0, (TileImageDrawGradInfo *)info->ptmp[0]);
}

mBool FilterDraw_color_gradmap(FilterDrawInfo *info)
{
	TileImageDrawGradInfo ginfo;

	drawOpSub_setDrawGradationInfo(&ginfo);

	info->ptmp[0] = &ginfo;

	return _color_common(info, _colfunc_gradmap);
}


/** 2値化 */

static void _colfunc_threshold(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	int v;

	v = RGB_TO_GRAY(pix->r, pix->g, pix->b);

	v = (v < info->val_bar[0])? 0: 255;

	pix->r = pix->g = pix->b = v;
}

mBool FilterDraw_color_threshold(FilterDrawInfo *info)
{
	return _color_common(info, _colfunc_threshold);
}


/** 2値化 (ディザ) */
/*
	0:bayer2x2, 1:bayer4x4, 2:渦巻き4x4, 3:網点4x4, 4:ランダム
*/

static void _colfunc_threshold_dither(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	uint8_t val_bayer2x2[2][2] = {{0,2},{3,1}},
	val_4x4[3][4][4] = {
		{{0 ,8,2,10}, {12,4,14, 6}, {3,11, 1,9}, {15, 7,13, 5}},
		{{13,7,6,12}, { 8,1, 0, 5}, {9, 2, 3,4}, {14,10,11,15}},
		{{10,4,6, 8}, {12,0, 2,14}, {7, 9,11,5}, { 3,15,13, 1}}
	};
	int v,cmp;

	//比較値

	switch(info->val_combo[0])
	{
		case 0:
			cmp = val_bayer2x2[y & 1][x & 1] * 255 >> 2;
			break;
		case 1:
		case 2:
		case 3:
			cmp = val_4x4[info->val_combo[0] - 1][y & 3][x & 3] * 255 >> 4;
			break;
		case 4:
			cmp = mRandXorShift_getIntRange(0, 254);
			break;
	}

	//

	v = RGB_TO_GRAY(pix->r, pix->g, pix->b);
	v = (v <= cmp)? 0: 255;

	pix->r = pix->g = pix->b = v;
}

mBool FilterDraw_color_threshold_dither(FilterDrawInfo *info)
{
	return _color_common(info, _colfunc_threshold_dither);
}


/** ポスタリゼーション */

mBool FilterDraw_color_posterize(FilterDrawInfo *info)
{
	uint8_t *buf;
	int i,level,n;

	//テーブル作成

	buf = (uint8_t *)mMalloc(256, FALSE);
	if(!buf) return FALSE;

	level = info->val_bar[0];

	for(i = 0; i < 255; i++)
	{
		n = i * level / 255;

		buf[i] = (n == level - 1)? 255: n * 255 / (level - 1);
	}

	buf[255] = 255;
	
	//

	info->ptmp[0] = buf;

	_color_common(info, _colfunc_from_table);

	mFree(buf);

	return TRUE;
}


//=============================
// 色置換
//=============================


/** 描画色置換 */

static void _colfunc_replace_drawcol(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	if(pix->r == info->pixdraw.r
		&& pix->g == info->pixdraw.g
		&& pix->b == info->pixdraw.b)
	{
		pix->r = info->val_bar[0];
		pix->g = info->val_bar[1];
		pix->b = info->val_bar[2];
	}
}

mBool FilterDraw_color_replace_drawcol(FilterDrawInfo *info)
{
	return _color_common(info, _colfunc_replace_drawcol);
}


/** 色置換 (共通) */

static mBool _commfunc_replace(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	mBool drawcol = FALSE;

	//描画色かどうか

	if(pix->a && info->ntmp[0] < 3)
	{
		drawcol = (pix->r == info->pixdraw.r
			&& pix->g == info->pixdraw.g
			&& pix->b == info->pixdraw.b);
	}

	//

	switch(info->ntmp[0])
	{
		//描画色を透明に
		case 0:
			if(drawcol)
			{
				pix->c = 0;
				return TRUE;
			}
			break;
		//描画色以外を透明に
		case 1:
			if(pix->a && !drawcol)
			{
				pix->c = 0;
				return TRUE;
			}
			break;
		//描画色を背景色に
		case 2:
			if(drawcol)
			{
				pix->r = info->pixbkgnd.r;
				pix->g = info->pixbkgnd.g;
				pix->b = info->pixbkgnd.b;
				return TRUE;
			}
			break;
		//透明色を描画色に
		case 3:
			if(pix->a == 0)
			{
				*pix = info->pixdraw;
				return TRUE;
			}
			break;
	}

	return FALSE;
}

mBool FilterDraw_color_replace(FilterDrawInfo *info)
{
	return FilterSub_drawCommon(info, _commfunc_replace);
}


//=============================
// アルファ操作
//=============================


/** アルファ操作 (チェックレイヤ) */

static mBool _commfunc_alpha_checked(FilterDrawInfo *info,int x,int y,PixelRGBA *pixdst)
{
	LayerItem *pi = (LayerItem *)info->ptmp[0];
	PixelRGBA pix;
	int type,a;

	type = info->ntmp[0];
	a = pixdst->a;

	switch(type)
	{
		//透明な部分を透明に
		//不透明な部分を透明に
		case 0:
		case 1:
			//各レイヤで不透明な点があるか
			
			for(; pi; pi = pi->link)
			{
				TileImage_getPixel(pi->img, x, y, &pix);

				if(pix.a) break;
			}

			if((type == 0 && !pi) || (type == 1 && pi))
				a = 0;
			break;
		//値をコピー (対象上で透明な部分は除く)
		case 2:
			if(pixdst->a)
			{
				//各レイヤのアルファ値を合成
				
				for(a = 0; pi; pi = pi->link)
				{
					TileImage_getPixel(pi->img, x, y, &pix);

					a = (((a + pix.a) << 4) - ((a * pix.a << 4) / 255) + 8) >> 4; //4bit 固定少数点で
				}
			}
			break;
		//値を足す
		case 3:
			if(pixdst->a != 255)
			{
				for(; pi; pi = pi->link)
				{
					TileImage_getPixel(pi->img, x, y, &pix);

					a += pix.a;

					if(a >= 255)
					{
						a = 255;
						break;
					}
				}
			}
			break;
		//値を引く
		case 4:
			if(pixdst->a != 0)
			{
				for(; pi; pi = pi->link)
				{
					TileImage_getPixel(pi->img, x, y, &pix);

					a -= pix.a;

					if(a <= 0)
					{
						a = 0;
						break;
					}
				}
			}
			break;
		//値を乗算
		case 5:
			if(pixdst->a != 0)
			{
				for(; pi && a; pi = pi->link)
				{
					TileImage_getPixel(pi->img, x, y, &pix);

					a = (a * pix.a + 127) / 255;
				}
			}
			break;
		//明るい色ほど透明に
		//暗い色ほど透明に
		case 6:
		case 7:
			TileImage_getPixel(pi->img, x, y, &pix);

			if(pix.a)
			{
				a = RGB_TO_GRAY(pix.r, pix.g, pix.b);
				if(type == 6) a = 255 - a;
			}
			break;
	}

	//アルファ値セット

	if(a == pixdst->a)
		return FALSE;
	else
	{
		if(a == 0)
			pixdst->c = 0;
		else
			pixdst->a = a;

		return TRUE;
	}
}

mBool FilterDraw_alpha_checked(FilterDrawInfo *info)
{
	//チェックレイヤのリンクをセット

	info->ptmp[0] = LayerList_setLink_checked(APP_DRAW->layerlist);

	return FilterSub_drawCommon(info, _commfunc_alpha_checked);
}


/** アルファ操作 (カレント) */

static mBool _commfunc_alpha_current(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	int a;

	a = pix->a;

	switch(info->ntmp[0])
	{
		//明るい色ほど透明に
		case 0:
			if(a)
				a = 255 - RGB_TO_GRAY(pix->r, pix->g, pix->b);
			break;
		//暗い色ほど透明に
		case 1:
			if(a)
				a = RGB_TO_GRAY(pix->r, pix->g, pix->b);
			break;
		//テクスチャ適用
		case 2:
			if(a)
				a = (a * ImageBuf8_getPixel_forTexture(APP_DRAW->tex.curimg, x, y) + 127) / 255;
			break;
		//アルファ値からグレイスケール作成
		case 3:
			pix->r = pix->g = pix->b = a;
			pix->a = 255;
			return TRUE;
		//透明以外をすべて完全不透明に
		case 4:
			if(a) a = 255;
			break;
	}

	//アルファ値セット

	if(a == pix->a)
		return FALSE;
	else
	{
		if(a == 0)
			pix->c = 0;
		else
			pix->a = a;

		return TRUE;
	}
}

mBool FilterDraw_alpha_current(FilterDrawInfo *info)
{
	return FilterSub_drawCommon(info, _commfunc_alpha_current);
}



/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**************************************
 * フィルタ処理
 *
 * いろいろ2
 **************************************/

#include <math.h>

#include "mDef.h"

#include "TileImage.h"
#include "TileImageDrawInfo.h"

#include "FilterDrawInfo.h"
#include "filter_sub.h"


/** 線画抽出 */

static mBool _commfunc_lumtoalpha(FilterDrawInfo *info,int x,int y,PixelRGBA *pix)
{
	if(pix->a == 0) return FALSE;

	pix->a = 255 - RGB_TO_GRAY(pix->r, pix->g, pix->b);
	pix->r = pix->g = pix->b = 0;

	return TRUE;
}

mBool FilterDraw_lumtoAlpha(FilterDrawInfo *info)
{
	return FilterSub_drawCommon(info, _commfunc_lumtoalpha);
}

/** 立体枠 */

mBool FilterDraw_3Dframe(FilterDrawInfo *info)
{
	TileImage *img = info->imgdst;
	int i,j,w,h,cnt,fw,smooth,vs,v,xx,yy;

	w = info->box.w;
	h = info->box.h;
	
	fw = info->val_bar[0];
	smooth = info->val_ckbtt[0];

	//横線

	vs = (smooth)? 135: 83;
	if(info->val_ckbtt[1]) vs = -vs;

	v = vs;

	for(i = 0, cnt = w; i < fw; i++, cnt -= 2)
	{
		if((i << 1) >= h) break;

		if(smooth) v = vs - vs * i / fw;

		//上

		xx = info->box.x + i;
		yy = info->box.y + i;

		for(j = 0; j < cnt; j++)
			TileImage_setPixel_draw_addsub(img, xx + j, yy, v);

		//下

		xx++;
		yy = info->rc.y2 - i;

		for(j = 0; j < cnt - 2; j++)
			TileImage_setPixel_draw_addsub(img, xx + j, yy, -v);
	}

	//縦線

	vs = (smooth)? 105: 64;
	if(info->val_ckbtt[1]) vs = -vs;

	v = vs;

	for(i = 0, cnt = h - 1; i < fw; i++, cnt -= 2)
	{
		if((i << 1) >= w) break;

		if(smooth) v = vs - vs * i / fw;

		//上

		xx = info->box.x + i;
		yy = info->box.y + i + 1;

		for(j = 0; j < cnt; j++)
			TileImage_setPixel_draw_addsub(img, xx, yy + j, v);

		//下

		xx = info->rc.x2 - i;

		for(j = 0; j < cnt; j++)
			TileImage_setPixel_draw_addsub(img, xx, yy + j, -v);
	}

	return TRUE;
}

/** シフト */

mBool FilterDraw_shift(FilterDrawInfo *info)
{
	int ix,iy,sx,sy,mx,my,w,h,xx,yy;
	PixelRGBA pix;
	TileImageSetPixelFunc setpix;

	FilterSub_getPixelFunc(&setpix);

	mx = info->val_bar[0];
	my = info->val_bar[1];
	
	xx = info->box.x;
	yy = info->box.y;
	w = info->box.w;
	h = info->box.h;

	for(iy = 0; iy < h; iy++)
	{
		sy = iy - my;
		while(sy < 0) sy += h;
		while(sy >= h) sy -= h;
	
		for(ix = 0; ix < w; ix++)
		{
			sx = ix - mx;
			while(sx < 0) sx += w;
			while(sx >= w) sx -= w;

			TileImage_getPixel(info->imgsrc, xx + sx, yy + sy, &pix);

			(setpix)(info->imgdst, xx + ix, yy + iy, &pix);
		}

		FilterSub_progIncSubStep(info);
	}

	return TRUE;
}

/** 1px ドット線の補正 */

mBool FilterDraw_dot_thinning(FilterDrawInfo *info)
{
	TileImage *img = info->imgdst;
	int ix,iy,jx,jy,n,pos,flag;
	PixelRGBA pixtp,pix;
	uint8_t c[25];
	int8_t erasex,erasey;

	pixtp.c = 0;

	FilterSub_progSetMax(info, 50);

	//------ phase1 (3x3 余分な点を消す)

	FilterSub_progBeginSubStep(info, 25, info->box.h);

	for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
	{
		for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
		{
			//透明なら処理なし

			if(TileImage_isPixelTransparent(img, ix, iy)) continue;

			//3x3 範囲 : 透明でないか

			for(jy = -1, pos = 0; jy <= 1; jy++)
				for(jx = -1; jx <= 1; jx++, pos++)
					c[pos] = TileImage_isPixelOpaque(img, ix + jx, iy + jy);

			//上下、左右が 0 と 1 の組み合わせでない場合

			if(!(c[3] ^ c[5]) || !(c[1] ^ c[7]))
				continue;

			//

			flag = FALSE;
			n = c[0] + c[2] + c[6] + c[8];

			if(n == 0)
				//斜めがすべて 0
				flag = TRUE;
			else if(n == 1)
			{
				/* 斜めのうちひとつだけ 1 で他が 0 の場合、
				 * 斜めの点の左右/上下どちらに点があるか */

				if(c[0])
					flag = c[1] ^ c[3];
				else if(c[2])
					flag = c[1] ^ c[5];
				else if(c[6])
					flag = c[3] ^ c[7];
				else
					flag = c[5] ^ c[7];
			}

			//消す

			if(flag)
				TileImage_setPixel_draw_direct(img, ix, iy, &pixtp);
		}

		FilterSub_progIncSubStep(info);
	}

	//------- phase2 (5x5 不自然な線の補正)

	FilterSub_progBeginSubStep(info, 25, info->box.h);

	for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
	{
		for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
		{
			//不透明なら処理なし

			if(TileImage_isPixelOpaque(img, ix, iy)) continue;

			//5x5 範囲

			for(jy = -2, pos = 0; jy <= 2; jy++)
				for(jx = -2; jx <= 2; jx++, pos++)
					c[pos] = TileImage_isPixelOpaque(img, ix + jx, iy + jy);

			//3x3 内に点が 4 つあるか

			n = 0;

			for(jy = 0, pos = 6; jy < 3; jy++, pos += 5 - 3)
				for(jx = 0; jx < 3; jx++, pos++)
					n += c[pos];

			if(n != 4) continue;

			//各判定

			flag = 0;

			if(c[6] + c[7] + c[13] + c[18] == 4)
			{
				if(c[1] + c[2] + c[3] + c[9] + c[14] + c[19] == 0)
				{
					erasex = 1;
					erasey = -1;
					flag = 1;
				}
			}
			else if(c[8] + c[13] + c[16] + c[17] == 4)
			{
				if(c[9] + c[14] + c[19] + c[21] + c[22] + c[23] == 0)
				{
					erasex = 1;
					erasey = 1;
					flag = 1;
				}
			}
			else if(c[7] + c[8] + c[11] + c[16] == 4)
			{
				if(c[1] + c[2] + c[3] + c[5] + c[10] + c[15]  == 0)
				{
					erasex = -1;
					erasey = -1;
					flag = 1;
				}
			}
			else if(c[6] + c[11] + c[17] + c[18] == 4)
			{
				if(c[5] + c[10] + c[15] + c[21] + c[22] + c[23] == 0)
				{
					erasex = -1;
					erasey = 1;
					flag = 1;
				}
			}

			//セット

			if(flag)
			{
				TileImage_getPixel(img, ix + erasex, iy, &pix);
				TileImage_setPixel_draw_direct(img, ix, iy, &pix);

				TileImage_setPixel_draw_direct(img, ix + erasex, iy, &pixtp);
				TileImage_setPixel_draw_direct(img, ix, iy + erasey, &pixtp);
			}
		}

		FilterSub_progIncSubStep(info);
	}

	return TRUE;
}

/** 縁取り */

mBool FilterDraw_hemming(FilterDrawInfo *info)
{
	TileImage *imgsrc,*imgdst,*imgref,*imgdraw;
	PixelRGBA pixdraw,pix;
	TileImageSetPixelFunc setpix;
	int i,ix,iy;

	imgdraw = info->imgdst;

	FilterSub_getDrawColor(info, info->val_combo[0], &pixdraw);

	//判定元イメージ

	imgref = NULL;

	if(info->val_ckbtt[0])
		imgref = FilterSub_getCheckedLayerImage();

	if(!imgref) imgref = info->imgsrc;

	//準備

	g_tileimage_dinfo.funcColor = TileImage_colfunc_normal;

	setpix = (info->in_dialog)? TileImage_setPixel_new_colfunc: TileImage_setPixel_draw_direct;

	FilterSub_copyImage_forPreview(info);

	//

	i = info->val_bar[0] * 10;
	if(info->val_ckbtt[1]) i += 10;

	FilterSub_progSetMax(info, i);

	//------- 縁取り描画 (imgsrc を参照して、imgdraw と imgdst へ描画)

	imgsrc = imgdst = NULL;

	for(i = 0; i < info->val_bar[0]; i++)
	{
		//ソース用としてコピー

		TileImage_free(imgsrc);
	
		imgsrc = TileImage_newClone((i == 0)? imgref: imgdst);
		if(!imgsrc) goto ERR;

		//描画先作成

		TileImage_free(imgdst);

		imgdst = TileImage_newFromRect(TILEIMAGE_COLTYPE_RGBA, &info->rc);
		if(!imgdst) goto ERR;

		//imgsrc を参照し、点を上下左右に合成

		FilterSub_progBeginSubStep(info, 10, info->box.h);

		for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
		{
			for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
			{
				TileImage_getPixel(imgsrc, ix, iy, &pix);

				if(pix.a)
				{
					pixdraw.a = pix.a;

					//imgdst

					TileImage_setPixel_new_colfunc(imgdst, ix - 1, iy, &pixdraw);
					TileImage_setPixel_new_colfunc(imgdst, ix + 1, iy, &pixdraw);
					TileImage_setPixel_new_colfunc(imgdst, ix, iy - 1, &pixdraw);
					TileImage_setPixel_new_colfunc(imgdst, ix, iy + 1, &pixdraw);

					//imgdraw

					(setpix)(imgdraw, ix - 1, iy, &pixdraw);
					(setpix)(imgdraw, ix + 1, iy, &pixdraw);
					(setpix)(imgdraw, ix, iy - 1, &pixdraw);
					(setpix)(imgdraw, ix, iy + 1, &pixdraw);
				}
			}

			FilterSub_progIncSubStep(info);
		}
	}

	TileImage_free(imgsrc);
	TileImage_free(imgdst);

	//------- 元画像を切り抜く
	/* info->imgsrc は info->imgdst からコピーしているので、
	 * カレントレイヤが対象でも問題ない */

	if(info->val_ckbtt[1])
	{
		g_tileimage_dinfo.funcColor = TileImage_colfunc_erase;

		FilterSub_progBeginSubStep(info, 10, info->box.h);

		for(iy = info->rc.y1; iy <= info->rc.y2; iy++)
		{
			for(ix = info->rc.x1; ix <= info->rc.x2; ix++)
			{
				TileImage_getPixel(imgref, ix, iy, &pix);

				if(pix.a)
					(setpix)(imgdraw, ix, iy, &pix);
			}

			FilterSub_progIncSubStep(info);
		}
	}
	
	return TRUE;

ERR:
	TileImage_free(imgsrc);
	TileImage_free(imgdst);
	return FALSE;
}

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * TileImage
 ************************************/

#ifndef TILEIMAGE_H
#define TILEIMAGE_H

#include "defPixel.h"

typedef struct _TileImage  TileImage;
typedef struct _ImageBuf24 ImageBuf24;
typedef struct _mPopupProgress mPopupProgress;
typedef struct _CanvasViewParam CanvasViewParam;
typedef struct _FillPolygon FillPolygon;

typedef void (*TileImageSetPixelFunc)(TileImage *,int,int,PixelRGBA *);

typedef struct _TileImageInfo
{
	int tilew,tileh,offx,offy;
}TileImageInfo;

typedef struct
{
	int width,height,dpi,transparent;
}TileImageLoadFileInfo;

typedef struct
{
	mRect rctile,
		rcclip;		//x2,y2 は+1
	mPoint pxtop;
	int pitch;
}TileImageTileRectInfo;

typedef struct _TileImageDrawGradInfo
{
	const uint8_t *buf;
	PixelRGB pixdraw,
		pixbkgnd;
	int opacity,
		flags;
}TileImageDrawGradInfo;


enum
{
	TILEIMAGE_COLTYPE_RGBA,
	TILEIMAGE_COLTYPE_1BIT,
	TILEIMAGE_COLTYPE_8BIT
};

#define TILEIMAGE_DRAWGRAD_F_LOOP       1
#define TILEIMAGE_DRAWGRAD_F_SINGLE_COL 2
#define TILEIMAGE_DRAWGRAD_F_REVERSE    4


/*---- function ----*/

mBool TileImage_init();
void TileImage_finish();

/* free */

void TileImage_free(TileImage *p);
void TileImage_freeTile(uint8_t **pptile);
void TileImage_freeTile_atPos(TileImage *p,int tx,int ty);
void TileImage_freeTile_atPixel(TileImage *p,int x,int y);
void TileImage_freeTileBuf(TileImage *p);
void TileImage_freeAllTiles(TileImage *p);
void TileImage_freeEmptyTiles(TileImage *p);
void TileImage_freeEmptyTiles_byUndo(TileImage *p,TileImage *imgundo);

/* new */

TileImage *TileImage_new(int type,int w,int h);
TileImage *TileImage_newFromInfo(int type,TileImageInfo *info);
TileImage *TileImage_newFromRect(int type,mRect *rc);
TileImage *TileImage_newFromRect_forFile(int type,mRect *rc);
TileImage *TileImage_newClone(TileImage *src);

/* */

TileImage *TileImage_createSame(TileImage *p,TileImage *src,int type);
void TileImage_clear(TileImage *p);

/* tilebuf */

mBool TileImage_reallocTileBuf_fromImageSize(TileImage *p,int w,int h);
mBool TileImage_resizeTileBuf_includeImage(TileImage *p);
mBool TileImage_resizeTileBuf_combine(TileImage *p,TileImage *src);
mBool TileImage_resizeTileBuf_forUndo(TileImage *p,TileImageInfo *info);

/* tile */

uint8_t *TileImage_allocTile(TileImage *p,mBool clear);
mBool TileImage_allocTile_atptr(TileImage *p,uint8_t **ppbuf,mBool clear);
uint8_t *TileImage_getTileAlloc_atpos(TileImage *p,int tx,int ty);
uint8_t **TileImage_getTileAlloc_atpixel(TileImage *p,int px,int py);

/* set */

void TileImage_setOffset(TileImage *p,int offx,int offy);
void TileImage_moveOffset_rel(TileImage *p,int x,int y);

/* calc */

mBool TileImage_pixel_to_tile(TileImage *p,int x,int y,int *tilex,int *tiley);
void TileImage_pixel_to_tile_nojudge(TileImage *p,int x,int y,int *tilex,int *tiley);
void TileImage_pixel_to_tile_rect(TileImage *p,mRect *rc);
void TileImage_tile_to_pixel(TileImage *p,int tx,int ty,int *px,int *py);
void TileImage_getAllTilesPixelRect(TileImage *p,mRect *rc);

/* get */

void TileImage_getOffset(TileImage *p,mPoint *pt);
void TileImage_getInfo(TileImage *p,TileImageInfo *info);
void TileImage_getCanDrawRect_pixel(TileImage *p,mRect *rc);
mBool TileImage_getTileRect_fromPixelBox(TileImage *p,mRect *rc,mBox *box);
uint8_t **TileImage_getTileRectInfo(TileImage *p,TileImageTileRectInfo *info,mBox *box);
mBool TileImage_getHaveImageRect_pixel(TileImage *p,mRect *rc,int *tilenum);
int TileImage_getHaveTileNum(TileImage *p);

mBool TileImage_clipCanDrawRect(TileImage *p,mRect *rc);

/* etc */

int TileImage_getTile_forUndo(uint8_t *dst,uint8_t *src);
void TileImage_setTile_forUndo(uint8_t *dst,uint8_t *src);

uint32_t *TileImage_getHistogram(TileImage *p);

void TileImage_blendToImage24_rgba(TileImage *p,ImageBuf24 *dst,mBox *boxdst,int opacity,int blendmode);
void TileImage_blendXorToPixbuf(TileImage *p,mPixbuf *pixbuf,mBox *boxdst);
void TileImage_drawPreview(TileImage *p,mPixbuf *pixbuf,int x,int y,int boxw,int boxh,int sw,int sh);
void TileImage_drawFilterPreview(TileImage *p,mPixbuf *pixbuf,mBox *box);

/* pixel */

mBool TileImage_isPixelOpaque(TileImage *p,int x,int y);
mBool TileImage_isPixelTransparent(TileImage *p,int x,int y);

void TileImage_getPixel(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_getPixel_clip(TileImage *p,int x,int y,PixelRGBA *pix);
uint32_t TileImage_getPixel_pac(TileImage *p,int x,int y);
int TileImage_getPixel_forUndo(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_getBlendPixel(TileImage *p,int x,int y,PixelRGBA *pix,int opacity,int blendmode);

void TileImage_setPixel_draw_direct(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_setPixel_draw_stroke(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_setPixel_draw_brush_stroke(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_setPixel_draw_dotpen_direct(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_setPixel_draw_dotpen_stroke(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_setPixel_draw_dotpen_overwrite_square(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_setPixel_draw_addsub(TileImage *p,int x,int y,int v);

void TileImage_setPixel_new(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_setPixel_new_notp(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_setPixel_new_drawrect(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_setPixel_new_colfunc(TileImage *p,int x,int y,PixelRGBA *pix);
void TileImage_setPixel_subdraw(TileImage *p,int x,int y,PixelRGBA *pix);

mBool TileImage_setFingerBuf(TileImage *p,int x,int y);

void TileImage_colfunc_normal(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param);
void TileImage_colfunc_compareA(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param);
void TileImage_colfunc_overwrite(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param);
void TileImage_colfunc_erase(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param);
void TileImage_colfunc_dodge(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param);
void TileImage_colfunc_burn(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param);
void TileImage_colfunc_finger(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param);
void TileImage_colfunc_blur(TileImage *p,PixelRGBA *dst,PixelRGBA *src,void *param);

/* draw */

void TileImage_drawLineB(TileImage *p,
	int x1,int y1,int x2,int y2,PixelRGBA *pix,mBool nostart);
void TileImage_drawLineF(TileImage *p,int x1,int y1,int x2,int y2,PixelRGBA *pix);
void TileImage_drawLine_forFilter(TileImage *p,
	int x1,int y1,int x2,int y2,PixelRGBA *pix,mRect *rcclip);

void TileImage_drawEllipse(TileImage *p,double cx,double cy,double xr,double yr,
    PixelRGBA *pix,mBool dotpen,CanvasViewParam *param,mBool mirror);
void TileImage_drawEllipse_dot(TileImage *p,int x1,int y1,int x2,int y2,PixelRGBA *pix);
void TileImage_drawBox(TileImage *p,int x,int y,int w,int h,PixelRGBA *pix);
void TileImage_drawBezier_forXor(TileImage *p,mPoint *pt,mBool drawline2);
void TileImage_drawBezier(TileImage *p,mDoublePoint *pt,PixelRGBA *pix,
	mBool dotpen,uint32_t headtail);

void TileImage_drawFillBox(TileImage *p,int x,int y,int w,int h,PixelRGBA *pix);
void TileImage_drawFillEllipse_dot(TileImage *p,int x1,int y1,int x2,int y2,PixelRGBA *pixdraw);
void TileImage_drawFillEllipse(TileImage *p,
	double cx,double cy,double xr,double yr,PixelRGBA *pix,mBool antialias,
	CanvasViewParam *param,mBool mirror);
mBool TileImage_drawFillPolygon(TileImage *p,FillPolygon *fillpolygon,
	PixelRGBA *pix,mBool antialias);

void TileImage_getGradationColor(PixelRGBA *pixdst,double pos,TileImageDrawGradInfo *info);

void TileImage_drawGradation_line(TileImage *p,
	int x1,int y1,int x2,int y2,TileImageDrawGradInfo *info);
void TileImage_drawGradation_circle(TileImage *p,
	int x1,int y1,int x2,int y2,TileImageDrawGradInfo *info);
void TileImage_drawGradation_box(TileImage *p,
	int x1,int y1,int x2,int y2,TileImageDrawGradInfo *info);
void TileImage_drawGradation_radial(TileImage *p,
	int x1,int y1,int x2,int y2,TileImageDrawGradInfo *info);

/* draw brush */

void TileImage_beginDrawBrush_free(TileImage *p,double x,double y,double pressure);

void TileImage_drawBrushFree(TileImage *p,double x,double y,double pressure);
double TileImage_drawBrushLine(TileImage *p,
	double x1,double y1,double x2,double y2,
	double press_st,double press_ed,double t_start);
void TileImage_drawBrushLine_headtail(TileImage *p,
	double x1,double y1,double x2,double y2,uint32_t headtail);

void TileImage_drawBrushBox(TileImage *p,mDoublePoint *pt);

/* draw sub */

void TileImage_drawPixels_fromA1(TileImage *dst,TileImage *src,PixelRGBA *pix);
void TileImage_combine_forA1(TileImage *dst,TileImage *src);
mBool TileImage_drawLineH_forA1(TileImage *p,int x1,int x2,int y);
mBool TileImage_drawLineV_forA1(TileImage *p,int y1,int y2,int x);

/* edit */

void TileImage_procTilePixels(TileImage *p,
	void (*pixfunc)(TileImage *,PixelRGBA *,int,int,void *),void *param);

void TileImage_replaceColor_fromTP(TileImage *p,PixelRGBA *pixdst);
void TileImage_replaceColor(TileImage *p,PixelRGBA *pixsrc,PixelRGBA *pixdst);

void TileImage_combine(TileImage *dst,TileImage *src,mRect *rc,
	int opasrc,int opadst,int blendmode,mPopupProgress *prog);

void TileImage_setImage_fromImageBuf24(TileImage *p,ImageBuf24 *src,mPopupProgress *prog,mBool psdload);

void TileImage_copyImage_inRect(TileImage *p,TileImage *src,mRect *rc,int offx,int offy);

TileImage *TileImage_createCopyImage_box(TileImage *p,mBox *box);
void TileImage_pasteEditImage(TileImage *dst,TileImage *src,mBox *boxsrc,mBox *boxdst);
TileImage *TileImage_createCopyImage_forBoxSel(TileImage *p,mBox *box,mBool cut);
TileImage *TileImage_createCopyImage_forStamp(TileImage *src,TileImage *sel,mRect *rc);
void TileImage_pasteImage_forStamp(TileImage *dst,int x,int y,TileImage *src,int srcw,int srch);
TileImage *TileImage_createCopyImage_forSelImage(TileImage *src,TileImage *sel,mRect *rc,mBool cut);
void TileImage_pasteImage_forSelImage(TileImage *dst,TileImage *src,mBox *box);
void TileImage_replaceImage_box(TileImage *p,mBox *box,mPoint *ptdst);

void TileImage_fullReverse_horz(TileImage *p);
void TileImage_fullReverse_vert(TileImage *p);

void TileImage_copyImage_and_clear(TileImage *p,TileImage *copy,mBox *box);
void TileImage_rectReverse_horz(TileImage *p,mBox *box);
void TileImage_rectReverse_vert(TileImage *p,mBox *box);
void TileImage_rectRotate_left(TileImage *p,TileImage *src,mBox *box);
void TileImage_rectRotate_right(TileImage *p,TileImage *src,mBox *box);

void TileImage_arrangeTile(TileImage *p,int type,mBox *box);

/* scaling, rotate */

mBool TileImage_scaling(TileImage *dst,TileImage *src,int type,mSize *size_src,mSize *size_dst,mPopupProgress *prog);
void TileImage_rotate(TileImage *dst,TileImage *src,mBox *box,int type,int angle,mPopupProgress *prog);

/* imagefile */

TileImage *TileImage_loadFile(const char *filename,
	int format,mBool ignore_alpha,mBool leave_trans,int maxsize,
	TileImageLoadFileInfo *info,mPopupProgress *prog,char **errmes);

void TileImage_convertTile_fromA8(uint8_t *buf,uint32_t col);
void TileImage_convertTile_fromAPD(uint8_t *dst,uint8_t *src,int type,uint32_t col);
void TileImage_setRowImage_forA8(TileImage *p,uint8_t *buf,int y,int w,int h,uint32_t col);
void TileImage_setRowImage_forBGRA_bottomup(TileImage *p,uint8_t *buf,int y,int w,int h);
void TileImage_setImage_from8bitGray(TileImage *p,uint8_t *buf,int w,int h,mPopupProgress *prog);
mBool TileImage_setChannelImage(TileImage *p,uint8_t *chbuf,int chno,int srcw,int srch,mBool grayscale);

mBool TileImage_savePNG_rgba(TileImage *p,const char *filename,int dpi,mBox *box,
	mPopupProgress *prog);

#endif

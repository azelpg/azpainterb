/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**********************************
 * ブラシ選択ウィジェット
 **********************************/

#ifndef SELECT_BRUSH_H
#define SELECT_BRUSH_H

typedef struct _SelectBrush SelectBrush;
typedef struct _BrushItem BrushItem;

SelectBrush *SelectBrush_new(mWidget *parent,int id,int listno,BrushItem *item);

#endif

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * 24bit RGB イメージ
 ************************************/

#ifndef IMAGEBUF24_H
#define IMAGEBUF24_H

typedef struct _CanvasDrawInfo CanvasDrawInfo;

typedef struct _ImageBuf24
{
	uint8_t *buf;
	int w,h,pitch;
}ImageBuf24;

/* ピクセルデータ */

typedef struct
{
	uint8_t r,g,b;
}ImageBuf24Pix;

/* キャンバス描画用データ */

typedef struct
{
	int scrollx,scrolly;
	double originx,originy,
		scalediv;
	uint32_t bkgndcol;
}ImageBuf24CanvasInfo;


/*---- function ----*/

void ImageBuf24_coltopix(ImageBuf24Pix *dst,uint32_t col);

ImageBuf24 *ImageBuf24_new(int w,int h);
void ImageBuf24_free(ImageBuf24 *p);

uint8_t *ImageBuf24_getPixelBuf(ImageBuf24 *p,int x,int y);

void ImageBuf24_fill(ImageBuf24 *p,uint32_t col);
void ImageBuf24_fillWhite(ImageBuf24 *p);
void ImageBuf24_fillBox(ImageBuf24 *p,mBox *box,uint32_t col);
void ImageBuf24_fillPlaid_box(ImageBuf24 *p,mBox *box,uint32_t col1,uint32_t col2);

ImageBuf24 *ImageBuf24_loadFile(const char *filename,uint32_t bkgndcol);
uint8_t *ImageBuf24_getPalette(ImageBuf24 *p,int *pnum,int *tpcol);
void ImageBuf24_setRow_forPalette(ImageBuf24 *p,uint8_t *dst,uint8_t *src,uint8_t *palbuf,int palnum);

/* canvas */

void ImageBuf24_drawMainCanvas_nearest(ImageBuf24 *src,mPixbuf *dst,CanvasDrawInfo *info);
void ImageBuf24_drawMainCanvas_oversamp(ImageBuf24 *src,mPixbuf *dst,CanvasDrawInfo *info);
void ImageBuf24_drawMainCanvas_rotate_normal(ImageBuf24 *src,mPixbuf *dst,CanvasDrawInfo *info);
void ImageBuf24_drawMainCanvas_rotate_oversamp(ImageBuf24 *src,mPixbuf *dst,CanvasDrawInfo *info);

void ImageBuf24_drawCanvas_nearest(ImageBuf24 *src,mPixbuf *dst,mBox *boxdst,ImageBuf24CanvasInfo *info);
void ImageBuf24_drawCanvas_oversamp(ImageBuf24 *src,mPixbuf *dst,mBox *boxdst,ImageBuf24CanvasInfo *info);

#endif

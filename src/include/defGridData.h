/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/***************************
 * グリッドデータ
 ***************************/

#ifndef DEF_GRIDDATA_H
#define DEF_GRIDDATA_H

#include "mListDef.h"

typedef struct
{
	mListItem i;

	uint32_t col;	//色 (上位 8bit は濃度 [0-128])
	uint16_t w,h,	//グリッドの幅と高さ [1-]
		zoom_min,	//表示倍率 (100%=1000)
		zoom_max,
		area[4];	//範囲 (x1,y1,x2,y2)
	uint8_t flags;
}GridListData;

enum
{
	GRIDDATA_F_ON = 1<<0,
	GRIDDATA_F_HAVE_ZOOM = 1<<1,
	GRIDDATA_F_HAVE_AREA = 1<<2
};


#define GRID_MAX_NUM   100
#define GRID_MIN_SIZE  1
#define GRID_MAX_SIZE  1000

#endif

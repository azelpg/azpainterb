/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/********************************
 * ブラシリストデータ
 ********************************/

#ifndef BRUSHLIST_H
#define BRUSHLIST_H

typedef struct _BrushItem BrushItem;
typedef struct _BrushDrawParam BrushDrawParam;

enum
{
	BRUSHLIST_PEN,
	BRUSHLIST_BRUSH,
	BRUSHLIST_WATER,
	BRUSHLIST_ERASE,
	BRUSHLIST_DODGE,
	BRUSHLIST_BURN,
	BRUSHLIST_BLUR,

	BRUSHLIST_NUM
};


mBool BrushList_new();
void BrushList_free();

BrushItem *BrushList_edit_newItem(mList *list,int listno,BrushItem *src);
mBool BrushList_dup(int listno,mList *dst);
void BrushList_replace(int listno,mList *src,BrushItem *selitem);

int BrushList_getListNo_fromTool(int tool);
BrushItem *BrushList_getTopItem(int listno);
BrushItem *BrushList_getEditItem(int listno);
BrushItem *BrushList_getSelectItem(int listno);

void BrushList_setSelectItem(int listno,BrushItem *item);
void BrushList_saveItemValue(int listno);
BrushDrawParam *BrushList_getDrawParam(int toolno,BrushItem **ppitem);

void BrushList_update_radius(int listno,int val);
void BrushList_update_opacity(int listno,int val);
void BrushList_update_smoothing_type(int listno,int val);
void BrushList_update_smoothing_str(int listno,int val);
void BrushList_update_water_type(int listno,int val);
void BrushList_update_interval(int listno,int val);

void BrushList_loadConfig();
void BrushList_saveConfig();

#endif

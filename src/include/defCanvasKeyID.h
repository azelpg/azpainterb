/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/***************************************
 * キャンバスキーコマンドID
 ***************************************/

#ifndef DEF_CANVASKEY_ID_H
#define DEF_CANVASKEY_ID_H

enum
{
	CANVASKEY_CMD_TOOL = 1,			//ツール変更
	CANVASKEY_CMD_DRAWTYPE = 50,	//描画タイプ変更
	CANVASKEY_CMD_OTHER = 70,		//他コマンド
	CANVASKEY_OP_TOOL = 100,		//+キー:ツール
	CANVASKEY_OP_DRAWTYPE = 150,	//+キー:描画タイプ
	CANVASKEY_OP_OTHER = 170,		//+キー:ほか

	CANVASKEY_CMD_OTHER_NUM = 9,
	CANVASKEY_OP_OTHER_NUM = 6
};

//+キーでの操作か
#define CANVASKEY_IS_PLUS_MOTION(id)  ((id) >= CANVASKEY_OP_TOOL && (id) < CANVASKEY_OP_OTHER + CANVASKEY_OP_OTHER_NUM)

#endif

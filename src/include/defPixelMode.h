/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**********************************
 * 塗りタイプ
 **********************************/

#ifndef DEF_PIXELMODE_H
#define DEF_PIXELMODE_H

enum
{
	PIXELMODE_BLEND_STROKE,
	PIXELMODE_BLEND_PIXEL,
	PIXELMODE_COMPARE_A,
	PIXELMODE_OVERWRITE_STYLE,
	PIXELMODE_OVERWRITE_SQUARE,

	PIXELMODE_BLEND,
	PIXELMODE_OVERWRITE,
	PIXELMODE_ERASE
};

#endif

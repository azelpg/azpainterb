/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/********************************
 * ブラシアイテムデータ
 ********************************/

#ifndef BRUSHITEM_H
#define BRUSHITEM_H

#include "mListDef.h"

#define BRUSHITEM(p)  ((BrushItem *)(p))

#define BRUSHITEM_VAL16_NUM  7
#define BRUSHITEM_VAL8_NUM   7


typedef struct _BrushItem
{
	mListItem i;
	
	char *name;

	uint16_t radius,	//半径サイズ [10=1.0]
		radius_min,		//半径最小 (0 で最小値)
		radius_max,		//半径最大 (0 で最大値)
		min_size,		//最小サイズ(%) [10=1.0]
		min_opacity,	//最小濃度(%) [10=1.0]
		interval,		//点の間隔 [100=1.00]
		pressure_gamma;	//筆圧ガンマ値
	uint8_t opacity,	//濃度
		pixmode,		//塗りタイプ
		smoothing_type,	//補正タイプ
		smoothing_str,	//補正強さ (1-)
		water_type,		//水彩タイプ
		shape,			//形状
		flags;			//フラグ
}BrushItem;

enum
{
	BRUSHITEM_F_ANTIALIAS = 1<<0,
	BRUSHITEM_F_CURVE     = 1<<1
};

enum
{
	BRUSHITEM_RADIUS_MIN = 3,
	BRUSHITEM_RADIUS_MAX = 4000,
	BRUSHITEM_INTERVAL_MIN = 5,
	BRUSHITEM_INTERVAL_MAX = 200,
	BRUSHITEM_PRESSURE_MIN = 1,
	BRUSHITEM_PRESSURE_MAX = 600,
	BRUSHITEM_SMOOTHING_STR_MAX = 15,
	BRUSHITEM_SHAPE_NUM = 4
};


int BrushItem_adjustRadius(BrushItem *p,int val);

#endif

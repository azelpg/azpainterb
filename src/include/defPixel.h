/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

#ifndef DEF_PIXEL_H
#define DEF_PIXEL_H

typedef union _PixelRGB
{
	struct{ uint8_t r,g,b; };
	uint8_t c[3];
}PixelRGB;

typedef union _PixelRGBA
{
	struct{ uint8_t r,g,b,a; };
	uint8_t ar[4];
	uint32_t c;
}PixelRGBA;

#endif

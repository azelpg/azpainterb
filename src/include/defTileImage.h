/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************
 * TileImage 本体の定義
 *****************************/

#ifndef DEF_TILEIMAGE_H
#define DEF_TILEIMAGE_H

typedef struct _TileImage TileImage;
typedef union _PixelRGBA  PixelRGBA;

struct _TileImage
{
	uint8_t **ppbuf;	//タイル配列
	TileImage *link;	//リンク (塗りつぶし時など)
	int coltype,		//カラータイプ
		tilesize,		//1つのタイルのバイト数
		tilew,tileh,	//タイル配列の幅と高さ
		offx,offy;		//オフセット位置

	uint8_t *(*getPixelBuf_fromTile)(TileImage *p,uint8_t *,int,int);
	void (*getPixelCol_fromTile)(TileImage *p,uint8_t *,int,int,PixelRGBA *);
	void (*setPixel)(TileImage *p,uint8_t *,int,int,PixelRGBA *);
};


#define TILEIMAGE_TILE_EMPTY  ((uint8_t *)1)

#define TILEIMAGE_GETTILE_BUFPT(p,tx,ty)  ((p)->ppbuf + (ty) * (p)->tilew + (tx))
#define TILEIMAGE_GETTILE_PT(p,tx,ty)     *((p)->ppbuf + (ty) * (p)->tilew + (tx))

#endif

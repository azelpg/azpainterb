/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**************************************
 * DrawData : 操作関連のサブ処理関数
 **************************************/

#ifndef DRAW_OP_SUB_H
#define DRAW_OP_SUB_H

typedef struct _DrawData DrawData;
typedef struct _DrawPoint DrawPoint;
typedef struct _TileImageDrawGradInfo TileImageDrawGradInfo;

enum
{
	CANDRAWLAYER_OK,
	CANDRAWLAYER_FOLDER,
	CANDRAWLAYER_LOCK,
	CANDRAWLAYER_SELMODE
};


/*-----------*/

void drawOpSub_setOpInfo(DrawData *p,int optype,
	void (*motion)(DrawData *,uint32_t),mBool (*release)(DrawData *),int opsubtype);

int drawOpSub_canDrawLayer(DrawData *p);
int drawOpSub_canDrawLayer_andSelMode(DrawData *p);

mBool drawOpSub_isFolder_curlayer();
mBool drawOpSub_isDotPenDraw();
mBool drawOpSub_isAttachGrid();
mBool drawOpSub_isPolyFillToolDraw_dot();

/* 作業用イメージなど */

mBool drawOpSub_createTmpImage_area(DrawData *p);
TileImage *drawOpSub_createTileImage_box(mBox *box);

void drawOpSub_freeTmpImage(DrawData *p);
void drawOpSub_freeSelMoveCopyImage(DrawData *p);

void drawOpSub_freeFillPolygon(DrawData *p);

/* 描画 */

void drawOpSub_beginDraw(DrawData *p);
void drawOpSub_endDraw(DrawData *p,mRect *rc,mBox *boximg);

void drawOpSub_beginDraw_single(DrawData *p);
void drawOpSub_endDraw_single(DrawData *p);

void drawOpSub_addrect_and_update(DrawData *p,mBool btimer);
void drawOpSub_finishDraw_workrect(DrawData *p);
void drawOpSub_finishDraw_single(DrawData *p);

void drawOpSub_setDrawInfo(DrawData *p,int toolno,int param);
void drawOpSub_setDrawInfo_forOverwrite();
void drawOpSub_clearDrawMasks();

void drawOpSub_setDrawGradationInfo(TileImageDrawGradInfo *info);

mPixbuf *drawOpSub_beginAreaDraw();
void drawOpSub_endAreaDraw(mPixbuf *pixbuf,mBox *box);

/* 座標取得など */

mBool drawOpSub_pointAttachGrid(DrawData *p,mDoublePoint *dpt);
void drawOpSub_pointAttachGrid_area(DrawData *p,mPoint *pt);

void drawOpSub_getAreaPoint_int(DrawData *p,mPoint *pt);
void drawOpSub_getAreaPoint_int_raw(DrawData *p,mPoint *pt);

void drawOpSub_getImagePoint_fromDrawPoint(DrawData *p,mDoublePoint *pt,DrawPoint *dpt);
void drawOpSub_getImagePoint_double(DrawData *p,mDoublePoint *pt);
void drawOpSub_getImagePoint_int(DrawData *p,mPoint *pt);
void drawOpSub_getImagePoint_int_raw(DrawData *p,mPoint *pt);

void drawOpSub_getDrawPoint(DrawData *p,DrawPoint *dst);
void drawOpSub_getDrawPoint_dotpen(DrawData *p,mPoint *pt);

void drawOpSub_getDrawLinePoints(DrawData *p,mDoublePoint *pt1,mDoublePoint *pt2,mBool coordinate_image);
void drawOpSub_getDrawBoxPoints(DrawData *p,mDoublePoint *pt,mBool coordinate_image);
void drawOpSub_getDrawBox_noangle(DrawData *p,mBox *box);
void drawOpSub_getDrawEllipseParam(DrawData *p,mDoublePoint *pt_ct,mDoublePoint *pt_radius,mBool coordinate_image);

void drawOpSub_copyTmpPoint(DrawData *p,int num);

mBool drawOpSub_calc_moveForSelect(DrawData *p,uint32_t state,mPoint *ptret,mBool in_image);
mBool drawOpSub_isPoint_inSelect(DrawData *p);

#endif

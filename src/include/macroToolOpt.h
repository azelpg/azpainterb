/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**********************************
 * ツールオプションデータのマクロ
 **********************************/

#ifndef MACRO_TOOL_OPTION_H
#define MACRO_TOOL_OPTION_H

/* <dotpen>
 * [24](1)細線 | [20](4)style | [16](4)pixelmode | [8](8)opacity | [0](8)size
 */

#define DOTPEN_OPT_DEFAULT    ((255<<8) | 1)

#define DOTPEN_GET_SIZE(n)    (n & 255)
#define DOTPEN_GET_OPACITY(n) ((n >> 8) & 255)
#define DOTPEN_GET_PIXMODE(n) ((n >> 16) & 15)
#define DOTPEN_GET_STYLE(n)   ((n >> 20) & 15)
#define DOTPEN_IS_FINE(n)     ((n & (1<<24)) != 0)

#define DOTPEN_SET_SIZE(p,n)	*(p) = (*(p) & (~255)) | (n)
#define DOTPEN_SET_OPACITY(p,n)	*(p) = (*(p) & ~(255<<8)) | ((n) << 8)
#define DOTPEN_SET_PIXMODE(p,n)	*(p) = (*(p) & ~(15<<16)) | ((n) << 16)
#define DOTPEN_SET_STYLE(p,n)	*(p) = (*(p) & ~(15<<20)) | ((n) << 20)
#define DOTPEN_TOGGLE_FINE(p)	*(p) ^= (1<<24)

/* <fill polygon>
 * [12](1)antialias | [8](4)pixelmode | [0](8)opacity
 */

#define FILLPOLY_OPT_DEFAULT  255

#define FILLPOLY_GET_OPACITY(n)  (n & 255)
#define FILLPOLY_GET_PIXMODE(n)  ((n >> 8) & 15)
#define FILLPOLY_IS_ANTIALIAS(n) ((n & (1<<12)) != 0)

#define FILLPOLY_SET_OPACITY(p,n)    *(p) = (*(p) & ~255) | (n)
#define FILLPOLY_SET_PIXMODE(p,n)    *(p) = (*(p) & ~(15<<8)) | ((n) << 8)
#define FILLPOLY_TOGGLE_ANTIALIAS(p) *(p) ^= (1<<12)

/* <fill>
 * [23](1)disable ref | [20](3)type | [16](4)pixelmode | [8](8)diff | [0](8)opacity
 */

#define FILL_OPT_DEFAULT  255

#define FILL_GET_OPACITY(n)     (n & 255)
#define FILL_GET_COLOR_DIFF(n)  ((n >> 8) & 255)
#define FILL_GET_PIXMODE(n)     ((n >> 16) & 15)
#define FILL_GET_TYPE(n)        ((n >> 20) & 7)
#define FILL_IS_DISABLE_REF(n)  ((n & (1<<23)) != 0)

#define FILL_SET_OPACITY(p,n)       *(p) = (*(p) & ~255) | (n)
#define FILL_SET_COLOR_DIFF(p,n)    *(p) = (*(p) & ~(255<<8)) | ((n) << 8)
#define FILL_SET_PIXMODE(p,n)       *(p) = (*(p) & ~(15<<16)) | ((n) << 16)
#define FILL_SET_TYPE(p,n)          *(p) = (*(p) & ~(7<<20)) | ((n) << 20)
#define FILL_TOGGLE_DISABLE_REF(p)  *(p) ^= (1<<23)

/* <finger>
 * [8](8)strength | [0](8)size
 */

#define FINGER_OPT_DEFAULT  16 | (64<<8)

#define FINGER_GET_SIZE(n)   (n & 255)
#define FINGER_GET_STRENGTH(n) ((n >> 8) & 255)

#define FINGER_SET_SIZE(p,n)   *(p) = (*(p) & ~255) | (n)
#define FINGER_SET_STRENGTH(p,n) *(p) = (*(p) & ~(255<<8)) | ((n) << 8)

/* <gradation>
 * [22](1) loop | [21](1)reverse | [20](1)custom | [12](8)sel | [8](4)pixmode | [0](8)opacity
 */

#define GRAD_OPT_DEFAULT  255

#define GRAD_GET_OPACITY(n)   (n & 255)
#define GRAD_GET_PIXMODE(n)   ((n >> 8) & 15)
#define GRAD_GET_SELNO(n)     ((n >> 12) & 255)
#define GRAD_IS_NOT_CUSTOM(n) (!(n & (1<<20)))
#define GRAD_IS_CUSTOM(n)     ((n & (1<<20)) != 0)
#define GRAD_IS_REVERSE(n)    ((n & (1<<21)) != 0)
#define GRAD_IS_LOOP(n)       ((n & (1<<22)) != 0)

#define GRAD_SET_OPACITY(p,n)  *(p) = (*(p) & ~255) | (n)
#define GRAD_SET_PIXMODE(p,n)  *(p) = (*(p) & ~(15<<8)) | ((n) << 8)
#define GRAD_SET_SELNO(p,n)    *(p) = (*(p) & ~(255<<12)) | ((n) << 12)
#define GRAD_SET_CUSTOM_OFF(p) *(p) &= ~(1<<20)
#define GRAD_SET_CUSTOM_ON(p)  *(p) |= (1<<20)
#define GRAD_TOGGLE_REVERSE(p) *(p) ^= (1<<21)
#define GRAD_TOGGLE_LOOP(p)    *(p) ^= (1<<22)

/* <stamp> */

#define STAMP_OPT_DEFAULT 0

#define STAMP_IS_OVERWRITE(n)  ((n & 1) != 0)
#define STAMP_IS_LEFTTOP(n)    ((n & 2) != 0)

#define STAMP_TOGGLE_OVERWRITE(p)  *(p) ^= 1
#define STAMP_TOGGLE_LEFTTOP(p)    *(p) ^= 2

/* <select move/copy> */

#define SELMOVECOPY_OPT_DEFAULT 0

#define SELMOVECOPY_IS_MOVE_OVERWRITE(n)  ((n & 1) != 0)
#define SELMOVECOPY_IS_COPY_OVERWRITE(n)  ((n & 2) != 0)

#define SELMOVECOPY_TOGGLE_MOVE_OVERWRITE(p)  *(p) ^= 1
#define SELMOVECOPY_TOGGLE_COPY_OVERWRITE(p)  *(p) ^= 2

/* <select> */

#define SELECT_OPT_DEFAULT 0

#define SELECT_IS_OVERWRITE(n)   ((n & 1) != 0)

#define SELECT_TOGGLE_OVERWRITE(p)  *(p) ^= 1

#endif

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

#ifndef UNDOITEM_H
#define UNDOITEM_H

#include "mListDef.h"

#define UNDO_VAL_NUM  5

typedef struct _LayerItem LayerItem;
typedef struct _TileImage TileImage;
typedef struct _UndoUpdateInfo UndoUpdateInfo;

typedef struct
{
	mListItem i;

	uint8_t *buf;	//データのバッファ
	uint32_t size;	//データサイズ
	int type,		//UNDO種類
		fileno,		//ファイル時、ファイル名の番号 (-1 でバッファ)
		val[UNDO_VAL_NUM];	//データ値
}UndoItem;

typedef struct
{
	uint8_t opacity,
		alphamask,
		blendmode;
	uint32_t flags;
}UndoLayerInfo;


#define UNDOLAYERINFO_F_FOLDER  (1<<31)

enum
{
	UNDO_TYPE_TILES_IMAGE,
	UNDO_TYPE_SMALL_IMAGE,
	UNDO_TYPE_LAYER_NEW,
	UNDO_TYPE_LAYER_COPY,
	UNDO_TYPE_LAYER_DELETE,
	UNDO_TYPE_LAYER_CLEARIMG,
	UNDO_TYPE_LAYER_DROP_OR_COMBINE,
	UNDO_TYPE_LAYER_COMBINE_ALL,
	UNDO_TYPE_LAYER_COMBINE_FOLDER,
	UNDO_TYPE_LAYER_FLAGS,
	UNDO_TYPE_LAYER_REVERSE_HORZVERT,
	UNDO_TYPE_LAYER_MOVE_OFFSET,
	UNDO_TYPE_LAYER_MOVE_LIST,
	UNDO_TYPE_RESIZECANVAS_MOVEOFFSET,
	UNDO_TYPE_RESIZECANVAS_CROP,
	UNDO_TYPE_SCALECANVAS
};


/* UndoItem.c */

mBool UndoItem_common_runLayerDelete(LayerItem *item,UndoUpdateInfo *info);

mBool UndoItem_writeSingleLayerImage(UndoItem *p,LayerItem *item);
mBool UndoItem_restoreSingleLayerImage(UndoItem *p,LayerItem *item);

mBool UndoItem_writeSingleLayer(UndoItem *p,LayerItem *li);
mBool UndoItem_restoreSingleLayer(UndoItem *p,int parent,int pos,mRect *rc);

mBool UndoItem_writeTwoLayersImage(UndoItem *p,LayerItem *item);
mBool UndoItem_restoreTwoLayersImage(UndoItem *p,LayerItem *item);
mBool UndoItem_writeLayerCombine(UndoItem *p,int settype);

mBool UndoItem_writeSingleLayer_forFolder(UndoItem *p,LayerItem *item);
mBool UndoItem_writeAllLayers(UndoItem *p);
mBool UndoItem_writeAllLayersImage(UndoItem *p);
mBool UndoItem_restoreLayers(UndoItem *p,mRect *rcupdate);
mBool UndoItem_restoreAllLayersImage(UndoItem *p);

//

mBool UndoItem_runLayerReverseHorzVert(UndoItem *p,UndoUpdateInfo *info);

mBool UndoItem_writeLinkLayersNo(UndoItem *p,LayerItem *top,int num);
mBool UndoItem_runLayerMoveOffset(UndoItem *p,UndoUpdateInfo *info);

mBool UndoItem_runLayerNew(UndoItem *p,UndoUpdateInfo *info,int runtype);
mBool UndoItem_runLayerCopy(UndoItem *p,UndoUpdateInfo *info,int runtype);
mBool UndoItem_runLayerDelete(UndoItem *p,UndoUpdateInfo *info,int runtype);
mBool UndoItem_runLayerClearImage(UndoItem *p,UndoUpdateInfo *info,int runtype);
mBool UndoItem_runLayerDropOrCombine(UndoItem *p,UndoUpdateInfo *info,int runtype);
mBool UndoItem_runLayerCombineAll(UndoItem *p,UndoUpdateInfo *info,int runtype);
mBool UndoItem_runLayerCombineFolder(UndoItem *p,UndoUpdateInfo *info,int runtype);
mBool UndoItem_runLayerMoveList(UndoItem *p,UndoUpdateInfo *info);

mBool UndoItem_runCanvasResizeOrScale(UndoItem *p,UndoUpdateInfo *info);

/* UndoItem_sub.c */

LayerItem *UndoItem_getLayerFromNo(int no);
LayerItem *UndoItem_getLayerFromNo_forParent(int parent,int pos);

void UndoItem_setval_curlayer(UndoItem *p,int valno);
void UndoItem_setval_layerno(UndoItem *p,int valno,LayerItem *item);
void UndoItem_setval_layerno_forParent(UndoItem *p,int valno,LayerItem *item);

void UndoItem_writeLayerInfo(UndoItem *p,LayerItem *li);
void UndoItem_writeTileImage(UndoItem *p,TileImage *img);

mBool UndoItem_readLayer(UndoItem *p,int parent_pos,int rel_pos,mRect *rcupdate);
mBool UndoItem_readLayerOverwrite(UndoItem *p,LayerItem *item,mRect *rcupdate);
mBool UndoItem_readLayerImage(UndoItem *p,LayerItem *item);

mBool UncompressUndoTile(int outsize,int insize,int alphasize);
int CompressUndoTile(int size,int alphasize);

/* UndoItem_tileimg.c */

mBool UndoItem_writeSmallImage(UndoItem *p,TileImage *img);
mBool UndoItem_restoreSmallImage(UndoItem *p);

mBool UndoItem_writeUndoTilesImage(UndoItem *p,TileImageInfo *info);
mBool UndoItem_writeUndoTilesReverseImage(UndoItem *dst,UndoItem *src,int settype);
mBool UndoItem_restoreUndoTilesImage(UndoItem *p,int runtype);

/* UndoItem_base.c */

void UndoItem_free(UndoItem *p);
mBool UndoItem_alloc(UndoItem *p,int size);
mBool UndoItem_copy(UndoItem *dst,UndoItem *src);

mBool UndoItem_openWrite(UndoItem *p);
void UndoItem_write(UndoItem *p,void *buf,int size);
void UndoItem_closeWrite(UndoItem *p);

mBool UndoItem_openRead(UndoItem *p);
mBool UndoItem_read(UndoItem *p,void *buf,int size);
void UndoItem_readSeek(UndoItem *p,int seek);
void UndoItem_closeRead(UndoItem *p);

#endif

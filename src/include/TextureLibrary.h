/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * テクスチャライブラリデータ
 ************************************/

#ifndef TEXTURE_LIBRARY_H
#define TEXTURE_LIBRARY_H

typedef struct _ImageBuf8 ImageBuf8;

#define TEXTURE_LIBRARY_NUM  2


void TextureLibrary_init();
void TextureLibrary_free();

int TextureLibrary_getNum(int libno);

mBool TextureLibrary_append(int libno,ImageBuf8 *img);
void TextureLibrary_delete(int libno,int tno);
void TextureLibrary_empty(int libno);

ImageBuf8 *TextureLibrary_createImage(int libno,int tno);
void TextureLibrary_drawImage(int libno,int no,mPixbuf *pixbuf,int x,int y,int w,int h);

ImageBuf8 *TextureLibrary_loadConfig();
void TextureLibrary_saveConfig(ImageBuf8 *curimg,mBool save_curimg);

mBool TextureLibrary_loadFile(const char *filename,int libno);
mBool TextureLibrary_saveFile(const char *filename,int libno);

#endif

/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/********************************************************
 * 外部からドックウィジェットの内容を変更する時の関数
 ********************************************************/

#ifndef DOCKS_EXTERNAL_H
#define DOCKS_EXTERNAL_H

typedef struct _LayerItem LayerItem;

//ツール

void DockTool_changeTool();
void DockTool_changeToolSub();
void DockTool_changeIconSize();

//オプション

void DockOption_changeTool();
void DockOption_changeBrushRadius(int radius);
void DockOption_changeStampImage();

//レイヤ

void DockLayer_update_all();
void DockLayer_update_curparam();
void DockLayer_update_curlayer(mBool setparam);
void DockLayer_update_layer(LayerItem *item);
void DockLayer_update_changecurrent_visible(LayerItem *lastitem,mBool update_all);
void DockLayer_changeIconSize();

//カラー

void DockColor_changeDrawColor();
void DockColor_changeDrawColor_hsv(int col);
void DockColor_changeColorMask();
void DockColor_changeTheme();

//カラーパレット

void DockColorPalette_changeTheme();

//カラーホイール

void DockColorWheel_changeDrawColor(int hsvcol);
void DockColorWheel_changeTheme();

//キャンバス操作

void DockCanvasCtrl_setPos();

//キャンバスビュー

void DockCanvasView_update();
void DockCanvasView_changeImageSize();
void DockCanvasView_updateRect(mBox *boximg);
void DockCanvasView_changeLoupePos(mPoint *pt);
void DockCanvasView_changeIconSize();

//イメージビューア
void DockImageViewer_changeIconSize();

#endif

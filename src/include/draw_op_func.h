/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**********************************
 * DrawData 操作関数
 **********************************/

#ifndef DRAW_OP_FUNC_H
#define DRAW_OP_FUNC_H

/* brush/dot */

mBool drawOp_dotpen_free_press(DrawData *p);
mBool drawOp_brush_free_press(DrawData *p,mBool pressure_max);
mBool drawOp_finger_press(DrawData *p);

void drawOpDraw_line(DrawData *p,mBool coordinate_image);
void drawOpDraw_box(DrawData *p,mBool coordinate_image);
void drawOpDraw_ellipse(DrawData *p,mBool coordinate_image);
void drawOpDraw_lineSuccConc(DrawData *p);

mBool drawOp_xorline_to_bezier(DrawData *p);

mBool drawOp_dragBrushSize_press(DrawData *p);

/* func1 */

mBool drawOp_common_norelease(DrawData *p);

void drawOpDraw_fillBox_forImage(DrawData *p);
void drawOpDraw_fillBox_forArea(DrawData *p);
void drawOpDraw_fillEllipse(DrawData *p,mBool draw_dot);
void drawOpDraw_fillPolygon(DrawData *p);
void drawOpDraw_gradation(DrawData *p);

mBool drawOp_fill_press(DrawData *p);
mBool drawOp_movetool_press(DrawData *p);

mBool drawOp_canvasMove_press(DrawData *p);
mBool drawOp_canvasRotate_press(DrawData *p);
mBool drawOp_canvasZoom_press(DrawData *p);

mBool drawOp_spoit_press(DrawData *p,mBool enable_alt);
mBool drawOp_intermediateColor_press(DrawData *p);
mBool drawOp_replaceColor_press(DrawData *p,mBool rep_tp);

mBool drawOp_drawtext_press(DrawData *p);

/* func2 */

void drawOp_setStampImage(DrawData *p);
int drawOp_stamp_press(DrawData *p);

mBool drawOp_select_press(DrawData *p);

mBool drawOp_pasteMove_press(DrawData *p,TileImage *img);

int drawOp_selectMoveCopy_press(DrawData *p);
void drawOp_selMoveCopy_select(DrawData *p);

mBool drawOp_selectReplace_press(DrawData *p);
void drawOp_selReplace_select(DrawData *p);

void drawOp_selectSetCursor_motion(DrawData *p);
mBool drawOp_selectResize_press(DrawData *p);

/* xor */

mBool drawOpXor_line_press(DrawData *p,int opsubtype,mBool coordinate_image);
mBool drawOpXor_boxarea_press(DrawData *p,int opsubtype);
mBool drawOpXor_boximage_press(DrawData *p,int opsubtype);
mBool drawOpXor_ellipse_press(DrawData *p,int opsubtype,mBool coordinate_image);
mBool drawOpXor_sumline_press(DrawData *p,int opsubtype,mBool coordinate_image);
mBool drawOpXor_polygon_press(DrawData *p,int opsubtype);
mBool drawOpXor_lasso_press(DrawData *p,int opsubtype);
mBool drawOpXor_rulepoint_press(DrawData *p);

void drawOpXor_drawline(DrawData *p);
void drawOpXor_drawBox_area(DrawData *p);
void drawOpXor_drawBox_image(DrawData *p);
void drawOpXor_drawEllipse(DrawData *p);
void drawOpXor_drawBezier(DrawData *p,mBool erase);
void drawOpXor_drawPolygon(DrawData *p);
void drawOpXor_drawLasso(DrawData *p,mBool erase);
void drawOpXor_drawCrossPoint(DrawData *p);
void drawOpXor_drawBrushSizeCircle(DrawData *p,mBool erase);

#endif

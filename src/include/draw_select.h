/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**********************************
 * DrawData 選択範囲系処理
 **********************************/

#ifndef DRAW_SELECT_H
#define DRAW_SELECT_H

void drawSelect_canvasviewUpdate_inPasteMode(DrawData *p);
void drawSelect_clearResizeCursor();
void drawSelect_cancel(mBool update);

void drawSelect_getSelectBox(DrawData *p,mBox *box);
mBool drawSelect_getSelectBox_forFilter(DrawData *p,mRect *rc,mBox *box,mBool proc_color);
void drawSelect_getSelectBox_forFilter_inImage(mRect *rc,mBox *box);

void drawSelect_setSelectBox(DrawData *p,mBox *box);
void drawSelect_allSelect(DrawData *p);

void drawSelect_copy_and_cut(DrawData *p,mBool cut);
void drawSelect_paste(DrawData *p);

void drawSelect_fill(DrawData *p);
void drawSelect_erase(DrawData *p);
void drawSelect_trim(DrawData *p);
void drawSelect_arrange_tile(DrawData *p,int type);
void drawSelect_reverse_and_rotate90(DrawData *p,int type);
int drawSelect_scaling(DrawData *p,mBox *box,int type,int w,int h);
void drawSelect_rotate(DrawData *p,int type,int angle);

int drawSelect_pasteFromFile(DrawData *p,const char *filename,
	mBool ignore_alpha,mBool leave_trans);

void drawSelect_clearStamp(DrawData *p);
void drawSelect_loadStampImage(DrawData *p,const char *filename);

#endif

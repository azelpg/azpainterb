/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * フィルタ描画情報
 ************************************/

#ifndef FILTERDRAWINFO_H
#define FILTERDRAWINFO_H

#include "defPixel.h"

typedef struct _FilterDrawInfo FilterDrawInfo;
typedef struct _mPopupProgress mPopupProgress;
typedef struct _TileImage TileImage;

typedef mBool (*FilterDrawFunc)(FilterDrawInfo *);


struct _FilterDrawInfo
{
	TileImage *imgsrc,
		*imgdst,
		*imgref;
	mPopupProgress *prog;

	mRect rc;	//処理する範囲 (イメージ座標)
	mBox box;

	FilterDrawFunc drawfunc;

	PixelRGBA pixdraw,	//描画色
		pixbkgnd;		//背景色

	int imgx,imgy,
		val_bar[5];
	uint8_t	val_ckbtt[3],
		val_combo[3];

	mBool in_dialog,	//ダイアログ中は TRUE、実際の描画時は FALSE
		clipping;		//イメージ範囲外をクリッピングするか

	/* フィルタ処理中の作業用 */

	int ntmp[4];
	double dtmp[3];
	void *ptmp[1];
};

#endif

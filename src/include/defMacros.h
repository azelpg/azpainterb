/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**********************
 * 共通マクロ定義
 **********************/

#ifndef DEF_MACROS_H
#define DEF_MACROS_H

#define APPNAME           "AzPainterB"

#define IMAGE_SIZE_MAX    20000		//イメージ最大サイズ
#define CANVAS_ZOOM_MIN   10		//キャンバス表示倍率、最小
#define CANVAS_ZOOM_MAX   20000

#define FILEFILTER_NORMAL_IMAGE "Image File (BMP/PNG/GIF/JPEG)\t*.bmp;*.png;*.gif;*.jpg;*.jpeg\tAll Files (*)\t*\t"

#define CONFIG_FILENAME_BRUSHLIST    "brushlist.dat"
#define CONFIG_FILENAME_GRADATION    "grad.dat"
#define CONFIG_FILENAME_TEXTURELIB   "texlib.dat"
#define CONFIG_FILENAME_SHORTCUTKEY  "sckey.conf"

#endif

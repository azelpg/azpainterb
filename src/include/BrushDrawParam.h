/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/******************************
 * ブラシ描画用パラメータ
 ******************************/

#ifndef BRUSH_DRAWPARAM_H
#define BRUSH_DRAWPARAM_H

typedef struct _BrushDrawParam
{
	double radius,		//半径 (px)
		opacity,		//最大濃度
		min_size,		//サイズ最小 [0-1]
		min_opacity,	//濃度最小 [0-1]
		interval,		//間隔
		pressure_gamma,	//筆圧ガンマ値
		water[3];		//水彩 ([0]描画色の補填 [1]描画先の色の割合 [2]混合色の割合)
	uint8_t smoothing_type,	//手ブレ補正タイプ
		smoothing_str,		//手ブレ補正強さ
		shape;				//形状
	uint32_t flags;
}BrushDrawParam;


enum 
{
	BRUSHDP_F_ANTIALIAS = 1<<0,		//アンチエイリアス
	BRUSHDP_F_CURVE     = 1<<1,		//曲線補間
	BRUSHDP_F_PRESSURE_GAMMA = 1<<2,//筆圧のガンマ補正あり
	BRUSHDP_F_WATER = 1<<3			//水彩
};

#endif

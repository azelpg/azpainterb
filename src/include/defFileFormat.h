/*$
 Copyright (C) 2016-2020 Azel.

 This file is part of AzPainterB.

 AzPainterB is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AzPainterB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * 画像ファイルフォーマット定義
 ************************************/

#ifndef DEF_FILEFORMAT_H
#define DEF_FILEFORMAT_H

enum
{
	FILEFORMAT_UNKNOWN,
	FILEFORMAT_PNG,
	FILEFORMAT_JPEG,
	FILEFORMAT_GIF,
	FILEFORMAT_BMP,

	FILEFORMAT_NOT_NORMAL_TOP = 50,
	FILEFORMAT_APD = 50,	//保存時
	FILEFORMAT_ADW,
	FILEFORMAT_PSD,

	FILEFORMAT_APD_v1v2,	//読み込み時
	FILEFORMAT_APD_v3,

	FILEFORMAT_PNG_ALPHA = 100	//PNG + アルファチャンネル (保存時のみ)
};

/* MainWindow_file.c */
int FileFormat_getbyFileHeader(const char *filename);
mBool FileFormat_isNormalImage(int format);
mBool FileFormat_isAPD(int format);

#endif
